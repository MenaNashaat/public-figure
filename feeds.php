<?php 

    ob_start();
    session_start();

	$pageTitle = "Feeds";
	require_once("../includes/initialize.php");


	//get database connection
    $dbConnection = getDatabaseConnection();

    // print_r($_SESSION);
    



    include "header.php";
    

?>


    <div id="tg-homebanner" class="tg-homebanner tg-haslayout">
        <figure class="item">
            <div class="cover" style="max-height: 220px">
                <figcaption>
                    <div class="container">
                        <div class="row text-center white-color">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="tg-bannercontent">
                                    <h2>Feeds from 12,45,754 Awesome Public Figures ....!</h2>

                                </div>
                            </div>
                        </div>
                    </div>
                </figcaption>
            </div>
        </figure>
    </div>

    <main id="tg-main" class="tg-main tg-haslayout">

    <!--************************************
						Categories Search Start
				*************************************-->
		<section class="tg-haslayout" style="margin-bottom: 3em">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">

                    <div class="card posts" style="height: auto">

                        <?php 

                            //get all posts
                            $sqlQuery="SELECT * FROM public_figure_posts ORDER BY id DESC LIMIT 14 OFFSET 0";
                            $posts = $dbConnection->performQuery($sqlQuery);
                            $i = 0;

                            while($post = mysqli_fetch_assoc($posts)){
                                $userId = $post["user_id"];
                                $userQuery = "SELECT first_name, last_name, username, photo_path FROM public_figure_user WHERE id='{$userId}'";
                                $user = $dbConnection->performQuery($userQuery);

                                $user = mysqli_fetch_assoc($user);
                        ?>

                            <div class="row" style="border-bottom: 1px dotted gray">

                                <div class="col-md-12">
                                    <img alt="" src="<?php echo "uploads/" . $user["photo_path"] ?>" class="avatar avatar-wordpress-social-login avatar-30 photo"
                                        height="30" width="30" style="border-radius: 50%">

                                    <h4 style="display: inline-block; margin-left: 3px; color: #0083ff"> <?php echo $user["username"]; ?> </h4>

                                    <small class="pull-right">
                                    
                                    <?php 
                                        $postDate = strtotime($post["created_at"]);
                                        // $currenDate = strtotime(date("Y-m-d H:i:s"));
                                        $formatteDate = strftime("%B %e, %Y", $postDate);
                                

                                        // echo $formatteDate . " (age " . floor((($currenDate - $postDate) / (60 * 60 * 24 * 30 * 12))) . ")"; 
                                        echo $formatteDate;
                                    ?>
                                    
                                    </small>
                                </div>

                                <div class="col-md-12" style="padding: 1em 2em;">

                                <p>
                                    
                                    <?php 

                                        echo htmlentities($post["text"]);

                                    ?>

                                </p>

                                </div>

                            </div>

                            <hr />

                            <?php } ?>

                        </div>

                        <div class="spinner pull-right"  style="">
                            <div class="double-bounce1"></div>
                            <div class="double-bounce2"></div>
                        </div>

					</div>
                </div>
                
                
			</div>
		</section>

        <!--************************************
					Categories Search Start
			*************************************-->
        <!-- <section class="tg-haslayout" style="margin-top:10pc">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
                        <div class="tg-categoriessearch">
                            <div class="tg-title">
                                <h2>filters</h2>
                            </div>
                            
      

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="loader pull-right" style="margin-right: 10%"></div>
                </div>
            </div>
        </section> -->

    </main>

    <?php include "footer.php"; ?>

	<!-- Main js -->
	<script src="js/main.js"></script>
	<script>
		$(document).ready(function(){


            var offset = 0;
            var limit = 7;

            var status = 0;

            function loadPosts(limit, offset){

                var data = new FormData();
                data.append('load_feeds', 'load_feeds');
                // data.append('user_id', $("#user_id").val());
                data.append('limit', limit);
                data.append('offset', offset);


                $.ajax({
                    url: 'api/post.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
                        // console.log(returnData);
                        // return;
                        returnData = JSON.parse(returnData);

                        if(returnData["error"] == 0){

                            if(returnData["posts"].length > 0){

                                for($i = 0; $i < returnData["posts"].length; $i++){
                                    $(".posts").append(    
                                        "<div class=\"row\" style=\"border-bottom: 1px dotted gray\">" + 
                                            "<div class=\"col-md-12\">" + 
                                                "<img alt=\"\" src=\"uploads/" + returnData["photo_path"] + "\" class=\"avatar avatar-wordpress-social-login avatar-30 photo\" " + 
                                                    "height=\"30\" width=\"30\" style=\"border-radius: 50%\">" + 

                                                "<h4 style=\"display: inline-block; margin-left: 3px; color: #0083ff\">" + returnData["username"] + "</h4>" + 

                                                "<small class=\"pull-right\">" + returnData["posts"][$i]["created_at"] + "</small>" + 
                                            "</div>" + 

                                            "<div class=\"col-md-12\" style=\"padding: 1em 2em;\">" + 
                                            "<p>" + returnData["posts"][$i]["text"] + "</p>" + 
                                            "</div>" + 
                                        "</div>" + 

                                        "<hr />"
                                    );

                                    status = 0;

                                }

                            }else{
                                $(".posts").append("<button type=\"button\" class=\"btn btn-info\" style=\"margin-left: 40%\"> No Data Found.. </button>");
                                status = 1;
                            }

                        }else {

                            
                        }

                        $(".spinner").hide();

                    }
                });

            }

            $(window).scroll(function(){
                // console.log($("div.page-header-padding.page-header-bg").outerHeight());
                // var height = 0;
                // if($("#user_id").data("session") == "1")
                //     height = $(".description").height() + $(".page-header-bg").outerHeight();
                // else height = $(".posts").height() + $(".page-header-bg").outerHeight();

                var height = height = $(".posts").height() + $(".page-header-bg").outerHeight();

                

                // height = $(".posts").height() + $(".page-header-bg").outerHeight();
                // console.log($(window).scrollTop() + $(window).height() + " - " + height + " - " + status);
                
                if($(window).scrollTop() + $(window).height() >= height && status == 0){
                    $(".spinner").show();

                    status = 1;
                    offset += limit;

                    setTimeout(() => {
                        loadPosts(limit, offset);
                    }, 1000);

                    
                }
            });









          $("#modal_submit").on("click", function(e){
				e.preventDefault();

				//ajax check login
				var username = $("#login_username");
				var password = $("#login_password");

				if(username.val() == ""){
					username.css("border", "1px solid red");
					return;
				}else username.css("border", "1px solid #ccc");

				if(password.val() == ""){
					password.css("border", "1px solid red");
					return;
				}else password.css("border", "1px solid #ccc");


				var data = new FormData();
                data.append('login', 'login');
                data.append('username', username.val());
                data.append('password', password.val());


                $.ajax({
                    url: 'api/login.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
						// console.log(returnData);
						returnData = JSON.parse(returnData);

						if(returnData["error"] == 0){
							location.href="http://localhost:8080/public_figure/mena_mahmoud/profile.php";
						}else{
							$("#login_hint").text("username/password not correct!");
						}
					}

                    

				});


			});	
    });

	</script>



</body>

</html>