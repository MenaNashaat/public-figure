<?php 

    ob_start();
    session_start();


    // $loggedIn = false;
    // if(isset($_SESSION["user"]))
    // 	$loggedIn = true;

    $pageTitle = "favorite";
    require_once("../includes/initialize.php");


    //get database connection
    $dbConnection = getDatabaseConnection();

    if(!isset($_SESSION["user"])){
        redirect_to("index.php");
    }

    $userId = $_SESSION["user_id"];

    //get user favorites
    $userFavorites = array();
    $favQuery = "SELECT favorite_id FROM users_favorites WHERE user_id='{$userId}'";
    $favResult = $dbConnection->performQuery($favQuery);

    if($favResult){

        while($favorite = mysqli_fetch_assoc($favResult))
            $userFavorites[] = $favorite["favorite_id"];

    }

    $pageNumber = isset($_GET["page_number"]) ? urldecode($_GET["page_number"]) : "1";

    $favoriteTotalCount = count($userFavorites);
    $perPage = 2;

    if($pageNumber > ceil($favoriteTotalCount / $perPage)) $pageNumber = 1;

    $pagination = new Pagination($pageNumber, $perPage, $favoriteTotalCount);



    include "header.php";

?>





    <div class="page-header-padding page-header-bg">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  
                  <h1 class="page-title white-color">My Favourites
    
                </h1>
                </div>
              </div>
            </div>
          </div>
   
    <section class="padding-main">
        <div class="container">
            <div class="row">
                <?php for($i = ($pageNumber - 1) * $perPage; $i < ($pageNumber - 1) * $perPage + $perPage && $i < count($userFavorites); $i++):

                    $favoriteId = $userFavorites[$i];
                    
                    $favQuery = "SELECT * FROM public_figure_user WHERE id='{$favoriteId}' LIMIT 1";
                    $favResult = $dbConnection->performQuery($favQuery);

                    // $favoriteUser = mysqli_fetch_assoc($favResult);
                    if($favResult){

                        while($favoriteUser = mysqli_fetch_assoc($favResult)){
                    
                    
                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                            <div class="mainflip">
                                <div class="frontside">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <p><a href="<?php echo "profile.php?username=" . $favoriteUser["username"]; ?>"><img class="img-responsive img-circle center-block" style="width: 120px; height: 120px" src="<?php echo "uploads/" . $favoriteUser["photo_path"]; ?>"
                                                    alt="card image"></a></p>
                                            <h4 class="card-title m-t-em-2"><a href="<?php echo "profile.php?username=" . $favoriteUser["username"]; ?>"> <?php echo $favoriteUser["first_name"] . " " . $favoriteUser["last_name"]; ?></a></h4>
                                            <strong class="card-text">
                                                <?php 
                                                $categoryPath = explode("-", $favoriteUser["category_path"]);
                                                array_pop($categoryPath);
                                                
                                                $parentId = $categoryPath[0];
                                                $childId = $categoryPath[count($categoryPath) - 1];

                                                $output = "";
                                                
                                                $sqlQuery="SELECT name FROM categories_chain WHERE id='{$parentId}'";
                                                $category = $dbConnection->performQuery($sqlQuery);
                                                $output .= mysqli_fetch_assoc($category)["name"];

                                                if(count($categoryPath) > 1){
                                                    $sqlQuery="SELECT name FROM categories_chain WHERE id='{$childId}'";
                                                    $category = $dbConnection->performQuery($sqlQuery);
                                                    $output .= " - " . mysqli_fetch_assoc($category)["name"];
                                                }

                                                echo $output;
                                                
                                                ?>
                                            </strong>
                                            <p class="card-text" style="margin-top: 7px">
                                                <?php
                                                    $countryId = $favoriteUser["residence"];
                                                    $sqlQuery="SELECT * FROM countries WHERE id='{$countryId}'";
                                                    $country = $dbConnection->performQuery($sqlQuery);
                
                                                    echo mysqli_fetch_assoc($country)["country_name"];
                                                ?>
                                            </p>
                                            <!-- <a href="#" class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>
                                            <a href="#" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a> -->
                                            <div class="clearfix"></div>
                                            <i class="fas fa-heart favorite" data-fav="1" data-user="<?php echo $_SESSION["user_id"]; ?>" data-favorite="<?php echo $favoriteUser["id"]; ?>" style="color: #F00; font-size: 27px"></i>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php 
                        }
                    }
                endfor; ?>
                
                <div class="col-md-12">
                        <nav class="tg-pagination">
                                <ul>
                                    <?php if($pageNumber != 1): ?>
                                        <li class="tg-prevpage"><a href="<?php echo "favorite.php?page_number=" . $pagination->previous(); ?>"><i class="fa fa-angle-left"></i></a></li>
                                    <?php endif; ?>
                                    <li class="<?php if($pageNumber == 1) echo "tg-active";  ?>"><a href="<?php echo "favorite.php?page_number=" . "1"; ?>"><?php echo "1"; ?></a></li>

                                    <?php if($pagination->pagesNumber() >= 4){ ?>

                                        <?php if($pageNumber > 2): ?>
                                            <span> ... </span>
                                        <?php endif; ?>

                                        
                                        <?php if($pageNumber <= 2): ?>
                                            <li class="<?php if($pageNumber == 2) echo "tg-active";  ?>"><a href="<?php echo "favorite.php?page_number=" . "2"; ?>"><?php echo "2"; ?></a></li>
                                            <li class=""><a href="<?php echo "favorite.php?page_number=" . "3"; ?>"><?php echo "3"; ?></a></li>
                                        <?php elseif($pageNumber > $pagination->pagesNumber() - 2):  ?>
                                            <li class=""><a href="<?php echo "favorite.php?page_number=" . ($pagination->pagesNumber() - 2); ?>"><?php echo ($pagination->pagesNumber() - 2); ?></a></li>
                                            <li class="<?php if($pageNumber == $pagination->pagesNumber() - 1) echo "tg-active";  ?>"><a href="<?php echo "favorite.php?page_number=" . ($pagination->pagesNumber() - 1); ?>"><?php echo ($pagination->pagesNumber() - 1); ?></a></li>
                                        <?php else: ?>
                                            <li class=""><a href="<?php echo "favorite.php?page_number=" . ($pageNumber - 1); ?>"><?php echo ($pageNumber - 1); ?></a></li>
                                            <li class="tg-active"><a href="<?php echo "favorite.php?page_number=" . $pageNumber; ?>"><?php echo $pageNumber; ?></a></li>
                                            <li class=""><a href="<?php echo "favorite.php?page_number=" . ($pageNumber + 1); ?>"><?php echo ($pageNumber + 1); ?></a></li>
                                        <?php endif; ?>

                                        <?php if($pageNumber <= $pagination->pagesNumber() - 2): ?>
                                            <span> ... </span>
                                        <?php endif; ?>

                                    <?php }else if($pagination->pagesNumber() == 3){ ?>
                                        <li class="<?php if($pageNumber == 2) echo "tg-active";  ?>"><a href="<?php echo "favorite.php?page_number=" . "2"; ?>"><?php echo "2"; ?></a></li>
                                    <?php } ?>

                                    <?php if($pagination->pagesNumber() >= 2): ?>
                                        <li class="<?php if($pageNumber == $pagination->pagesNumber()) echo "tg-active";  ?>"><a href="<?php echo "favorite.php?page_number=" . $pagination->pagesNumber(); ?>"><?php echo $pagination->pagesNumber(); ?></a></li>
                                    <?php endif; ?>
                                    <?php if($pageNumber != $pagination->pagesNumber()): ?>
                                        <li class="tg-nextpage"><a href="<?php echo "favorite.php?page_number=" . $pagination->next(); ?>"><i class="fa fa-angle-right"></i></a></li>
                                    <?php endif; ?>
                                </ul>
                            </nav>

                </div>
                


            </div>

        </div>
    </section>




    <?php include "footer.php"; ?>


    <script>
    
    $(document).ready(function(){
        $(".favorite").on("click", function(){

            if($(this).data("fav") == "0"){
                $(this).data("fav", "1");
                $(this).css("color", "#F00");
            }else if($(this).data("fav") == "1"){
                $(this).data("fav", "0");
                $(this).css("color", "#444444");
            }

            //sync database
            var data = new FormData();
            data.append('favorite_sync', 'favorite_sync');
            data.append('user_id', $(this).data("user"));
            data.append('favorite_id', $(this).data("favorite"));
            data.append('favorite', $(this).data("fav"));


            $.ajax({
                url: 'api/favorite_sync.php',
                data: data,
                dataType: "text",
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(returnData){
                    // console.log(returnData);
                    returnData = JSON.parse(returnData);

                    if(returnData["error"] == 0){
                        
                    }else{
                        
                    }
                }

            // console.log($(this).data("fav"));
            });


        });
    });
    
    
    </script>