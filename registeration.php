<?php 

  session_start();

  // $pageTitle = "Public Figure";
  require_once("../includes/initialize.php");


  //get database connection
  $dbConnection = getDatabaseConnection();

?>


<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Multi Step Form using semantic-ui</title>
  
  
  
      <!-- <link rel="stylesheet" href="css/style.css"> -->
      <link rel="stylesheet" href="css/style2.css">
      <!-- <link rel="stylesheet" href="css/style3.css"> -->

  
</head>

<body>

  <html>

<head>
  <title>SignUp</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">
  <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
  <!-- <link rel="stylesheet" href="signUp.css"> -->

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.js"></script>
  <!-- <script type="text/javascript" src="signUp.js"></script> -->
</head>

<body style="background-color: #300032;">
  <div class="ui centered  grid container">

    <div class="row"></div>
    <div class="row"></div>
    <div class="row"></div>


    <div class="ui text container">


      <div class="three ui  buttons center aligned grid container" style="margin:20px;">


        <div class=" ui  big basic violet button" id="accountS">

          <div class="content " style="font-size:12px;color:white;">PERSONAL INFORMATION</div>
        </div>


        <button class=" ui  big disabled blue basic button" id="socialP">
             <div class="content" style="font-size:12px;color:white;">CATEGORY</div>
             </button>


        <button class=" disabled ui big orange basic button" id="details">

             <div class="content" style="font-size:12px;color:white;">Uploads</div>
             </button>
      </div>


      <div class="row"></div>
      <div class="row"></div>


      <div id="account">


        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color:#300032;">Personal Info</h1>
          </div>

          <form class="ui form">
            <!-- <div class="field">
              <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="text" placeholder="Email-id">
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" placeholder="Password">
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" placeholder="Confirm Password">
              </div>
            </div> -->

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="pull-left">First Name</label>
                    <input id="first_name" name="first_name" type="text" class="form-control require_text" placeholder="First Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Middle Name</label>
                    <input  id="middle_name" name="middle_name" type="text" class="form-control" placeholder="Middle Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Last Name </label>
                    <input type="text" id="last_name" name="last_name" class="form-control require_text" placeholder="Last Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left">Gender </label>
                    <select class="form-control require_select" id="gander" name="gander">
                        <option value="">- - Choose Gender - -</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>

                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group" id="">
                    <label class=" control-label pull-left"> Birth Date</label>

                    <div class=" input-group " style="display: block;">
                        <span class="input-group-addon" style=" background-color: transparent !important; border: none !important;"></span>
                        <input style="padding: 6px;" name="birth_date" id="birth_date" placeholder="M/D/Y" type="date" class="form-control require_select" value="">
                    </div>

                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left">Nationality </label>
                    <select id="nationality"   class="form-control require_select" name="nationality">
                        <option value="-1">---Nationality---</option>
                        <?php
                        // $residenceSet = get_all_from_tbl('countries_tbl');
                        $sqlQuery="SELECT * FROM certigd5_newiscndb20.countries_tbl";
                        $residenceSet = $dbConnection->performQuery($sqlQuery);
                        while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                        <option value="<?php echo $residence['id']; ?>">
                            <?php echo $residence['country_name']; ?></option><?php
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left" >Residence ( Optional ) </label>
                    <select id="residence"  class="form-control" name="residence">
                        <option value="-1">---Residence---</option><?php
                        // $residenceSet = get_all_from_tbl('countries_tbl');
                        $sqlQuery="SELECT * FROM certigd5_newiscndb20.countries_tbl";
                        $residenceSet = $dbConnection->performQuery($sqlQuery);
                        while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                        <option value="<?php echo $residence['id']; ?>">
                            <?php echo $residence['country_name']; ?></option><?php
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Email</label>
                    <input type="email" id="email_register" name="email" class="form-control require_email" placeholder="Email">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="pull-left"> Phone </label>
                    <input type="text"  id="phone" name="phone" class="form-control require_text" placeholder="Phone">
                </div>
                    
            </div>

            <div class="row" style="margin-top: 7em;">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="pull-left">username</label>
                        <input id="user_name" type="text" class="form-control require_text" placeholder="username">
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="pull-left">passwrod</label>
                        <input  id="password" type="password" class="form-control require_text" placeholder="passwrod">
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="pull-left">confirm password</label>
                        <input type="password" id="confirm_password"  class="form-control require_text" placeholder="confirm password">
                    </div>
                </div>

            <div class="one ui buttons">
              <button class="ui  inverted violet  medium button next1">Next</button>
            </div>

          </div>

        </form>


      </div>

    </div>



      <div id="social">


        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color:rgb(50,153,153);">Category</h1>
          </div>

          <form class="ui form" id="categories">
            <!-- <div class="field">
              <div class="ui left icon input">
                <i class="twitter icon"></i>
                <input type="text" placeholder="twitter">
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="facebook icon"></i>
                <input type="text" placeholder="facebook">
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="github icon"></i>
                <input type="text" placeholder="github">
              </div>
            </div> -->

            <div class="col-sm-4 ">
                <div class="form-group">
                  
                    <select id="category1"  class="form-control category" name="category1" data-level="1">
                        <option value="-1">---Category---</option><?php

                        $level = 1;
                        $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id IS NULL";
                        $categories = $dbConnection->performQuery($sqlQuery);
                        while ($category = mysqli_fetch_assoc($categories)) { 
                          // if($category["level"] == 1):
                          ?>
                          <option value="<?php echo $category['id']; ?>">
                            <?php echo $category['name']; ?></option><?php

                          // endif;
                        } ?>
                          
                    </select>
                </div>
            </div>


            <div class="two ui buttons">
              <button class="ui  inverted violet  medium button prev1">Previous</button>
              <button class="ui  inverted violet  medium button next2">Next</button>
            </div>

          </form>


        </div>

      </div>


      <div id="personal">


        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color:orange;">Uploads</h1>
          </div>

          <form class="ui form">
            <div class="col-md-4">

              <div class="x_content">
                  <div class="wrap">
                      <h4> ID or Passport</h4>
                      <div class="upload-thumb thumbnail">
                          <img style="max-height: 200px;min-height: 200px;" id="img1" src="" alt="">
                      </div>
                    
                      <label for="file1" class="btn btn-default">
                          Upload Image
                          <input data-id="1" id="file1" type="file" class="upload">
                      </label>
                      
                  </div>
              </div>


            </div>

            <div class="col-md-4">

              <div class="x_content">
                  <div class="wrap">
                      <h4> Personal Photo (optional)</h4>
                      <div class="upload-thumb thumbnail">
                          <img style="max-height: 200px;min-height: 200px;" id="img2" src="" alt="">
                      </div>
                    
                      <label for="file2" class="btn btn-default">
                          Upload Image
                          <input data-id="2" id="file2" type="file" class="upload">
                      </label>
                      
                  </div>
              </div>


            </div>
            <div class="two ui buttons">
              <button class="ui  inverted violet  medium button prev2">Previous</button>
              <button class="ui  inverted violet  medium button submit">Submit</button>
            </div>

          </form>


        </div>

      </div>


    </div>
  </div>


</body>

</html>
  
  

    <!-- <script  src="js/index.js"></script> -->
    <script  src="js/index1.js"></script>

    <script>
    
        $(document).ready(function(){
          //var oldLevel = -1;
          $("#categories").on("change", ".category", function(e){
            

            // console.log(categoryDone);
            var selection = $(e.target);
            var categoryId = parseInt(selection.children("option:selected").val());
            // console.log(selection.data("level"));

            selection.css("border", "1px solid rgba(34,36,38,.15)");
            
            if(categoryId != -1){

              //get next level categories
              oldLevel = categoryLevel;
              categoryLevel = selection.data("level");
              var nextLevel = categoryLevel + 1;

              if(categoryLevel > oldLevel) /*categoryPath += categoryId + "-"*/ categoryPath.push(categoryId);
              else {
                while(categoryPath.length >= categoryLevel)
                  categoryPath.pop();

                  categoryPath.push(categoryId);
              }
              // else if(categoryLevel == oldLevel) categoryPath = categoryPath.slice(0, categoryPath.lastIndexOf("-", categoryPath.length - 2) + 1) + categoryId + "-"


              $.get( "api/next_level_categories.php?category_id=" + categoryId + "&category_level=" + categoryLevel, function( data ) {
                data = JSON.parse(data);
                if(data["error"] == 0){
                  var categories = data["categories"];

                  // console.log(categories);

                  selection.nextAll().remove();

                  if(categories.length > 0){
                    var nextSelection = $('<select id="category' + nextLevel + '"  class="form-control category" name="category' + nextLevel + '" data-level="' + nextLevel + '"></select>');
                
                    nextSelection.append($('<option value="' + -1 + '"> ' + '---Sub Category---' + ' </option>'));
                    //add options
                    for(var i = 0; i < categories.length; i++)
                      nextSelection.append($('<option value="' + categories[i]["id"] + '"> ' + categories[i]["name"] + ' </option>'));

                    selection.parent().append(nextSelection);

                    categoryDone = false;
                  }else categoryDone = true;
                }

              });
              

            }else {
              selection.nextAll().remove();
              selection.css("border", "1px solid red");
              categoryDone = false;
              oldLevel = categoryLevel;
              categoryLevel = selection.data("level");
              while(categoryPath.length >= categoryLevel)
                categoryPath.pop();
            }

            // console.log(oldLevel + "==" + categoryLevel);
            // console.log(categoryPath);
          });

          $(document).on('change','.upload',function (){

            var img_id=$(this).data('id');

            $("#img"+img_id).css({top: 0, left: 0});
            preview(this,img_id);

          });

        });

        function preview(input,img_id) {
        if (input.files && input.files[0]) {
          var ext= (input.files[0]["name"].split('.').pop()).toLowerCase();

            var reader = new FileReader();
            reader.onload = function (e) {
                if(ext=="pdf")
                    $('#img'+img_id).attr('src', "images/pdf.jpg");
                else
                $('#img'+img_id).attr('src', e.target.result);

            }
            reader.readAsDataURL(input.files[0]);

        }   
      }

    

    </script>



</body>

</html>
