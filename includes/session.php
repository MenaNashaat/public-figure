<?php 

    class Session{

        private $loggedIn = false;
        public $userId;
        public $message;

        private static $me = null;

        private function __construct(){
            session_start();
            $this->checkLogin();
            $this->checkMessage();
        }

        public function isLoggedIn(){
            return $this->loggedIn;
        }

        public function login($user = null){
            if(isset($user) && $user != null){
                $this->userId = $_SESSION["userId"] = $user->id;
                $this->$loggedIn = true;
            }

            return null;
        }

        public function logout(){
            unset($_SESSION["userId"]);
            unset($this->userId);
            $this->$loggedIn = false;
        }

        private function checkLogin(){
            if(isset($_SESSION['userId'])){
                $this->userId = $_SESSION['userId'];
                $this->loggedIn = true;
            }else {
                unset($this->userId);
                $this->loggedIn = false;
            }
        }

        private function checkMessage(){
            if(isset($_SESSION["message"]))
                $this->message = $_SESSION["message"];
            else $this->message = "";
        }

        public function message($message = ""){
            if(!empty($message)){
                //set message
                $this->message = $message;
                $_SESSION["message"] = $this->message;

                return null;
            }else {
                //get message
                $this->message = $_SESSION["message"];
                return $this->message;
            }
        }

        public function deleteMessage(){
            unset($this->message);
            unset($_SESSION["message"]);
        }

        public static function singleton(){
            if(!isset(self::$me)) self::$me = new self;
            return self::$me;
        }

    }

    //singleton method
    function getSession(){
        return Session::singleton();
    }
?>