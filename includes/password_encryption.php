

<?php 

    class PasswordEncryption{

        private const FORMAT = "$2y$7$";  //blowfish(2y) run 7 times using crypt
        private const SALT_LENGTH = 22;  //for blofish algo

        //uses blowfish encription algorithm 
        public static function passwordEncrypt($password){
            $formatedSalt = static::generateFormatedSalt();
            return crypt($password, $formatedSalt);
        }

        //generate formated salt for password
        private static function generateFormatedSalt(){
            //Not 100% unique, not 100% random, but good enough for a salt
            // MD5 returns 32 characters
            $uniqueRandomSalt = md5(uniqid(mt_rand(), true));
            
            // Valid characters for a salt are [a-zA-Z0-9./]
            $uniqueRandomSalt = base64_encode($uniqueRandomSalt);
            
            // But not '+' which is valid in base64 encoding
            $uniqueRandomSalt = str_replace('+', '.', $uniqueRandomSalt);
            
            // Truncate string to the correct length
            $uniqueRandomSalt = substr($uniqueRandomSalt, 0, static::SALT_LENGTH);
            
              return static::FORMAT . $uniqueRandomSalt;
        }

        public static function passwordVerify($password, $existingHashedPassword /*for same user*/){
            $hashedPassword = crypt($password, $existingHashedPassword /*take format and 22 length salt only and strip rest of string*/);

            return $hashedPassword === $existingHashedPassword;
        }



    }

?>