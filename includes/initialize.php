<?php 

    //include files config
    defined("DS") ? null : define("DS", DIRECTORY_SEPARATOR);
    defined("SITE_ROOT") ? null : define("SITE_ROOT", "C:" . DS . "xampp" . DS . "htdocs" . DS . "public_figure");
    defined("LIB_PATH") ? null : define("LIB_PATH", SITE_ROOT . DS . "includes");
    // defined("PUBLIC_INCLUDE_PATH") ? null : define("PUBLIC_INCLUDE_PATH", SITE_ROOT . DS . "green-horizotal" . DS . "public_includes");

    //php backend library
    require_once(LIB_PATH . DS . "config.php");
    require_once(LIB_PATH . DS . "functions.php");
    require_once(LIB_PATH . DS . "database.php");
    require_once(LIB_PATH . DS . "database_helper.php");
    require_once(LIB_PATH . DS . "user.php");
    require_once(LIB_PATH . DS . "session.php");
    require_once(LIB_PATH . DS . "template.php");
    require_once(LIB_PATH . DS . "password_encryption.php");
    require_once(LIB_PATH . DS . "pagination.php");
    require_once(LIB_PATH . DS . "public_figure_exception.php");
    require_once(LIB_PATH . DS . "search.php");
    require_once(LIB_PATH . DS . "search_result.php");

    

?>