<?php 


    //redirect to another page
    function redirect_to($location = null){
        if(isset($location) && $location != null){
            header("Location: " . $location);  //before any html output or turn on output buffering
            exit();
        }

        return null;
    }

    //auto loading not required used classes
    function __autoload($className){
        $className = strtolower($className);
        $classFilePath = LIB_PATH . DS . "{$className}.php";

        if(file_exists($classFilePath))
            require_once($classFilePath);
        else die("class file {$className}.php cannot be found!");
    }

?>