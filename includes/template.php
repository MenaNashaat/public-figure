

<?php 

class Template{

    public $fileName;
    public $assignedVariables = array();

    public function set($key = "", $value = ""){
        $this->assignedVariables[$key] = $value;
    }

    public function display(){
        if(!empty($this->fileName) && !(empty($this->assignedVariables))){

            //check file existance
            if(file_exists($this->fileName)){
                //gefile contents
                $output = file_get_contents($this->fileName);
                foreach ($this->assignedVariables as $key => $value) {
                    //replace each key name with it's value
                    $output = preg_replace('/{' . $key . '}/g', $value, $output);
                }

                echo $output;
                return $true;

            }
            return null;
        }

        return null;
    }

}

?>