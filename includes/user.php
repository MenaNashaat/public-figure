<?php require_once("database.php") ?>


<?php

    class User /*extends DatabaseHelper*/{

        public static $dbConnection;

        //database attributes will be added dynamically
        //custom attributes

        //db info attributes
        protected static $tableName = "public_figure_user";
        protected static $tableFillableFields = array();
        protected $tableFields = array();
        private static $FILLABLE = 1;
        private static $NON_FILLABLE = 0;

        //instantiate user with his data
        public function __construct($record = null){

            //construct database attributes/fields
            $this->constructDBObject();
            
            if(isset($record) && $record != null)
                static::userAssign($this, $record);

        }

        private function constructDBObject(){
            
            //describe table query
            $describeQuery = "DESCRIBE " . static::$tableName;

            //return assoc array for each database filed
            $queryResult = static::$dbConnection->performQuery($describeQuery);

            //define each databae filed in our class
            while($databaseField = static::$dbConnection->fetchRecordArray($queryResult) /* assoc array describes database field*/){
                // $this->{$databaseField["Field" /*key for db field name*/]} = $databaseField["Default" /*default value for db field*/];
                $this->tableFields[$databaseField["Field" /*key for db field name*/]] = null /*$databaseField["Default" default value for db field]*/;
                static::$tableFillableFields[$databaseField["Field" /*key for db field name*/]] = static::$NON_FILLABLE;
            
            }

        }

        //edit fillable database fields
        public static function fillableFields($fillable = null){
            if($fillable == null || empty($fillable))
                return false;

            for($i = 0; $i < count($fillable); $i++)
                if(array_key_exists($fillable[$i], static::$tableFillableFields))
                    static::$tableFillableFields[$fillable[$i]] = static::$FILLABLE;

            return true;
        }

        public function __get($property){
            if(array_key_exists($property, $this->tableFields))
                return $this->tableFields[$property];
            die("Property does not exist");
        }

        public function __set($property, $value){
            if(array_key_exists($property, $this->tableFields) && static::$tableFillableFields[$property] == static::$FILLABLE)
                $this->tableFields[$property] = $value;
            else die("Property deos not exist or cannot be overriden!");
        }


        //return user full name
        public function fullName(){
            if(isset($this->first_name) && isset($this->middle_name) && isset($this->last_name))
                return $this->first_name . " " . $this->last_name;

            return "";
        }

        //authenticate users
        
        public static function authenticate($username = "", $password = ""){
            //safe input!
            $username = static::$dbConnection->prepareQueryValue($username);
            $password = static::$dbConnection->prepareQueryValue($password);

            $sqlQuery = "SELECT * FROM " . static::$tableName ." ";
            $sqlQuery .= "WHERE username='{$username}' ";
            // $sqlQuery .= "AND password='{$password}' ";
            $sqlQuery .= "LIMIT 1";

            $user = static::findBySql($sqlQuery);

            if($user){

                //verify user password
                $user = array_shift($user);
                return PasswordEncryption::passwordVerify($password /*password*/, $user->password/*existing hashed*/);
            }

            return false;
        }

        //return array of objects all users
        public static function findAll(){
            $sqlQuery = "SELECT * FROM " . static::$tableName;
            return static::findBySql($sqlQuery);
        }

        //find user by id and return user object if found
        public static function findById($id = 0){
            $sqlQuery = "SELECT * FROM " . static::$tableName . " WHERE id=" . static::$dbConnection->prepareQueryValue($id) . " LIMIT 1";
            $user = static::findBySql($sqlQuery);
            return $user ? array_shift($user) : null;
        }

        //find users by sql query and return them in array
        public static function findBySql($sqlQuery = "" /*inputs are not sanitized, even no validation on sql query*/){
            $resultSet = static::$dbConnection->performQuery($sqlQuery);
            
            if($resultSet){
                //create array of users by this result set
                $users = array();
                while($user = static::$dbConnection->fetchRecordArray($resultSet))
                    $users[] = static::instantiate($user);

                return !empty($users) ? $users : null;
            }

            return null;
        }

        //count all users
        public static function countAll(){
            $sqlQuery = "SELECT COUNT(*) FROM " . static::$tableName;
            $usersCount = static::$dbConnection->performQuery($sqlQuery);
            $usersCount = static::$dbConnection->fetchRecordArray($usersCount);
            return array_shift($usersCount);
        }
        
        //instaniate user object from user database record
        private static function instantiate($record = null){
            $me = new static;
            if(isset($record) && $record != null){
                
                return static::userAssign($me, $record);
            }

            return null;
        }

        //assign user object to a database record (assoc_array / std object)
        private static function userAssign($user, $record, $privateFlage = false){
            // var_dump($user->tableFields);
            foreach($record as $attribute => $value)
                    if($user->hasAttribute($attribute))  //normal class attribute
                        $user->$attribute = $value;
                    else if(array_key_exists($attribute, $user->tableFields) && $privateFlage)  //database data
                        $user->tableFields[$attribute] = $value;
                    else if(array_key_exists($attribute, $user->tableFields) && !$privateFlage && static::$tableFillableFields[$attribute] == static::$FILLABLE)  //outsource data
                        $user->tableFields[$attribute] = $value;

            return $user;
        }

        //check if protperty exist
        private function hasAttribute($attribute){
            $myVars = get_object_vars($this);  //including private variables

            return array_key_exists($attribute, $myVars);
        }

        //-------------DB CRUD----------------------
        public function DBInsert($user = null){
            //object attributes
            // $attributes = $this->attributes();
            $preparedAttributes = $this->sanitizeSQLAttributes($this->tableFields);

            //excluding id from array, it's auto increment
            array_shift($preparedAttributes);

            //prepare query values
            
            $sqlQuery = "INSERT INTO " . static::$tableName . " ( ";

            $sqlSeperator = ", ";
            foreach($this->tableFields as $key => $value)
                if($value != null && static::$tableFillableFields[$key] == static::$FILLABLE)
                    $sqlQuery .= $key . $sqlSeperator;

            
            $sqlQuery = substr($sqlQuery, 0, strlen($sqlQuery) - strlen($sqlSeperator));  //remove last ", "
            $sqlQuery .= " ) VALUES ( '";

            $sqlSeperator = "', '";
            foreach($this->tableFields as $key => $value)
                if($value != null && static::$tableFillableFields[$key] == static::$FILLABLE)
                    $sqlQuery .= $value . $sqlSeperator;

            $sqlQuery = substr($sqlQuery, 0, strlen($sqlQuery) - strlen($sqlSeperator));  //remove last "', '"
            $sqlQuery .= "' )";

            // $sqlQuery = "INSERT INTO " . static::$tableName . " ( ";
            // $sqlQuery .= join(",  ", array_keys($preparedAttributes));
            // $sqlQuery .= " ) VALUES ( '";
            // $sqlQuery .= join("', '", array_values($preparedAttributes)) . "' )";

            //perform query
            $result = static::$dbConnection->performQuery($sqlQuery);
            if($result){
                $this->tableFields["id"] = static::$dbConnection->insertId();
                return true;
            }

            return false;
            
        }

        public function DBUpdate($user = null){
            //get object attributes
            $preparedAttributes = $this->sanitizeSQLAttributes($this->tableFields);

            //excluding id from array, it's auto increment
            array_shift($preparedAttributes);

            //construct key=>'value' pair
            // $attributesPairs = array();
            // foreach($preparedAttributes as $key => $value)
            //     $attributesPairs[] = "{$key}='{$value}'"; 

            //prepare query
            // $sqlQuery = "UPDATE " . static::$tableName . " SET ";
            // $sqlQuery .= join(", ", $attributesPairs);
            
            // $sqlQuery .= " WHERE id=" . static::$dbConnection->prepareQueryValue($this->tableFields["id"]);

            $sqlQuery = "UPDATE " . static::$tableName . " SET ";
            $sqlSeperator = "', ";
            foreach($preparedAttributes as $key => $value)
                if($value != null && static::$tableFillableFields[$key] == static::$FILLABE)
                    $sqlQuery .= $key . "='" . $value . $sqlSeperator;

            $sqlQuery = substr($sqlQuery, 0, strlen($sqlQuery) - strlen($sqlSeperator));  //remove last "', "
            
            $sqlQuery .= " WHERE id=" . static::$dbConnection->prepareQueryValue($this->tableFields["id"]);

            //perform query
            static::$dbConnection->performQuery($sqlQuery);
            return static::$dbConnection->affectedRows() == 1 ? true : false; 
        }

        protected function sanitizeSQLAttributes($attributes){
            //prepare each query value
            $preparedAttributes = array();
            foreach ($attributes as $key => $value)
                $preparedAttributes[$key] = static::$dbConnection->prepareQueryValue($attributes[$key]);

            return $preparedAttributes;
        }

        public function DBSave(){
            return isset($this->tableFields["id"]) ? $this->DBUpdate() : $this->DBInsert();
        }

        public function DBDelete(){
            //prepare query
            $sqlQuery = "DELETE FROM " . static::$tableName;
            $sqlQuery .= " WHERE id=" . static::$dbConnection->prepareQueryValue($this->tableFields["id"]);
            $sqlQuery .= " LIMIT 1";

            //perform query
            static::$dbConnection->performQuery($sqlQuery);
            return static::$dbConnection->affectedRows()  == 1 ? true : false;
        }

        //get object attributes
        // private function attributes(){
        //     //create attributes assoc array array(key=>value)
        //     $attributes = array();
        //     foreach (static::$tableColumns as $column)
        //         if(property_exists($this, $column))
        //             $attributes[$column] = $this->$column;

        //     return $attributes;
        // }

        //-------------------------------------------------------------------------------------------------
        // private function prepareClassValues(){

        // }

        
        // public static function applyDBObjectStructure(){

        //     //initialize columns array
        //     self::$tableColumns = array();

        //     //get assigned table structre
        //     $sqlQuery = "DESCRIBE " . self::$tableName;

        //     $columns = self::$dbConnection->performQuery($sqlQuery);
        //     while($column = self::$dbConnection->fetchRecord($columns)){

        //         self::$tableColumns[ $column["Field"] ] = null;  //defualt value

        //         //if(isset($column["Default"]) || $column["Default"] === null) echo "hello" . "<br />";
        //         //echo $column["Default"] . "<br />";
        //         //echo $column["Default"] === null ? "yes" : "no";
        //         // echo "<pre>";
        //         // print_r($column);
        //         // echo "</pre>";
        //     }
                
        //     // print_r(self::$tableColumns);
        //     // foreach(self::$tableColumns as $key => $value)
        //     //     echo $key . " = " . $value . "<br />";
        //     // print_r(self::$tableColumns);
        //     // echo self::$tableColumns["id"];
            
        // }


        
    }

    //initializing database connection
    User::$dbConnection = getDatabaseConnection();

?>