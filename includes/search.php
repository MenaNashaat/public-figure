<?php require_once("database.php") ?>

<?php 

    class Search{

        public static $dbConnection;

        public static $lastSearchString;
        
        //search with exact username/id, like name or like category
        public static function exactSeacrh($searchString){
            static::$lastSearchString = $searchString;

            //start searching

            //first check exact username/id----------------

            //escape query params
            $searchString = static::$dbConnection->prepareQueryValue($searchString);
            $searchSqlQuery = "SELECT " . join(", ", SearchResult::$searchReturnFileds);
            $searchSqlQuery.= " FROM public_figure_user WHERE unq_code='{$searchString}' OR username='{$searchString}' LIMIT 1";


            $searchResults = static::$dbConnection->performQuery($searchSqlQuery);
            
            return static::$dbConnection->fetchRecordArray($searchResults);
            

        }


        public static function normalSearch($searchString){
            static::$lastSearchString = $searchString;

            //start searching
            $users = array();

            //escape query params
            $searchString = static::$dbConnection->prepareQueryValue($searchString);

            //first check username/id and name
            $searchSqlQuery = "SELECT " . join(", ", SearchResult::$searchReturnFileds);
            $searchSqlQuery.= " FROM public_figure_user WHERE unq_code LIKE '%{$searchString}%' OR username LIKE '%{$searchString}%'";
            $searchSqlQuery .= " OR first_name LIKE '%{$searchString}%' OR last_name LIKE '%{$searchString}%'";

            // echo $searchSqlQuery . "<br />";

            $searchResults = static::$dbConnection->performQuery($searchSqlQuery);    
            
            while($user = static::$dbConnection->fetchRecordArray($searchResults))
                $users[] = $user;

            //second check category
            // $searchSqlQuery = "SELECT id FROM public_figure_categories WHERE category_name LIKE '%{$searchString}%'";
            // $searchResults = static::$dbConnection->performQuery($searchSqlQuery); 

            // while($category = static::$dbConnection->fetchRecordArray($searchResults)){
            //     //for each matched category, get it users
            //     $category_id = $category["id"];

            //     $searchSqlQuery = "SELECT " . join(", ", SearchResult::$searchReturnFileds);
            //     $searchSqlQuery.= " FROM public_figure_user WHERE category_id='{$category_id}'";

            //     $subSeacrhResults = static::$dbConnection->performQuery($searchSqlQuery);

            //     while($user = static::$dbConnection->fetchRecordArray($subSeacrhResults))
            //         $users[] = $user;
            // }

            // print_r($users);

            return !empty($users) ? $users : null;

        }

        public static function searchUsersNumber($searchString, $searchFields){
            static::$lastSearchString = $searchString;

            //start searching
            // $users = array();

            //escape query params
            $searchString = static::$dbConnection->prepareQueryValue($searchString);

            $gander = static::$dbConnection->prepareQueryValue($searchFields[0]);
            $nationality = static::$dbConnection->prepareQueryValue($searchFields[1]);

            $categoryId = static::$dbConnection->prepareQueryValue($searchFields[2]);
            $categoryLevel = static::$dbConnection->prepareQueryValue($searchFields[3]);
            $categoryPath = static::$dbConnection->prepareQueryValue($searchFields[4]);

            //dynamic fileds
            


            //begin searching
            // $searchSqlQuery = "SELECT " . join(", ", SearchResult::$searchReturnFileds);
            $searchSqlQuery = "SELECT COUNT(*) as usersNumber";
            $searchSqlQuery.= " FROM public_figure_user WHERE ";

            //search string
            $searchSqlQuery .= "(unq_code LIKE '%{$searchString}%' OR username LIKE '%{$searchString}%'";
            $searchSqlQuery .= " OR first_name LIKE '%{$searchString}%' OR last_name LIKE '%{$searchString}%')";

            //static fields
            if($gander != "-1")
                $searchSqlQuery .= " AND gander='$gander'";

            if($nationality != "-1")
                $searchSqlQuery .= " AND nationality='$nationality'";
            
            // //category
            // if($categoryId != "reset")
            //     $searchSqlQuery .= " AND category_id='$categoryId'";

            if($categoryPath != "")
                $searchSqlQuery .= " AND category_path LIKE '{$categoryPath}%'";

            // echo $searchSqlQuery;

            $searchResults = static::$dbConnection->performQuery($searchSqlQuery);
            return $searchResults != null ? static::$dbConnection->fetchRecordArray($searchResults)["usersNumber"] : null;
        }

        public static function searchWithParams($searchString, $searchFields, $pagination = null){
            static::$lastSearchString = $searchString;

            //start searching
            $users = array();

            //escape query params
            $searchString = static::$dbConnection->prepareQueryValue($searchString);

            $gander = static::$dbConnection->prepareQueryValue($searchFields[0]);
            $nationality = static::$dbConnection->prepareQueryValue($searchFields[1]);

            $categoryId = static::$dbConnection->prepareQueryValue($searchFields[2]);
            $categoryLevel = static::$dbConnection->prepareQueryValue($searchFields[3]);
            $categoryPath = static::$dbConnection->prepareQueryValue($searchFields[4]);

            //dynamic fileds
            


            //begin searching
            $searchSqlQuery = "SELECT " . join(", ", SearchResult::$searchReturnFileds);
            $searchSqlQuery.= " FROM public_figure_user WHERE ";

            //search string
            $searchSqlQuery .= "(unq_code LIKE '%{$searchString}%' OR username LIKE '%{$searchString}%'";
            $searchSqlQuery .= " OR first_name LIKE '%{$searchString}%' OR last_name LIKE '%{$searchString}%')";

            //static fields
            if($gander != "-1")
                $searchSqlQuery .= " AND gander='$gander'";

            if($nationality != "-1")
                $searchSqlQuery .= " AND nationality='$nationality'";
            
            // //category
            // if($categoryId != "reset")
            //     $searchSqlQuery .= " AND category_id='$categoryId'";

            if($categoryPath != "")
                $searchSqlQuery .= " AND category_path LIKE '{$categoryPath}%'";


            if($pagination != null)
                $searchSqlQuery .= " LIMIT " . $pagination->perPage . " OFFSET " . $pagination->offset();
            // echo $searchSqlQuery;

            $searchResults = static::$dbConnection->performQuery($searchSqlQuery);    
        
            while($user = static::$dbConnection->fetchRecordArray($searchResults))
                $users[] = $user;

            // print_r($users);
            // echo $searchSqlQuery;

            // //dynamic fields
            // $matchedUsers = array();
            // foreach($users as $user){
            //     $searchSqlQuery = "SELECT * FROM public_figure_filed_values WHERE user_id='" . $user["id"] . "' ";
            //     foreach($dynamicFields as $field)
            //         $searchSqlQuery .= "AND field_id='" . $field->id . "' AND value LIKE '%" . $field->value . "%' ";

            //     $resultSet = static::$dbConnection->performQuery($searchSqlQuery);
            //     if(static::$dbConnection->numRows($resultSet) > 0) $matchedUsers[] = $user;

            //     // echo $searchSqlQuery;
            // }

            // print_r($matchedUsers);
            return $users;

        }

    }

    //initializing database connection
    Search::$dbConnection = getDatabaseConnection();

?>