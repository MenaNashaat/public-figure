<?php require_once("database.php") ?>

<?php 

    class SearchResult{

        public static $dbConnection;

        public static $searchReturnFileds = ["id", "unq_code", "category_id", "first_name", "last_name", "username", "birth_date", "gander", "nationality", "residence", "category_path", "photo_path", "description"];  //return fileds from user table

        protected $searchReturn = array();

         //instantiate search result with his data
         public function __construct($record = null){
            
            if(isset($record) && $record != null)
                static::searchResultAssign($this, $record);

        }

        public function __get($property){
            if(array_key_exists($property))
                return $this->searchResult[$property];
            else die("Error: property not found!");
        }

        public function __set($property, $value){
            if(array_key_exists($property))
                $this->searchResult[$property] = $value;
            else die("Error: property not found!");
        }

      

        //assign user object to a database record (assoc_array / std object)
        private static function searchResultAssign($searchResult, $record/*, $privateFlage = true*/){
            // var_dump($user->tableFields);
            foreach($record as $attribute => $value)
                    if($searchResult->hasAttribute($attribute))  //normal class attribute
                        $searchResult->$attribute = $value;
                    else if(array_key_exists($attribute, $searchResult->searchReturn) /*&& $privateFlage*/)  //database data
                        $searchResult->searchReturn[$attribute] = $value;
                    // else if(array_key_exists($attribute, $user->tableFields) && !$privateFlage && static::$tableFillableFields[$attribute] == static::FILLABLE)  //outsource data
                    //     $user->tableFields[$attribute] = $value;

            return $user;
        }


    }

    //initializing database connection
    SearchResult::$dbConnection = getDatabaseConnection();

?>