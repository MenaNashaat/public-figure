

<?php 

class Pagination{
    public $currentPage;
    public $pagesNumber;
    public $perPage;
    public $totalCount;

    public function __construct($currentPage = 1, $perPage = 1, $totalCount=  0){
        $this->currentPage = $currentPage;
        $this->perPage = $perPage;
        $this->totalCount = $totalCount;
        $this->pagesNumber = $this->pagesNumber();
    }

    public function pagesNumber(){
        return ceil($this->totalCount / $this->perPage);
    }

    public function previous(){
        if($this->hasPrevios()) return $this->currentPage - 1;
        return 1;
    }

    public function next(){
        if($this->hasNext()) return $this->currentPage + 1;
        return $this->pagesNumber;
    }

    public function hasPrevios(){
        if($this->currentPage - 1 >= 1) return true;
        return false;
    }

    public function hasNext(){
        if($this->currentPage + 1 <= $this->pagesNumber) return true;
        return false;
    }

    public function offset(){
        return ($this->currentPage - 1) * $this->perPage;
    }
}

?>