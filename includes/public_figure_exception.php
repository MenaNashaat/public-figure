


<?php

class PublicFigureException extends Exception{


    public function __toString(){
        return __CLASS__ . " - " . $this->code . " - " . $this->message . "<br />";
    }
}

?>