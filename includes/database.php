<?php require_once(LIB_PATH . DS . "config.php"); ?>

<?php 


    //get table columns query
    // 1 => DESCRIBE table_name
    // 2 => SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'database_name' AND TABLE_NAME = 'table_name';
    //mysql database

    
    class MySQLDatabase{

        private $connection;
        private $lastQuery;
        private $magicQuotesActive;
        private $mysqliRealEscapeStringExists;

        //singleton instance
        private static $me = null;  //singleton pattern

        //open connection (constructor)
        private function __construct(){
            $this->magicQuotesActive = get_magic_quotes_gpc();
            $this->mysqliRealEscapeStringExists = function_exists("mysqli_real_escape_string");
            $this->openConnection();
        }

        //close connection if exist (destructor)
        function __destruct(){
            $this->closeConnection();
        }

        //open connection method
        public function openConnection(){
            $this->connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
            if(! $this->connection) die("database connection failed: " . mysqli_errno($this->connection) . " - " . mysqli_error($this->connection) . ".");
        }

        //close connection
        public function closeConnection(){
            if(isset($this->connection)){
                mysqli_close($this->connection);
                $this->connection = null;
                unset($this->connection);
            }
        }

        //performe query method
        public function performQuery($sqlQuery = null){
            if(isset($sqlQuery) && $sqlQuery != null){
                //prepare query
                //$sqlQuery = $this->prepareQuery($sqlQuery);

                //store last query
                $this->lastQuery = $sqlQuery;

                //performe our query
                $queryResult = mysqli_query($this->connection, $sqlQuery);
                
                //confirm query
                return $this->confirmQuery($queryResult);
                
            }

            return null;
        }

        //prepare query values (sanitize)
        public function prepareQueryValue($queryValue = null){
           if(isset($queryValue) && $queryValue != null){
                //$magic_quotes_active = get_magic_quotes_gpc();
                if($this->mysqliRealEscapeStringExists)  //PHP >= 4.3.0
                    return $this->magicQuotesActive ? mysqli_real_escape_string(stripslashes($this->connection, $queryValue)) : mysqli_real_escape_string($this->connection, $queryValue);
                else return $this->magicQuotesActive ? $queryValue : addslashes($queryValue);
           }

           return null;
        }

        //confirm query after excusion
        private function confirmQuery($queryResult /*private*/){
            if($queryResult) return $queryResult;
            $outputMessage = "database query failed: " . mysqli_errno($this->connection) . " - " . mysqli_error($this->connection) . ".";
            $outputMessage .= "<br /> last query: " . $this->lastQuery;
            die($outputMessage);
            return null;
        }

        //return assoc array of fetched record
        public function fetchRecordArray($queryResult = null /*passing by reference*/, $arrayType = MYSQLI_ASSOC){
            if(isset($queryResult) && $queryResult != null)
                return mysqli_fetch_array($queryResult, $arrayType);

            return null;
        }

        //return object of fetched record
        public function fetchRecordObject($queryResult = null /*passing by reference*/){
            if(isset($queryResult) && $queryResult != null)
                return mysqli_fetch_object($queryResult);

            return null;
        }

        //return number of rows for excuted query result
        public function numRows($queryResult = null){
            if(isset($queryResult) && $queryResult != null)
                return mysqli_num_rows($queryResult);

            return null;
        }

        //return last inserted id
        public function insertId(){
            return mysqli_insert_id($this->connection);
        }

        //return number of rows affected by last query
        public function affectedRows(){
            return mysqli_affected_rows($this->connection);
        }

        //singleton pattern for creating only one connection
        public static function singleton(){
            if(!isset(self::$me))  self::$me = new self();  //create instance if not exists
            return self::$me;
        }

        //singleton function
        function getDatabaseConnection(){
            return MySQLDatabase::singleton();
        }
        
    }

    //singleton function
    function getDatabaseConnection(){
        return MySQLDatabase::singleton();
    }

?>