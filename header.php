<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Add Your favicon here -->
	<!--<link rel="icon" href="img/favicon.ico">-->
	<title><?php echo $pageTitle; ?></title>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	<!-- Animation CSS -->
	<link href="css/animate.css" rel="stylesheet">

	<!-- font awesome  -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
	 crossorigin="anonymous">
	<!-- owl carousel  -->
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	<!-- Custom styles for this slider -->

	<!-- <link rel="stylesheet" href="assets/css/styles.css"> -->
    <link rel="stylesheet" href="css/select2/select2.min.css">
    <link href="css/summernote/summernote.css" rel="stylesheet">
    <link href="css/summernote/summernote-bs3.css" rel="stylesheet">

	<!-- Main css -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">

	<link rel="shortcut icon" type="img/png" href="img/logo-2.png" />


<style>
    .loader {

    display: none;
    border: 3px solid #f3f3f3;
    border-radius: 50%;
    border: 3px solid #ffffff;
    border-top: 3px solid #3498db;
    width: 25px;
    height: 25px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    margin-top: 7px;
    }

    /* Safari */
    @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }


.spinner {
  display: none;
  width: 27px;
  height: 27px;

  position: relative;
  /* margin: 100px auto; */
}

.double-bounce1, .double-bounce2 {
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: #333;
  opacity: 0.6;
  position: absolute;
  top: 0;
  left: 0;
  
  -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
  animation: sk-bounce 2.0s infinite ease-in-out;
}

.double-bounce2 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

@-webkit-keyframes sk-bounce {
  0%, 100% { -webkit-transform: scale(0.0) }
  50% { -webkit-transform: scale(1.0) }
}

@keyframes sk-bounce {
  0%, 100% { 
    transform: scale(0.0);
    -webkit-transform: scale(0.0);
  } 50% { 
    transform: scale(1.0);
    -webkit-transform: scale(1.0);
  }
}

.favorite{
    cursor: pointer;
}
</style>

</head>

<body>


<header style="margin-bottom: 0px;">

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="fa fa-bars fa-lg"></span>
            </button>
            <a class="navbar-brand" href="index.php">

                <img style="display: inline; padding: 17px 0;" src="img/logo-2.png" alt="" class="logo">
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
                
                <li class="<?php if($pageTitle == "home") echo "active" ?>" data-name="home">
                    <a class="page-scroll" href="index.php">Home</a>
                </li>

                <li class="<?php if($pageTitle == "Feeds") echo "active" ?>" data-name="Feeds">
                    <a class="page-scroll" href="feeds.php">Feeds</a>
                </li>

                <li data-name="search" class="<?php if($pageTitle == "search") echo "active" ?>">
                    <a class="page-scroll" href="results.php">Advanced Search</a>
                </li>

                <li>
                    <a class="page-scroll" href="aboutUS.php">About US</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">ISCN System <b class="caret"></b></a>


                    <ul class="dropdown-menu">
                        <li><a href="about-us-iscn-system.php">About Us</a></li>
                        <li><a href="https://iscnsystem.org/">ISCN System Website</a></li>

                    </ul>
                </li>

<!--                <li>-->
<!--                    <a class="page-scroll" href="abouttUS.html">About US</a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a class="page-scroll" href="contactUS.html">contact US</a>-->
<!--                </li>-->


                <?php if(!isset($_SESSION["user"])){ ?>

                
                <li data-name="register" class="<?php if($pageTitle == "register") echo "active" ?>">
                    <a class="page-scroll" href="form.php">Register</a>
                </li>
                
                <li>
                    <a class="page-scroll" data-toggle="modal" data-target="#login"> login</a>
                </li>



                <?php } ?>


                <?php
                    global $dbConnection;
                    if(isset($_SESSION["user"])){
                        $username = $_SESSION["user"];
                        $sqlQuery = "SELECT first_name, last_name, photo_path from public_figure_user where username='{$username}' LIMIT 1";

                        $queryResult = $dbConnection->performQuery($sqlQuery);
                        $photo = "img-01.jpg";
                        $userName = "";
                        if($queryResult){
                            $tempUser = mysqli_fetch_assoc($queryResult);
                            $photo = $tempUser["photo_path"];
                            $username = $tempUser["first_name"] . $tempUser["last_name"];
                        }
                
                ?>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
                        <img src="<?php echo "uploads/" . $photo; ?>" class="img-circle img-responsive special-img" style="max-width: 50px; max-height:50px"> <?php echo $username; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li style="margin: 3px 0;"><a href="home.php"></i> Home </a></li>
                    <li style="margin: 3px 0;"><a href="profile.php"></i> View Profile </a></li>
                    <li style="margin: 3px 0;"><a href="edit.php"></i> Edit Profile </a></li>
                        <li data-name="favorite"><a href="favorite.php?page_number=1"><i class="fas fa-heart"></i> My Favourite</a></li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fas fa-sign-out-alt"></i> Sign-out</a></li>
                    </ul>
                </li>

                    <?php } ?>



            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-->
</nav>


<!--cover-->

</header>

<!-- Modal -->
<div id="login" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Login</h4>
				</div>
				<div class="modal-body " style="padding: 3em 1em">
					<div class="row">
						<div class="col-md-6">
							<img src="img/img-01.png" class="img-responsive">

						</div>
						<div class="col-md-6">
							<form action="" style="padding: 3em .5em 0;">
								<div class="form-group">

									<input id="login_username" type="text" class="form-control btn-rounded input-lg
                                        "
									 placeholder="username" id="email">
								</div>
								<div class="form-group">

									<input id="login_password" type="password" class="form-control btn-rounded input-lg" id="pwd" placeholder="Password">
                                </div>
                                
                                <small id="login_hint" style="color: red"></small>

								<button id="modal_submit" type="submit" class="btn btn-primary inverse btn-lg m-b-em-2">Submit</button>
							</form>

						</div>

					</div>


				</div>

			</div>

		</div>
	</div>
