
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

var categoryLevel = 1;
var categoryPath = [];
var categoryDone = false;

var goNext = true;
var done = false;

$(document).on('click','.next', function(event){
	event.preventDefault();



	var pageNumber = $(this).data("id");
	var errors = "";

    if(pageNumber == 1){
        var firstName = $("#first_name");
        var lastName = $("#last_name");

        var gander = $("#gander");
        var birthDate = $("#birth_date");
        var nationality = $("#nationality");

        var email = $("#email_register");
        var phone = $("#phone");

        var username = $("#user_name");
        var password = $("#password");
        var confirmPassword = $("#confirm_password");

        if (firstName.val() == "") {
            firstName.css("border", "solid 1px red");
            goNext = false;
        }else firstName.css("border", "1px solid rgba(34,36,38,.15)");

        if (lastName.val() == "") {
            // lastName.addClass("false_input");
            // lastName.removeClass("empty_input");
            // lastName.removeClass("true_input");
            lastName.css("border", "solid 1px red");
            goNext = false;
        }else lastName.css("border", "1px solid rgba(34,36,38,.15)");

        if (gander.val() == "") {
            // gander.addClass("false_input");
            // gander.removeClass("empty_input");
            // gander.removeClass("true_input");
            gander.css("border", "solid 1px red");
            goNext = false;
        }else gander.css("border", "1px solid rgba(34,36,38,.15)");

        if (birthDate.val() == "") {
            // birthDate.addClass("false_input");
            // birthDate.removeClass("empty_input");
            // birthDate.removeClass("true_input");
            birthDate.css("border", "solid 1px red");
            goNext = false;
        }else birthDate.css("border", "1px solid rgba(34,36,38,.15)");

        if (nationality.val() == "") {
            // nationality.addClass("false_input");
            // nationality.removeClass("empty_input");
            // nationality.removeClass("true_input");
            nationality.css("border", "solid 1px red");
            goNext = false;
        }else nationality.css("border", "1px solid rgba(34,36,38,.15)");

        if (email.val() == "") {
            // email.addClass("false_input");
            // email.removeClass("empty_input");
            // email.removeClass("true_input");
            email.css("border", "solid 1px red");
            goNext = false;
        }else email.css("border", "1px solid rgba(34,36,38,.15)");

        if (phone.val() == "") {
            // phone.addClass("false_input");
            // phone.removeClass("empty_input");
            // phone.removeClass("true_input");
            phone.css("border", "solid 1px red");
            goNext = false;
        }else phone.css("border", "1px solid rgba(34,36,38,.15)");

        if(username.val() == ""){
            // username.addClass("false_input");
            // username.removeClass("empty_input");
            // username.removeClass("true_input");
            username.css("border", "solid 1px red");
            goNext = false;
        }else username.css("border", "1px solid rgba(34,36,38,.15)");
        if(password.val() == "" || password.val().length < 4){
            // password.addClass("false_input");
            // password.removeClass("empty_input");
			// password.removeClass("true_input");
			// if(password.val().length < 4) errors += "password must be at least 4 characters";
            password.css("border", "solid 1px red");
            goNext = false;
        }else password.css("border", "1px solid rgba(34,36,38,.15)");
        if(confirmPassword.val() == "" || confirmPassword.val() !== password.val() || confirmPassword.val().length < 4){
            // confirmPassword.addClass("false_input");
            // confirmPassword.removeClass("empty_input");
            // confirmPassword.removeClass("true_input");
            confirmPassword.css("border", "solid 1px red");
            goNext = false;
        }else confirmPassword.css("border", "1px solid rgba(34,36,38,.15)");

    }else if(pageNumber == 2){
		if(!categoryDone){
			$("#category" + (categoryLevel + 1)).css("border", "1px solid red");
			// console.log($("#category" + categoryLevel));
			goNext = false;
		  }

		//   console.log(categoryPath);
		

    }else{
		
		var file1=$("#file1");
		var file2=$("#file2");

		if ($("#file1").val() == "") {
			$("#text_1").css("color", "#c90808");
			goNext = false;
		}

		if ($("#file2").val() == "") {
			$("#text_2").css("color", "#c90808");
			goNext = false;
		}

		var description = $("#desc");

		if (description.val() == "") {
			description.css("border", "solid 1px red");
			goNext = false;
		}else description.css("border", "1px solid rgba(34,36,38,.15)");

		if(goNext) done = true;
	}


	if(done) // all data is good and page 4
	{//send request
		// then go to payment

		$(".spinner").show();
        $(this).attr("disabled", true);

		var firstName = $("#first_name").val();
    	var middleName = $("#middle_name").val();
		var lastName = $("#last_name").val();

		var gander = $("#gander").val();
		var birthDate = $("#birth_date").val();
		var nationality = $("#nationality").val();

		var residence = $("#residence").val();
		var email = $("#email_register").val();
		var phone = $("#phone").val();

		var username = $("#user_name").val();
		var password = $("#password").val();
		var confirmPassword = $("#confirm_password").val();

		var description = $("#desc").val();

		var data = new FormData();
		data.append('register', 'register');

		data.append('first_name', firstName);
		data.append('middle_name', middleName);
		data.append('last_name', lastName);

		data.append('gander', gander);
		data.append('birth_date', birthDate);
		data.append('nationality', nationality);

		data.append('residence', residence);
		data.append('email', email);
		data.append('phone', phone);

		data.append('user_name', username);
		data.append('password', password);
		data.append('confirm_password', confirmPassword);

		var categoryPathString = "";
		for(var i = 0; i < categoryPath.length; i++)
		categoryPathString += categoryPath[i] + "-";

		data.append('category_level', categoryPath.length);
		data.append('category_id', categoryPath.pop());
		data.append('category_path', categoryPathString);

		data.append('description', description);
		


		$.each(file1[0].files, function(i, file) {
			data.append('national_id', file);
		});

		$.each(file2[0].files, function(i, file) {
			data.append('personal', file);
		});
		
		// data.append('category_fields', JSON.stringify(categoryFieldsValues));


		$.ajax({
			url: 'api/register_user.php',
			data: data,
			dataType: "text",
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			success: function(data){

				// console.log(data);
				var data = JSON.parse(data);

				if(data["error"] == 0){
					// alert("user registered");

					// location.href = "http://localhost:8080/public_figure/mena_mahmoud/profile.php";

					Swal.fire(
                        'Registered Successfully!',
                        'Your has been registered successfully..',
                        'success'
                    ).then(() => {location.href = "http://localhost:8080/public_figure/mena_mahmoud/profile.php";});
				}
			}

		});

	}//end done

});

$(document).on('click','.next', function(e){
	e.preventDefault();
	

	if(!goNext) {goNext = true; return false;}

	//check username
	if($(this).data("id") == "1"){
		
		var nextButton = $(this);
		nextButton.attr("disabled", true);
		var username = $("#user_name");

		var data = new FormData();
		data.append('check_username', 'check_username');
		data.append('username', username.val());

		$.ajax({
			url: 'api/check_username.php',
			data: data,
			dataType: "text",
			cache: false,
			contentType: false,
			processData: false,
			async: false,
			type: 'POST',
			success: function(returnData){

				// console.log(returnData);

				returnData = JSON.parse(returnData);

				if(returnData["error"] == 0){
					if(returnData["username"] == -1) {
						$("#hint").text("username already token!");
						username.css("border", "solid 1px red");
						username.addClass("false_input");
						username.removeClass("empty_input");
						username.removeClass("true_input");
						nextButton.attr("disabled", false);

						goNext = false;
					}
				}
			}
		});
	}

	if(!goNext) {goNext = true; return false;}
	else if(done) return false;

	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(e){
	e.preventDefault();

	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})