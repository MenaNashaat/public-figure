
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
var date_from_to;

var categoryLevel = 1;
var categoryPath = [];
var categoryDone = false;


$(document).on('keyup','.require_text',function(){
    if($(this).val() != "")
    {
        $(this).addClass("true_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("false_input");
    }
    else
    {
        $(this).addClass("empty_input");
        $(this).removeClass("true_input");
        $(this).removeClass("false_input");
    }


});

$(document).on('keyup','.require_password',function(){
    if($(this).val() != "" && $(this).val().length >= 4) 
    {
        $(this).addClass("true_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("false_input");
    }
    else if($(this).val() != "")
    {
        $(this).addClass("false_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("true_input");
    }else{
        $(this).addClass("empty_input");
        $(this).removeClass("true_input");
        $(this).removeClass("false_input");
    }


});

$(document).on('change','.require_select',function(){

    if($(this).val() == "Invalid date - Invalid date")
    {
        $(this).addClass("false_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("true_input");
    }
    else if($(this).val() != "" )
    {
        $(this).addClass("true_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("false_input");
    }
    else
    {
        $(this).addClass("empty_input");
        $(this).removeClass("true_input");
        $(this).removeClass("false_input");
    }


});

$(document).on('keyup','.require_email',function() {


    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var email_valid= re.test(String($(this).val()).toLowerCase());


    if ($(this).val() == "") {
        $(this).addClass("empty_input");
        $(this).removeClass("true_input");
        $(this).removeClass("false_input");
    }
    else  if(!email_valid ){
        $(this).addClass("false_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("true_input");
    }

    else
    {
        $(this).addClass("true_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("false_input");
    }

});

$(document).on('keyup','.optional_email',function() {


    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var email_valid= re.test(String($(this).val()).toLowerCase());


    if ($(this).val() == "") {
        $(this).removeClass("empty_input");
        $(this).removeClass("true_input");
        $(this).removeClass("false_input");
    }
    else  if(!email_valid ){
        $(this).addClass("false_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("true_input");
    }
    else  {


        $(this).addClass("true_input");
        $(this).removeClass("empty_input");
        $(this).removeClass("false_input");
    }


});



$(document).on('change','.require_file',function(){
    if($(this).val()=="") {
        var img_num = $(this).data("id");

        $("#text_" + img_num).css("color","rgb(201, 8, 8);");

    }
    else
    {
        var img_num = $(this).data("id");

        $("#text_" + img_num).css("color","black");
    }



});



