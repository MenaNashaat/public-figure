
var ct = 0;

var categoryLevel = 1;
var categoryPath = [];
var categoryDone = false;
$(document).ready(function() {


  

  $('.next1').on('click', function(e) {

    e.preventDefault();

    
    
    //client side validation
    var done = true;

    var firstName = $("#first_name");
    var lastName = $("#last_name");

    var gander = $("#gander");
    var birthDate = $("#birth_date");
    var nationality = $("#nationality");

    var email = $("#email_register");
    var phone = $("#phone");

    var username = $("#user_name");
    var password = $("#password");
    var confirmPassword = $("#confirm_password");

    if (firstName.val() == "") {
        firstName.css("border", "solid 1px red");
        done = false;
    }else firstName.css("border", "1px solid rgba(34,36,38,.15)");

    if (lastName.val() == "") {
        // lastName.addClass("false_input");
        // lastName.removeClass("empty_input");
        // lastName.removeClass("true_input");
        lastName.css("border", "solid 1px red");
        done = false;
    }else lastName.css("border", "1px solid rgba(34,36,38,.15)");

    if (gander.val() == "") {
        // gander.addClass("false_input");
        // gander.removeClass("empty_input");
        // gander.removeClass("true_input");
        gander.css("border", "solid 1px red");
        done = false;
    }else gander.css("border", "1px solid rgba(34,36,38,.15)");

    if (birthDate.val() == "") {
        // birthDate.addClass("false_input");
        // birthDate.removeClass("empty_input");
        // birthDate.removeClass("true_input");
        birthDate.css("border", "solid 1px red");
        done = false;
    }else birthDate.css("border", "1px solid rgba(34,36,38,.15)");

    if (nationality.val() == "") {
        // nationality.addClass("false_input");
        // nationality.removeClass("empty_input");
        // nationality.removeClass("true_input");
        nationality.css("border", "solid 1px red");
        done = false;
    }else nationality.css("border", "1px solid rgba(34,36,38,.15)");

    if (email.val() == "") {
        // email.addClass("false_input");
        // email.removeClass("empty_input");
        // email.removeClass("true_input");
        email.css("border", "solid 1px red");
        done = false;
    }else email.css("border", "1px solid rgba(34,36,38,.15)");

    if (phone.val() == "") {
        // phone.addClass("false_input");
        // phone.removeClass("empty_input");
        // phone.removeClass("true_input");
        phone.css("border", "solid 1px red");
        done = false;
    }else phone.css("border", "1px solid rgba(34,36,38,.15)");

    if(username.val() == ""){
        // username.addClass("false_input");
        // username.removeClass("empty_input");
        // username.removeClass("true_input");
        username.css("border", "solid 1px red");
        done = false;
    }else username.css("border", "1px solid rgba(34,36,38,.15)");
    if(password.val() == ""){
        // password.addClass("false_input");
        // password.removeClass("empty_input");
        // password.removeClass("true_input");
        password.css("border", "solid 1px red");
        done = false;
    }else password.css("border", "1px solid rgba(34,36,38,.15)");
    if(confirmPassword.val() == "" || confirmPassword.val() !== password.val()){
        // confirmPassword.addClass("false_input");
        // confirmPassword.removeClass("empty_input");
        // confirmPassword.removeClass("true_input");
        confirmPassword.css("border", "solid 1px red");
        done = false;
    }else confirmPassword.css("border", "1px solid rgba(34,36,38,.15)");

    if(!done) return;


    $('#account').animate('slow', function() {

      if (ct > 0) {
        $('#account').removeClass('transition visible');
        $('#account').addClass('transition hidden');

      }
      $('#account').css('display', 'none');

      $('#accountS').addClass('disabled');
      $('#socialP').removeClass('disabled');
      $("#social").transition('fly right');
      $('body').css('background-color', '#06000a');
      $('#social button').removeClass('inverted violet');
      $('#social button').addClass('inverted blue');
      ct++;

    });

  });

  $('.prev1').one('click', function(e) {

    e.preventDefault();
    $('#accountS').removeClass('disabled');
    $('#socialP').addClass('disabled');

    $('#social').animate('slow', function() {

      $('body').css('background-color', '#300032');
      $('#social').transition('hide');
      $("#account").transition('fly right');

    });

  });

  $('.next2').on('click', function(m) {

    m.preventDefault();

    if(!categoryDone){
      $("#category" + (categoryLevel + 1)).css("border", "1px solid red");
      // console.log($("#category" + categoryLevel));
      return; 
    }

    $('#socialP').addClass('disabled');
    $('#details').removeClass('disabled');

    $('#social').animate('slow', function() {

      $('#personal button').removeClass("inverted violet");
      $('#personal button').addClass("inverted orange");
      $('body').css('background-color', '#251605');
      $('#social').transition('hide');

      $('#personal').transition('fly right');
    });

  });

  $('.prev2').one('click', function(m) {

    m.preventDefault();
    $('#details').addClass('disabled');
    $('#socialP').removeClass('disabled');

    $('#personal').animate('slow', function() {

      $('body').css('background-color', '#06000a');
      $('#personal').transition('hide');

      $('#social').transition('fly right');
    });

  });

  $('.submit').on('click', function(p) {

    p.preventDefault();

    //client side validation
    var done = true;

    var file1=$("#file1");
    var file2=$("#file2");

    if ($("#file1").val() == "") {
        $("#text_1").css("color", "#c90808");
        done = false;
    }

    if ($("#file2").val() == "") {
      $("#text_2").css("color", "#c90808");
      done = false;
    }

    if(!done) return;

    var firstName = $("#first_name").val();
    var middleName = $("#middle_name").val();
    var lastName = $("#last_name").val();

    var gander = $("#gander").val();
    var birthDate = $("#birth_date").val();
    var nationality = $("#nationality").val();

    var residence = $("#residence").val();
    var email = $("#email_register").val();
    var phone = $("#phone").val();

    var username = $("#user_name").val();
    var password = $("#password").val();
    var confirmPassword = $("#confirm_password").val();

    var data = new FormData();
    data.append('register', 'register');

    data.append('first_name', firstName);
    data.append('middle_name', middleName);
    data.append('last_name', lastName);

    data.append('gander', gander);
    data.append('birth_date', birthDate);
    data.append('nationality', nationality);

    data.append('residence', residence);
    data.append('email', email);
    data.append('phone', phone);

    data.append('user_name', username);
    data.append('password', password);
    data.append('confirm_password', confirmPassword);

    var categoryPathString = "";
    for(var i = 0; i < categoryPath.length; i++)
      categoryPathString += categoryPath[i] + "-";

    data.append('category_level', categoryPath.length);
    data.append('category_id', categoryPath.pop());
    data.append('category_path', categoryPathString);
    


    $.each(file1[0].files, function(i, file) {
      data.append('national_id', file);
    });

    $.each(file2[0].files, function(i, file) {
        data.append('personal', file);
    });
    
    // data.append('category_fields', JSON.stringify(categoryFieldsValues));


    $.ajax({
        url: 'api/register_user.php',
        data: data,
        dataType: "text",
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){

            // console.log(data);
            var data = JSON.parse(data);

            if(data["error"] == 0){
                alert("user registered");

                location.href = "http://localhost:8080/public_figure/public_figure/profile.php?username=" + username;
            }
            
            

        }




    });


  });

});