<?php 

	ob_start();
	session_start();

	// ini_set("display_errors", 1);


	// $loggedIn = false;
	// if(isset($_SESSION["user"]))
	// 	$loggedIn = true;

	$pageTitle = "home";
	require_once("../includes/initialize.php");


	//get database connection
	$dbConnection = getDatabaseConnection();


	include "header.php";

	// if(isset($_POST["search"])){
	// 	if($_POST["search_params"] != "")
	// 		$_SESSION["search_params"] = $_POST["search_params"];

	// 	redirect_to("results.php");
	// }

?>

	<div id="tg-homebanner" class="tg-homebanner tg-haslayout">
		<figure class="item">
			<div class="cover">
				<figcaption>
					<div class="container m-t-em-3">
						<div class="row text-center white-color">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tg-bannercontent">
									<h1>We will help you to find all
									</h1>
									<h2>Search from 12,45,754 Awesome Singers, Representatives, Businessmen ....!</h2>

								</div>
							</div>
						</div>
					</div>
				</figcaption>
			</div>
		</figure>
	</div>

	<main id="tg-main" class="tg-main tg-haslayout">
		<!--************************************
						Categories Search Start
				*************************************-->
		<section class="tg-haslayout">
			<div class="container">
			<form class="domain-checker" id="search_form" method="GET" action="results.php">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
						<div class="tg-categoriessearch" style="border-bottom: 1px dotted #0083ff; background-color: #0083ff; margin: -137px 0 0;">
							<!-- <div class="tg-title">
								<h2><span>Boost your search with</span> Trending Categories</h2>
							</div> -->
							<div class="row">
								<div class="col-md-4">
								<select class="cat form-control" id="category1" data-level="1" style="height: 50px; display: inline-block">
									<option value="">---Category---</option><?php

										$level = 1;
										$sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id IS NULL";
										$categories = $dbConnection->performQuery($sqlQuery);
										while ($category = mysqli_fetch_assoc($categories)) { 
										// if($category["level"] == 1):
										?>
										<option value="<?php echo $category['id']; ?>">
											<?php echo $category['name']; ?></option><?php

										// endif;
										} ?>
									</select>
								</div>

								<div class="col-md-8">
									
										<div class="group" style="  position: relative;display: table;border-collapse: separate;">
												<select class="select2_demo_3 form-control" style="">
														<option></option>
														
													</select>

											<div class="input-group-btn">
												<button id="search" class="btn btn-primary" type="submit" style="height: 50px;">
													Search
												</button>
											</div>
										</div>

										<input type="hidden" id="search_string" name="search_string" value="" >
										<input type="hidden" id="category_path" name="category_path" value="" >
									


								</div>

								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
										<div class="row">
											<div class="col-md-3">
													<div class="form-group">
														<label for="sel1" style="color: white">Gender:</label>

														<select class="form-control" id="sel2" name="gander">

															<option value="" selected>Gender</option>
															<option value="Male">Male</option>
															<option value="Female">Female</option>


														</select>

													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label for="sel3" style="color: white">nationality:</label>

														<select class="form-control" id="sel3" name="nationality">
															<option value="" selected> nationality </option>
																	<?php
																		$sqlQuery="SELECT * FROM countries";
																		$countries = $dbConnection->performQuery($sqlQuery);
																		while ($country = mysqli_fetch_assoc($countries)) { ?>
																		<option value="<?php echo $country['id']; ?>">
																			<?php echo $country['country_name']; ?></option><?php
																	} ?>

														</select>

													</div>
												</div>

											</div>

											
										</div>

										<div class="loader pull-right" style="margin-top: 4em; margin-right: 3em;"></div>
								</div>

								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
										<div class="row" id="categories">
										</div>

										<div class="clearfix"></div>
										<!-- <div class="loader pull-right"></div> -->
									</div>
								</div>

								

							</div>

							<!-- <div class="row" style="margin-top: 1em;">
								<div class="col-md-2 col-md-offset-7">
								<a id="search" class="btn btn-primary" type="submit" href="results.php">
													Advanced Search
	</a>
								</div>
							</div> -->


						</div>

						<!-- <div class="loader pull-right"></div> -->
						<a type="button" href="results.php" class="btn btn-primary pull-right" style="margin: 1em 0; background-color: #0083ff; border: none; padding: 1em 2em"> Advanced Search</a>

					</div>

					
				</div>
				</form>

				
			</div>
		</section>

	</main>
	<section id="fact">
		<div class="container">

			<div class="wpb_wrapper">
				<div class="row fact-items">
					<div class="col-sm-4 col-xs-6">
						<div class="fact">
							<div class="fact-icon">
								<img src="http://themerail.com/wp/theme/markesia/wp-content/uploads/2018/07/icon-1-1.png" class="img-fluid" alt="Achievement Icon">
							</div>
							<p class="fact-number"><span class="count animated" data-form="0" data-to="350">350</span></p>
							<p class="fact-name">Satisfied Clients</p>
						</div>
					</div>
					<div class="col-sm-4 col-xs-6">
						<div class="fact">
							<div class="fact-icon">
								<img src="http://themerail.com/wp/theme/markesia/wp-content/uploads/2018/07/icon-2-1.png" class="img-fluid" alt="Achievement Icon">
							</div>
							<p class="fact-number"><span class="count animated" data-form="0" data-to="1258">1258</span></p>
							<p class="fact-name">Project Submitted</p>
						</div>
					</div>
					<div class="col-sm-4 col-xs-6">
						<div class="fact">
							<div class="fact-icon">
								<img src="http://themerail.com/wp/theme/markesia/wp-content/uploads/2018/07/icon-3-1.png" class="img-fluid" alt="Achievement Icon">
							</div>
							<p class="fact-number"><span class="count animated" data-form="0" data-to="98">98</span></p>
							<p class="fact-name">Success Rate</p>
						</div>
					</div>
				</div>


				
					

							
			
			</div>


		</div>
	</section>



	<section id="how_work" class="padding-main ">
		<div class="container">
			<div class="row ">
				<div class="col-md-6 col-md-offset-3">
					<div class="section-title text-center m-b-em-2">
						<h2 class="title head ">How It is Work</h2>
						<p>While many prospective students tend to think of the United Kingdom and the United
								States first when considering where in the</p>
					</div>
				</div>
			</div>
			<div class="row ">
					<div class="col-md-3">
						<div class="block text-center m-b-em-2 arrow">
							<img class="img-responsive center-block" src="img/process-icon-1.png">
							<strong class="m-t-em-2">Research</strong>

						</div>
					</div>
					<div class="col-md-3">
							<div class="block text-center m-b-em-2 arrow">
								<img class="img-responsive center-block" src="img/process-icon-2.png">
								<strong class="m-t-em-2">Data Collection</strong>
	
							</div>
						</div>
						<div class="col-md-3">
								<div class="block text-center m-b-em-2 arrow">
									<img class="img-responsive center-block" src="img/process-icon-3.png">
									<strong class="m-t-em-2">targeting</strong>
		
								</div>
							</div>
							<div class="col-md-3">
									<div class="block text-center m-b-em-2 ">
										<img class="img-responsive center-block" src="img/process-icon-4.png">
										<strong class="m-t-em-2">Result</strong>
			
									</div>
								</div>
				</div>
		</div>
	</section>

	<!-- News-->
	<section id="News" class="padding-main ">
		<div class="container">
			<div class="row ">
				<div class="col-md-10 col-md-offset-1">
					<div class="section-title text-center m-b-em-2">
						<h2 class="title head ">News</h2>
					</div>
				</div>
				<div id="owl-demo">
					<div class="item">

						<div class="post">
							<a class="post-img" href="https://www.thelocal.de/20180221/germany-remains-best-country-for-international-students-above-uk-and-france" target="_balnk"><img class="img-responsive" src="img/thumb-3.jpg" alt=""></a>
							<div class="post-body">
								<!--<div class="post-meta">
											<a class="post-category cat-1" href="#">Category</a>
											
										</div>-->
								<h3 class="post-title "><a href="https://www.thelocal.de/20180221/germany-remains-best-country-for-international-students-above-uk-and-france"
									 class="secound-color" target="_blank"> Germany remains ‘best country for international
										students,’ above UK and France</a></h3>
								<p class="m-t-em-2 ">
									While many prospective students tend to think of the United Kingdom and the United
									States first when considering where in the world they want to study - as these
									countries have the best institutio
								</p>
								<a href="https://www.thelocal.de/20180221/germany-remains-best-country-for-international-students-above-uk-and-france"
								 target="_blank" class="secound-color" style="cursor: pointer">Read More</a>
							</div>
						</div>


					</div>
					<div class="item">

						<div class="post">
							<a class="post-img" href="https://www.japantimes.co.jp/news/2017/10/29/national/finance-ministry-opposes-free-higher-education-students/#.W3gSfugzbIV" target="_blank"><img
								 class="img-responsive" src="img/thumb-3.jpg" alt=""></a>
							<div class="post-body">
								<!--<div class="post-meta">
											<a class="post-category cat-1" href="#">Category</a>
											
										</div>-->
								<h3 class="post-title "><a href="https://www.japantimes.co.jp/news/2017/10/29/national/finance-ministry-opposes-free-higher-education-students/#.W3gSfugzbIV" target="_blank" class="secound-color"> Finance Ministry opposes free
										higher education for all students</a></h3>
								<p class="m-t-em-2 ">
									The Finance Ministry considers it inappropriate to make higher education in Japan
									free without exception, informed sources said.The ministry now believes the
									proposed program, as part of Prime Min
								</p>
								<a href="https://www.japantimes.co.jp/news/2017/10/29/national/finance-ministry-opposes-free-higher-education-students/#.W3gSfugzbIV"
								 target="_blank" class="secound-color" style="cursor: pointer">Read More</a>
							</div>
						</div>


					</div>
					<div class="item">

						<div class="post">
							<a class="post-img" href="https://www.japantimes.co.jp/news/2017/10/29/national/finance-ministry-opposes-free-higher-education-students/#.W3gSfugzbIV" target="_blank"><img
								 class="img-responsive" src="img/thumb-3.jpg" alt="" ></a>
							<div class="post-body">
								<!--<div class="post-meta">
											<a class="post-category cat-1" href="#">Category</a>
											
										</div>-->
								<h3 class="post-title "><a href="https://www.japantimes.co.jp/news/2017/10/29/national/finance-ministry-opposes-free-higher-education-students/#.W3gSfugzbIV" target="_blank" class="secound-color"> Finance Ministry opposes free
										higher education for all students</a></h3>
								<p class="m-t-em-2 ">
									The Finance Ministry considers it inappropriate to make higher education in Japan
									free without exception, informed sources said.The ministry now believes the
									proposed program, as part of Prime Min
								</p>
								<a href="https://www.japantimes.co.jp/news/2017/10/29/national/finance-ministry-opposes-free-higher-education-students/#.W3gSfugzbIV"
								 target="_blank" class="secound-color" style="cursor: pointer">Read More</a>
							</div>
						</div>


					</div>










				</div>
			</div>
		</div>
	</section>



	<?php include "footer.php"; ?>

	<!-- Main js -->
	<script src="js/main.js"></script>
	<script>
		$(".select2_demo_1").select2();
		$(".select2_demo_2").select2();
		// $(".select2_demo_3").select2({
		// 	placeholder: "Select a state",
		// 	allowClear: true,
			
		// });

		// var i = 1;
		$(".select2_demo_3").select2({
			placeholder: "Search Username/Id",
			allowClear: true,
			width: 'resolve',
			closeOnSelect: false,
			height: 20,
			ajax: {
				url: 'api/search_api.php',
				data: function (params) {
					var query = {};
					if(params.term != "")
						query = {
							search: params.term
						};

					// Query parameters will be ?search=[term]&type=public
					return query;
				},

				processResults: function (data) {
					// Tranforms the top-level key of the response object from 'items' to 'results'
					// console.log(data);
					var data = JSON.parse(data);  //array of result set
					var searchString = data["search_string"];
					var searchResult = [];
					
					if(data["error"] == 0 && data["search"] != null && data["search"].length > 0){
						
						for(var i = 1; i < data["search"].length; i++){
							var item = {};
							item.id = i;
							item.text = data["search"][i]["username"];

							searchResult.push(item);
						}

						// console.log("hello!");
						
						
						if(searchResult.length > 0){
							var item = {};
							item.id = -1;
							item.text = "search: " + searchString;

							searchResult.push(item);
							return {
								results: searchResult
							};
						}else {
							var item = {};
							item.id = i;
							item.text = "search: " + searchString;

							searchResult.push(item);
							return {results: searchResult};

						}
					}else{
						
						if(searchString != ""){
							var item = {};
							item.id = -1;
							item.text = "search: " + searchString;

							searchResult.push(item);
							return {results: searchResult};
						}else return {results: []};
					} 
				}
				
			}
		});




		$(document).ready(function(){
			categoryPath = [];
			// $("#search_results").append("<option value=" + "hello" + ">" + "hello" + "</option>");

			
			// $(document).on("keyup", ".select2-search__field", function(){
			// 	// console.log($(".select2-search__field").val());
			// 	var searchString =  $(".select2-search__field").val();
				
			// 	$("#search_results").empty();
			// 	if(searchString == "") return;



				
			// 	// $.ajax({
			// 	// 	url: 'api/search_api.php',
			// 	// 	data: data,
			// 	// 	dataType: "text",
			// 	// 	cache: false,
			// 	// 	contentType: false,
			// 	// 	processData: false,
			// 	// 	type: 'POST',
			// 	// 	success: function(data){

			// 	// 		// console.log(data);
			// 	// 		var data = JSON.parse(data);  //array of result set
			// 	// 		resultSearch = data["searchResult"];
						
			// 	// 		if(data["error"] == 0 && data["searchResult"] != null){

			// 	// 			var searchResult = [];
			// 	// 			for(var i = 0; i < data["searchResult"].length; i++){
			// 	// 				var item = {};
			// 	// 				item.id = i;
			// 	// 				item.text = data["searchResult"][i]["username"];

			// 	// 				searchResult.push(item);
			// 	// 			}
							
			// 	// 			// for(var i = 0; i < data["searchResult"].length; i++)
			// 	// 			// 	// console.log("<option value=\"" + data["searchResult"][i]["username"] + "\">" + data["searchResult"][i]["username"] + "</option>");
			// 	// 			//     $("#search_results").append("<option>" + data["searchResult"][i]["username"] + "</option>");

			// 	// 			$(".select2_demo_3").select2({
			// 	// 				data: searchResult
			// 	// 			});
								
							
			// 	// 		}

			// 	// 	}




			// 	// });
			// });

			// $(document).on("keyup", ".select2-search__field", function(){
			// 	// console.log("keypress");
			// });

			// console.log($(".select2_demo_3").find(':selected'));

			// $(".select2_demo_3").change(function(){
			// 	var selectedOption = $(this).children("option:selected").data("data");

			// 	console.log(selectedOption);
				
			// 	// if(selectedOption.id != i)
			// 	// 	location.href = "profile.php?username=" + selectedOption.text;
				
    		// });

			// $("#search").on("click", function(e){
			// 	e.preventDefault();

			// 	var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
				
			// 	if(selectedOption.id != i)
			// 		location.href = "profile.php?username=" + selectedOption.text;
			// 	else location.href = "results.php?search=" + selectedOption.text.substring(8);
				
			// });

			$("#search_form").on("submit", function(){
				var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
				
				if(selectedOption.id > 0){
					location.href = "profile.php?username=" + selectedOption.text;
					return false;
				}


				$("#search_string").val(encodeURI(selectedOption.text.substring(8)));
				var catPath = "";
				for(var i = 0; i < categoryPath.length; i++)
					catPath += categoryPath[i] + "-";

				$("#category_path").val(encodeURI(catPath));

				// return false;

				// var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
				// var searchParams = [];
				// var searchParams = {};

				// searchParams["search_string"] = selectedOption.text.substring(8);
				// searchParams["gander"] = $("#sel2").val();
				// searchParams["nationality"] = $("#sel3").val();

				// var catPath = "";
				// for(var i = 0; i < categoryPath.length; i++)
				// 	catPath += categoryPath[i] + "-";


				// searchParams["category_path"] = catPath;

				// $("#search_params").val(encodeURI(JSON.stringify(searchParams)));
				return true;
			});


			$(".cat").change(function(){

				// $(".loader").show();
				$(".loader").css("display", "inline-block");

				// console.log(categoryDone);
				var selection = $(this);
				var categoryId = parseInt(selection.children("option:selected").val());
				// console.log(selection.data("level"));

				if(categoryId != -1){

					//get next level categories
					// oldLevel = categoryLevel;
					var categoryLevel = selection.data("level");
					var nextLevel = categoryLevel + 1;

					while(categoryPath.length >= categoryLevel)
						categoryPath.pop();

					categoryPath.push(categoryId);

					// if(categoryLevel > oldLevel) /*categoryPath += categoryId + "-"*/ categoryPath.push(categoryId);
					// else {
					//     while(categoryPath.length >= categoryLevel)
					//     categoryPath.pop();

					//     categoryPath.push(categoryId);
					// }
					// else if(categoryLevel == oldLevel) categoryPath = categoryPath.slice(0, categoryPath.lastIndexOf("-", categoryPath.length - 2) + 1) + categoryId + "-"


					$.get( "api/next_level_categories.php?category_id=" + categoryId + "&category_level=" + categoryLevel, function( data ) {
						data = JSON.parse(data);
						if(data["error"] == 0){
							var categories = data["categories"];

							// console.log(categories);

							// selection.nextAll().remove();
							$("#categories").empty();

							if(categories.length > 0){
								var container = $('<div class="col-md-3"></div>');
								var nextSelection = $('<select id="category' + nextLevel + '"  class="form-control category" data-level="' + nextLevel + '"></select>');
							
								nextSelection.append($('<option value="' + -1 + '"> ' + '---Sub Category---' + ' </option>'));
								//add options
								for(var i = 0; i < categories.length; i++)
									nextSelection.append($('<option value="' + categories[i]["id"] + '"> ' + categories[i]["name"] + ' </option>'));

								container.append(nextSelection);
								$("#categories").append(container);

							}
						}

						$(".loader").css("display", "none");

					});


				}else {
					
					// selection.nextAll().remove();
					// selection.css("border", "1px solid red");
					// categoryDone = false;
					// oldLevel = categoryLevel;
					// categoryLevel = selection.data("level");
					// while(categoryPath.length >= categoryLevel)
					//     categoryPath.pop();

					categoryPath = [];
					$("#categories").empty();
				}

				// console.log(categoryPath);

				// 	var categoryId = $(this).children("option:selected").val();
				//     if(categoryId == "reset"){
				//         $(".search-fields").empty();
				//         return;
				//     }

				// 	//get category searchable fileds
				//     $.get( "api/category_searchable_fields.php?category_id=" + categoryId, function( data ) {
				//         // console.log(data);
				//         data = JSON.parse(data);

				//         //add search filds
				//         fields = data["fields"];
				//         // console.log(fields);
				//         for(var i = 0; i < fields.length; i++){
				//             $(".search-fields").empty();
				//             $(".search-fields").append("<div class=\"col-sm-4\">" + 
				//                 "<div class=\"form-group\">" +
				//                     "<label class=\"pull-left\">" + fields[i]["name"] + " </label>" +
				//                     "<input id=search" + fields[i]["id"] + " type=" + fields[i]["type"] + " class=\"form-control\" placeholder=" + fields[i]["name"] + ">"+
				//                 "</div>" + 
				//             "</div>");
				//         }
				//     });

					// $(".loader").hide();
					
				});

				$("#categories").on("change", ".category", function(e){

					// $(".loader").show();
					$(".loader").css("display", "inline-block");
					

				// console.log(categoryDone);
				var selection = $(e.target);
				var categoryId = parseInt(selection.children("option:selected").val());
				// console.log(selection.data("level"));

				if(categoryId != -1){

					//get next level categories
					// oldLevel = categoryLevel;
					var categoryLevel = selection.data("level");
					var nextLevel = categoryLevel + 1;

					while(categoryPath.length >= categoryLevel)
						categoryPath.pop();

					categoryPath.push(categoryId);

					// if(categoryLevel > oldLevel) /*categoryPath += categoryId + "-"*/ categoryPath.push(categoryId);
					// else {
					//     while(categoryPath.length >= categoryLevel)
					//         categoryPath.pop();

					//     categoryPath.push(categoryId);
					// }
					// else if(categoryLevel == oldLevel) categoryPath = categoryPath.slice(0, categoryPath.lastIndexOf("-", categoryPath.length - 2) + 1) + categoryId + "-"


					$.get( "api/next_level_categories.php?category_id=" + categoryId + "&category_level=" + categoryLevel, function( data ) {
						data = JSON.parse(data);
						if(data["error"] == 0){
							var categories = data["categories"];

							// console.log(categories);

							selection.parent().nextAll().remove();

							if(categories.length > 0){
								var container = $('<div class="col-md-3" style="margin-bottom: 1em"></div>');
								var nextSelection = $('<select id="category' + nextLevel + '"  class="form-control category" data-level="' + nextLevel + '"></select>');
							
								nextSelection.append($('<option value="' + -1 + '"> ' + '---Sub Category---' + ' </option>'));
								//add options
								for(var i = 0; i < categories.length; i++)
									nextSelection.append($('<option value="' + categories[i]["id"] + '"> ' + categories[i]["name"] + ' </option>'));

								container.append(nextSelection);
								selection.parent().parent().append(container);

								
							}
						}

						$(".loader").css("display", "none");

					});
					

				}else {
					selection.parent().nextAll().remove();
					var categoryLevel = selection.data("level");
					while(categoryPath.length >= categoryLevel)
						categoryPath.pop();
				}

				// console.log(oldLevel + "==" + categoryLevel);
				// console.log(categoryPath);

				// $(".loader").hide();
				
			});


			$("#modal_submit").on("click", function(e){
				e.preventDefault();

				//ajax check login
				var username = $("#login_username");
				var password = $("#login_password");

				if(username.val() == ""){
					username.css("border", "1px solid red");
					return;
				}else username.css("border", "1px solid #ccc");

				if(password.val() == ""){
					password.css("border", "1px solid red");
					return;
				}else password.css("border", "1px solid #ccc");


				var data = new FormData();
                data.append('login', 'login');
                data.append('username', username.val());
                data.append('password', password.val());


                $.ajax({
                    url: 'api/login.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
						// console.log(returnData);
						returnData = JSON.parse(returnData);

						if(returnData["error"] == 0){
							location.href="http://localhost:8080/public_figure/mena_mahmoud/profile.php";
						}else{
							$("#login_hint").text("username/password not correct!");
						}
					}

				});

				
			});

			// $(".nav").children("a").removeClass("active");
			// $('*[data-name="home"]').addClass("active");

			
			
		});

	</script>


</body>

</html>