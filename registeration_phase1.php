<?php
    session_start();

    // $pageTitle = "Public Figure";
	require_once("../includes/initialize.php");


	//get database connection
    $dbConnection = getDatabaseConnection();
    
    include "header.php";


    
    
    //check form submittion
    if(isset($_POST["submit"])){
        //store values to next page

        //server side validation TODO---------------

        $_SESSION["first_name"] = $_POST["first_name"];
        $_SESSION["middle_name"] = $_POST["middle_name"];
        $_SESSION["last_name"] = $_POST["last_name"];

        $_SESSION["gander"] = $_POST["gander"];
        $_SESSION["birth_date"] = $_POST["birth_date"];
        $_SESSION["nationality"] = $_POST["nationality"];

        $_SESSION["residence"] = $_POST["residence"];
        $_SESSION["email"] = $_POST["email"];
        $_SESSION["phone"] = $_POST["phone"];

        $_SESSION["category"] = $_POST["category"];

        redirect_to("registeration_phase2.php");
        
    }

?>

<style>
    .error{
        border: 1px solid red;
    }
</style>


<!-- Modal -->
<div id="login" class="modal fade" role="dialog">
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body " style="padding: 3em 1em">
            <div class="row">
                <div class="col-md-6">
                    <img src="img/img-01.png" class="img-responsive">

                </div>
                <div class="col-md-6">
                    <form action="" style="padding: 3em .5em 0;">
                        <div class="form-group">

                            <input type="email" class="form-control btn-rounded input-lg
                                "
                             placeholder="Email address" id="email">
                        </div>
                        <div class="form-group">

                            <input type="password" class="form-control btn-rounded input-lg" id="pwd" placeholder="Password">
                        </div>

                        <button id="" type="submit" class="btn btn-primary inverse btn-lg   m-b-em-2">Submit</button>
                    </form>

                </div>

            </div>


        </div>

    </div>

</div>
</div>

    


<!-- registeration form -->
<div class="row col-md-8 col-md-offset-2" style="margin-bottom: 6em;">
    <!-- fieldsets -->
    <form id="form1" action="<?php echo htmlspecialchars(basename($_SERVER["PHP_SELF"])) ?>" method="post">
    <fieldset>
        <div class="row col-md-8 col-md-offset-3">
            <h2 class="" style="margin: 1em;">Personal Information</h2>
        </div>

        
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="pull-left">First Name</label>
                    <input id="first_name" name="first_name" type="text" class="form-control require_text" placeholder="First Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Middle Name</label>
                    <input  id="middle_name" name="middle_name" type="text" class="form-control" placeholder="Middle Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Last Name </label>
                    <input type="text" id="last_name" name="last_name" class="form-control require_text" placeholder="Last Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left">Gender </label>
                    <select class="form-control require_select" id="gander" name="gander">
                        <option value="">- - Choose Gender - -</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>

                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group" id="">
                    <label class=" control-label pull-left"> Birth Date</label>

                    <div class=" input-group " style="display: block;">
                        <span class="input-group-addon" style=" background-color: transparent !important; border: none !important;"></span>
                        <input style="padding: 6px;" name="birth_date" id="birth_date" placeholder="M/D/Y" type="date" class="form-control require_select" value="">
                    </div>

                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left">Nationality </label>
                    <select id="nationality"   class="form-control require_select" name="nationality">
                        <option value="">---Nationality---</option>
                        <?php
                        // $residenceSet = get_all_from_tbl('countries_tbl');
                        $sqlQuery="SELECT * FROM certigd5_newiscndb20.countries_tbl";
                        $residenceSet = $dbConnection->performQuery($sqlQuery);
                        while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                        <option value="<?php echo $residence['id']; ?>">
                            <?php echo $residence['country_name']; ?></option><?php
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left" >Residence ( Optional ) </label>
                    <select id="residence"  class="form-control" name="residence">
                        <option value="">---Residence---</option><?php
                        // $residenceSet = get_all_from_tbl('countries_tbl');
                        $sqlQuery="SELECT * FROM certigd5_newiscndb20.countries_tbl";
                        $residenceSet = $dbConnection->performQuery($sqlQuery);
                        while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                        <option value="<?php echo $residence['id']; ?>">
                            <?php echo $residence['country_name']; ?></option><?php
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Email</label>
                    <input type="email" id="email_register" name="email" class="form-control require_email" placeholder="Email">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="pull-left"> Phone </label>
                    <input type="text"  id="phone" name="phone" class="form-control require_text" placeholder="Phone">
                </div>
                    
                </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="pull-left">Field</label>
                    <select id="category"  class="form-control require_select" name="category">
                        <option value="">---Field---</option><?php
                        // $residenceSet = get_all_from_tbl('countries_tbl');
                        $sqlQuery="SELECT * FROM public_figure_categories";
                        $categoriesSet = $dbConnection->performQuery($sqlQuery);
                        while ($category = mysqli_fetch_assoc($categoriesSet)) { ?>
                        <option value="<?php echo $category['id']; ?>">
                            <?php echo $category['category_name']; ?></option><?php
                        } ?>
                    </select>
                </div>


            </div>

            


            </div>
            
            <div class="clearfix"></div>
            
            <!-- <button type="button" name="sign_up" class="btn btn-success notika-btn-success waves-effect pull-right next">Sign Up</button> -->
            <!-- <a type="button" name="sign_up" class="btn btn-success notika-btn-success waves-effect pull-right" href="registeration_phase2.php">Next</a> -->
            <input type="submit" name="submit" class="btn btn-success notika-btn-success waves-effect pull-right" value="Next">
        </div>
    </fieldset>

    </form>
</div>
<div class="clearfix"></div>


<?php include "footer.php"; ?>

<script>

    $(document).ready(function(){
        $("#form1").on("submit", function(){
            //client side validation
            var done = true;

            var firstName = $("#first_name");
            var lastName = $("#last_name");

            var gander = $("#gander");
            var birthDate = $("#birth_date");
            var nationality = $("#nationality");

            var email = $("#email_register");
            var phone = $("#phone");

            var field = $("#category");

            if (firstName.val() == "") {
                firstName.addClass("false_input");
                firstName.removeClass("empty_input");
                firstName.removeClass("true_input");
                done = false;
            }

            if (lastName.val() == "") {
                lastName.addClass("false_input");
                lastName.removeClass("empty_input");
                lastName.removeClass("true_input");
                done = false;
            }

            if (gander.val() == "") {
                gander.addClass("false_input");
                gander.removeClass("empty_input");
                gander.removeClass("true_input");
                done = false;
            }

            if (birthDate.val() == "") {
                birthDate.addClass("false_input");
                birthDate.removeClass("empty_input");
                birthDate.removeClass("true_input");
                done = false;
            }

            if (nationality.val() == "") {
                nationality.addClass("false_input");
                nationality.removeClass("empty_input");
                nationality.removeClass("true_input");
                done = false;
            }

            if (email.val() == "") {
                email.addClass("false_input");
                email.removeClass("empty_input");
                email.removeClass("true_input");
                done = false;
            }

            if (phone.val() == "") {
                phone.addClass("false_input");
                phone.removeClass("empty_input");
                phone.removeClass("true_input");
                done = false;
            }

            if (field.val() == "") {
                field.addClass("false_input");
                field.removeClass("empty_input");
                field.removeClass("true_input");
                done = false;
            }

            return done;
            
                        
        });
    });

</script>