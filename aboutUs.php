<?php 

    // $pageTitle = "home";
    require_once("../includes/initialize.php");

    //get database connection
	$dbConnection = getDatabaseConnection();

    $pageTitle = "About Us";
    include "header.php";

?>
        
    <div class="page-header-padding page-header-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              
              <h1 class="page-title white-color">About Us

            </h1>
            </div>
          </div>
        </div>
      </div>
       <section id="details" >
        <div class="container">
          <div class="row">
            
               <div class="col-md-12 m-b-30">
              <div class="pb_form_v1 m-b-em-3 m-t-em-3">
                <h1 class="  wow fadeInDown main-color m-b-20"> About Recruitment system </h1>

                <?php
                
                    $sqlQuery = "SELECT about_us FROM public_figure_content LIMIT 1";
                    $queryResult = $dbConnection->performQuery($sqlQuery);

                    if($queryResult) echo mysqli_fetch_assoc($queryResult)["about_us"];
                
                ?>
              </div>
            </div>
          
            
           </div>
        </div>  
      </section>




      <?php include "footer.php"; ?>

      <script>
      
        $(document).ready(function(){
            $("#modal_submit").on("click", function(e){
				e.preventDefault();

				//ajax check login
				var username = $("#login_username");
				var password = $("#login_password");

				if(username.val() == ""){
					username.css("border", "1px solid red");
					return;
				}else username.css("border", "1px solid #ccc");

				if(password.val() == ""){
					password.css("border", "1px solid red");
					return;
				}else password.css("border", "1px solid #ccc");


				var data = new FormData();
                data.append('login', 'login');
                data.append('username', username.val());
                data.append('password', password.val());


                $.ajax({
                    url: 'api/login.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
						// console.log(returnData);
						returnData = JSON.parse(returnData);

						if(returnData["error"] == 0){
							location.href="http://localhost:8080/public_figure/mena_mahmoud/profile.php";
						}else{
							$("#login_hint").text("username/password not correct!");
						}
					}

				});

				
			});

	
        });
      
      </script>