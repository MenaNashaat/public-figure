<?php 

    ob_start();
    session_start();

//    ini_set("display_errors", "1");
    // $loggedIn = false;
	// if(isset($_SESSION["user"]))
	// 	$loggedIn = true;

	$pageTitle = "edit";
	require_once("../includes/initialize.php");


	//get database connection
    $dbConnection = getDatabaseConnection();
    
    if(!isset($_SESSION["user"])){
        redirect_to("index.php");
    }

    $username = $dbConnection->prepareQueryValue($_SESSION["user"]);
    $sqlQuery="SELECT * FROM public_figure_user WHERE username='{$username}' LIMIT 1";

    $user = $dbConnection->performQuery($sqlQuery);
    if($dbConnection->numRows($user) < 1){
        redirect_to("index.php");
    }

    $user = mysqli_fetch_assoc($user);
    $categoryPath = explode("-", $user["category_path"]);
    array_pop($categoryPath);

//     print_r($user);

	include "header.php";

	

?>




    <div class="page-header-padding page-header-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h1 class="page-title white-color"> <?php echo $user["first_name"] . " " . $user["last_name"]; ?>

                    </h1>
                </div>
            </div>
        </div>
    </div>
<section id="profile" >

    <form id="edit" method="POST" action="<?php htmlspecialchars(basename($_SERVER["PHP_SELF"])); ?>" enctype= multipart/form-data>

        <input id="user_id" type="hidden" name="user_id" value="<?php echo $user["id"]; ?>">
        <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                                <h2>Edit Image</h2>
                                <hr>
                                <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file' name="file1" id="file1" accept=".png, .jpg, .jpeg" />
                                        <label for="file1"></label>
                                    </div>
                                    <div class="avatar-preview">
                                        <!-- <div id="imagePreview" style="background-image: url(img/download.jpg);"> -->
                                        <img alt="" id="imagePreview" src="<?php echo "uploads/" . $user["photo_path"] ?>" class="avatar avatar-wordpress-social-login avatar-160 photo"
                                            height="100%" width="100%" style="border-radius:50%">
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <!-- </div> -->
                    <div class="col-md-8">
                            <div class="card">
                        <h2>Edit Info</h2>
                        <hr>
                            <div class="row">

                            
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="pull-left">First Name</label>
                                        <input id="first_name" name="first_name" type="text" class="form-control" value="<?php echo $user["first_name"]; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4 ">
                                    <div class="form-group">
                                        <label class="pull-left">Middle Name</label>
                                        <input  id="middle_name" name="middle_name" type="text" class="form-control" value="<?php echo $user["middle_name"]; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4 ">
                                    <div class="form-group">
                                        <label class="pull-left">Last Name </label>
                                        <input type="text" id="last_name" name="last_name" class="form-control" value="<?php echo $user["last_name"]; ?>">
                                    </div>
                                </div>

                            </div>
                            <div class="row" id="categories">

                            <div class="col-md-4" >
        
                                <select class="cat category form-control" id="category1" data-level="1">
                                    <option value="-1">---Category---</option><?php

                                        $level = 1;
                                        $catId = count($categoryPath) > 0 ? $categoryPath[0] : -1;
                                        $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id IS NULL";
                                        $categories = $dbConnection->performQuery($sqlQuery);
                                        while ($category = mysqli_fetch_assoc($categories)) { 
                                        // if($category["level"] == 1):
                                        ?>

                                        <?php 
                                        if($category['id'] == $catId){ ?>
                                            <option value="<?php echo $category['id']; ?>" selected> <?php echo $category['name']; ?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $category['id']; ?>"> <?php echo $category['name']; ?></option>
                                        <?php } ?>

                                    <?php } ?>
                                </select>
                                
                                    <!-- <button id="search" class="btn btn-primary" type="submit" style="height: 50px;">
                                        Search
                                    </button> -->
                            </div>
                                <?php for($i = 1; $i < count($categoryPath); $i++){
                                        $parentCategory = $categoryPath[$i - 1]; 
                                        ?>
                                            <div class="col-md-4" style="margin-bottom: 1em">
                                            <select id="<?php echo "category" . ($i + 1);  ?>" class="form-control category" data-level="<?php echo ($i + 1);  ?>">
                                            <option value="-1"> ---Sub Category--- </option>
                                        <?php
                                            $level = $i + 1;
                                            $catId = $categoryPath[$i];
                                            $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id='{$parentCategory}'";
                                            $categories = $dbConnection->performQuery($sqlQuery);
                                            while ($category = mysqli_fetch_assoc($categories)) {   
                                                
                                        ?>
                                        <?php 
                                            
                                                if($category['id'] == $catId){ ?>
                                                    <option value="<?php echo $category['id']; ?>" selected> <?php echo $category['name']; ?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $category['id']; ?>"> <?php echo $category['name']; ?></option>
                                                <?php } ?>
                                            

                                            <?php } ?>
                                            </select>
                                            </div>
                                                
                                    <?php } ?>

                                    <?php 
                                    if(count($categoryPath) > 0){
                                        //if leave has childs
                                        $level = count($categoryPath) + 1;
                                        $parentCategory = $categoryPath[$level - 2];
                                        $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id='{$parentCategory}'";
                                        $categories = $dbConnection->performQuery($sqlQuery);
                                        if($dbConnection->numRows($categories) > 0){ ?>

                                            <div class="col-md-4" style="margin-bottom: 1em">
                                            <select id="<?php echo "category" . $level;  ?>" class="form-control category" data-level="<?php echo $level;  ?>">
                                            <option value="-1"> ---Sub Category--- </option>
                                        <?php
                                            while ($category = mysqli_fetch_assoc($categories)) {
                                                ?>

                                                    <option value="<?php echo $category['id']; ?>"> <?php echo $category['name']; ?></option>

                                                <?php

                                            } ?>
                                            </select>
                                        </div>
                                        <?php
                                        
                                        }

                                    }
                                        
                                    ?>

                                    <input id="category_path" type="hidden" name="category_path" value="">
                                </div>
                                <div class="row">

                                    <div class="col-sm-4 ">
                                        <div class="form-group" id="">
                                            <label class=" control-label pull-left"> Birth Date</label>

                                            <div class=" input-group " style="display: block;">
                                                <span class="input-group-addon" style=" background-color: transparent !important; border: none !important;"></span>
                                                <input style="padding: 6px;" name="birth_date" id="birth_date" placeholder="M/D/Y" type="date" class="form-control" value="<?php echo $user["birth_date"]; ?>">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="sel1" class="pull-left" >Residence ( Optional ) </label>
                                            <select id="residence"  class="form-control" name="residence">
                                                <option value="-1">---Residence---</option><?php
                                                // $residenceSet = get_all_from_tbl('countries_tbl');
                                                $sqlQuery="SELECT * FROM countries";
                                                $residenceSet = $dbConnection->performQuery($sqlQuery);
                                                while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                                                <?php 
                                                    if($residence['id'] == $user['residence']){ ?>
                                                        <option value="<?php echo $residence['id']; ?>" selected> <?php echo $residence['country_name']; ?></option>
                                                    <?php }else{ ?>
                                                        <option value="<?php echo $residence['id']; ?>"> <?php echo $residence['country_name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 ">
                                        <div class="form-group">
                                            <label for="sel1" class="pull-left">Nationality </label>
                                            <select id="nationality"   class="form-control" name="nationality">
                                                <option value="-1">---Nationality---</option>
                                                <?php
                                                // $residenceSet = get_all_from_tbl('countries_tbl');
                                                $sqlQuery="SELECT * FROM countries";
                                                $residenceSet = $dbConnection->performQuery($sqlQuery);
                                                while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                                                <?php 
                                                    if($residence['id'] == $user['residence']){ ?>
                                                        <option value="<?php echo $residence['id']; ?>" selected> <?php echo $residence['country_name']; ?></option>
                                                    <?php }else{ ?>
                                                        <option value="<?php echo $residence['id']; ?>"> <?php echo $residence['country_name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                    <div class="row">
                                    <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label class="pull-left">Email</label>
                                                <input type="email" id="email_register" name="email" class="form-control" value="<?php echo $user["email"]; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="pull-left"> Phone </label>
                                                <input type="text"  id="phone" name="phone" class="form-control" value="<?php echo $user["phone"]; ?>">
                                            </div>
                                                
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="pull-left">Facebook</label>
                                                <input id="facebook" name="facebook" type="text" class="form-control" value="<?php echo $user["facebook"]; ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="pull-left">Twitter</label>
                                                <input id="twitter" name="twitter" type="text" class="form-control" value="<?php echo $user["twitter"]; ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="pull-left">LinkedIn</label>
                                                <input id="linkedin" name="linkedin" type="text" class="form-control" value="<?php echo $user["linkedin"]; ?>">
                                            </div>
                                        </div>

                                    </div>

                                        <div class=" form-group mail-text h-200">
                                                <label for="">Description</label>
                                                <textarea id="description" class="summernote" name="description">
                                                    <?php

                                                        $userId = $user["id"];
                                                        $catId = $categoryPath[0];
                                                        $fieldId = 7;
                                                        $sqlQuery="SELECT * FROM public_figure_filed_values WHERE category_id='{$catId}' AND field_id='{$fieldId}' AND user_id='{$userId}'";
                                                        $value = $dbConnection->performQuery($sqlQuery);
                                                        
                                                        if($value = mysqli_fetch_assoc($value)) echo htmlentities($value["value"]);
                                                        
                                                    ?>
                                                </textarea>
                                            </div>
                            
                            <div class="row" style="">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                                <div class="spinner pull-right"  style="">
                                    <div class="double-bounce1"></div>
                                    <div class="double-bounce2"></div>
                                </div>
                            
                            </div>
                            </div>
                            </div>

                        <!-- </div> -->


                </div>
            </div>
        </form>
    </section>

    <?php include "footer.php"; ?>
     <script>
         $(document).ready(function(){
 
             $('.summernote').summernote();
 
         });
 
     </script>
    <script>
        function readURL(input) {
    if (input.files && input.files[0]) {    
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').attr("src", e.target.result);
            // $('#imagePreview').hide();
            // $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#file1").change(function() {
    readURL(this);
});

var categoryPath = [];
$(document).ready(function(){

    categoryPath = [];
    categoryPath = ('<?php echo $user["category_path"]; ?>').split("-");
    categoryPath.pop();
    $("#categories").on("change", ".category", function(e){
                
        // console.log(categoryDone);
        // console.log(e.target);
        var selection = $(e.target);
        var categoryId = parseInt(selection.children("option:selected").val());
        // console.log(selection.data("level"));

        selection.css("border", "1px solid rgba(34,36,38,.15)");
        
        if(categoryId != -1){

        //get next level categories
        oldLevel = categoryLevel;
        categoryLevel = selection.data("level");
        var nextLevel = categoryLevel + 1;

        if(categoryLevel > oldLevel) /*categoryPath += categoryId + "-"*/ categoryPath.push(categoryId);
        else {
            while(categoryPath.length >= categoryLevel)
            categoryPath.pop();

            categoryPath.push(categoryId);
        }
        // else if(categoryLevel == oldLevel) categoryPath = categoryPath.slice(0, categoryPath.lastIndexOf("-", categoryPath.length - 2) + 1) + categoryId + "-"


        $.get( "api/next_level_categories.php?category_id=" + categoryId + "&category_level=" + categoryLevel, function( data ) {
            data = JSON.parse(data);
            if(data["error"] == 0){
            var categories = data["categories"];

            // console.log(categories);

            selection.parent().nextAll().remove();

            if(categories.length > 0){
                var container = $('<div class="col-md-4" style="margin-bottom: 1em"></div>');
                var nextSelection = $('<select id="category' + nextLevel + '"  class="form-control category" name="category' + nextLevel + '" data-level="' + nextLevel + '"></select>');
            
                nextSelection.append($('<option value="' + -1 + '"> ' + '---Sub Category---' + ' </option>'));
                //add options
                for(var i = 0; i < categories.length; i++)
                    nextSelection.append($('<option value="' + categories[i]["id"] + '"> ' + categories[i]["name"] + ' </option>'));

                container.append(nextSelection);
                selection.parent().parent().append(container);

                categoryDone = false;
            }else categoryDone = true;
            }

        });
        

        }else {
        selection.parent().nextAll().remove();
        selection.css("border", "1px solid red");
        categoryDone = false;
        oldLevel = categoryLevel;
        categoryLevel = selection.data("level");
        while(categoryPath.length >= categoryLevel)
            categoryPath.pop();
        }

        // console.log(oldLevel + "==" + categoryLevel);
        // console.log(categoryPath);
    });

    // $("#edit").on("submit", function(){
        
    //     var categoryPathString = "";
    //     for(var i = 0; i < categoryPath.length; i++)
    //         categoryPathString += categoryPath[i] + "-";

    //     $("#category_path").val(categoryPathString);
    //     return true;

    // });

    $("#submit").on("click", function(e){
        e.preventDefault();

        $(".spinner").show();
        $(this).attr("disabled", true);

        var firstName = $("#first_name").val();
    	var middleName = $("#middle_name").val();
		var lastName = $("#last_name").val();

		var birthDate = $("#birth_date").val();
		var nationality = $("#nationality").val();

		var residence = $("#residence").val();
		var email = $("#email_register").val();
		var phone = $("#phone").val();

        var facebook = $("#facebook").val();
        var twitter = $("#twitter").val();
        var linkedin = $("#linkedin").val();

        var description = $("#description").val();
        var userId = $("#user_id").val();

		var data = new FormData();
		data.append('edit', 'edit');

		data.append('first_name', firstName);
		data.append('middle_name', middleName);
		data.append('last_name', lastName);

		data.append('birth_date', birthDate);
		data.append('nationality', nationality);

		data.append('residence', residence);
		data.append('email', email);
		data.append('phone', phone);

        data.append('facebook', facebook);
        data.append('twitter', twitter);
        data.append('linkedin', linkedin);

		var categoryPathString = "";
		for(var i = 0; i < categoryPath.length; i++)
		    categoryPathString += categoryPath[i] + "-";

		data.append('category_level', categoryPath.length);
		data.append('category_id', categoryPath[categoryPath.length - 1]);
		data.append('category_path', categoryPathString);

        data.append('description', description);
        data.append('user_id', userId);
        
        
        var file1=$("#file1");
        if(file1.val() == "") data.append('same_photo', "true");
        else data.append('same_photo', "false");

		$.each(file1[0].files, function(i, file) {
			data.append('personal_photo', file);
		});
		
		// data.append('category_fields', JSON.stringify(categoryFieldsValues));


		$.ajax({
			url: 'api/edit_user.php',
			data: data,
			dataType: "text",
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			success: function(returnData){

				// console.log(returnData);
				var returnData = JSON.parse(returnData);

				if(returnData["error"] == 0){
                    // alert("your info has been updated!");
                    
                    Swal.fire(
                        'Edited Successfully!',
                        'Your Information has been edited successfully..',
                        'success'
                    ).then(() => {location.href = "http://localhost:8080/public_figure/mena_mahmoud/edit.php";});

					
				}
			}

		});
    });

});

</script>


</body>

</html>