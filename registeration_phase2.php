<?php
    session_start();
    $pageTitle = "Public Figure";
    // require_once("public_includes/public_initialize.php");

    // //get database object
    // $dbConnection = getDatabaseConnection();

    // if(isset($_GET["application"])){
    //     // $fields = array();

    //     $app_id = $_GET["application"];

    //     //get all fields for that person using app_id => category => fields
    //     $query = "SELECT category_id FROM public_figure_user WHERE app_id='$app_id' LIMIT 1";
    //     $result = $dbConnection->performQuery($query);
    //     if($result){
    //         $cat_id = mysqli_fetch_assoc($result)["category_id"];
    //         //get all fields for that category
    //         $query = "SELECT * FROM public_figure_category_fields WHERE category_id='$cat_id'";
    //         $result = $dbConnection->performQuery($query);
    //         if($result){
    //             //fetch fields and construct html
                
    //         }else die("database error2");

    //     }else diE("database error1");

    // }else redirect_to("myindex.php");

    // $pageTitle = "Public Figure";
	require_once("../includes/initialize.php");

    // print_r($_SESSION);
    // echo $_SESSION["middle_name"] == "" ? "yes" : "no";

	//get database connection
    $dbConnection = getDatabaseConnection();

    $category = -1;
    $categoryFieldsJson = array();
    if(isset($_SESSION["category"])){
        $category = $_SESSION["category"];

        //get category fields
        $sqlQuery = "SELECT * FROM public_figure_category_fields WHERE category_id='{$category}'";
        $categoryFields = $dbConnection->performQuery($sqlQuery);

    } else redirect_to("registeration_phase1.php");

    include "header.php";


?>


<!-- Modal -->
<div id="login" class="modal fade" role="dialog">
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body " style="padding: 3em 1em">
            <div class="row">
                <div class="col-md-6">
                    <img src="img/img-01.png" class="img-responsive">

                </div>
                <div class="col-md-6">
                    <form action="" style="padding: 3em .5em 0;">
                        <div class="form-group">

                            <input type="email" class="form-control btn-rounded input-lg
                                "
                             placeholder="Email address" id="email">
                        </div>
                        <div class="form-group">

                            <input type="password" class="form-control btn-rounded input-lg" id="pwd" placeholder="Password">
                        </div>

                        <button id="" type="submit" class="btn btn-primary inverse btn-lg   m-b-em-2">Submit</button>
                    </form>

                </div>

            </div>


        </div>

    </div>

</div>
</div>



    <!-- registeration form -->
    <div class="row col-md-8 col-md-offset-2" style="margin-bottom: 6em;">
        <!-- fieldsets -->
        <fieldset>
            <!-- <div class="row col-md-8 col-md-offset-3">
                <h2 class="" style="margin: 1em;">Personal Information</h2>
            </div> -->

            <!-- append session data -->

            <input id="first_name" type="hidden" value="<?php echo $_SESSION["first_name"] ?>" />
            <input id="middle_name" type="hidden" value="<?php echo $_SESSION["middle_name"] ?>" />
            <input id="last_name" type="hidden" value="<?php echo $_SESSION["last_name"] ?>" />

            <input id="gander" type="hidden" value="<?php echo $_SESSION["gander"] ?>" />
            <input id="birth_date" type="hidden" value="<?php echo $_SESSION["birth_date"] ?>" />
            <input id="nationality" type="hidden" value="<?php echo $_SESSION["nationality"] ?>" />

            <input id="residence" type="hidden" value="<?php echo $_SESSION["residence"] ?>" />
            <input id="email_register" type="hidden" value="<?php echo $_SESSION["email"] ?>" />
            <input id="phone" type="hidden" value="<?php echo $_SESSION["phone"] ?>" />

            <input id="category" type="hidden" value="<?php echo $_SESSION["category"] ?>" />


            <!-- append session data -->

            
            <div class="row" style="margin-top: 7em;">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="pull-left">username</label>
                        <input id="user_name" type="text" class="form-control require_text" placeholder="username">
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="pull-left">passwrod</label>
                        <input  id="password" type="password" class="form-control require_text" placeholder="passwrod">
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="pull-left">confirm password</label>
                        <input type="password" id="confirm_password"  class="form-control require_text" placeholder="confirm password">
                    </div>
                </div>

                <!-- dynamiclly fields creation based on category -->
                <?php if($category != -1): ?>
                    <?php while($field = mysqli_fetch_assoc($categoryFields)): ?>
                        
                        <div class="col-sm-4 ">
                            <div class="form-group">
                                <label class="pull-left"><?php echo $field["name"]; ?></label>
                                <input type="<?php echo $field["type"]; ?>" id="<?php echo $field["name"]; ?>"  class="form-control require_text" placeholder="<?php echo $field["name"]; ?>">
                            </div>
                        </div>
                    <?php 

                    //add field to array
                    $categoryFieldsJson[] = $field;
                    
                    endwhile;

                    //for javascript handling
                    $categoryFieldsJson = json_encode($categoryFieldsJson);

                    endif; ?>
                <!-- dynamiclly fields creation based on category -->
                
                <div class="clearfix"></div>
                
                <!-- <button type="button" name="sign_up" class="btn btn-success notika-btn-success waves-effect pull-right next">Sign Up</button> -->
                <button id="sign_up" type="button" name="sign_up" class="btn btn-success notika-btn-success waves-effect pull-right">finish</button>
            </div>
        </fieldset>

    </div>
    <div class="clearfix"></div>

    <?php include "footer.php"; ?>

    <?php 

        //clear session registeration data
        if($_SESSION["first_name"]){
            $_SESSION["first_name"] = null;
            unset($_SESSION["first_name"]);
        }
        if($_SESSION["middle_name"]){
            $_SESSION["middle_name"] = null;
            unset($_SESSION["middle_name"]);
        }
        if($_SESSION["last_name"]){
            $_SESSION["last_name"] = null;
            unset($_SESSION["last_name"]);
        }
        if($_SESSION["gander"]){
            $_SESSION["gander"] = null;
            unset($_SESSION["gander"]);
        }
        if($_SESSION["birth_date"]){
            $_SESSION["birth_date"] = null;
            unset($_SESSION["birth_date"]);
        }
        if($_SESSION["nationality"]){
            $_SESSION["nationality"] = null;
            unset($_SESSION["nationality"]);
        }
        if($_SESSION["residence"]){
            $_SESSION["residence"] = null;
            unset($_SESSION["residence"]);
        }
        if($_SESSION["email"]){
            $_SESSION["email"] = null;
            unset($_SESSION["email"]);
        }
        if($_SESSION["phone"]){
            $_SESSION["phone"] = null;
            unset($_SESSION["phone"]);
        }
        if($_SESSION["category"]){
            $_SESSION["category"] = null;
            unset($_SESSION["category"]);
        }
            
    ?>

    <script>
    
        $(document).ready(function(){
            $("#sign_up").on("click", function(){
                var done = true;

                var categoryFields = JSON.parse('<?php echo $categoryFieldsJson; ?>');
                // console.log(categoryFields);

                var firstName = $("#first_name").val();
                var middleName = $("#middle_name").val();
                var lastName = $("#last_name").val();

                var gander = $("#gander").val();
                var birthDate = $("#birth_date").val();
                var nationality = $("#nationality").val();

                var residence = $("#residence").val();
                var email = $("#email_register").val();
                var phone = $("#phone").val();

                var field = $("#category").val();

                var username = $("#user_name");
                var password = $("#password");
                var confirmPassword = $("#confirm_password");

                if(username.val() == ""){
                    username.addClass("false_input");
                    username.removeClass("empty_input");
                    username.removeClass("true_input");
                    done = false;
                }
                if(password.val() == ""){
                    password.addClass("false_input");
                    password.removeClass("empty_input");
                    password.removeClass("true_input");
                    done = false;
                }
                if(confirmPassword.val() == "" || confirmPassword.val() !== password.val()){
                    confirmPassword.addClass("false_input");
                    confirmPassword.removeClass("empty_input");
                    confirmPassword.removeClass("true_input");
                    done = false;
                }

                var categoryFieldsValues = [];
                for(var i = 0; i < categoryFields.length; i++){
                    var categoryField = $("#" + categoryFields[i]["name"]);
                    if(categoryField.val() == ""){
                        categoryField.addClass("false_input");
                        categoryField.removeClass("empty_input");
                        categoryField.removeClass("true_input");
                        done = false;
                    }

                    var temp = {};
                    temp[categoryFields[i]["name"]] = categoryField.val();

                    categoryFieldsValues.push(temp);

                }

                
                if(!done) return;


                var data = new FormData();
                data.append('register', 'register');

                data.append('first_name', firstName);
                data.append('middle_name', middleName);
                data.append('last_name', lastName);

                data.append('gander', gander);
                data.append('birth_date', birthDate);
                data.append('nationality', nationality);

                data.append('residence', residence);
                data.append('email', email);
                data.append('phone', phone);

                data.append('category', field);

                data.append('user_name', username.val());
                data.append('password', password.val());
                data.append('confirm_password', confirmPassword.val());
                
                data.append('category_fields', JSON.stringify(categoryFieldsValues));


                $.ajax({
                    url: 'api/register_user.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){

                        // console.log(data);
                        var data = JSON.parse(data);

                        if(data["error"] == 0){
                            alert("user registered");

                            location.href = "http://localhost:8080/public_figure/public_figure/profile.php?username=" + username.val();
                        }
                        
                        

                    }




                });


            
            });
        });


    </script>