<footer id="footer">
		<div class="container">
			<div class="row m-t-10 m-b-10 text-center">

				<div class="col-md-2 m-t-10">
					<ul class="list-inline m-t-10 social">
						<li class="active list-inline-item"><a class="page-scroll" href="https://verifiedphd.org/index.html" target="_blank">
								<i class="fab fa-youtube"></i> </a></li>
						<li class="active list-inline-item"><a class="page-scroll" href="https://verifiedphd.org/index.html" target="_blank">
								<i class="fab fa-facebook-square"></i> </a></li>
						<li class="active list-inline-item"><a class="page-scroll" href="https://verifiedphd.org/index.html" target="_blank">
								<i class="fab fa-twitter-square"></i> </a></li>
						<li class="active list-inline-item"><a class="page-scroll" href="https://verifiedphd.org/index.html" target="_blank">
								<i class="fab fa-linkedin"></i> </a></li>

					</ul>

				</div>
				<div class="col-md-6 m-t-10">
					<ul class="list-inline m-t-10">
						<li class="active list-inline-item"><a class="page-scroll" href="index.html">Home</a></li>


						<li class="list-inline-item"><a class="page-scroll" href="register/get_phd_id.html">Get ID</a></li>


						<li class="list-inline-item"><a class="page-scroll" href="faq.html">FAQ</a></li>
						<li class="list-inline-item"><a class="page-scroll" href="phd_data_policy.html">Data Policy </a></li>
						<li class="list-inline-item"><a class="page-scroll" href="phd_data_quality_policy.html">Data
								Quality Policy</a></li>
						<li class="list-inline-item"><a class="page-scroll" href="phd_privacy_policy.html">Privacy
								Policy</a></li>
						<li class="list-inline-item"><a class="page-scroll" href="phd_terms_conditions.html">Terms
								&amp; Condition</a></li>
						<li class="list-inline-item"><a class="page-scroll" href="phd_terms_of_use.html">Terms Of Use</a></li>
						<li class="list-inline-item"><a class="page-scroll" href="phd_use_of_cookies.html">Use Of
								Cookies</a></li>
						<li class="list-inline-item"><a class="page-scroll" href="phd_fees.html">Fees</a></li>
						<li class="list-inline-item"><a class="page-scroll" href="traning.html"> Training video &amp;
								Brochure</a></li>



					</ul>
					<p>©2018 All Rights Reserved.Powered by ISE.</p>
				</div>

				<div class="col-lg-2">
					<h5 class="black-color">Powered By :</h5>

					<img src="img/logo.png" class="m-b-em-1">
				</div>
				<div class="col-md-2">
					<img src="img/qrcode-IS57.png">
				</div>
			</div>
		</div>
	</footer>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.min.js"></script>

	<script src="js/wow.min.js"></script>
	<script src="js/anchorscrol.js"></script>

	<script type="text/javascript" src="js/jquery.owl.carousel.js"></script>
	<script type="text/javascript" src="js/select2/select2.full.min.js"></script>


	<!-- Main js -->
	<script src="js/main.js"></script>
	<script src="js/index.js"></script>
	<script src="js/form.js"></script>
	<script src="js/summernote/summernote.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	
	<script type="text/javascript">
		$(document).ready(function () {


			$("#owl-demo").owlCarousel({

				autoPlay: 3000, //Set AutoPlay to 3 seconds
				dots: true,
				items: 3,
				itemsDesktop: [1199, 3],
				itemsDesktopSmall: [979, 4]
			});
		});
	</script>
</body>

</html>
