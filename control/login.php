<?php  

    // ob_start();
    // session_start();

	// $pageTitle = "register";
    require_once("../../includes/initialize.php");
    
    //get database connection
	$dbConnection = getDatabaseConnection();


?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Public Figure Control | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">P.V</h1>

            </div>
            <h3>Public Figure</h3>
            <form class="m-t" role="form" action="api/functions.php" method="POST">
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Username" required="required">
                    <?php if (isset($_SESSION["EVENT_CONTROL_SESSION_CODE"])) {
                        if ($_SESSION["EVENT_CONTROL_SESSION_CODE"] == "1") { ?><span style="color: red"> &nbsp; &nbsp;   <i
                                    class="fas fa-times"></i> <small>Wrong username</small> </span>
                             <?php unset($_SESSION["EVENT_CONTROL_SESSION_CODE"]);
                        }
                    } ?>
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                    <?php if (isset($_SESSION["EVENT_CONTROL_SESSION_CODE"])) {
                        if ($_SESSION["EVENT_CONTROL_SESSION_CODE"] == "2") { ?><span style="color: red"> &nbsp; &nbsp;   <i
                                    class="fas fa-times"></i> <small>Wrong Password</small> </span>
                            <?php unset($_SESSION["EVENT_CONTROL_SESSION_CODE"]);
                        }
                    } ?>
                </div>
                <button type="submit" name="control_login" class="btn btn-primary block full-width m-b">Login</button>
            </form>

        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
