<?php 

	// $pageTitle = "Public Figure";
	require_once("../../includes/initialize.php");


    $pageTitle = "admin";

	//get database connection
    $dbConnection = getDatabaseConnection();

    $page_title = "Public Figure | Categories";
    require_once("include/header.php");
    require_once("include/navigation.php");

    //get all categories
    class Category{
        public $name;
        public $id;

        public $parentId;


        public function __construct($name = "", $id = -1, $parentId = -1){
            $this->name = $name;
            $this->id = $id;

            $this->parentId = $parentId;
        }
    }

    $categoryTree = array();
    // $categoryTree["root"] = array();


    $i = 6;
    $n = 0;
    $sqlQuery = "SELECT * FROM categories_chain WHERE category_level='{$i}'";
    $sqlRun = $dbConnection->performQuery($sqlQuery);

    $categoryTree[$n] = array();
    while($category = mysqli_fetch_assoc($sqlRun)){
        $cat = new Category($category["name"], $category["id"], $category["prev_id"] ? $category["prev_id"] : 0);

        $categoryTree[$n][] = $cat;
    }
    
    $i--;
    $n++;


    for(; $i > 0; $i--){

        $sqlQuery = "SELECT * FROM categories_chain WHERE category_level='{$i}'";
        $sqlRun = $dbConnection->performQuery($sqlQuery);
        

        $categoryTree[$n] = array();
        while($category = mysqli_fetch_assoc($sqlRun)){
            $cat = new Category($category["name"], $category["id"], $category["prev_id"] ? $category["prev_id"] : 0);

            $categoryTree[$n][] = $cat;
        }
        $n++;


    }

    $categoryTree[$n][] = new Category("root", 0, null);

    $categoryTreeJson = json_encode($categoryTree);

?>

<style type="text/css">
        #mynetwork {
            width: 100%;
            height: 700px;
            border: 1px solid lightgray;
        }
    </style>

<div id="mynetwork"></div>


    

    </div><?php /*require_once("include/footer.php");*/ ?><!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/select2/select2.full.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script><!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.js"></script>
    
    
    


<script>

        $(document).ready(function(){

            var categoryTree = JSON.parse('<?php echo $categoryTreeJson ?>');
            console.log(categoryTree);

            // create an array with nodes
            var nodesConstruct = [];
            for(var i = 0; i < categoryTree.length; i++){

                for(var j = 0; j < categoryTree[i].length; j++){
                    var node = {};
                    node["id"] = categoryTree[i][j]["id"];
                    node["label"] = categoryTree[i][j]["name"];
                    nodesConstruct.push(node);
                }
            }

            var nodes = new vis.DataSet( nodesConstruct
                // [
                // {id: 1, label: 'Node 1'},
                // {id: 2, label: 'Node 2'},
                // {id: 3, label: 'Node 3'},
                // {id: 4, label: 'Node 4'},
                // {id: 5, label: 'Node 5'}
                // ]
            );

            // create an array with edges
            var edgesConstruct = [];
            for(var i = 0; i < categoryTree.length - 1; i++){

                for(var j = 0; j < categoryTree[i].length; j++){
                    var edge = {};
                    edge["from"] = categoryTree[i][j]["id"];
                    edge["to"] = categoryTree[i][j]["parentId"];
                    edge["arrows"] = "from";
                    edgesConstruct.push(edge);
                }
            }

            console.log(edgesConstruct);
            var edges = new vis.DataSet( edgesConstruct
                // [
                // {from: 1, to: 3},
                // {from: 1, to: 2},
                // {from: 2, to: 4},
                // {from: 2, to: 5}
                // ]
            );

            // create a network
            var container = document.getElementById('mynetwork');

            // provide the data in the vis format
            var data = {
                nodes: nodes,
                edges: edges
            };
            // var options = {"hierarchical": {"hierarchical.enabled": true, "hierarchical.levelSeparation": 100, "hierarchical.direction": "UD"}};

            var options = {
                layout: {
                    randomSeed: undefined,
                    improvedLayout:true,
                    hierarchical: {
                    enabled:true,
                    levelSeparation: 150,
                    nodeSpacing: 100,
                    treeSpacing: 200,
                    blockShifting: true,
                    edgeMinimization: true,
                    parentCentralization: true,
                    direction: 'DU',        // UD, DU, LR, RL
                    sortMethod: 'directed'   // hubsize, directed
                    }
                    // nodes:{
                    //     color: '#ff0000',
                    //     fixed: false,
                    //     font: '12px arial red',
                    //     scaling: {
                    //     label: true
                    //     },
                    //     shadow: true
                    // }
                }
            }

                // network.setOptions(options);

            // initialize your network!
            var network = new vis.Network(container, data, options);
        });
		// $(".select2_demo_1").select2();
		// $(".select2_demo_2").select2();
		// $(".select2_demo_3").select2({
		// 	placeholder: "Select a state",
		// 	allowClear: true,
			
        // });

        // var i = 1;
		// $(".select2_demo_3").select2({
		// 	placeholder: "Search Category",
		// 	allowClear: true,
		// 	width: 'resolve',
		// 	closeOnSelect: false,
		// 	height: 20,
		// 	ajax: {
        //         url: function (params) {
        //             // if(params.term == "") return;
        //             return 'api/parent_category.php?category=' + params.term;
        //         },
		// 		// url: 'api/parent_category.php?category=' + params.term,
		// 		// 	data: function (params) {
		// 		// 		var query = {};
		// 		// 		if(params.term != "")
		// 		// 			query = {
		// 		// 				search: params.term
		// 		// 			};

		// 		// 		// Query parameters will be ?search=[term]&type=public
		// 		// 		return query;
		// 		// },

		// 		processResults: function (data) {
		// 			// Tranforms the top-level key of the response object from 'items' to 'results'
		// 			// console.log(data);
		// 			var data = JSON.parse(data);  //array of result set
					
        //             // console.log(data);
        //             if(data["error"] == 0){
        //                 var categories = data["categories"];
        //                 var searchResult = [];
        //                 for(i = 1; i < categories.length; i++){
		// 					var item = {};
		// 					item.id = categories[i]["id"];
		// 					item.text = categories[i]["name"];
        //                     item.level = categories[i]["category_level"];

		// 					searchResult.push(item);
		// 				}

        //                 return {
		// 						results: searchResult
		// 					};

						
        //             }

        //             return {results: []};
        //         }
        //     }
        // });

        // $(document).ready(function(){

        //     $("#add").on("click", function(e){
        //         e.preventDefault()

        //         var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
        //         // console.log(selectedOption); return;
        //         // var category = selectedOption.text;
        //         var categoryPrevId = selectedOption.id;
        //         var categoryLevel = parseInt(selectedOption.level) + 1;

        //         if(selectedOption.id == ""){
        //             $(".select2").css("border", "1px solid red");
        //             return;
        //         }else $(".select2_demo_3").css("border", "none");
        //         // console.log(selectedOption.id);
        //         // return;

        //         // bool done = true;
        //         var categoryName = $("#add_name");
        //         if(categoryName.val() == ""){
        //             categoryName.css("border", "1px solid red");
        //             // done = false;
        //             return;
        //         } 

        //         var data = new FormData();
        //         data.append('add_category', 'add_category');
        //         data.append('prev_id', categoryPrevId);
        //         data.append('category_level', categoryLevel);
        //         data.append('category_name', categoryName.val());


        //         $.ajax({
        //             url: 'api/parent_category.php',
        //             data: data,
        //             dataType: "text",
        //             cache: false,
        //             contentType: false,
        //             processData: false,
        //             type: 'POST',
        //             success: function(returnData){
        //                 // console.log(returnData);
        //                 var returnData = JSON.parse(returnData);
        //                 if(returnData["error"] == 0){
        //                     // alert("category has been added!");
        //                     Swal.fire(
        //                     'added Successfully!',
        //                     'category has been added successfully..',
        //                     'success'
        //                     ).then(() => {$("#add_name").val('');});
        //                 }

        //             }
        //         });
        //     });

        //     $("#update").on("click", function(e){
        //         e.preventDefault()

        //         var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
        //         // console.log(selectedOption); return;
        //         // var category = selectedOption.text;
        //         var categoryId = selectedOption.id;
        //         var categoryLevel = parseInt(selectedOption.level);

        //         if(selectedOption.id == ""){
        //             $(".select2").css("border", "1px solid red");
        //             return;
        //         }else $(".select2_demo_3").css("border", "none");
        //         // console.log(selectedOption.id);
        //         // return;

        //         // bool done = true;
        //         var categoryName = $("#update_name");
        //         if(categoryName.val() == ""){
        //             categoryName.css("border", "1px solid red");
        //             // done = false;
        //             return;
        //         } 

        //         var data = new FormData();
        //         data.append('update_category', 'update_category');
        //         data.append('category_id', categoryId);
        //         data.append('category_level', categoryLevel);
        //         data.append('old_name', selectedOption.text);
        //         data.append('new_name', categoryName.val());


        //         $.ajax({
        //             url: 'api/parent_category.php',
        //             data: data,
        //             dataType: "text",
        //             cache: false,
        //             contentType: false,
        //             processData: false,
        //             type: 'POST',
        //             success: function(returnData){
        //                 // console.log(returnData);
        //                 var returnData = JSON.parse(returnData);
        //                 if(returnData["error"] == 0){
        //                     // alert("category has been updated!");
        //                     Swal.fire(
        //                     'updated Successfully!',
        //                     'category has been updated successfully..',
        //                     'success'
        //                     ).then(() => {$("#update_name").val('');});
        //                 }

        //             }
        //         });
        //     });

        //     $("#delete").on("click", function(e){
        //         e.preventDefault()

        //         var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
        //         // console.log(selectedOption); return;
        //         // var category = selectedOption.text;
        //         var categoryId = selectedOption.id;
        //         var categoryLevel = parseInt(selectedOption.level);

        //         if(selectedOption.id == ""){
        //             $(".select2").css("border", "1px solid red");
        //             return;
        //         }else $(".select2_demo_3").css("border", "none");
        //         // console.log(selectedOption.id);
        //         // return;

        //         // bool done = true;
        //         // var categoryName = $("#update_name");
        //         // if(categoryName.val() == ""){
        //         //     categoryName.css("border", "1px solid red");
        //         //     // done = false;
        //         //     return;
        //         // } 

        //         var data = new FormData();
        //         data.append('delete_category', 'delete_category');
        //         data.append('category_id', categoryId);
        //         data.append('category_level', categoryLevel);
        //         data.append('category_name', selectedOption.text);
        //         // data.append('new_name', categoryName.val());


        //         $.ajax({
        //             url: 'api/parent_category.php',
        //             data: data,
        //             dataType: "text",
        //             cache: false,
        //             contentType: false,
        //             processData: false,
        //             type: 'POST',
        //             success: function(returnData){
        //                 // console.log(returnData);
        //                 var returnData = JSON.parse(returnData);
        //                 if(returnData["error"] == 0){
        //                     // alert("category has been deleted!");
        //                     Swal.fire(
        //                     'deleted Successfully!',
        //                     'category has been deleted successfully..',
        //                     'success'
        //                     )
        //                 }

        //             }
        //         });
        //     });

        //     $("#modal_submit").on("click", function(e){
		// 		e.preventDefault();

		// 		//ajax check login
		// 		var username = $("#login_username");
		// 		var password = $("#login_password");

		// 		if(username.val() == ""){
		// 			username.css("border", "1px solid red");
		// 			return;
		// 		}else username.css("border", "1px solid #ccc");

		// 		if(password.val() == ""){
		// 			password.css("border", "1px solid red");
		// 			return;
		// 		}else password.css("border", "1px solid #ccc");


		// 		var data = new FormData();
        //         data.append('login', 'login');
        //         data.append('username', username.val());
        //         data.append('password', password.val());


        //         $.ajax({
        //             url: 'api/login.php',
        //             data: data,
        //             dataType: "text",
        //             cache: false,
        //             contentType: false,
        //             processData: false,
        //             type: 'POST',
        //             success: function(returnData){
		// 				// console.log(returnData);
		// 				returnData = JSON.parse(returnData);

		// 				if(returnData["error"] == 0){
		// 					location.href="http://localhost:8080/public_figure/mena_mahmoud/profile.php";
		// 				}else{
		// 					$("#login_hint").text("username/password not correct!");
		// 				}
		// 			}

		// 		});

				
		// 	});

        // });
		
</script>

</body></html>