<?php

$page_title="Event | Training";
include 'db-connect.php';

require_once("include/header.php");

require_once("include/navigation.php");
?>

<style>


</style>
<div class="wrapper" id="file-page">


    <div class="content-wrapper">
        <!--   <section class="content-header">
              <h1>

                  <small>Control panel</small>
              </h1>
              <ol class="breadcrumb">
                  <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
              </ol>
          </section> -->
        <?php
        $Qry_sql2 = "SELECT s1_header,s1_paragraph,s2_header,s2_paragraph,s3_video,s3_header,s3_paragraph FROM  `event_website_info` WHERE id='1'";
        $Qry2 = mysqli_query($connection, $Qry_sql2);
        while ($row5 = mysqli_fetch_assoc($Qry2)) {
            $s1_header=$row5["s1_header"];
            $s1_paragraph=$row5["s1_paragraph"];
            $s2_header=$row5["s2_header"];
            $s2_paragraph=$row5["s2_paragraph"];
            $s3_video=$row5["s3_video"];

            $s3_header=$row5["s3_header"];
            $s3_paragraph=$row5["s3_paragraph"];
        }

        //  echo mysqli_insert_id($connection);
        echo mysqli_error($connection);
        ?>


        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h2 style="text-align:center;">Section One</h2>
                    </div>
                    <div class="ibox-content">



                        <div class="ibox-content">
                            <form method="POST" class="form-horizontal" action="include/php_function.php" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image In Section One</label>
                                    <div class=" col-lg-10">
                                        <div  class="fileinput fileinput-new input-group" data-provides="fileinput">

                                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="s1_img"></span>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-md-2 control-label">Header</label>

                                    <div class="col-md-10"><input type="text" placeholder="" value="<?php echo $s1_header; ?>" name="s1_header" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-md-2 control-label">Pharagraph</label>


                                    <div class="col-xs-12">
                                        <div class="form-group" style="height: auto;">
                                       <textarea   id="summernote" name="s1_paragraph" required="">
                                          <?php
                                          echo $s1_paragraph;
                                          ?>
                                       </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-primary pull-right" type="submit" name="btn_save_s1_est">Save changes</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h2 style="text-align:center;">Section Two</h2>
                    </div>
                    <div class="ibox-content">


                        <div class="ibox-content">
                            <form method="POST" class="form-horizontal" action="include/php_function.php" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image In Section Two</label>
                                    <div class=" col-lg-10">
                                        <div  class="fileinput fileinput-new input-group" data-provides="fileinput">

                                            <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="s2_img"></span>
                                            <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-md-2 control-label">Header</label>

                                    <div class="col-md-10"><input type="text" placeholder="" value="<?php echo $s2_header; ?> " name="s2_header" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-md-2 control-label">Pharagraph</label>

                                    <div class="col-xs-12">
                                        <div class="form-group" style="height: auto;">
                                       <textarea   id="summernote2" name="s2_paragraph" required="">
                                          <?php
                                          echo $s2_paragraph;
                                          ?>
                                       </textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-primary pull-right" type="submit" name="btn_save_s2_est">Save changes</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h2 style="text-align:center;">Section Three</h2>
                    </div>
                    <div class="ibox-content">



                        <div class="ibox-content">
                            <form method="POST" class="form-horizontal" action="include/php_function.php" enctype="multipart/form-data">
                                <div class="form-group"><label class="col-md-2 control-label">Video URl </label>

                                    <div class="col-md-10"><input type="text" placeholder="" name="s3_video"value="<?php echo $s3_video; ?> " class="form-control">
                                    </div>
                                </div>
                                <div class="form-group"><label class="col-md-2 control-label">Header</label>

                                    <div class="col-md-10"><input type="text" placeholder="" name="s3_header"  value="<?php echo $s3_header; ?> "class="form-control">
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-md-2 control-label">Pharagraph</label>
                                    <div class="col-xs-12">
                                        <div class="form-group" style="height: auto;">
                                       <textarea   id="summernote3" name="s3_paragraph" required="">
                                          <?php
                                          echo $s3_paragraph;
                                          ?>
                                       </textarea>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button class="btn btn-primary pull-right" name="btn_save_s3_est" type="submit">Save changes</button>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>



                </div>
            </div>

        </div>




        <?php require_once("include/footer.php");?>

    </div>

</div>
        <!-- Mainly scripts -->

        <script src="js/jquery-2.1.1.js"></script>

        <script src="js/bootstrap.min.js"></script>

        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

        <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



        <!-- Custom and plugin javascript -->

        <script src="js/inspinia.js"></script>

        <script src="js/plugins/pace/pace.min.js"></script>



        <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- SUMMERNOTE -->
        <script src="js/plugins/summernote/summernote.min.js"></script>
        <script>
            $(document).ready(function(){

                $('#summernote').summernote({
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ]
                });

                $('#summernote2').summernote({
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ]
                });

                $('#summernote3').summernote({
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']]
                    ]
                });

            });
        </script>



        </body>



        </html>

