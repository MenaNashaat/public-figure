<?php 

	// $pageTitle = "Public Figure";
	require_once("../../includes/initialize.php");


    $pageTitle = "admin";

	//get database connection
    $dbConnection = getDatabaseConnection();

    $page_title = "Public Figure | Categories";
    require_once("include/header.php");
    require_once("include/navigation.php");

?>

    <div class="container" style="margin-top: 70px; margin-bottom: 70px">
        <div class="row" style="margin-bottom: 7em;">
            <div class="col-md-8 col-md-offset-2">
                <form class="domain-checker m-t-em-1" id="search_form">
                    <div class="group" style="padding-bottom: 7em;">
                        <select class="select2_demo_3 form-control" style="">
                                <option></option>
                                
                        </select>

                            <!-- <div class="input-group-btn">
                                <button id="search" name="search" class="btn btn-primary" type="submit" style="height: 50px;">
                                    Search
                                </button>
                            </div>
                        </div> -->

                        <input type="hidden" id="search_string" name="search_string" value="" >
                    </div>
                </form>


            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-sm-12 col-md-4 col-md-offset-1">
                <div class="form-group">
                    <label for="exampleInputEmail1"> Child Category Name </label>
                    <input type="text" id="add_name" class="form-control" placeholder="child category name">
                    <button id="add" class="btn btn-primary pull-right" style="margin-top: 7px">Add</button>  
                </div>
            </div> 
            
            <div class="col-sm-12 col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1"> Category New Name </label>
                    <input type="text" id="update_name" class="form-control" placeholder="category new name">
                    <button id="update" class="btn btn-primary pull-right" style="margin-top: 7px">Update</button>  
                </div>
            </div> 
            <div class="col-sm-12 col-md-2">
                <div class="form-group">
                    <!-- <label for="exampleInputEmail1"> Child Category Name </label>
                    <input type="text" id="cat_name" class="form-control" placeholder="childcategory name"> -->
                    <!-- <button id="del" class="btn btn-danger pull-right" style="margin-top: 20%; width: 100%">Delete Category</button> -->
                    <button id="delete" class="btn btn-danger pull-right" style="margin-top: 43%; width: 100%">Delete</button>  
                </div>
            </div> 
        
        </div>
    </div>

    </div><?php require_once("include/footer.php"); ?><!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/select2/select2.full.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script><!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script></body></html>


<script>
		$(".select2_demo_1").select2();
		$(".select2_demo_2").select2();
		$(".select2_demo_3").select2({
			placeholder: "Select a state",
			allowClear: true,
			
        });

        var i = 1;
		$(".select2_demo_3").select2({
			placeholder: "Search Category",
			allowClear: true,
			width: 'resolve',
			closeOnSelect: false,
			height: 20,
			ajax: {
                url: function (params) {
                    // if(params.term == "") return;
                    return 'api/parent_category.php?category=' + params.term;
                },
				// url: 'api/parent_category.php?category=' + params.term,
				// 	data: function (params) {
				// 		var query = {};
				// 		if(params.term != "")
				// 			query = {
				// 				search: params.term
				// 			};

				// 		// Query parameters will be ?search=[term]&type=public
				// 		return query;
				// },

				processResults: function (data) {
					// Tranforms the top-level key of the response object from 'items' to 'results'
					// console.log(data);
					var data = JSON.parse(data);  //array of result set
					
                    // console.log(data);
                    if(data["error"] == 0){
                        var categories = data["categories"];
                        var searchResult = [];
                        for(i = 1; i < categories.length; i++){
							var item = {};
							item.id = categories[i]["id"];
							item.text = categories[i]["name"];
                            item.level = categories[i]["category_level"];

							searchResult.push(item);
						}

                        return {
								results: searchResult
							};

						
                    }

                    return {results: []};
                }
            }
        });

        $(document).ready(function(){

            $("#add").on("click", function(e){
                e.preventDefault()

                var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
                // console.log(selectedOption); return;
                // var category = selectedOption.text;
                var categoryPrevId = selectedOption.id;
                var categoryLevel = parseInt(selectedOption.level) + 1;

                if(selectedOption.id == ""){
                    $(".select2").css("border", "1px solid red");
                    return;
                }else $(".select2_demo_3").css("border", "none");
                // console.log(selectedOption.id);
                // return;

                // bool done = true;
                var categoryName = $("#add_name");
                if(categoryName.val() == ""){
                    categoryName.css("border", "1px solid red");
                    // done = false;
                    return;
                } 

                var data = new FormData();
                data.append('add_category', 'add_category');
                data.append('prev_id', categoryPrevId);
                data.append('category_level', categoryLevel);
                data.append('category_name', categoryName.val());


                $.ajax({
                    url: 'api/parent_category.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
                        // console.log(returnData);
                        var returnData = JSON.parse(returnData);
                        if(returnData["error"] == 0){
                            // alert("category has been added!");
                            Swal.fire(
                            'added Successfully!',
                            'category has been added successfully..',
                            'success'
                            ).then(() => {$("#add_name").val('');});
                        }

                    }
                });
            });

            $("#update").on("click", function(e){
                e.preventDefault()

                var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
                // console.log(selectedOption); return;
                // var category = selectedOption.text;
                var categoryId = selectedOption.id;
                var categoryLevel = parseInt(selectedOption.level);

                if(selectedOption.id == ""){
                    $(".select2").css("border", "1px solid red");
                    return;
                }else $(".select2_demo_3").css("border", "none");
                // console.log(selectedOption.id);
                // return;

                // bool done = true;
                var categoryName = $("#update_name");
                if(categoryName.val() == ""){
                    categoryName.css("border", "1px solid red");
                    // done = false;
                    return;
                } 

                var data = new FormData();
                data.append('update_category', 'update_category');
                data.append('category_id', categoryId);
                data.append('category_level', categoryLevel);
                data.append('old_name', selectedOption.text);
                data.append('new_name', categoryName.val());


                $.ajax({
                    url: 'api/parent_category.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
                        // console.log(returnData);
                        var returnData = JSON.parse(returnData);
                        if(returnData["error"] == 0){
                            // alert("category has been updated!");
                            Swal.fire(
                            'updated Successfully!',
                            'category has been updated successfully..',
                            'success'
                            ).then(() => {$("#update_name").val('');});
                        }

                    }
                });
            });

            $("#delete").on("click", function(e){
                e.preventDefault()

                var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
                // console.log(selectedOption); return;
                // var category = selectedOption.text;
                var categoryId = selectedOption.id;
                var categoryLevel = parseInt(selectedOption.level);

                if(selectedOption.id == ""){
                    $(".select2").css("border", "1px solid red");
                    return;
                }else $(".select2_demo_3").css("border", "none");
                // console.log(selectedOption.id);
                // return;

                // bool done = true;
                // var categoryName = $("#update_name");
                // if(categoryName.val() == ""){
                //     categoryName.css("border", "1px solid red");
                //     // done = false;
                //     return;
                // } 

                var data = new FormData();
                data.append('delete_category', 'delete_category');
                data.append('category_id', categoryId);
                data.append('category_level', categoryLevel);
                data.append('category_name', selectedOption.text);
                // data.append('new_name', categoryName.val());


                $.ajax({
                    url: 'api/parent_category.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
                        // console.log(returnData);
                        var returnData = JSON.parse(returnData);
                        if(returnData["error"] == 0){
                            // alert("category has been deleted!");
                            Swal.fire(
                            'deleted Successfully!',
                            'category has been deleted successfully..',
                            'success'
                            )
                        }

                    }
                });
            });

            $("#modal_submit").on("click", function(e){
				e.preventDefault();

				//ajax check login
				var username = $("#login_username");
				var password = $("#login_password");

				if(username.val() == ""){
					username.css("border", "1px solid red");
					return;
				}else username.css("border", "1px solid #ccc");

				if(password.val() == ""){
					password.css("border", "1px solid red");
					return;
				}else password.css("border", "1px solid #ccc");


				var data = new FormData();
                data.append('login', 'login');
                data.append('username', username.val());
                data.append('password', password.val());


                $.ajax({
                    url: 'api/login.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
						// console.log(returnData);
						returnData = JSON.parse(returnData);

						if(returnData["error"] == 0){
							location.href="http://localhost:8080/public_figure/mena_mahmoud/profile.php";
						}else{
							$("#login_hint").text("username/password not correct!");
						}
					}

				});

				
			});

        });
		
</script>