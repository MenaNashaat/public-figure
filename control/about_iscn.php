<?php 

ob_start();

// $pageTitle = "Public Figure";
require_once("../../includes/initialize.php");


$page_title = "About Us";

//get database connection
$dbConnection = getDatabaseConnection();


require_once("include/header.php");
require_once("include/navigation.php");

$connection = $dbConnection;

if(isset($_POST["save_text"]))
{
    $text=$_POST["text"];
    $text =$dbConnection->prepareQueryValue($text);
    $Qry_sql2 = "UPDATE `public_figure_content` SET about_iscn='$text'";
    $Qry2 = $dbConnection->performQuery($Qry_sql2);

    if($Qry2)
    {
        redirect_to("about_iscn.php");
    }
}
?>

<style>
    .note-editor.note-frame {
        border: 1px solid #fff;
    }
</style>


<div class="wrapper wrapper-content animated">
    <?php

    $Qry_sql2 = "SELECT about_iscn FROM `public_figure_content` LIMIT 1";

    $Qry2 = $dbConnection->performQuery($Qry_sql2);
    while ($row5 = mysqli_fetch_assoc($Qry2)) {
        $about_iscn=$row5["about_iscn"];

    }

    //  echo mysqli_insert_id($connection);

    ?>
    <!-- <section class="content-header">
        <h1>

            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
        </ol>
    </section>
-->

    <div class="wrapper wrapper-content">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5> About ISCN System </h5>

                    </div>
                    <div class="ibox-content no-padding">
                        <form  action="" method="POST">
                            <div class="row">
                                <div class="col-xs-12">

                                    <textarea  id="summernote" name="text" required="" style="width: 100%; padding: 10px 15px; border: 1px solid #ddd; height: 420px;"><?php  echo $about_iscn;?></textarea>
                                </div>


                                <button type="submit" name="save_text" class="btn btn-primary btn-sm pull-right">Edit & Save Data</button>
                                <br>


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <?php require_once("include/footer.php");?>

</div>

<!-- Mainly scripts -->

<script src="js/jquery-2.1.1.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



<!-- Custom and plugin javascript -->

<script src="js/inspinia.js"></script>

<script src="js/plugins/pace/pace.min.js"></script>



<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- SUMMERNOTE -->
<script src="js/plugins/summernote/summernote.min.js"></script>





<script>
    $(document).ready(function(){

        $('#summernote').summernote();

    });
</script>




</body>



</html>

