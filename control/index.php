<?php 

ob_start();
// session_start();

$page_title = "Public Figure | Home";
require_once("include/header.php");
require_once("include/navigation.php");

// $pageTitle = "profile";
require_once("../../includes/initialize.php");


//get database connection
$dbConnection = getDatabaseConnection();
$connection = $dbConnection;

$Qry = "SELECT COUNT(*) AS total FROM public_figure_user";
$Run = $dbConnection->performQuery($Qry);
$Record = mysqli_fetch_assoc( $Run );
$total=$Record["total"];

// $Qry = "SELECT COUNT(*) AS num_approved FROM event WHERE status ='4'";
// $Run = mysqli_query( $connection, $Qry );
// $Record = mysqli_fetch_assoc( $Run );
// $num_approved=$Record["num_approved"];

// $Qry = "SELECT COUNT(*) AS num_waiting FROM event WHERE status !='4'";
// $Run = mysqli_query( $connection, $Qry );
// $Record = mysqli_fetch_assoc( $Run );
// $num_waiting=$Record["num_waiting"];


// $Qry = "SELECT COUNT(*) AS num_paid FROM event WHERE last_payment !=''";
// $Run = mysqli_query( $connection, $Qry );
// $Record = mysqli_fetch_assoc( $Run );
// $num_paid=$Record["num_paid"];

?>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading m1">
                    <div class="row">
                        <div class="col-xs-9 text-center">
                            <div class="huge" style="font-size: 1.3em"> Public Figures  <br/> <?php echo $total ?></div>
                        </div>
                        <div class="col-xs-3"><i class="fas fa-university fa-3x"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-3 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading m1">
                    <div class="row">
                        <div class="col-xs-9 text-center">
                            <div class="huge" style="font-size: 1.3em">Approved Events<br/>  <?php /*echo $num_approved*/ ?></div>
                        </div>
                        <div class="col-xs-3"><i class="fas fa-check fa-3x"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading m1">
                    <div class="row">
                        <div class="col-xs-9 text-center">
                            <div class="huge" style="font-size: 1.3em">Waiting Events<br/>  <?php /*echo $num_waiting*/ ?></div>
                        </div>
                        <div class="col-xs-3"><i class="fas fa-pause fa-3x"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading m1">
                    <div class="row">
                        <div class="col-xs-9 text-center">
                            <div class="huge" style="font-size: 1.3em">Paid Events <br/>  <?php /*echo $num_paid*/ ?></div>
                        </div>
                        <div class="col-xs-3"><i class="fas fa-dollar-sign fa-3x"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content text-center p-md">
                    <div class="row"><br/>

                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div><?php require_once("include/footer.php"); ?><!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script><!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script></body></html>