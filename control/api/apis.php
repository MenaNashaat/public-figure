<?php 

    ob_start();
    session_start();

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();



define('NOW', date('Y-d-m h:i:s A'));

function remove_white_spaces( $data )

{

    $data = trim( $data );

    $data = stripslashes( $data );

    $data = htmlspecialchars( $data );

    $data = preg_replace('/\s+/', '', $data);

    return $data;

}

function mysql_prep( $string )
{
    global $connection;
    $escapeString = mysqli_real_escape_string( $connection, $string );
    return $escapeString;
}

// function redirect_to( $newLocation )
// {
//     header( "Location: " . $newLocation );
//     exit( );
// }

function safe_string($string){

    $safeString = filter_var($string, FILTER_SANITIZE_STRING);

    return $safeString;

}

function safe_email($string){

    $safeString = filter_var($string, FILTER_SANITIZE_EMAIL);

    return $safeString;

}

function safe_int($string){

    $safeInt = filter_var($string, FILTER_SANITIZE_NUMBER_INT);

    return $safeInt;

}

function safe_float($string){

    //$safeFloat = var_dump(filter_var($string, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));

    $safeFloat = filter_var($string, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    return $safeFloat;

}

function get_file_ext($file)

{

    $ext = pathinfo($file, PATHINFO_EXTENSION);

    return $ext ? $ext : false;

}





function confirm_query( $resultSet )

{

    if ( !$resultSet ) {

        //die("Database query failed.");

        echo $query;

    } //!$resultSet

}






/*----------------- total_rows ----------------- */

/*

* This is a detailed explanation

* of the below function.

*/


function get_all_from_tbl( $tbl )

{

    global $connection;

    $safeTableName = mysqli_real_escape_string( $connection, $tbl );

    $query         = "SELECT * FROM `$safeTableName`";

    $recordSet     = mysqli_query( $connection, $query );

    confirm_query( $recordSet );

    if ( $recordSet ) {

        return $recordSet;

    } //$recordSet

    else {

        echo $query;

    }

}





function get_col_On_key( $tbl, $col, $where, $key )

{

    global $connection;

    $tbl       = mysqli_real_escape_string( $connection, $tbl );

    $col       = mysqli_real_escape_string( $connection, $col );

    $where     = mysqli_real_escape_string( $connection, $where );

    $key       = mysqli_real_escape_string( $connection, $key );

    if (is_string($key)) { $key = "'$key'";  }

    $query     = "SELECT `$col` FROM `$tbl` WHERE `$where` = $key";

    confirm_query( $query );

    $queryRun  = mysqli_query( $connection, $query );

    if ($queryRun) {

        $row       = mysqli_fetch_assoc($queryRun);

        $data      =$row[$col];

        return $data;

    }



}

function get_all_cols_on_key( $tbl, $whereColumn, $key )

{

    global $connection;

    $safeTableName = mysqli_real_escape_string( $connection, $tbl );

    $safeKey       = mysqli_real_escape_string( $connection, $key );

    $MySQLQry      = "SELECT * FROM `$tbl` WHERE `$whereColumn` = $key";

    $recordSet     = mysqli_query( $connection, $MySQLQry );

    confirm_query( $recordSet );

    if ( $recordSet ) {

        return $recordSet;

    } //$recordSet

}




function statusId_to_string($status)

{

    switch ($status) {

        case 1:

            return $status = "Arrived";

            break;

        case 2:

            return $status = "Missing Data";

            break;

        case 3:

            return $status = "Under Process";

            break;

        case 4:

            return $status = "Approved";

            break;

        case 5:

            return $status = "Public";

            break;

        default:

            return $status = "Arrived";

            break;

    }

}

// function estb_action($from) {


//     global $connection;


//          $action = mysql_prep($_POST['estb-action']);


//     $estbID = mysql_prep($_POST['event_id']);

//     $senderEmail = mysql_prep($_POST['sender']);

//     $subject = mysql_prep($_POST['subject']);

//     $estbName = get_col_On_key('event', 'name', 'id', $estbID);

//     $estbEmail= get_col_On_key('event', 'email', 'id', $estbID);

//     $presentEstbStatus = get_col_On_key('event', 'status', 'id', $estbID);

//     $payment=get_col_On_key('payments','payment_status','event_id',$estbID);

//     if ($payment == '' || $payment == NULL) {

//         $payment = "Not Paid";

//     }

//     $message = $_POST['email-body'];

//     $message = str_replace(array("\n", "\r"), '', $message);

//     $message = str_replace("<p></p>", '', $message);

//     $message = str_replace("<p><br></p>", '', $message);

//     $message     = mysql_prep($message);

//     $note  = mysql_prep($_POST['note']);

//     $now = NOW;



//     if ($presentEstbStatus == "approved") {

//         $_SESSION['msgType'] = "0";

//         $_SESSION['msg'] = "Already Approved !";

//         if ($from == "events.php") {

//             redirect_to( 'events.php' );

//         }



//         else{

//             redirect_to( 'events.php' );

//             exit();

//         }

//     }

//     if ($payment!=1|| $payment!="1"||$payment==''||$payment=="Not Paid"){

//         $_SESSION[ 'msgType' ] = "0";

//         $_SESSION['msg'] = "Cannot approved . Payment not recieved.";

//         if ($from == "events.php") {

//           //  redirect_to( 'events.php' );

//         }


//         else{

//            // redirect_to( 'events.php' );

//             exit();

//         }

//     }

//     if ($action == "approved") {



//         $updEstbQry = " UPDATE event SET ";

//         $updEstbQry .= "modify_by   = 'admin' , ";

//         $updEstbQry .= "last_modify   = '$now' , ";

//         $updEstbQry .= "missing_note   = '$note' , ";

//         $updEstbQry .= "status   = '$action'  ";

//         $updEstbQry .= "WHERE id = $estbID";

//         $updEstbRun = mysqli_query( $connection, $updEstbQry );

//         if ( $updEstbRun ) {


//             // Query Run Successfully.

//             // Note added successfully.";

//             // Now we will mail the newly created username

//             // and password to establishment on thier email id
//             $estbPassword = get_col_On_key('users', 'plain_password', 'estb_id', $estbID);
//             $estbuserName = get_col_On_key('est', 'username', 'id', $estbID);
//             $estbqrPath = "https://verifiedevent.org/uploads/event/qr_profile/".get_col_On_key('event', 'qr_path', 'id', $estbID);

//             $estblink = "https://estid.org/event_profile.php?est_id=".$estbuserName;


//             $to      = $estbEmail;

//             // For testing I put my email

//             // $to = "jahangirkhattak13@gmail.com";

//             $subject = "Congratulations Your Event has been approved";

//             $message = "

//                         <html>

//                         <head>

//                         <title>Event Approved</title>

//                         </head>

//                         <body>

//                         <p>To <span style='color:orange'>".$estbName."</span>:</p>

//                         <h3>Welcome to ISCN system!!!</h3>

//                         <p>The ISCN admissions committee has reviewed your application for your esteemed Event, and it is our pleasure to notify you that is now approved.</p>

//                         <p>Find username and password below: </p>

//                         <table border='1'>

//                         <tr><th>UserName: </th><th>".$estbuserName."</th></tr>

//                         <tr><th>Password: </th><th>".$estbPassword."</th></tr>

//                         <tr><th>Qr Code</th><th><img src='".$estbqrPath."' /></th></tr>

//                         <tr><th>Link for details</th><th><a href='".$estblink."'>Click</a></th></tr>

//                         </table>

//                         <br />

//                         <p>For further information or if you have any questions please do not hesitate to contact us: <span style='color:blue'>Info@iscnsystem.org</span></p>

//                         <br />

//                         <p style='font-weight:900'>Thanks in advance.</p>

//                         <p style='font-weight:900' >Best Regards</p>

//                         <br />

//                         <pre style='color:blue;font-size:14px;'>

//                         International standard certificate number (<span style='font-weight:900'>ISCN</span>) team.

//                         International Standards Dataflow 

//                         www.iscnsystem.org 

//                         </pre>

//                         </body>

//                         </html>

//                     ";

//             // Always set content-type when sending HTML email

//             $headers = "MIME-Version: 1.0" . "\r\n";

//             $headers.= "Content-type:text/html;charset=UTF-8"."\r\n";

//             // More headers

//             $headers .= 'From: '.$senderEmail."\r\n";

//             $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";
//              $send_mail=mail($to,$subject,$message,$headers);
//             if ($send_mail) {

//                 session_start();

//                 $_SESSION['msgType'] = "1";

//                 $_SESSION['msg']="Event has been approved successfully!";

//                 if ($from == "events.php") {

//                   // redirect_to( 'events.php' );

//                 }


//             } // End: if(mail($to,$subject,$message,$headers)) {

//             else {

//                 // Establishment has  been Approved but mail

//                 // doesn't send on LocalServer.";

//                 $_SESSION[ 'msgType' ] = "1";

//                 $_SESSION['msg'] = "Event has  been Approved. Email not sent.";

//                 if ($from == "events.php") {

//                    redirect_to( 'events.php' );

//                 }



//             } // End: else {

//             // End: if ($addUserRun) {

//             // End: if ($addNoteRun) {

//         } // End: if ($updateEstbQueryRun) {

//     } // End: if ($action == 4) {

//     else {

//         // You want to change establishment status

//         $updEstbQry  = "UPDATE event SET ";

//         $updEstbQry .= "`last_modify`='$now', ";

//         $updEstbQry .= "missing_note   = '$note' , ";

//         $updEstbQry .= "status='$action', ";

//         $updEstbQry .= "modify_by   = 'admin'  ";

//         $updEstbQry .= "WHERE ";

//         $updEstbQry .= "id = $estbID";

//         $updEstbRun = mysqli_query( $connection, $updEstbQry );


//         if ( $updEstbRun ) {

//             // Always set content-type when sending HTML email

//             $headers = "MIME-Version: 1.0" . "\r\n";

//             $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

//             $headers .= 'From: '.$senderEmail."\r\n";

//             $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";

//             $to = $estbEmail;
//                 $mail_send=mail($to,$subject,$message,$headers);
//             if ($mail_send) {

//                 $_SESSION[ 'msgType' ] = "1";

//                 $_SESSION[ 'msg' ]     = "Event status has been changed.";

//                 if ($from == "events.php") {

//                     redirect_to( 'events.php' );

//                 }




//             } // End: if(mail(...)) {

//             else {



//                 $_SESSION[ 'msgType' ] = "1";

//                 $_SESSION['msg']="Event status has been changed.Email not sent";

//                 if ($from == "events.php") {

//                     redirect_to( 'events.php' );

//                 }



//             } // End: else {

//         } // End: if ($updEstbRun) {

//     } // End: else

// } // End: function change_estb_status($from) {



//confirm login automatically
if( isset($_SESSION["EVENT_CONTROL_SESSION_CODE"]) && isset($_SESSION["EVENT_ADMIN_CONTROL_ID"]) &&  $_SESSION["EVENT_CONTROL_SESSION_CODE"]==0 ) {

    $ADMIN_ID_VALUE = $_SESSION["EVENT_ADMIN_CONTROL_ID"];
    $ADMIN_ID_VALUE = $dbConnection->prepareQueryValue($ADMIN_ID_VALUE);
    $Qry = "SELECT name FROM  public_figure_admin WHERE id=$ADMIN_ID_VALUE ";
    $QryRun = $dbConnection->performQuery($Qry);
    $count  = mysqli_num_rows($QryRun);


    if ($count == 1) {


        $Record = mysqli_fetch_assoc($QryRun);
        $admin_name=$Record["name"];
    } // End: if ( $count == 1 ) {

    else {


        $_SESSION['EVENT_CONTROL_SESSION_CODE'] = "3";

        redirect_to('   login.php');

    }
}
else {
    redirect_to('login.php');
}




?>