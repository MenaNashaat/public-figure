<?php   

ob_start();
session_start();
require "../../includes/initialize.php";

//get database connection
$dbConnection = getDatabaseConnection();
$connection = $dbConnection;

define('NOW', date('Y-d-m h:i:s A'));

function remove_white_spaces( $data )

{

    $data = trim( $data );

    $data = stripslashes( $data );

    $data = htmlspecialchars( $data );

    $data = preg_replace('/\s+/', '', $data);

    return $data;

}

function mysql_prep( $string )
{
    global $connection;
    $escapeString = mysqli_real_escape_string( $connection, $string );
    return $escapeString;
}

// function redirect_to( $newLocation )
// {
//     header( "Location: " . $newLocation );
//     exit( );
// }

if(isset($_POST["control_login"]))
{

    if( isset($_POST["username"]) && isset($_POST["password"]) )
    {
        // $username=mysql_prep($_POST["username"]);
        // $password=mysql_prep($_POST["password"]);

        $username=$dbConnection->prepareQueryValue($_POST["username"]);
        $password=$dbConnection->prepareQueryValue($_POST["password"]);
        $password= remove_white_spaces($password);

        $Qry    = "SELECT id,name,password FROM  public_figure_admin WHERE username = '$username'";

        // $Run = mysqli_query( $connection, $Qry );
        $Run = $dbConnection->performQuery($Qry);

        // Mysql_num_row is counting table row

        $count      = mysqli_num_rows( $Run );

        // If the entered username exist in database then $count

        // must be equal to 1

        if ( $count == 1 ) {

            
            //user existed. Now we are going to check password.

            $Record         = mysqli_fetch_assoc( $Run );

            // Geting md5 password from database of this username

            $PasswordDB = $Record['password'];

            // convert user entered password with md5 function and add Salt

            // $salt               = "isdf2017";

            // $saltedpass           = sha1( $password . $salt );

            // $hashedPassword       = md5($saltedpass);

            if (password_verify($password, $PasswordDB)) {

                // password is correct

                $_SESSION['EVENT_ADMIN_CONTROL_ID'] = $Record[ 'id' ];

                $_SESSION['EVENT_CONTROL_SESSION_CODE']     = "0";

                redirect_to( '../index.php' );

                // exit();

            } // End: if ($passwordSalted == $estbUserPasswordDB) {

            else {

                // Wrong password.

                $_SESSION['EVENT_CONTROL_SESSION_CODE']     = "2";

                redirect_to( '../login.php');

                // exit();

            } // End: else {

        } // End: if ( $count == 1 ) {

        else {


            $_SESSION['EVENT_CONTROL_SESSION_CODE']  = "1";

            redirect_to( '../login.php');

            // exit();

        } // End: else {




    }//end if



}

function safe_string($string){

    $safeString = filter_var($string, FILTER_SANITIZE_STRING);

    return $safeString;

}

function safe_email($string){

    $safeString = filter_var($string, FILTER_SANITIZE_EMAIL);

    return $safeString;

}

function safe_int($string){

    $safeInt = filter_var($string, FILTER_SANITIZE_NUMBER_INT);

    return $safeInt;

}

function safe_float($string){

    //$safeFloat = var_dump(filter_var($string, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));

    $safeFloat = filter_var($string, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    return $safeFloat;

}

function get_file_ext($file)

{

    $ext = pathinfo($file, PATHINFO_EXTENSION);

    return $ext ? $ext : false;

}

function confirm_query( $resultSet )

{

    if ( !$resultSet ) {

        //die("Database query failed.");

        echo $query;

    } //!$resultSet

}

function get_all_from_tbl( $tbl )

{

    global $connection;

    $safeTableName = mysqli_real_escape_string( $connection, $tbl );

    $query         = "SELECT * FROM `$safeTableName`";

    $recordSet     = mysqli_query( $connection, $query );

    confirm_query( $recordSet );

    if ( $recordSet ) {

        return $recordSet;

    } //$recordSet

    else {

        echo $query;

    }

}

function get_col_On_key( $tbl, $col, $where, $key )

{

    global $connection;

    $tbl       = mysqli_real_escape_string( $connection, $tbl );

    $col       = mysqli_real_escape_string( $connection, $col );

    $where     = mysqli_real_escape_string( $connection, $where );

    $key       = mysqli_real_escape_string( $connection, $key );

    if (is_string($key)) { $key = "'$key'";  }

    $query     = "SELECT `$col` FROM `$tbl` WHERE `$where` = $key";

    confirm_query( $query );

    $queryRun  = mysqli_query( $connection, $query );

    if ($queryRun) {

        $row       = mysqli_fetch_assoc($queryRun);

        $data      =$row[$col];

        return $data;

    }



}

function get_all_cols_on_key( $tbl, $whereColumn, $key )

{

    global $connection;

    $safeTableName = mysqli_real_escape_string( $connection, $tbl );

    $safeKey       = mysqli_real_escape_string( $connection, $key );

    $MySQLQry      = "SELECT * FROM `$tbl` WHERE `$whereColumn` = $key";

    $recordSet     = mysqli_query( $connection, $MySQLQry );

    confirm_query( $recordSet );

    if ( $recordSet ) {

        return $recordSet;

    } //$recordSet

}

function statusId_to_string($status)

{

    switch ($status) {

        case 1:

            return $status = "Arrived";

            break;

        case 2:

            return $status = "Missing Data";

            break;

        case 3:

            return $status = "Under Process";

            break;

        case 4:

            return $status = "Approved";

            break;

        case 5:

            return $status = "Public";

            break;

        default:

            return $status = "Arrived";

            break;

    }

}


















?>