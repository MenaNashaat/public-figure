<?php

ob_start();

// $pageTitle = "Public Figure";
require_once("../../includes/initialize.php");



$page_title = "Public Figures";

//get database connection
$dbConnection = getDatabaseConnection();

require_once("api/change_user_status.php");


require_once("include/header.php");
require_once("include/navigation.php");

$connection = $dbConnection;

// $usersTotalCount = Search::searchUsersNumber($search, $searchFields);
$pageNumber = isset($_GET["page_number"]) ? urldecode($_GET["page_number"]) : "1";

$sqlQuery = "SELECT COUNT(*) AS count FROM public_figure_user";
$sqlRun = $dbConnection->performQuery($sqlQuery);
$usersTotalCount = mysqli_fetch_assoc($sqlRun)["count"];

$perPage = 2;

if($pageNumber > ceil($usersTotalCount / $perPage)) $pageNumber = 1;

$pagination = new Pagination($pageNumber, $perPage, $usersTotalCount);
// $matchedUsers = Search::searchWithParams($search, $searchFields, $pagination);

$sqlQuery="SELECT " ;
$sqlQuery .="id, ";
$sqlQuery .="first_name, last_name, ";
$sqlQuery .="username, ";
$sqlQuery .="email, ";
$sqlQuery .="residence, ";
$sqlQuery .="category_id, ";
$sqlQuery .="status, ";
$sqlQuery .="category_path, ";
$sqlQuery .="photo_path ";
$sqlQuery .="FROM public_figure_user ORDER BY id DESC ";
$sqlQuery .= " LIMIT " . $pagination->perPage . " OFFSET " . $pagination->offset();


$sqlRun = $dbConnection->performQuery($sqlQuery);

if ( isset($_POST['btn-estb-status'])) {

    estb_action($from='public_figures.php');

}




?>

<style>

    @media screen and (min-width: 1024px) {

        .modal-dialog {

            width: 1024px;



        }

    }

    ul{

        list-style: none;

    }

    li{

        display: inline-block;

    }

</style>

<div class="row wrapper border-bottom white-bg ">

    <br/>

    <div class="col-lg-12">



        <ol class="breadcrumb">

            <li>

                <a href="index.php">Home</a>

            </li>



            <li class="active">

                <strong>Public Figures </strong>

            </li>

        </ol>

    </div>

    <div class="clearfix"><br/></div><br/>

</div>

<div class="clearfix"><br/></div>

<div class="wrapper animated">



    <div class="row">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">



                <div class="row">

                    <div class="col-lg-12">

                        <div class="ibox float-e-margins">

                            <div class="ibox-content">

                                <div class="row" style="margin-bottom: 4em;">
                                    <button  data-toggle="modal" data-target="#send_all_modal" class="get_all_mails btn btn-warning btn-xs"
                                             title="Action"> <h4> &nbsp;Send Mail &nbsp; <span class="glyphicon glyphicon-envelope"> </span>&nbsp; </h4></button>
                                </div>

                                <div class="table-responsive">

                                <table class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">photo</th>
                                            <th scope="col">Username</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Country</th>
                                            <th scope="col">Category</th>
                                            <th scope="col">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 

                                            $i = 1;
                                            while($user = mysqli_fetch_assoc($sqlRun)):

                                        ?>
                                            <tr>
                                                <th scope="row"><?php echo $i; ?></th>
                                                <td><img height="70" width="70" src='<?php echo "../uploads/" . $user["photo_path"]; ?>' /></td>
                                                <td>
                                                    <?php echo $user["first_name"] . $user["last_name"]; ?>
                                                    <br />
                                                    username: <b> <?php echo $user["username"]; ?> </b>
                                                
                                                </td>
                                                
                                                <td><?php echo $user["email"]; ?></td>


                                                <td><?php 
                                                    $countryId = $user["residence"];
                                                    $sqlQuery="SELECT * FROM countries WHERE id='{$countryId}'";
                                                    $country = $dbConnection->performQuery($sqlQuery);
                
                                                    echo mysqli_fetch_assoc($country)["country_name"];
                                                ?></td>

                                                <td><?php 
                                                    $categoryPath = explode("-", $user["category_path"]);
                                                    array_pop($categoryPath);
                                                    
                                                    $parentId = $categoryPath[0];
                                                    $childId = $categoryPath[count($categoryPath) - 1];
                
                                                    $output = "";
                                                    
                                                    $sqlQuery="SELECT name FROM categories_chain WHERE id='{$parentId}'";
                                                    $category = $dbConnection->performQuery($sqlQuery);
                                                    $output .= mysqli_fetch_assoc($category)["name"];
                
                                                    if(count($categoryPath) > 1){
                                                        $sqlQuery="SELECT name FROM categories_chain WHERE id='{$childId}'";
                                                        $category = $dbConnection->performQuery($sqlQuery);
                                                        $output .= " - " . mysqli_fetch_assoc($category)["name"];
                                                    }
                
                                                    echo $output;
                                                ?></td>

                                                <td>
                                                    <?php 
                                                    $btnClasses = "";
                                                    $iconClass = "";
                                                    if ($user['status']=="new") {
                                                        $btnClasses = "btn-primary";
                                                        $iconClass = "fa-exchange-alt";
                                                    }
                                                    elseif ($user['status']=="under_process") {
                                                        $btnClasses = "btn-warning";
                                                        $iconClass = "fa-exclamation-triangle";
                                                    }elseif ($user['status']=="missing_fields") {
                                                        $btnClasses = "btn btn-xs btn-success";
                                                        $iconClass = "fa-ban ";
                                                    }



                                                    if($user['status'] !="approved"){ ?>

                                                        <button type="button" data-toggle="modal" data-target="#EstbChangeStatusModel" class="btn btn-xs btn-estb-status <?php echo $btnClasses; ?>" data-id="<?php echo $user['id']; ?>"><?php echo $user['status']; ?> <i class="fa <?php echo $iconClass; ?>"></i></button>

                                                    <?php }
                                                    else
                                                    {?>

                                                    <label class="btn btn-info">Approved</label>

                                                    <?php

                                                    }?>



                                                </td>
                                            </tr>

                                            <?php $i++; endwhile; ?>
                                    </tbody>
                                    </table>


                                    </div>

                            </div>

                        </div>

                    </div>



                    <div class="col-md-4 pull-right">
                        <nav aria-label="Page navigation example" class="tg-pagination">
                            <ul class="pagination pagination-lg">
                                <?php if($pageNumber != 1): ?>
                                    <li class="tg-prevpage page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . $pagination->previous(); ?>"><i class="fa fa-angle-left"></i></a></li>
                                <?php endif; ?>
                                <li class="<?php if($pageNumber == 1) echo "tg-active";  ?> page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . "1"; ?>"><?php echo "1"; ?></a></li>

                                <?php if($pagination->pagesNumber() >= 4){ ?>

                                    <?php if($pageNumber > 2): ?>
                                        <li class="tg-prevpage page-item disabled"> <span> ... </span> </li>
                                    <?php endif; ?>

                                    
                                    <?php if($pageNumber <= 2): ?>
                                        <li class="<?php if($pageNumber == 2) echo "tg-active";  ?> page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . "2"; ?>"><?php echo "2"; ?></a></li>
                                        <li class="page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . "3"; ?>"><?php echo "3"; ?></a></li>
                                    <?php elseif($pageNumber > $pagination->pagesNumber() - 2):  ?>
                                        <li class="page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . ($pagination->pagesNumber() - 2); ?>"><?php echo ($pagination->pagesNumber() - 2); ?></a></li>
                                        <li class="<?php if($pageNumber == $pagination->pagesNumber() - 1) echo "tg-active";  ?> page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . ($pagination->pagesNumber() - 1); ?>"><?php echo ($pagination->pagesNumber() - 1); ?></a></li>
                                    <?php else: ?>
                                        <li class="page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . ($pageNumber - 1); ?>"><?php echo ($pageNumber - 1); ?></a></li>
                                        <li class="tg-active page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . $pageNumber; ?>"><?php echo $pageNumber; ?></a></li>
                                        <li class="page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . ($pageNumber + 1); ?>"><?php echo ($pageNumber + 1); ?></a></li>
                                    <?php endif; ?>

                                    <?php if($pageNumber <= $pagination->pagesNumber() - 2): ?>
                                        <li class="tg-prevpage page-item disabled"> <span> ... </span> </li>
                                    <?php endif; ?>

                                <?php }else if($pagination->pagesNumber() == 3){ ?>
                                    <li class="<?php if($pageNumber == 2) echo "tg-active";  ?> page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . "2"; ?>"><?php echo "2"; ?></a></li>
                                <?php } ?>

                                <?php if($pagination->pagesNumber() >= 2): ?>
                                    <li class="<?php if($pageNumber == $pagination->pagesNumber()) echo "tg-active";  ?> page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . $pagination->pagesNumber(); ?>"><?php echo $pagination->pagesNumber(); ?></a></li>
                                <?php endif; ?>
                                <?php if($pageNumber != $pagination->pagesNumber()): ?>
                                    <li class="tg-nextpage page-item"><a class="page-link" href="<?php echo "public_figures.php?page_number=" . $pagination->next(); ?>"><i class="fa fa-angle-right"></i></a></li>
                                <?php endif; ?>
                            </ul>
                        </nav>

                </div>

                </div>

            </div>

        </div>

    </div>

</div>



<div class="modal fade" id="EstbChangeStatusModel" style="z-index: 1042">
<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <form id="edit_estb_form" action="" method="POST">

            <input type="hidden" name="event_id" id="edit_event_id" >

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">×</button>

                <h4 class="modal-title">Change Establishment</h4>

            </div>

            <div class="modal-body">

                <div id="form-wrapper">

                    <div class="col-xs-6">

                        <div class="form-group" style="">

                            <input type="text" required="true"  class="form-control" value="admin@iscnsystem.org" name="sender"/>

                        </div>

                    </div>

                    <div class="col-xs-6">

                        <div class="form-group" style="">

                            <input type="text" required="true" class="form-control" placeholder="Email Subject" name="subject"/>

                        </div>

                    </div>

                    <div class="col-xs-6">

                        <div class="form-group" style="height: auto;">

                            <select required="required" class="form-control" name="estb-action" id="EstbStatusChange">

                                <option value="new">Choose Status</option>

                                <option value="missing_fields">Missing Information</option>

                                <option value="under_process">Under Process</option>

                                <option value="approved">Approved</option>

                            </select>
                        </div>
                    </div>

                    <div class="col-xs-6">

                        <div class="form-group" style="height: auto;">

                            <input type="text" required="true" class="form-control" placeholder="Note" name="note"/>

                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="col-xs-12">

                        <div  id="emailBodyWrapper" class="form-group" style="height: auto;">

                            <textarea class="form-control"  style="width: 100%; padding: 10px 15px; border: 1px solid #ddd;" id="EmailBody" name="email-body" rows="10" cols="80" placeholder="Email Body..."></textarea>

                        </div>

                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>

            </div>



            <div class="modal-footer">

                <button type="submit" name="btn-estb-status" class="btn btn-success btn-sm">Yes</button>

                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>

            </div>

        </form>

    </div>

</div>



</div> <!-- End: modal-second -->




<?php require_once("include/footer.php"); ?><!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script><!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wysihtml5/0.3.0/wysihtml5.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">



<!-- bootstrap wysihtml5 - text editor -->

<link rel="stylesheet" href=js/bootstrap3-wysihtml5.min.css">

<!--  -->

<script src="js/bootstrap3-wysihtml5.all.min.js"></script>



<script>

    $(".btn-estb-status").click(function () {
        var id=$(this).data("id");
        $("#edit_event_id").val(id);

    });

    // $(function(){

    //     $(".chosen").chosen(); 

    // });

    $('#emailBody').wysihtml5();

    $('#EmailBody').wysihtml5();

    $('#EmailBodycert').wysihtml5();

    // $('#message').wysihtml5();

    // $('#RequestMsg').wysihtml5();

    // $('#paragraph1').wysihtml5();

    // $('#paragraph2').wysihtml5();

    // $('#paragraph3').wysihtml5();



    $('#EstbStatusChange').on('change', function() {

    var status = $('#EstbStatusChange :selected').val();

    if (status == "missing_fields" ) {

        $("[name = 'subject']").val('Establishment Details InComplete : From ISCN');

        $("[name = 'note']").val('Establishment Details InComplete : Check Your Email for Details.');

        $("#emailBodyWrapper").removeClass('hide');



        missingInfoEmailMsg = "<p>Hi</p><p>Hope this email find you well.</p><p>Kindly we would like to inform you that your establishment is incomplete.</p><p>Your application will not be reviewed by the admissions committee until it is complete.</p><p>Please send required information to the following email: missing@iscnsystem.org.</p><p>............ Required information..............................</p><p><i>Do not forget to add the following application number to the email:</i></p><p>Application number ... </p><p>Thanks in advance.</p><p>Best Regards,</p><p>International Standard Certificate Number (ISCN) team.</p><p>www.iscnsystem.org</p>";

        $('iframe').contents().find('.wysihtml5-editor').html(missingInfoEmailMsg);



    }

    else if (status == "under_process" ) {

        $("[name = 'subject']").val('Establishment Under Process : From ISCN');

        $("[name = 'note']").val('Under Process : ISCN is Working on Your Establishment Data.');

        $("#emailBodyWrapper").removeClass('hide');



        underProcessEmailMsg = "<p>Hi</p><p>Hope this email find you well.</p><p>Kindly we would like to inform you that your establishment is now under process.</p><p>You will get an approval email along with your ISCN number as your establishment approved.</p><p>Application number ... </p><p>Thanks in advance.</p><p>Best Regards,</p><p>International Standard Certificate Number (ISCN) team.</p><p>www.iscnsystem.org</p>";

        $('iframe').contents().find('.wysihtml5-editor').html(underProcessEmailMsg);

    }

    else if (status == "approved") {

        confirm("Check its payment status and all details carefully ! If everything is fine then press OK and then Yes.")

        $("[name = 'subject']").val('Congratulation Your Establishment Approved : From ISCN');

        $("[name = 'note']").val('Approved : Check Your Email for ISCN number : From ISCN');

        $("#emailBodyWrapper").addClass('hide');

    }

    });


</script>