
<div id="wrapper">
    <!-- MainNavWrap -->
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <li class="nav-header">
                    <div class="dropdown profile-element">
                            <span>
                                <img alt="image" class="img-circle center-block" src="../img/img-01.jpg" style="max-width: 100px" />
                            </span>
                        <a data-toggle="dropdown" class="dropdown-toggle text-center" href="#">
                                <span class="clear">

                                    <span class="text-muted text-xs block"> <?php echo $admin_name; ?>                                        <b class="caret"></b>
                                    </span>
                                </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">


                            <li class="divider"></li>
                            <li>
                                <a href="include/logout.php"> Logout</a>
                            </li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        P V
                    </div>
                </li>


                <li class="home">
                    <a href="index.php">
                        <i class="fas fa-home"></i>
                        <span class="nav-label">Home </span>
                    </a>
                </li>

                <li class="categories">
                    <a href="show_categories.php">
                        <i class="fas fa-window-restore"></i>
                        <span class="nav-label">Show Categories </span>
                    </a>
                </li>

                <li class="categories">
                    <a href="categories.php">
                        <i class="fas fa-window-restore"></i>
                        <span class="nav-label"> Edit Categories </span>
                    </a>
                </li>

                <li class="events">
                    <a href="public_figures.php">
                        <i class="fas fa-window-restore"></i>
                        <span class="nav-label"> public figures </span>
                    </a>
                </li>

                <!-- <li class="events">
                    <a href="visitors.php">
                        <i class="fas fa-globe"></i>
                        <span class="nav-label">Website Visitors </span>
                    </a>
                </li> -->

                <li>
                    <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Website Content </span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">

                        <li class=""><a href="about_us.php"><i class="fas fa-window-restore"></i> About Public Figure </a></li>
                        <li class=""><a href="about_iscn.php"><i class="fas fa-window-restore"></i> About ISCN </a></li>
                        <!-- <li class="event_terms_conditions_active"><a href="event_terms_conditions.php"><i class="fas fa-file-signature"></i>Event Terms $ Conditions </a></li>
                        <li class="event_terms_of_use_active"><a href="event_terms_of_use.php"><i class="fas fa-users-cog"></i>Event Terms Of Use </a></li>
                        <li class="event_use_of_cookies_active"><a href="event_use_of_cookies.php"><i class="fas fa-thumbtack"></i>Event Use Of Cookies</a></li>
                        <li class="event_data_policy_active"><a href="event_data_policy.php"><i class="fas fa-info-circle"></i>Event Data Policy </a></li>
                        <li class="event_training"><a href="event_training.php"><i class="fas fa-language"></i>Event Training & Brochure</a></li> -->

                        <!--                        <li>-->
<!--                            <a href="#">establishment News <span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-third-level">-->
<!--                                <li>-->
<!--                                    <a href="est_add_news.php">Add New</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="est_news.php">All News</a>-->
<!--                                </li>-->
<!--                               -->
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!--                          <li>-->
<!--                            <a href="#">establishment FAQ <span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-third-level">-->
<!--                                <li>-->
<!--                                    <a href="est_add_faq.php">Add FAQ</a>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="est_faq.php">All FAQ</a>-->
<!--                                </li>-->
<!--                               -->
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!--                        <li>-->
<!--                            <a href="#">Home content <span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-third-level">-->
<!--                                <li class="est_why_need_active"><a href="est_top_left.php"><i  class="fas fa-edit"></i>Top Left Body</a></li>-->
<!--                                <li class="est_why_need_active"><a href="est_top_right.php"><i  class="fas fa-edit"></i>Top Right Body</a></li>-->
<!--                                <li class="est_how_to_book_active"><a href="est_how_to_book.php"><i  class="fas fa-edit"></i>How To Book</a></li>-->
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!---->
<!--                        <li>-->
<!--                            <a href="#">About us <span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-third-level">-->
<!---->
<!--                                <li class="est_what_is_verified_active"><a href="est_what_is_verified.php"><i class="fa fa-question"></i>What is for establishment ?</a></li>-->
<!--                                <li class="est_who_we_are_active"><a href="est_who_we_are.php"><i class="fa fa-question-circle"></i>Who we are ?</a></li>-->
<!--                                <li class="est_benefits_active"><a href="est_benefits.php"><i  class="fas fa-edit"></i>Benefits for establishment</a></li>-->
<!--                                <li class="est_id_mechanisms_active"><a href="est_mechanisms.php"><i class="fab fa-medapps"></i>Est Mechanisms</a></li>-->
<!--                                <li class="est_verified_community_active"><a href="est_verified_community.php"><i class="fa fa-users"></i>establishment community</a></li>-->
<!--                                <li class="est_training"><a href="est_training.php"><i class="fas fa-language"></i>establishment Training & Brochure</a></li>-->
<!---->
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!---->
<!---->
<!--                        <li>-->
<!--                            <a href="#"> Terms & privacy <span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-third-level">-->
<!--                                <li class="est_data_quality_policy_active"><a href="est_data_quality_policy.php"><i class="fas fa-database"></i>establishment Data Quality Policy </a></li>-->
<!--                                <li class="est_privacy_policy_active"><a href="est_privacy_policy.php"><i class="fas fa-lock"></i>establishment Privacy Policy </a></li>-->
<!--                                <li class="est_terms_conditions_active"><a href="est_terms_conditions.php"><i class="fas fa-file-signature"></i>establishment Terms $ Conditions </a></li>-->
<!--                                <li class="est_terms_of_use_active"><a href="est_terms_of_use.php"><i class="fas fa-users-cog"></i>establishment Terms Of Use </a></li>-->
<!--                                <li class="est_use_of_cookies_active"><a href="est_use_of_cookies.php"><i class="fas fa-thumbtack"></i>establishment Use Of Cookies</a></li>-->
<!--                                <li class="est_data_policy_active"><a href="est_data_policy.php"><i class="fas fa-info-circle"></i>establishment Data Policy </a></li>-->
<!--                                <li class="est_fees"><a href="est_fees.php"><i class="fas fa-dollar-sign"></i>establishment fees</a></li>-->
<!---->
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!--                        <li>-->
<!--                            <a href="#">About & contacts<span class="fa arrow"></span></a>-->
<!--                            <ul class="nav nav-third-level">-->
<!---->
<!--                                <li class="est_social_media_active"><a href="est_social_media.php"><i  class="fas fa-sign-out-alt"></i>Est Social Media</a></li>-->
<!---->
<!--                                <li class="est_about_us_active"><a href="est_about_us.php"><i class="fa fa-credit-card"></i>About iscn system</a></li>-->
<!---->
<!---->
<!---->
<!---->
<!--                            </ul>-->
<!--                        </li>-->
<!---->
<!---->
<!---->
<!---->
<!--                  <li class="est_downloading"><a href="est_downloading.php"><i class="fas fa-download"></i>establishment Download</a></li>-->
<!---->
<!--                  <li class="est_intoduaction"><a href="est_intoduaction.php"><i class="fas fa-info-circle"></i>establishment Intoduaction</a></li>-->
<!---->
<!--                  <li class="est_verifying_traing"><a href="est_verifying_traing.php"><i class="fas fa-hand-rock"></i>establishment Verifying Traing</a></li>-->
<!---->
<!--                  <li class="est_training_content"><a href="est_training_content.php"><i class="fas fa-toolbox"></i>establishment Training Content</a></li>-->
<!---->
<!--                  <li class="est_training_event"><a href="est_training_event.php"><i class="far fa-calendar-alt"></i>establishment Training Training Event</a></li>-->
<!--                  -->
<!--                  <li class="est_verification"><a href="est_verification.php"><i class="fas fa-user-lock"></i>establishment Verification</a></li>-->
<!--                  <li ><a href="est_features_guide.php"><i class="fas fa-user-lock"></i>Feature Guide</a></li>-->
<!--                    </ul>-->
<!--                </li>-->





            </ul>

        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                        <i class="fa fa-bars"></i>
                    </a>

                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="include/logout.php">
                            <i class="fas fa-sign-out-alt"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

