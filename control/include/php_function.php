<?php
ob_start();
//ini_set("display_errors", TRUE); // Report all errors
error_reporting( E_ALL & ~E_NOTICE & ~E_WARNING );
include '../db-connect.php';



if(isset($_POST['btn_save_est_downloading']) )
{
    GLOBAL $connection;

    $file=$_FILES['est_downloading'];



    if(isset($_FILES['est_downloading']) && !(empty($_FILES['est_downloading']['name'])) && !(empty($_FILES['est_downloading']['tmp_name'])) && $_FILES['est_downloading']['size']>0)
    {
        $imgname=$_FILES['est_downloading']['name'];
        $imgtmp=$_FILES['est_downloading']['tmp_name'];
        $imgerror=$_FILES['est_downloading']['error'];
        $imgsize=$_FILES['est_downloading']['size'];
        $imgtype=$_FILES['est_downloading']['type'];
        $fileExt=explode('.', $imgname);
        $fileActualExt=strtolower(end($fileExt)) ;
        $allowed=array('pdf','docx');



        if(in_array($fileActualExt, $allowed))
        {

            $filenamenew=uniqid('',true).".".$fileActualExt;
            $filedestination='../est_downloading/'.$filenamenew;
            $db_destination=$filenamenew;
            move_uploaded_file($imgtmp, $filedestination);

            $estUpdateQry = "UPDATE  event_website_info  SET ";
            $estUpdateQry .= " event_website_info.est_downloading = '$db_destination' ";

            $estUpdateQry .= "WHERE  event_website_info.id =1";


            $estUpdateRun=mysqli_query($connection,$estUpdateQry);



            if($estUpdateRun)
            {
                header('Location: ../est_downloading.php');
                exit();
            }
            else
            {
                echo mysqli_error($connection);
            }
        }


        else
        {
            header('Location: ../est_downloading.php?fail');
            exit();
        }


    }
    else
    {
        header('Location: ../est_downloading.php?no');
        exit();
    }

}// end btn_save_est_downloading


if(isset($_POST['btn_save_s1_est']) )
{
    GLOBAL $connection;

    $file=$_FILES['s1_img'];



    if(isset($_FILES['s1_img']) && !(empty($_FILES['s1_img']['name'])) && !(empty($_FILES['s1_img']['tmp_name'])) && $_FILES['s1_img']['size']>0)
    {
        $imgname=$_FILES['s1_img']['name'];
        $imgtmp=$_FILES['s1_img']['tmp_name'];
        $imgerror=$_FILES['s1_img']['error'];
        $imgsize=$_FILES['s1_img']['size'];
        $imgtype=$_FILES['s1_img']['type'];
        $fileExt=explode('.', $imgname);
        $fileActualExt=strtolower(end($fileExt)) ;
        $allowed=array('jpg','jpeg','png');



        if(in_array($fileActualExt, $allowed))
        {

            $filenamenew=uniqid('',true).".".$fileActualExt;
            $filedestination='../image_event_training/'.$filenamenew;
            $db_destination=$filenamenew;
            move_uploaded_file($imgtmp, $filedestination);



            $s1_header=$_POST['s1_header'];
            $s1_header= mysqli_real_escape_string($connection,$s1_header);

            $s1_paragraph=$_POST['s1_paragraph'];
            $s1_paragraph= mysqli_real_escape_string($connection,$s1_paragraph);


            $estUpdateQry = "UPDATE event_website_info  SET ";
            $estUpdateQry .= "event_website_info.s1_img = '$db_destination', ";
            $estUpdateQry .= "event_website_info.s1_header = '$s1_header' ,";
            $estUpdateQry .= "event_website_info.s1_paragraph = '$s1_paragraph' ";
            $estUpdateQry .= "WHERE event_website_info.id =1";


            $estUpdateRun=mysqli_query($connection,$estUpdateQry);



            if($estUpdateRun)
            {
                header('Location: ../event_training.php');
                exit();
            }
            else
            {
                echo mysqli_error($connection);

            }
        }
        else
        {
            echo "no correct img";
        }
    }
    else
    {
        $s1_header=$_POST['s1_header'];
        $s1_header= mysqli_real_escape_string($connection,$s1_header);

        $s1_paragraph=$_POST['s1_paragraph'];
        $s1_paragraph= mysqli_real_escape_string($connection,$s1_paragraph);


        $estUpdateQry = "UPDATE event_website_info  SET ";
        $estUpdateQry .= "event_website_info.s1_header = '$s1_header' ,";
        $estUpdateQry .= "event_website_info.s1_paragraph = '$s1_paragraph' ";
        $estUpdateQry .= "WHERE event_website_info.id =1 ";


        $estUpdateRun=mysqli_query($connection,$estUpdateQry);



        if($estUpdateRun)
        {
            header('Location: ../event_training.php');
            exit();
        }
        else
        {
            echo "error";
        }
    }

}// end btn_save_s1_est

if(isset($_POST['btn_save_s2_est']) )
{
    GLOBAL $connection;

    $file=$_FILES['s2_img'];



    if(isset($_FILES['s2_img']) && !(empty($_FILES['s2_img']['name'])) && !(empty($_FILES['s2_img']['tmp_name'])) && $_FILES['s2_img']['size']>0)
    {
        $imgname=$_FILES['s2_img']['name'];
        $imgtmp=$_FILES['s2_img']['tmp_name'];
        $imgerror=$_FILES['s2_img']['error'];
        $imgsize=$_FILES['s2_img']['size'];
        $imgtype=$_FILES['s2_img']['type'];
        $fileExt=explode('.', $imgname);
        $fileActualExt=strtolower(end($fileExt)) ;
        $allowed=array('jpg','jpeg','png');



        if(in_array($fileActualExt, $allowed))
        {

            $filenamenew=uniqid('',true).".".$fileActualExt;
            $filedestination='../image_event_training/'.$filenamenew;
            $db_destination=$filenamenew;
            move_uploaded_file($imgtmp, $filedestination);
        }


        $s2_header=$_POST['s2_header'];
        $s2_header= mysqli_real_escape_string($connection,$s2_header);

        $s2_paragraph=$_POST['s2_paragraph'];
        $s2_paragraph= mysqli_real_escape_string($connection,$s2_paragraph);






        $estUpdateQry = "UPDATE event_website_info  SET ";
        $estUpdateQry .= "event_website_info.s2_img = '$db_destination', ";
        $estUpdateQry .= "event_website_info.s2_header = '$s2_header' ,";
        $estUpdateQry .= "event_website_info.s2_paragraph = '$s2_paragraph' ";
        $estUpdateQry .= "WHERE event_website_info.id =1";


        $estUpdateRun=mysqli_query($connection,$estUpdateQry);



        if($estUpdateRun)
        {
            header('Location: ../event_training.php');
            exit();
        }
        else
        {
            echo mysqli_error($connection);
        }
    }
    else
    {
        $s2_header=$_POST['s2_header'];
        $s2_header= mysqli_real_escape_string($connection,$s2_header);

        $s2_paragraph=$_POST['s2_paragraph'];
        $s2_paragraph= mysqli_real_escape_string($connection,$s2_paragraph);




        $estUpdateQry = "UPDATE event_website_info  SET ";
        $estUpdateQry .= "event_website_info.s2_header = '$s2_header' ,";
        $estUpdateQry .= "event_website_info.s2_paragraph = '$s2_paragraph' ";
        $estUpdateQry .= "WHERE event_website_info.id =1";


        $estUpdateRun=mysqli_query($connection,$estUpdateQry);



        if($estUpdateRun)
        {
            header('Location: ../event_training.php');
            exit();
        }
        else
        {
            echo "error";
        }
    }

}// end btn_save_s2_est

if(isset($_POST['btn_save_s3_est']) )
{
    GLOBAL $connection;

    $s3_video=$_POST['s3_video'];
    $s3_video= mysqli_real_escape_string($connection,$s3_video);


    $s3_header=$_POST['s3_header'];
    $s3_header= mysqli_real_escape_string($connection,$s3_header);

    $s3_paragraph=$_POST['s3_paragraph'];
    $s3_paragraph= mysqli_real_escape_string($connection,$s3_paragraph);


    $estUpdateQry = "UPDATE event_website_info  SET ";
    $estUpdateQry .= "event_website_info.s3_video = '$s3_video', ";
    $estUpdateQry .= "event_website_info.s3_header = '$s3_header' ,";
    $estUpdateQry .= "event_website_info.s3_paragraph = '$s3_paragraph' ";
    $estUpdateQry .= "WHERE event_website_info.id =1 ";


    $estUpdateRun=mysqli_query($connection,$estUpdateQry);



    if($estUpdateRun)
    {
        header('Location: ../event_training.php');
        exit();
    }
    else
    {
        echo mysqli_error($connection);
    }

}// end btn_save_s3_est


if(isset($_POST['btn_save_est_new']))
{
    GLOBAL $connection;

    $title_est_news         =$_POST['title_est_news'];
    $title_est_news =mysqli_real_escape_string($connection,$title_est_news);

    $url_est_news       =$_POST['url_est_news'];
    $url_est_news =mysqli_real_escape_string($connection,$url_est_news);



    $image_est_news     =$_FILES['image_est_news'];

    $imgname=$_FILES['image_est_news']['name'];
    $imgtmp=$_FILES['image_est_news']['tmp_name'];
    $imgerror=$_FILES['image_est_news']['error'];
    $imgsize=$_FILES['image_est_news']['size'];
    $imgtype=$_FILES['image_est_news']['type'];
    $fileExt=explode('.', $imgname);
    $fileActualExt=strtolower(end($fileExt)) ;
    $allowed=array('jpg','jpeg','png');



    if(in_array($fileActualExt, $allowed))
    {

        $filenamenew=uniqid('',true).".".$fileActualExt;
        $filedestination='../image_est_news/'.$filenamenew;
        $db_destination=$filenamenew;
        move_uploaded_file($imgtmp, $filedestination);


    }

    $datetime_est_news  =$_POST['datetime_est_news'];
    if($datetime_est_news=="")
    {
        $datetime_est_news=date('Y-m-d H:i:s');
    }


    $body      =$_POST['body'];
    $body =mysqli_real_escape_string($connection,$body);



    $est_news="INSERT INTO est_news (title,body,img,url,time_news) 
                VALUES ('$title_est_news','$body','$db_destination','$url_est_news','$datetime_est_news')";
    $est_news_query=mysqli_query($connection,$est_news);
    if($est_news_query)
    {
        //$_SESSION['error']="message has sent!";
        header('Location: ../est_news.php');
        exit();
    }
    else
    {
        echo mysqli_error($connection);
    }

} // end btn_save_est_new


if(isset($_POST['btn_save_edit_est_new']))
{
    $id=$_POST['news_id'];
    $title_est_edit_news         =$_POST['title_est_edit_news'];
    $image_est_edit_news     =$_FILES['image_est_edit_news'];


    if(isset($_FILES['image_est_edit_news']) && !(empty($_FILES['image_est_edit_news']['name'])) && !(empty($_FILES['image_est_edit_news']['tmp_name'])) && $_FILES['image_est_edit_news']['size']>0) {


        $imgname = $_FILES['image_est_edit_news']['name'];
        $imgtmp = $_FILES['image_est_edit_news']['tmp_name'];
        $imgerror = $_FILES['image_est_edit_news']['error'];
        $imgsize = $_FILES['image_est_edit_news']['size'];
        $imgtype = $_FILES['image_est_edit_news']['type'];
        $fileExt = explode('.', $imgname);
        $fileActualExt = strtolower(end($fileExt));
        $allowed = array('jpg', 'jpeg', 'png');


        if (in_array($fileActualExt, $allowed)) {

            $filenamenew = uniqid('', true) . "." . $fileActualExt;
            $filedestination = '../image_est_news/' . $filenamenew;
            $db_destination = $filenamenew;
            move_uploaded_file($imgtmp, $filedestination);





            $body_est_edit_news = $_POST['body_est_edit_news'];
            $body_est_edit_news = mysqli_real_escape_string($connection, $body_est_edit_news);
            $url_est_edit_news = $_POST['url_est_edit_news'];
            $datetime_est_edit_news=$_POST['datetime_est_edit_news'];

            $estUpdateQry = "UPDATE est_news  SET ";
            $estUpdateQry .= "est_news.title = '$title_est_edit_news', ";
            $estUpdateQry .= "est_news.body = '$body_est_edit_news' ,";
            $estUpdateQry .= "est_news.img = '$db_destination' ,";
            $estUpdateQry .= "est_news.url = '$url_est_edit_news' ,";
            $estUpdateQry .= "est_news.time_news = '$datetime_est_edit_news' ";

            $estUpdateQry .= "WHERE est_news.id ='$id' ";


            $est_news_query = mysqli_query($connection, $estUpdateQry);
            if ($est_news_query) {
                //$_SESSION['error']="message has sent!";
                header('Location: ../est_news.php');
                exit();
            } else {
                echo mysqli_error($connection);
            }
        }
        else
        {
            echo "error";
        }
    }

    else
    {
        $id=$_POST['news_id'];
        $title_est_edit_news         =$_POST['title_est_edit_news'];

        $body_est_edit_news = $_POST['body_est_edit_news'];
        $body_est_edit_news = mysqli_real_escape_string($connection, $body_est_edit_news);

        $url_est_edit_news = $_POST['url_est_edit_news'];
        $datetime_est_edit_news = $_POST['datetime_est_edit_news'];


        $estUpdateQry = "UPDATE est_news  SET ";
        $estUpdateQry .= "est_news.title = '$title_est_edit_news', ";
        $estUpdateQry .= "est_news.body = '$body_est_edit_news' ,";
        $estUpdateQry .= "est_news.url = '$url_est_edit_news' ,";
        $estUpdateQry .= "est_news.time_news = '$datetime_est_edit_news' ";

        $estUpdateQry .= "WHERE est_news.id ='$id' ";


        $est_news_query = mysqli_query($connection, $estUpdateQry);

        if ($est_news_query) {
            //$_SESSION['error']="message has sent!";
            header('Location: ../est_news.php');
            exit();
        } else {
            echo mysqli_error($connection);
        }
    }

} // end btn_save_edit_est_new

if (isset($_POST['btn_cancel_edit_est_new']))
{

    header('Location: ../est_news.php');
    exit();

} // end btn_cancel_edit_est_new


if (isset($_POST['btn_cancel_est_new']))
{

    header('Location: ../est_news.php');
    exit();

} // end btn_cancel_new


if(isset($_POST['btn_save_est_faq']))
{
    $est_question_faq        =$_POST['est_question_faq'];
    $est_question_faq       =mysqli_real_escape_string($connection,$est_question_faq);

    $est_answer_faq       =$_POST['est_answer_faq'];
    $est_answer_faq     =mysqli_real_escape_string($connection,$est_answer_faq);

    $est_status_faq= $_POST['status'];

    $datetime_est_faq =date('Y-m-d H:i:s');

    $est_faq="INSERT INTO est_faq (question,answer,`time`,status) 
                VALUES ('$est_question_faq','$est_answer_faq','$datetime_est_faq','$est_status_faq')";

    $est_faq_query=mysqli_query($connection,$est_faq);
    if($est_faq_query)
    {
        //$_SESSION['error']="message has sent!";
        header('Location: ../est_faq.php');
        exit();
    }
    else
    {
        echo mysqli_error($connection);
    }

} // end btn_save_est_faq



if (isset($_POST['btn_cancel_est_faq']))
{

    header('Location: ../est_faq.php');
    exit();

} // end btn_cancel_est_faq



if(isset($_POST['btn_save_edit_est_faq']))
{
    $est_question_faq     =$_POST['est_question_faq'];

    $id=$_POST['faq_id'];
    $est_answer_faq      =$_POST['est_answer_faq'];
    $est_answer_faq= mysqli_real_escape_string($connection,$est_answer_faq);


    $datetime_est_faq =date('Y-m-d H:i:s');
    $est_status_faq= $_POST['status'];

    $est_question_faq= mysqli_real_escape_string($connection,$est_question_faq);

    $estUpdateQry = "UPDATE est_faq  SET ";
    $estUpdateQry .= " est_faq.question = '$est_question_faq' ,";
    $estUpdateQry .= " est_faq.answer = '$est_answer_faq' ,";
    $estUpdateQry .= " est_faq.time = '$datetime_est_faq' ,";
    $estUpdateQry .= " est_faq.status = '$est_status_faq' ";
    $estUpdateQry .= "WHERE  est_faq.id = '$id'";


    $est_faq_query=mysqli_query($connection,$estUpdateQry);
    echo $est_faq_query;
    if($est_faq_query)
    {
        //$_SESSION['error']="message has sent!";
        header('Location: ../est_faq.php');
        exit();
    }
    else
    {
        echo mysqli_error($connection);
    }

} // end btn_save_edit_est_faq

if (isset($_POST['btn_cancel_edit_est_faq']))
{

    header('Location: ../est_faq.php');
    exit();

} // end btn_cancel_edit_est_faq


?>