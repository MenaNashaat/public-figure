<?php

ob_start();

//ini_set("display_errors", TRUE); // Report all errors

error_reporting( E_ALL & ~E_NOTICE & ~E_WARNING );

// Report all errors except E_NOTICE and E_WARNING

//mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);





date_default_timezone_set( 'Asia/Dubai' );

define('NOW', date('Y-d-m h:i:s A'));

/*-----------------------------------------------------------

                    GLOBAL FUNCTIONSbtn-login-est

 ---------------------------------------------------------- */



/*----------------- redirect_to ----------------- */

/*

* This is a detailed explanation

* of the below function.

*/

function redirect_to( $newLocation )

{

    header( "Location: " . $newLocation );

    exit( );

}



/*----------------- mysql_prep ----------------- */

/*

* This is a detailed explanation

* of the below function.

*/

function mysql_prep( $string )

{

    global $connection;

    $escapeString = mysqli_real_escape_string( $connection, $string );

    return $escapeString;

}



function safe_string($string){

    $safeString = filter_var($string, FILTER_SANITIZE_STRING);

    return $safeString;

}

function safe_email($string){

    $safeString = filter_var($string, FILTER_SANITIZE_EMAIL);

    return $safeString;

}

function safe_int($string){

    $safeInt = filter_var($string, FILTER_SANITIZE_NUMBER_INT);

    return $safeInt;

}

function safe_float($string){

    //$safeFloat = var_dump(filter_var($string, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));

    $safeFloat = filter_var($string, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    return $safeFloat;

}

/*----------------- get_file_ext($file) ----------------- */

function get_file_ext($file)

{

    $ext = pathinfo($file, PATHINFO_EXTENSION);

    return $ext ? $ext : false;

}



/*----------------- confirm_query ----------------- */

/*

* This is a detailed explanation

* of the below function.

*/

function confirm_query( $resultSet )

{

    if ( !$resultSet ) {

        //die("Database query failed.");

        echo $query;

    } //!$resultSet

}



/*----------------- remove_white_spaces ----------------- */

/*

* This is a detailed explanation

* of the below function.

*/

function remove_white_spaces( $data )

{

    $data = trim( $data );

    $data = stripslashes( $data );

    $data = htmlspecialchars( $data );

    $data = preg_replace('/\s+/', '', $data);

    return $data;

}





/*----------------- total_rows ----------------- */

/*

* This is a detailed explanation

* of the below function.

*/



////////////////////////////////////////////////// for phd  /////////////////////////////////

function total_phd($tbl, $col, $key)

{

    global $connection;

    if (is_string($key)) { $key = "'$key'";  }

    $qry  = "SELECT COUNT(*) AS totalElements FROM $tbl WHERE $col = $key ";

    $qryRun = mysqli_query($connection, $qry);

    confirm_query( $qry );

    if ( $qryRun ) {

        $row = mysqli_fetch_assoc($qryRun);

        return $totalElements = $row["totalElements"];

    }

    else{

        return $errorMsg = "Error in Query. Contact your admin.";

    }

}



///////////////////////////////////////////////// end for phd ///////////////////////////////////////////////////////////////////////////////////////////////////////

function total_eng($tbl, $col, $key)

{

    global $connection;

    if (is_string($key)) { $key = "'$key'";  }

    $qry  = "SELECT COUNT(*) AS totalElements FROM $tbl WHERE $col = $key ";

    $qryRun = mysqli_query($connection, $qry);

    confirm_query( $qry );

    if ( $qryRun ) {

        $row = mysqli_fetch_assoc($qryRun);

        return $totalElements = $row["totalElements"];

    }

    else{

        return $errorMsg = "Error in Query. Contact your admin.";

    }

}



///////////////////////////////////////////////// end for eng ///////////////////////////////////////////////////////////////////////////////////////////////////////


function total_rows($tbl, $col, $key)

{

    global $connection;

    if (is_string($key)) { $key = "'$key'";  }

    $qry  = "SELECT COUNT(*) AS totalElements FROM $tbl WHERE $col = $key AND status <> 0";

    $qryRun = mysqli_query($connection, $qry);

    confirm_query( $qry );

    if ( $qryRun ) {

        $row = mysqli_fetch_assoc($qryRun);

        return $totalElements = $row["totalElements"];

    }

    else{

        return $errorMsg = "Error in Query. Contact your admin.";

    }

}



function total_sum($tbl,$sumCol,$whereCol,$key)

{

    global $connection;

    $qry = "SELECT SUM($sumCol) total FROM ( SELECT * FROM $tbl WHERE $whereCol = $key ) tmp";

    $qryRun = mysqli_query($connection,$qry);

    $row = mysqli_fetch_assoc($qryRun);

    return $row['total'];

}





/*----------------- search_tbl ----------------- */

/*

* Get total number of rows on the

* basis of one column and one key

*/

function search_tbl($tbl, $col, $key)

{

    global $connection;

    if (is_string($key)) { $key = "'$key'";  }

    $qry  = "SELECT id FROM $tbl WHERE $col = $key";

    $qryRun = mysqli_query($connection, $qry);

    confirm_query( $qry );

    if ( $qryRun ) {

        return  $totalElements = mysqli_num_rows($qryRun);

    }

    else{

        return $errorMsg = "Error in Query. Contact your admin.";

    }

}



/*----------------- get_all_from_tbl ----------------- */

/*

* Get all columns and all

* rows from a table

*/

function get_all_from_tbl( $tbl )

{

    global $connection;

    $safeTableName = mysqli_real_escape_string( $connection, $tbl );

    $query         = "SELECT * FROM `$safeTableName`";

    $recordSet     = mysqli_query( $connection, $query );

    confirm_query( $recordSet );

    if ( $recordSet ) {

        return $recordSet;

    } //$recordSet

    else {

        echo $query;

    }

}





/*----------------- get_all_from_tbl ----------------- */

/*

* Get complete one column from a table

* $tbl= table name, $column = Column Name

*/

function get_col_from_tbl( $tbl, $column )

{

    global $connection;

    $safeTableName = mysqli_real_escape_string( $connection, $tbl );

    $query         = "SELECT '$column' FROM `$safeTableName`";

    $recordSet     = mysqli_query( $connection, $query );

    confirm_query( $recordSet );

    if ( $recordSet ) {

        return $recordSet;

    } //$recordSet

    else {

        echo $query;

    }

}



/*----------------- get_all_cols_on_key ----------------- */

/*

* Get All records from a table

* on the basis of one key

*/

function get_all_cols_on_key( $tbl, $whereColumn, $key )

{

    global $connection;

    $safeTableName = mysqli_real_escape_string( $connection, $tbl );

    $safeKey       = mysqli_real_escape_string( $connection, $key );

    $MySQLQry      = "SELECT * FROM `$tbl` WHERE `$whereColumn` = $key";

    $recordSet     = mysqli_query( $connection, $MySQLQry );

    confirm_query( $recordSet );

    if ( $recordSet ) {

        return $recordSet;

    } //$recordSet

}





/*----------------- get_col_On_key ----------------- */

/*

* Get one column from a

* table on the basis of one key

*/

function get_col_On_key( $tbl, $col, $where, $key )

{

    global $connection;

    $tbl       = mysqli_real_escape_string( $connection, $tbl );

    $col       = mysqli_real_escape_string( $connection, $col );

    $where     = mysqli_real_escape_string( $connection, $where );

    $key       = mysqli_real_escape_string( $connection, $key );

    if (is_string($key)) { $key = "'$key'";  }

    $query     = "SELECT `$col` FROM `$tbl` WHERE `$where` = $key";

    confirm_query( $query );

    $queryRun  = mysqli_query( $connection, $query );

    if ($queryRun) {

        $row       = mysqli_fetch_assoc($queryRun);

        $data      =$row[$col];

        return $data;

    }



}



/*----------------- getUserByID ----------------- */

/*

* Get User complete record

* on the basis of it's ID

*/

function getUserByID( $userID )

{

    global $connection;

    session_start();

    $userID = $_SESSION['userID'];

    $userNameQuery    = "SELECT * FROM admin WHERE id = $userID";

    $userNameQueryRun = mysqli_query( $connection, $userNameQuery );

    if ( $userNameQueryRun ) {

        $Count = mysqli_num_rows( $userNameQueryRun );

        if ( $Count > 0 ) {

            return $userRecord = mysqli_fetch_assoc( $userNameQueryRun );

            exit();

        } //$Count > 0

        else {

            //now your are going to logout because user not existed of the recieved user ID

            session_start();

            unset( $_SESSION[ 'userID' ] );

            session_destroy();

            redirect_to( '../admin/index.php' );

            exit;

        }

    } // End: if ($userNameQueryRun) {

    else {

        session_start();

        $_SESSION[ 'msg' ]     = "Session has been expired. Please login.";

        $_SESSION[ 'msgType' ] = "0";

        redirect_to( '../admin/index.php' );

    }

}





/*----------------- validate_email ----------------- */

/*

* Validate Email and return

* error message if email is invalid

*/

function validate_email( $email )

{

    // Validate email

    if ( !filter_var( $email, FILTER_VALIDATE_EMAIL ) === false ) {

        return $email;

    } //!filter_var( $email, FILTER_VALIDATE_EMAIL ) === false

    else {

        return "$email is not a valid email address";

    }

}



/*----------------- btnLogout ----------------- */

/*

* Admin Logout

*/

if ( isset( $_POST['btnLogout' ])) {

    //now your are going to logout

    session_start();

    unset( $_SESSION['userID']);

    unset( $_SESSION['userType']);

    session_destroy();

    redirect_to( '../admin/index.php' );

    exit;

} //isset( $_POST[ 'btnLogout' ] )





/*----------------- btn-estb-logout ----------------- */

/*

* Logout Establishment

*/

if ( isset($_POST['btn-estb-logout' ])) {

    //now your are going to logout

    session_start();

    unset( $_SESSION['estbID'] );

    unset( $_SESSION['userID'] );

    unset( $_SESSION['userType'] );

    session_destroy();

    redirect_to('../index.php');

    exit;

} //isset( $_POST[ 'btnLogout' ] )





/*----------------- confirm_login ----------------- */

/*

* Confirm login

*/

function confirm_admin_login( )

{

    if ( session_status() == PHP_SESSION_NONE ) {

        // if the session not yet started

        session_start();

    } //empty( $_SESSION )

    if ($_SESSION['userID'] == "" || $_SESSION['userType']=="" || $_SESSION['userType']==NULL) {

        //if not yet logged in

        header( "Location: ../index.php" ); // send to login page

        exit;

    } //!isset( $_SESSION[ 'userID' ] )

    else{



        GLOBAL $connection;

        $id = $_SESSION['userID'];

        $query = "SELECT id FROM admin WHERE status = 1 AND designation = 'Administrator' AND id=$id";

        $queryRun = mysqli_query($connection, $query);

        if ($queryRun) {

            $count = mysqli_num_rows($queryRun);

            if ($count != 1) {

                header( "Location: ../index.php" ); // send to login page

            }

        }

        else{



            header( "Location: ../index.php" ); // send to login page

        }

    }

}







function confirm_login( )

{

    if ( session_status() == PHP_SESSION_NONE ) {

        // if the session not yet started

        session_start();

    } //empty( $_SESSION )

    if ($_SESSION['userID'] == "") {

        //if not yet logged in

        header( "Location: ../index.php" ); // send to login page

        exit;

    } //!isset( $_SESSION[ 'userID' ] )

    else{

        GLOBAL $connection;

        $id = $_SESSION['userID'];

        $userType = $_SESSION['userType'];

        $estbID = get_col_On_key('users','estb_id','id',$id);

        $query = "SELECT users.id,users.username,est.username FROM users INNER JOIN est WHERE 

        users.estb_id=est.id AND est.id=$estbID AND users.estb_id=$estbID AND users.id=$id AND users.status = 1 AND est.status = 4";

        $queryRun = mysqli_query($connection, $query);

        if ($queryRun) {

            $count = mysqli_num_rows($queryRun);

            $row = mysqli_fetch_assoc($queryRun);

            if ($count != 1 || $userType == "admin") {

                header( "Location: ../index.php" ); // send to login page

            }

        }

        else{

            header( "Location: ../index.php" ); // send to login page

        }

    }

}





/*----------------- random_password ----------------- */

/*

* Generate Randon Password

*/

function random_password( $passLength )

{

    $alphabet= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

    $pass    = array( ); //remember to declare $pass as an array

    $alphaLength = strlen( $alphabet ) - 1; //put the length -1 in cache

    for ( $i = 0; $i < $passLength; $i++ ) {

        $n       = rand( 0, $alphaLength );

        $pass[ ] = $alphabet[ $n ];

    } //$i = 0; $i < 8; $i++

    return implode( $pass ); //turn the array into a string

}





/*----------------- random_number ----------------- */

/*

* Generate Randon Number

*/

function random_number( $nmbrLength )

{

    $digits= '0123456789';

    $randomNmbr    = array( ); //remember to declare $randomNmbr as an array

    $alphaLength = strlen( $digits ) - 1; //put the length -1 in cache

    for ( $i = 0; $i < $nmbrLength; $i++ ) {

        $n       = rand( 0, $alphaLength );

        $randomNmbr[ ] = $digits[ $n ];

    } //$i = 0; $i < 8; $i++

    return implode( $randomNmbr ); //turn the array into a string

}



/*----------------- statusId_to_string ----------------- */

/*

* It recives an integar ( 0 -5) and return

* an string accordingly

*/

function statusId_to_string($status)

{

    switch ($status) {

        case 1:

            return $status = "Arrived";

            break;

        case 2:

            return $status = "Missing Data";

            break;

        case 3:

            return $status = "Under Process";

            break;

        case 4:

            return $status = "Approved";

            break;

        case 5:

            return $status = "Public";

            break;

        default:

            return $status = "Arrived";

            break;

    }

}



/*----------------- certId_to_string ----------------- */

/*

* Takes one argument which is Certificate ID(integer)

* Takes all Certificates names along with their IDs from Database

* Compare the recieved Certificate ID with all IDs that are taken

* from Database, when matched, this function return that Certificate

* name

*/

function certId_to_string($id)

{



    $certRun =  get_all_from_tbl('certificate_tbl');

    while ($certRow = mysqli_fetch_assoc($certRun)) {

        if ($id == $certRow['id']) {

            return $certRow['certificate_name'];

            exit();

        }

    }

}



/*------------------------------------------------------------------------------

                    Admin Side Functions

------------------------------------------------------------------------------*/





/*----------------- btnLoadNewCert ----------------- */

/*

* Load New Entries in Certificate Table

*/

if (isset($_POST['action']) && $_POST['action']=="btnLoadNewCert") {

    global $connection;

    $query = "SELECT cert.id AS certID,indv.first_name,indv.middle_name,indv.last_name FROM cert LEFT JOIN indv ON cert.indv_id = indv.id WHERE cert.status = 1 ORDER BY cert.id DESC";

    $result = mysqli_query($connection, $query);

    $count  = mysqli_num_rows($result);

    if ($count > 0 ) {

        $count = 'Arrived '.$count;

        $output = '';

        $output .='<li><a href="cert-list.php"><i class="fa fa-certificate"></i>View All</a></li>';

        while($row = mysqli_fetch_array($result)) {

            $fullName = $row["first_name"]." ".$row["middle_name"]." ".$row["last_name"];

            $output .= '<li><a href="cert-details.php?id='.$row['certID'].'"><i class="fa fa-circle-o"></i>'.$fullName.'</a></li>';

        }

    }

    else{

        $output .='<li><a href="cert-list.php"><i class="fa fa-certificate"></i>View All</a></li>';

        $output .= '<li><a href="#"><i class="fa fa-circle-o"></i>No New Entry</a></li>';

        $count = NULL;

    }

    $data = array(

        'output' => $output,

        'count'  => $count

    );

    echo json_encode($data);

}





/*----------------- btnCountNewCerts ----------------- */

/*

* Count New Entries in Certificate Table

*/

if (isset($_POST['action']) && $_POST['action']=="btnCountNewCerts") {

    global $connection;

    $query = "SELECT id FROM indv WHERE status = 1";

    $result = mysqli_query($connection, $query);

    $count  = mysqli_num_rows($result);

    if ($count < 1) {

        $count = NULL;

    } else{

        $count =$count;

    }

    $data = array(

        'count'  => $count

    );

    echo json_encode($data);

}





/*----------------- btnLoadNewEsts ----------------- */

/*

* Load New Entries in Establishment Table

*/

if (isset($_POST['action']) && $_POST['action']=="btnLoadNewEsts") {

    global $connection;

    $query = "SELECT id,name FROM est WHERE status = 1 ORDER BY id DESC";

    $result = mysqli_query($connection, $query);

    $count  = mysqli_num_rows($result);

    if ($count > 0 ) {

        $count = $count;

        $output = '';

        $output .='<li><a href="estb-list.php"><i class="fa fa-building"></i>View All</a></li>';

        while($row = mysqli_fetch_array($result)) {

            $output .= '<li><a href="estb-details.php?id='.$row["id"].'"><i class="fa fa-building"></i>'.$row["name"].'</a></li>';

        }

    }

    else{

        $output .='<li><a href="estb-list.php"><i class="fa fa-building"></i>View All</a></li>';

        $output .= '<li><a href="#"><i class="fa fa-building"></i>No New Entry</a></li>';

        $count = NULL;

    }

    $data = array(

        'output' => $output,

        'count'  => $count

    );

    echo json_encode($data);

}





/*----------------- btnCountNewEsts ----------------- */

/*

* Count New Entries in Establishment Table

*/

if (isset($_POST['action']) && $_POST['action']=="btnCountNewEsts") {

    global $connection;

    $query = "SELECT id FROM est WHERE status = 1";

    $result = mysqli_query($connection, $query);

    $count  = mysqli_num_rows($result);

    if ($count < 1) {

        $count = NULL;

    } else{

        $count = $count;

    }

    $data = array(

        'count'  => $count

    );

    echo json_encode($data);

}



/*============================================

=            Certificate Function            =

============================================*/



/*----------------- cert_action ----------------- */

/*

* This function will call when user want

* to change Certificate Status

*/

function cert_action($certID,$indiID,$page) {

    session_start();

    global $connection;

    echo '<hr>';

    echo "certID = ".$certID     = mysql_prep($certID);

    echo '<br>';

    echo "indiID = ".$indiID     = mysql_prep($indiID);

    echo '<br>';

    echo "page = ".$page       = mysql_prep($page);

    echo '<br>';



    if ($certID == "" || $indiID == "") {

        $_SESSION['msgType'] = "0";

        $_SESSION['msg'] = "Page was not fully loaded ! When page is fully loaded then procced !";

        if ($page=="cert-details.php") {

            header('Location: cert-details.php?id='.$certID);

            exit();

        }

        else {

            redirect_to('index.php');

            exit();

        }



    }



    $now = NOW;

    $userType = $_SESSION['userType'];

    $userID = $_SESSION['userID'];

    $action     = mysql_prep($_POST['status']);

    $currentStatus = get_col_On_key('cert', 'status', 'id', $certID );

    if ($currentStatus == 5) {

        // This Certificate is already Approved and Published

        // This Certificate is Locked and un changeable.

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']     = "Already Published and Approved !";

        if ($page=="cert-details.php") {

            header('Location: cert-details.php?id='.$certID);

            exit();

        }else{

            redirect_to('index.php');

            exit();

        }

    } // End: if ($currentStatus == 5 || $currentStatus == 4 ) {

    $sender   = mysql_prep( $_POST['sender'] );

    $subject  = mysql_prep( $_POST['subject'] );

    $note     = mysql_prep($_POST['note']);

    $message  = $_POST['message'];

    $message  = str_replace(array("\n", "\r"), '', $message);

    $message  = str_replace("<p></p>", '', $message);

    $message  = str_replace("<p><br></p>", '', $message);

    $message  = mysql_prep($message);

    $indvQry  = "SELECT ";

    $indvQry .= "indv.email, ";

    $indvQry .= "indv.first_name, ";

    $indvQry .= "indv.middle_name, ";

    $indvQry .= "indv.last_name ";

    $indvQry .= "FROM ";

    $indvQry .= "indv ";

    $indvQry .= "WHERE ";

    $indvQry .= "indv.id = $indiID";

    $indvRun = mysqli_query($connection, $indvQry);

    if ($indvRun) {

        // $indvQry run successfully.

        $indvRecord = mysqli_fetch_assoc($indvRun);

        $firstName = $indvRecord['first_name'];

        $middleName = $indvRecord['middle_name'];

        $lastName = $indvRecord['last_name'];

        $fullName = $firstName." ".$middleName." ".$lastName;

        $recieverEmail = $indvRecord['email'];

        $to = $recieverEmail;

        // Always set content-type when sending HTML email

        $headers = "MIME-Version: 1.0" . "\r\n";

        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers

        $Cc = "it.manager@iscnsystem.org";

        //$Cc = "jahangirkhattak13@gmail.com";

        $headers .= 'From: ' .$sender. "\r\n";

        $headers .= 'Cc: '.$Cc."\r\n";

        // If action = 2 || action == 3 || action == 5

        if ( $action == 2 || $action == 3 || $action == 5) {

            //echo "<br/>".$action;

            if ($action == 5) {

                // Want to Publish

                if ($currentStatus != 4) {

                    // Can't be Publish before Approval

                    // First Approve and then Publish

                    $_SESSION['msgType'] = "0";

                    $_SESSION['msg']     = "First Approve and then Publish !";

                    if ($page=="cert-details.php") {

                        header('Location: cert-details.php?id='.$certID);

                        exit();

                    }

                    else{

                        redirect_to('home.php');

                        exit();

                    }

                } // End: if ($currentStatus != 4) {

            } // End: if ($action == 5) {

            $updCertQry = "UPDATE cert, indv SET ";

            if($action != 5){

                $updCertQry .= "cert.code = '', ";

                $updCertQry .= "cert.unq_code = '', ";

                $updCertQry .= "cert.qr_image = '', ";

            }



            $updCertQry .= "cert.missing_note = '$note', ";

            $updCertQry .= "cert.modify_date = '$now', ";

            $updCertQry .= "cert.status = $action, ";

            $updCertQry .= "indv.status = $action, ";

            $updCertQry .= "indv.user = $userID ";

            $updCertQry .= "WHERE ";

            $updCertQry .= "cert.id = $certID ";

            $updCertQry .= "AND ";

            $updCertQry .= "indv.id = $indiID";

            $updCertRun = mysqli_query($connection, $updCertQry);

            if ($updCertRun) {

                // individual status changed successfully

                // Now save this email in Database



                $insertEmailQry = "INSERT INTO email_tbl( ";

                $insertEmailQry .= "carbon_copy, ";

                $insertEmailQry .= "body, ";

                $insertEmailQry .= "sender_email, ";

                $insertEmailQry .= "reciever_email, ";

                $insertEmailQry .= "user, ";

                $insertEmailQry .= "status ";

                $insertEmailQry .= ") VALUES ( ";

                $insertEmailQry .= "'$Cc', ";

                $insertEmailQry .= "'$message', ";

                $insertEmailQry .= "'$sender', ";

                $insertEmailQry .= "'$to', ";

                $insertEmailQry .= "$userID, ";

                $insertEmailQry .= "$action";

                $insertEmailQry .= ")";

                $insertEmailRun = mysqli_query($connection, $insertEmailQry);

                if ($insertEmailRun) {

                    if (mail($to,$subject,$message,$headers)) {

                        $_SESSION['msgType'] = "1";

                        $_SESSION['msg']  = "Certificate status changed successfully";

                        if ($page=="cert-details.php") {

                            header('Location: cert-details.php?id='.$certID);

                            exit();

                        }

                        else{

                            redirect_to('home.php');

                            exit();

                        }

                    } // End: if (mail(...)) {

                    else {

                        $_SESSION['msgType'] = "1";

                        $_SESSION['msg']="Certificate status changed successfully.Email not";

                        if ($page=="cert-details.php") {

                            header('Location: cert-details.php?id='.$certID);

                            exit();

                        }

                        else{

                            redirect_to('home.php');

                            exit();

                        }

                    } // End: else {

                }//End: $insertEmailRun

                else{

                    $insertEmailQry;

                    $_SESSION['msgType'] = "0";

                    $_SESSION['msg']=$insertEmailQry;

                    //"Query Failed. Try again or contact with ISCN!";

                    if ($page=="cert-details.php") {

                        header('Location: cert-details.php?id='.$certID);

                        exit();

                    }else{

                        redirect_to('home.php');

                        exit();

                    }

                }

            } // End: if ($updCertRun) {

            else{

                //echo "<br/>updCertQry = ".$updCertQry;

                $_SESSION['msgType'] = "0";

                $_SESSION['msg']=$updCertQry;

                //"Query Failed. Try again or contact with ISCN!";

                if ($page=="cert-details.php") {

                    header('Location: cert-details.php?id='.$certID);

                    exit();

                }else{

                    redirect_to('home.php');

                    exit();

                }

            }



        }//End: if ( $action == 2 || $action == 3 || $action == 5) {

        elseif ($action == 4) {

            // We are going to Aprrove this certificate

            // But first w check it's current status

            // If its present status = 4 = Approved;

            // then we give a message to user

            if ($currentStatus == 4) {

                $_SESSION['msgType'] = "0";

                $_SESSION['msg']     = "This certificate already approved !";

                if ($page=="cert-details.php") {

                    header('Location: cert-details.php?id='.$certID);

                    exit();

                }else{

                    redirect_to('home.php');

                    exit();

                }

            } // End: if ($currentStatus != 4) {

            else{

                // Current Status is not 4

                // Now going to Approve

                // Before Approve first check it's payment status (Paid/Unpaid)

                $paymentQry  = "SELECT COUNT(*) AS count ";

                $paymentQry .= "FROM ";

                $paymentQry .= "payments ";

                $paymentQry .= "WHERE ";

                $paymentQry .= "payments.payment_status = 1 ";

                $paymentQry .= "AND ";

                $paymentQry .= "payments.cert_id = $certID";

                $paymentRun = mysqli_query( $connection, $paymentQry );

                if ($paymentRun) {

                    //paymentQry run successfully.

                    $paymentRecord = mysqli_fetch_assoc($paymentRun);

                    $count = $paymentRecord['count'];

                    if ($count < 1) {

                        // No payment record has found in payments table

                        // it means payment not recieved

                        $_SESSION[ 'msgType' ] = "0";

                        $_SESSION[ 'msg' ]     = "Cannot approve. Payment not recieved.";

                        if ($page=="cert-details.php") {

                            header('Location: cert-details.php?id='.$certID);

                            exit();

                        }else{

                            redirect_to('home.php');

                            exit();

                        }

                    } // End: if ($count < 1) {

                    else {

                        // Payment Recieved

                        // Now Generate ISCN Code

                        $certiQry = "SELECT ";

                        $certiQry .= "cert.country AS countryID, ";

                        $certiQry .= "cert.type AS certTypeID, ";

                        $certiQry .= "cert.cert_path AS certImgName, ";

                        $certiQry .= "cert.estb_field AS estbActiID, ";

                        $certiQry .= "cert.cert_date AS certDate ";

                        $certiQry .= "FROM cert ";

                        $certiQry .= "WHERE ";

                        $certiQry .= "cert.id = $certID";

                        $certiRun = mysqli_query($connection, $certiQry);

                        if ($certiRun) {

                            //certiQry query run successfully

                            $certi = mysqli_fetch_assoc( $certiRun );

                            $countryID = $certi['countryID'];

                            $countryAbbr = get_col_On_key('countries_tbl','country_abbr', 'id', $countryID);

                            $certTypeID = $certi['certTypeID'];

                            $estbActiID = $certi['estbActiID'];



                            $estbActiAbbr = get_col_On_key('estab__activities_tbl','abbreviation', 'id', $estbActiID);



                            $certTypeAbbr = get_col_On_key('certificate_tbl','abbreviation', 'id', $certTypeID);

                            $certIssueDate = $certi['certDate'];

                            $certIssuedYear = substr($certIssueDate,2,2);

                            $estbIntrlID ="XX00";

                            do {

                                $fourLetters = randomLetters( 4 );

                                $fourNumbers = randomNumbers( 4 );

                                $newUniqCode = $fourLetters . $fourNumbers;

                                $searchUniqCodeInStdTblQry = "SELECT COUNT(*) AS stdCount ";

                                $searchUniqCodeInStdTblQry .= "FROM student ";

                                $searchUniqCodeInStdTblQry .= "WHERE ";

                                $searchUniqCodeInStdTblQry .= "unq_code = '$newUniqCode'";

                                $searchUniqCodeInStdTblRun = mysqli_query($connection, $searchUniqCodeInStdTblQry);

                                $searchedStdRecord = mysqli_fetch_assoc($searchUniqCodeInStdTblRun);

                                $countStd = $searchedStdRecord['stdCount'];



                                $searchUniqCodeInCertQry = "SELECT COUNT(*) AS certCount ";

                                $searchUniqCodeInCertQry .= "FROM cert ";

                                $searchUniqCodeInCertQry .= "WHERE ";

                                $searchUniqCodeInCertQry .= "unq_code = '$newUniqCode'";

                                $searchUniqCodeInCertRun = mysqli_query($connection, $searchUniqCodeInCertQry);

                                $searchedCertRecord = mysqli_fetch_assoc($searchUniqCodeInCertRun);

                                $certCount = $searchedCertRecord['certCount'];

                            } while ( $countStd > 0 && $certCount > 0); // End: do {

                            // newISCNCode for Individual

                            $newISCNCode = "57".$countryAbbr."-".$estbActiAbbr."-".$certTypeAbbr."-".$certIssuedYear."-".$estbIntrlID."-".$newUniqCode;

                            // Now generate QR code for this individual

                            $domainName           = 'http://www.iscnsystem.org';

                            //$domainName           = 'http://localhost/iscn';

                            $fullPath = $domainName."/individual.php?iscn_code=" . $newUniqCode;

                            $tempDir              = "../include/qrimages/individual/";

                            $codeContents         = $fullPath;

                            $fileName = "qrcode-" . $newUniqCode . ".png";

                            $pngAbsoluteFilePath  = $tempDir . $fileName;

                            $qrPath = $domainName . "/include/qrimages/individual/" . $fileName;

                            $indivLink = $domainName . "/individual.php?iscn_code=" . $newUniqCode;

                            QRcode::png($codeContents,$pngAbsoluteFilePath);

                            // QR Code is also generated successfully.

                            $message = "<html><head><title>Application Approved</title></head>

                                    <body><p>Dear <span style='color:orange'>".$fullName."</span>:</p>

                                    <h2>Welcome to <span style='font-weight:900'>ISCN</span>!!!</h2><p>

                                    Your application to <span style='font-weight:900'>ISCN</span> has 

                                    been approved. We are proud to have you as one of our members.</p>

                                    <p>Find your details below: </p><table border='1'><tr><th>ISCN CODE

                                    </th><th>".$newISCNCode."</th></tr></table><p>

                                    <span style='font-weight:900'>Note:</th> You need just the last part

                                     to check the certificate.</p><table border='1'><tr><th>CODE:</th>

                                     <th>".$newUniqCode."</th></tr><tr><th>QR CODE:</th><th>

                                     <img src='".$qrPath."' /></th></tr><tr><th>Link:</th><th>

                                     <a href='".$indivLink."'>Click</a></th></tr></table><br/><p>The 

                                     entire ISCN team looks forward to a very professional working 

                                     relationship with you, and we are ready to support you in any way 

                                     possible to serve our members better.</p><br />

                                     <p style='font-weight:900'>Best Regards,</p><br /><pre 

                                     style='color:blue;font-size:14px;'>International standard certificate 

                                     number (<span style='font-weight:900'>ISCN</span>) team. International 

                                      Standards Dataflow www.iscnsystem.org</pre></body></html>";

                            // Update indv and cert

                            $updIndvCertQry = "UPDATE cert, indv SET ";

                            $updIndvCertQry .= "cert.missing_note = '$note', ";

                            $updIndvCertQry .= "cert.unq_code = '$newUniqCode', ";

                            $updIndvCertQry .= "cert.code = '$newISCNCode', ";

                            $updIndvCertQry .= "cert.qr_image = '$fileName', ";

                            $updIndvCertQry .= "indv.modify_date = '$now', ";

                            $updIndvCertQry .= "cert.status = $action, ";

                            $updIndvCertQry .= "cert.modify_date = '$now', ";

                            $updIndvCertQry .= "indv.status = $action, ";

                            $updIndvCertQry .= "indv.user = $userID ";

                            $updIndvCertQry .= "WHERE ";

                            $updIndvCertQry .= "cert.id = $certID ";

                            $updIndvCertQry .= "AND ";

                            $updIndvCertQry .= "indv.id = $indiID ";

                            $updIndvCertRun = mysqli_query($connection,$updIndvCertQry);

                            if ($updIndvCertRun) {

                                // certiQry Query run successfully.

                                // Status has been changed successfully.

                                // Now save this email in Database

                                $insrtEmailQry = "INSERT INTO email_tbl( ";

                                $insrtEmailQry .= "carbon_copy, ";

                                $insrtEmailQry .= "sender_email, ";

                                $insrtEmailQry .= "reciever_email, ";

                                $insrtEmailQry .= "user, ";

                                $insrtEmailQry .= "status ";

                                $insrtEmailQry .= ") VALUES ( ";

                                $insrtEmailQry .= "'$Cc', ";

                                $insrtEmailQry .= "'$sender', ";

                                $insrtEmailQry .= "'$recieverEmail', ";

                                $insrtEmailQry .= "$userID, ";

                                $insrtEmailQry .= "1";

                                $insrtEmailQry .= ")";

                                $insrtEmailRun=mysqli_query($connection,$insrtEmailQry);

                                if ($insrtEmailRun) {

                                    if (mail($to,$subject,$message,$headers)) {

                                        // Everything has done successfully.

                                        $_SESSION['msgType'] = "1";

                                        $_SESSION['msg']  = "Certificate status has been approved successfully.";

                                        if ($page=="cert-details.php") {

                                            header('Location: cert-details.php?id='.$certID);

                                            exit();

                                        }else{

                                            redirect_to('home.php');

                                            exit();

                                        }

                                    }// End: if (mail($to,$subject,$message,$headers)) {

                                    else{

                                        $_SESSION['msgType'] = "1";

                                        $_SESSION['msg']  = "Certificate status has been approved successfully. Email not sent";

                                        if ($page=="cert-details.php") {

                                            header('Location: cert-details.php?id='.$certID);

                                            exit();

                                        }else{

                                            redirect_to('home.php');

                                            exit();

                                        }

                                    }//End: else{

                                }//End: if ($insrtEmailRun) {

                                else{

                                    //echo $insrtEmailQry;

                                    $_SESSION['msgType'] = "0";

                                    $_SESSION['msg']="3 Query Failed. Try again or contact with ISCN!";

                                    if ($page=="cert-details.php") {

                                        header('Location: ../admin/cert-details.php?id='.$certID);

                                    }else{

                                        redirect_to('home.php');

                                        exit();

                                    }

                                } // End: else{

                            }// End: if ($updaIndvCertRun) {

                            else{

                                //echo $updIndvCertQry;

                                $_SESSION['msgType'] = "0";

                                $_SESSION['msg']="4 Query Failed. Try again or contact with ISCN!";

                                if ($page=="cert-details.php") {

                                    header('Location: ../admin/cert-details.php?id='.$certID);

                                }else{

                                    redirect_to('home.php');

                                    exit();

                                }

                            } // End: else{

                        }//End: if ($certiRun) {

                        else{

                            //echo $certiQry;

                            $_SESSION['msgType'] = "0";

                            $_SESSION['msg']="Query Failed. Try again or contact with ISCN!";

                            if ($page=="cert-details.php") {

                                header('Location: cert-details.php?id='.$certID);

                            }else{

                                redirect_to('home.php');

                                exit();

                            }

                        }

                    } // End: else {

                } // End: if ($paymentRun) {

            }// End: else{

        } // End: elseif ($status == 4) {

    } // End: if ($indvRun) {

} // End: function cert_action($certID,$indiID,$page) {







/*----------  Update Certificate Records  ----------*/



function edit_cert() {

    session_start();

    global $connection;

    $indiID = mysql_prep($_POST[ 'indiID' ]);

    $firstName = $_POST[ 'firstName' ];

    $middleName = $_POST[ 'middleName' ];

    $lastName = $_POST[ 'lastName' ];

    $gender = $_POST[ 'gender' ];

    $nationality = $_POST[ 'nationality' ];

    $residence = $_POST[ 'residence' ];

    $dob = $_POST[ 'dob' ];

    $email = $_POST[ 'email' ];

    // Individual Establishment Details

    $personalID = $_POST['personalID'];

    $estbName = $_POST['estbName'];

    $estbActivityID = $_POST['estbActivityID'];

    $estbCity = $_POST['estbCity'];

    $estbCountryID = $_POST['estbCountryID'];

    $estbAddress = $_POST['estbAddress'];

    $estbWebsite = $_POST['estbWebsite'];

    $estbEmail = $_POST['estbEmail'];

    $estbPhone = $_POST['estbPhone'];

    $estbFax = $_POST['estbFax'];

    $estbDirector = $_POST['estbDirector'];

    // Certificate Information indivCertType

    $certTypeID = $_POST['certTypeID'];

    $certField = $_POST['certField'];

    $certDate = $_POST['certDate'];

    $userID = $_SESSION['userID'];

    // Uploads

    $mainDir        = "../admin/uploads/individual/";

    // Photo

    $photoName = mysql_prep($_FILES["photo"]["name"]);

    if ($photoName != "") {

        //echo 'Photo is going to upload.';

        $photoDir     = $mainDir."photo";

        $photoTMP     = $_FILES['photo']['tmp_name'];

        $photoNewName = date("Y-d-m--h-i-s-A")."-".$photoName;

        $targetPhoto  = $photoDir."/".basename($photoNewName);

        if (!move_uploaded_file($photoTMP,$targetPhoto)) {

            //Photo failed !

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']     = "Your personal photo size is greater than 2 MB.";

            header('Location: ../admin/cert-edit.php?id='.$indiID);

            exit();



        }else{

            // Photo uplaoded.

            // Do nothing

            //echo 'Photo Uploaded <br/>';

        }

    }



    // ID Card Picture

    $IDCardName = mysql_prep($_FILES["id-card"]["name"]);

    if ($IDCardName != "") {

        $IDCardDir       = $mainDir."id-card";

        $IDCardTMP       = $_FILES['id-card']['tmp_name'];

        $IDCardNewName   = date("Y-d-m--h-i-s-A")."-".$IDCardName;

        $targetIDCard    = $IDCardDir."/".basename($IDCardNewName);

        if (!move_uploaded_file($IDCardTMP,$targetIDCard)) {

            //ID Card Uploaded !

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']     = "Your ID Card photo size is greater than 2 MB.";

            header('Location: ../admin/cert-edit.php?id='.$indiID);

            exit();



        }

        else{

            // Uploaded

            // Do nothing

        }

    }

    // Certificate Picture

    $ScannedCertName = mysql_prep($_FILES["scanned-cert"]["name"]);

    if ($ScannedCertName != "") {

        $scannedCertDir     = $mainDir."certificate";

        $ScannedCertTMP     = $_FILES['scanned-cert']['tmp_name'];

        $ScannedCertNewName = date("Y-d-m--h-i-s-A")."-".$ScannedCertName;

        $targetScannedCert  = $scannedCertDir."/".basename($ScannedCertNewName);

        if (!move_uploaded_file($ScannedCertTMP,$targetScannedCert)) {

            //Certificate Uploaded !

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']     = "Your certificate photo size is greater than 2 MB..";

            header('Location: ../admin/cert-edit.php?id='.$indiID);

            exit();

        }else{

            // Uploade.

            // Do nothing

            //echo 'certificate Uploaded <br/>';

        }

    }

    else{

        // Do nothing

        //echo '<br/>';

        //echo 'Certificate is not going to upload !';

    }

    // Now update the indv table

    $indvUpdateQry = "UPDATE indv, cert  SET ";

    $indvUpdateQry .= "indv.first_name = '$firstName', ";

    $indvUpdateQry .= "indv.middle_name = '$middleName', ";

    $indvUpdateQry .= "indv.last_name = '$lastName', ";

    $indvUpdateQry .= "indv.gender = '$gender', ";

    $indvUpdateQry .= "indv.nationality = $nationality, ";

    $indvUpdateQry .= "indv.residence = $residence, ";

    $indvUpdateQry .= "indv.birth_date = '$dob', ";

    $indvUpdateQry .= "indv.email = '$email', ";

    if ($IDCardName !="") {

        $indvUpdateQry .= "indv.id_path = '$IDCardNewName', ";

    }

    if ($photoName !="") {

        $indvUpdateQry .= "indv.photo_path = '$photoNewName', ";

    }

    $indvUpdateQry .= "indv.modify_date = '$now', ";

    $indvUpdateQry .= "indv.user = '$userID', ";

    // Now update the cert table

    $indvUpdateQry .= "cert.globalid = '$personalID', ";

    $indvUpdateQry .= "cert.estb_name = '$estbName', ";

    $indvUpdateQry .= "cert.estb_field = '$estbActivityID', ";

    $indvUpdateQry .= "cert.country = '$estbCountryID', ";

    $indvUpdateQry .= "cert.city = '$estbCity', ";

    $indvUpdateQry .= "cert.address = '$estbAddress', ";

    $indvUpdateQry .= "cert.website = '$estbAddress', ";

    $indvUpdateQry .= "cert.email = '$estbEmail', ";

    $indvUpdateQry .= "cert.phone = '$estbPhone', ";

    $indvUpdateQry .= "cert.fax = '$estbFax', ";

    $indvUpdateQry .= "cert.type = '$certTypeID', ";

    $indvUpdateQry .= "cert.cert_date = '$certDate', ";

    $indvUpdateQry .= "cert.cert_field = '$certField', ";

    $indvUpdateQry .= "cert.director = '$estbDirector', ";

    if ($ScannedCertName != "") {

        $indvUpdateQry .= "cert.cert_path = '$ScannedCertNewName', ";

    }

    $indvUpdateQry .= "cert.modify_date = '$now' ";

    $indvUpdateQry .= "WHERE ";

    $indvUpdateQry .= "indv.id= $indiID AND ";

    $indvUpdateQry .= "cert.indv_id= $indiID";



    $indvUpdateRun = mysqli_query($connection, $indvUpdateQry);

    if ($indvUpdateRun) {

        // indvUpdateQry query run successfully.

        // indv and cert table updated successfully.

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "Certificate record updated successfully!";

        header('Location: ../admin/cert-edit.php?id='.$indiID);

        exit();

    }

    else{

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ] = "Failed! Try again or contact with ISCN.";

        header('Location: ../admin/cert-edit.php?id='.$indiID);

        exit();

    }

}



/*=====  End of Certificate Function  ======*/







/*===============================================

=            Establishment Functions            =

===============================================*/



// Change Establishment Status

function estb_action($from) {

    echo 'string';

    global $connection;

    session_start();

    $user = $_SESSION['userID'];

    $userType = $_SESSION['userType'];

    echo $action = mysql_prep($_POST['estb-action']);

    $estbID = mysql_prep($_POST['EstbIDField']);

    if ($estbID == "" || $estbID == NULL){

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION['msg'] = "Failed. Open Change status modal when page is fully loaded. Try again ! ";

        if ($from == "estb-list.php") {

            redirect_to( '../admin/estb-list.php' );

        }

        elseif ($from=="estb-grid.php") {

            redirect_to( '../admin/estb-grid.php' );

            exit();

        }

        else{

            redirect_to( '../admin/home.php' );

            exit();

        }



    }

    $senderEmail = mysql_prep($_POST['sender']);

    $subject = mysql_prep($_POST['subject']);

    $estbName = get_col_On_key('est', 'name', 'id', $estbID);

    $estbEmail= get_col_On_key('est', 'email', 'id', $estbID);

    $estbManager = get_col_On_key('est', 'manager', 'id', $estbID);

    $presentEstbStatus = get_col_On_key('est', 'status', 'id', $estbID);

    $payment=get_col_On_key('payments','payment_status','est_id',$estbID);

    if ($payment == '' || $payment == NULL) {

        $payment = "Not Paid";

    }

    $message = $_POST['email-body'];

    $message = str_replace(array("\n", "\r"), '', $message);

    $message = str_replace("<p></p>", '', $message);

    $message = str_replace("<p><br></p>", '', $message);

    $message     = mysql_prep($message);

    $note  = mysql_prep($_POST['note']);

    $now = NOW;



    if ($presentEstbStatus == 4) {

        $_SESSION['msgType'] = "0";

        $_SESSION['msg'] = "Already Approved !";

        if ($from == "estb-list.php") {

            redirect_to( '../admin/estb-list.php' );

        }

        elseif ($from=="estb-grid.php") {

            redirect_to( '../admin/estb-grid.php' );

            exit();

        }

        else{

            redirect_to( '../admin/home.php' );

            exit();

        }

    }

    if ($payment!=1|| $payment!="1"||$payment==''||$payment=="Not Paid"){

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION['msg'] = "Cannot approved . Payment not recieved.";

        if ($from == "estb-list.php") {

            redirect_to( '../admin/estb-list.php' );

        }

        elseif ($from=="estb-grid.php") {

            redirect_to( '../admin/estb-grid.php' );

            exit();

        }

        else{

            redirect_to( '../admin/home.php' );

            exit();

        }

    }

    if ($action == 4) {

        // Approve the establishment

        // Now we will generate ISCN Code

        // ISCN Code = 4 characters (2 letter + 2 number)

//        do {
//
//            // Take 2 letters randomly by randomLetters() function
//
//            $twoLetters       = randomLetters( 2 );
//
//            // Take 2 number randomly by randomNumbers() function
//
//            $twoRandomnumbers = randomNumbers( 2 );
//
//            // Concatinate letters and number to make ISCN code
//
//            $estbUserName     = $twoLetters . $twoRandomnumbers;
//
//            // Check this newly generated ISCN code existence in the database
//
//            $userNameQry      = "SELECT COUNT(*) AS userNameCount ";
//
//            $userNameQry     .= "FROM est ";
//
//            $userNameQry     .= "WHERE ";
//
//            $userNameQry     .= "username = '$estbUserName'";
//
//            $userNameRun      = mysqli_query($connection, $userNameQry);
//
//            $userNameRecord   = mysqli_fetch_assoc($userNameRun);
//
//            $userNameCount    = $userNameRecord['userNameCount'];
//
//        } // End: do {
//
//        while ($userNameCount > 0);

        // Generate 8 characters Password by randomLetters() function

//        $newPassword         = randomLetters( 8 );
//
//        // Assigning Salt
//
//        $salt                = "isdf2017";
//
//        $saltedpass          = sha1( $newPassword . $salt );
//
//        $hashedPassword      = md5($saltedpass);

        // Now user name and password has been generated.

        // So we are going to to generate QR Code for this establishment

        //only domain name
//
//        $domain              = 'http://www.iscnsystem.org';
//
//        $fullPath=$domain."/establishment.php?isecode=".$estbUserName;
//
//        $tempDir             = "../include/qrimages/establishment/";
//
//        $codeContents        = $fullPath;
//
//        $fileName            = "qrcode-".$estbUserName . ".png";
//
//        $pngAbsoluteFilePath = $tempDir . $fileName;
//
//        QRcode::png( $codeContents, "$pngAbsoluteFilePath" );
//
//        $qrPath=$domain."/include/qrimages/establishment/".$fileName;
//
//        $link=$domain."/estb-list.php?isecode=".$estbUserName;
//
//        // Now we are going to update the status of stablishment
//
//        // give user name to the establishment

        $updEstbQry = " UPDATE est SET ";

        $updEstbQry .= "modify_by   = $user , ";

        $updEstbQry .= "last_modify   = '$now' , ";

        $updEstbQry .= "missing_note   = '$note' , ";

        $updEstbQry .= "status   = $action  ";

        $updEstbQry .= "WHERE id = $estbID";

        $updEstbRun = mysqli_query( $connection, $updEstbQry );

        if ( $updEstbRun ) {

            // Query Run Successfully.

            // Establishment record updated with new username,

            // password, and new status = Approved.

            // Now we create an admin user in users table for

            // this establishment

//            $userStatus = 1;
//
//            $addUserQry  = "INSERT INTO users ( ";
//
//            $addUserQry .= "username, ";
//
//            $addUserQry .= "email,  ";
//
//            $addUserQry .= "full_name, ";
//
//            $addUserQry .= "password, ";
//
//            $addUserQry .= "plain_password, ";
//
//            $addUserQry .= "designation, ";
//
//            $addUserQry .= "create_date, ";
//
//            $addUserQry .= "modify_date, ";
//
//            $addUserQry .= "modify_by, ";
//
//            $addUserQry .= "estb_id, ";
//
//            $addUserQry .= "status ";
//
//            $addUserQry .= ") VALUES ( ";
//
//            $addUserQry .= "'$estbUserName', ";
//
//            $addUserQry .= "'$estbEmail', ";
//
//            $addUserQry .= "'$estbUserName', ";
//
//            $addUserQry .= "'$hashedPassword', ";
//
//            $addUserQry .= "'$newPassword', ";
//
//            $addUserQry .= "'Administrator',";
//
//            $addUserQry .= "'$now', ";
//
//            $addUserQry .= "'$now', ";
//
//            $addUserQry .= "'$user', ";
//
//            $addUserQry .= "$estbID, ";
//
//            $addUserQry .= "$userStatus";
//
//            $addUserQry .= ") ";
//
//            $addUserRun = mysqli_query($connection,$addUserQry);



//                // New Admin user has been created.
//
//                $inputNote  = "Establishment has been Approved.";
//
//                $inputNote .= "User Name : ".$estbUserName;
//
//                $inputNote .= " - Password: " . $newPassword;
//
//                $now        = date( 'Y-d-m h:i:s A' );
//
//                $admin      = $_SESSION[ 'userID' ];
//
//                //Now Add new note to ests_notes table
//
//                $addNoteQry = "INSERT INTO ests_notes ( ";
//
//                $addNoteQry .= "text , ";
//
//                $addNoteQry .= "admin, ";
//
//                $addNoteQry .= "create_date, ";
//
//                $addNoteQry .= "est_id ";
//
//                $addNoteQry .= " ) VALUES ( ";
//
//                $addNoteQry .= "'$inputNote', ";
//
//                $addNoteQry .= "$admin, ";
//
//                $addNoteQry .= "'$now', ";
//
//                $addNoteQry .= "$estbID";
//
//                $addNoteQry .= " )";
//
//                $addNoteRun = mysqli_query( $connection, $addNoteQry );
//


                    // Query Run Successfully.

                    // Note added successfully.";

                    // Now we will mail the newly created username

                    // and password to establishment on thier email id
                   $estbPassword = get_col_On_key('users', 'plain_password', 'estb_id', $estbID);
                   $estbuserName = get_col_On_key('est', 'username', 'id', $estbID);
                   $estbqrPath = "https://estid.org/uploads/establishment/qr_profile/".get_col_On_key('est', 'qr_path', 'id', $estbID);

                   $estblink = "https://estid.org/est_profile.php?est_id=".$estbuserName;


                    $to      = $estbEmail;

                    // For testing I put my email

                    // $to = "jahangirkhattak13@gmail.com";

                    $subject = "Congratulations Your Establishment has been approved";

                    $message = "

                        <html>

                        <head>

                        <title>Establishment Approved</title>

                        </head>

                        <body>

                        <p>To <span style='color:orange'>".$estbName."</span>:</p>

                        <h3>Welcome to ISCN system!!!</h3>

                        <p>The ISCN admissions committee has reviewed your application for your esteemed establishment, and it is our pleasure to notify you that is now approved.</p>

                        <p>Find username and password below: </p>

                        <table border='1'>

                        <tr><th>UserName: </th><th>".$estbuserName."</th></tr>

                        <tr><th>Password: </th><th>".$estbPassword."</th></tr>

                        <tr><th>Qr Code</th><th><img src='".$estbqrPath."' /></th></tr>

                        <tr><th>Link for details</th><th><a href='".$estblink."'>Click</a></th></tr>

                        </table>

                        <br />

                        <p>For further information or if you have any questions please do not hesitate to contact us: <span style='color:blue'>Info@iscnsystem.org</span></p>

                        <br />

                        <p style='font-weight:900'>Thanks in advance.</p>

                        <p style='font-weight:900' >Best Regards</p>

                        <br />

                        <pre style='color:blue;font-size:14px;'>

                        International standard certificate number (<span style='font-weight:900'>ISCN</span>) team.

                        International Standards Dataflow 

                        www.iscnsystem.org 

                        </pre>

                        </body>

                        </html>

                    ";

                    // Always set content-type when sending HTML email

                    $headers = "MIME-Version: 1.0" . "\r\n";

                    $headers.= "Content-type:text/html;charset=UTF-8"."\r\n";

                    // More headers

                    $headers .= 'From: '.$senderEmail."\r\n";

                    $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";

                    if (mail($to,$subject,$message,$headers)) {

                        session_start();

                        $_SESSION['msgType'] = "1";

                        $_SESSION['msg']="Establishment has been approved successfully!";

                        if ($from == "estb-list.php") {

                            redirect_to( '../admin/estb-list.php' );

                        }

                        elseif ($from=="estb-grid.php") {

                            redirect_to( '../admin/estb-grid.php' );

                            exit();

                        }

                        else{

                            redirect_to( '../admin/home.php' );

                            exit();

                        }

                    } // End: if(mail($to,$subject,$message,$headers)) {

                    else {

                        // Establishment has  been Approved but mail

                        // doesn't send on LocalServer.";

                        session_start();

                        $_SESSION[ 'msgType' ] = "1";

                        $_SESSION['msg'] = "Establishment has  been Approved. Email not sent.";

                        if ($from == "estb-list.php") {

                            redirect_to( '../admin/estb-list.php' );

                        }

                        elseif ($from=="estb-grid.php") {

                            redirect_to( '../admin/estb-grid.php' );

                            exit();

                        }

                        else{

                            redirect_to( '../admin/home.php' );

                            exit();

                        }

                    } // End: else {

                 // End: if ($addUserRun) {

             // End: if ($addNoteRun) {

        } // End: if ($updateEstbQueryRun) {

    } // End: if ($action == 4) {

    else {

        // You want to change establishment status

        $updEstbQry  = "UPDATE est SET ";

        $updEstbQry .= "`last_modify`='$now', ";

        $updEstbQry .= "missing_note   = '$note' , ";

        $updEstbQry .= "status='$action', ";

        $updEstbQry .= "modify_by   = $user  ";

        $updEstbQry .= "WHERE ";

        $updEstbQry .= "id = $estbID";

        $updEstbRun = mysqli_query( $connection, $updEstbQry );

        if ( $updEstbRun ) {

            // Always set content-type when sending HTML email

            $headers = "MIME-Version: 1.0" . "\r\n";

            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: '.$senderEmail."\r\n";

            $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";

            $to = $estbEmail;

            if (mail($to,$subject,$message,$headers)) {

                session_start();

                $_SESSION[ 'msgType' ] = "1";

                $_SESSION[ 'msg' ]     = "Establishment status has been changed.";

                if ($from == "estb-list.php") {

                    redirect_to( '../admin/estb-list.php' );

                }

                elseif ($from=="estb-grid.php") {

                    redirect_to( '../admin/estb-grid.php' );

                    exit();

                }

                else{

                    redirect_to( '../admin/home.php' );

                    exit();

                }

            } // End: if(mail(...)) {

            else {

                session_start();

                $_SESSION[ 'msgType' ] = "1";

                $_SESSION['msg']="Establishment status has been changed.Email not sent";

                if ($from == "estb-list.php") {

                    redirect_to( '../admin/estb-list.php' );

                }

                elseif ($from=="estb-grid.php") {

                    redirect_to( '../admin/estb-grid.php' );

                    exit();

                }

                else{

                    redirect_to( '../admin/home.php' );

                    exit();

                }

            } // End: else {

        } // End: if ($updEstbRun) {

    } // End: else

} // End: function change_estb_status($from) {





/*----------  show_estb_orders  ----------*/



function show_estb_orders($estbID) {

    global $connection;

    $safeID = mysql_prep($estbID);

    $estbName = get_col_On_key('est','name','id',$safeID);

    $pageTitle = $estbName;

    $estbOrderQry  ="SELECT ";

    $estbOrderQry .="orders.id AS orderID, ";

    $estbOrderQry .="orders.title AS orderTitle, ";

    $estbOrderQry .="orders.visibility AS orderVisibility, ";

    $estbOrderQry .="orders.est_id AS estbID, ";

    $estbOrderQry .="orders.cert_type AS certType, ";

    $estbOrderQry .="orders.field AS orderField, ";

    $estbOrderQry .="orders.cert_date certIssueDate, ";

    $estbOrderQry .="orders.editor AS userID, ";

    $estbOrderQry .="orders.editor_type AS userType, ";

    $estbOrderQry .="orders.create_date AS orderCreateDate, ";

    $estbOrderQry .="orders.modify_date AS orderModifyDate, ";

    $estbOrderQry .="orders.status AS orderStatus ";

    $estbOrderQry .="FROM orders ";

    $estbOrderQry .="WHERE ";

    $estbOrderQry .="orders.est_id = $safeID ";

    $estbOrderQry .="AND ";

    $estbOrderQry .="orders.status <> 0 ORDER BY id DESC";

    $estbOrderRun = mysqli_query($connection, $estbOrderQry);

    if ($estbOrderRun) {

        return $estbOrderRun;

    }

    else{

        return NULL;

    }



} // End:



/*=====  End of Establishment Functions  ======*/







/*------------------------------------------------------------------------------

                    Portal Admin Functions

------------------------------------------------------------------------------*/



/*----------  Recharge Establishment Credit  ----------*/

function recharge_portal($pageFrom) {

    global $connection;

    session_start();

    $userID   = mysql_prep($_SESSION['userID']);

    $userType = mysql_prep($_SESSION['userType']);

    $estbID = mysql_prep($_POST['estb-ID']);

    $voucherNmbr = mysql_prep($_POST['voucher-nmbr']);

    $rechargeAmount = mysql_prep($_POST['recharge-amount']);

    // Check the existence of voucher

    $checkVoucherQry = "SELECT balance, status FROM voucher WHERE unq_num = $voucherNmbr AND status = 1 ";

    $checkVoucherRun = mysqli_query($connection, $checkVoucherQry);

    $count =  mysqli_num_rows($checkVoucherRun);

    if ($count > 0 ) {

        // voucher existed

        $voucherRecord = mysqli_fetch_assoc($checkVoucherRun);

        $voucherStatus = $voucherRecord['status'];

        $voucherPresentBalance = $voucherRecord['balance'];

        // Check that voucher has enough balance for requested recharge amount

        if ($voucherStatus == 0 || $voucherStatus == "0") {

            // Voucher is existed but its status is 0 ( means deleted)

            $_SESSION['msgType']     = "0";

            $_SESSION['msg']         = "Voucher is not existed anymore.";

            if ($pageFrom=="portal") {

                redirect_to( '../establishment/recharge.php' );

            }

            else{

                redirect_to( '../admin/estb-recharge.php');

            }

            exit();

        } // End: if ($voucherStatus == 0 || $voucherStatus == "0") {

        if ($voucherPresentBalance < $rechargeAmount) {

            // Voucher present balance is not enough to recharge your

            // credit with the requested amount

            $_SESSION['msgType']     = "0";

            $_SESSION['msg']         = "Your current balance in this voucher is less than your recharge amount !";

            if ($pageFrom=="portal") {

                redirect_to( '../establishment/recharge.php' );

            }

            else{

                redirect_to( '../admin/estb-recharge.php');

            }

            exit();

        } // End: if ($voucherPresentBalance < $rechargeAmount) {

        else {

            // Voucher is existed

            // voucher has enough balance to recharge

            $sqlQry = "SELECT present_credit FROM est WHERE id = $estbID";

            $estabPresentCreditRun = mysqli_query($connection, $sqlQry);

            if ($estabPresentCreditRun) {

                $estabPresentRecord = mysqli_fetch_assoc($estabPresentCreditRun);

                $estbPresentCredit = $estabPresentRecord['present_credit'];

                $estbNewCredit =     $estbPresentCredit +  $rechargeAmount ;

                $voucherNewBalance = $voucherPresentBalance -  $rechargeAmount;

                // Now add requested amount to establishment credit

                $updateCreditQry = "UPDATE est SET present_credit = $estbNewCredit WHERE id = $estbID";

                $updateCreditRun = mysqli_query($connection, $updateCreditQry);

                if ($updateCreditRun) {

                    // requested amount is added to establishment credit

                    // Subtract recquested amount from the voucher balance

                    $updateVoucherBalanceQry = "UPDATE voucher SET balance = $voucherNewBalance WHERE unq_num = $voucherNmbr";

                    $updateVoucherBalanceRun = mysqli_query($connection, $updateVoucherBalanceQry);

                    if ($updateVoucherBalanceRun) {

                        // requested amount has been substracted from voucher balane

                        $_SESSION['msgType']     = "1";

                        $_SESSION['msg']         = "Recharged successfully !";

                        if ($pageFrom=="portal") {

                            redirect_to( '../establishment/recharge.php' );

                        }

                        else{

                            redirect_to( '../admin/estb-recharge.php');

                        }

                        exit();

                    } // End: if ($updateVoucherBalanceRun) {

                    else {

                        $_SESSION['msgType']     = "0";

                        $_SESSION['msg'] = "Recharge Failed ! Try again or contact with ISCN.";

                        if ($pageFrom=="portal") {

                            redirect_to( '../establishment/recharge.php' );

                        }

                        else{

                            redirect_to( '../admin/estb-recharge.php');

                        }

                        exit();

                    } // End: else {

                } // End: if ($updateCreditRun) {

                else{

                    $_SESSION['msgType']     = "0";

                    $_SESSION['msg'] = "RRecharge Failed ! Try again or contact with ISCN.";

                    if ($pageFrom=="portal") {

                        redirect_to( '../establishment/recharge.php' );

                    }

                    else{

                        redirect_to( '../admin/estb-recharge.php');

                    }

                    exit();

                }

            } // End: if ($estabPresentCreditRun) {

            else{

                $_SESSION['msgType']     = "0";

                $_SESSION['msg'] = "Recharge Failed ! Try again or contact with ISCN.";

                if ($pageFrom=="portal") {

                    redirect_to( '../establishment/recharge.php' );

                }

                else{

                    redirect_to( '../admin/estb-recharge.php');

                }

                exit();

            }

        } // End: else {

    } // End: if ($count > 0 ) {

    else{

        $_SESSION['msgType']     = "0";

        $_SESSION['msg'] = "You have entered invalid voucher number !";

        if ($pageFrom=="portal") {

            redirect_to( '../establishment/recharge.php' );

        }

        else{

            redirect_to( '../admin/estb-recharge.php');

        }

        exit();

    }

}







function add_new_cert($from) {

    // Initialize all the variables with empty values

    $orderID = $firstName = $middleName = $lastName = $gender = $dob = $orderID ='';

    $nationality = $residence = $phone =  $email = $certDate = $globalID = $personPhotoName = '';

    global $connection;

    session_start();

    $userType = $_SESSION['userType'];

    $user = $_SESSION['userID'];

    $orderID = $_POST['order-id'];

    $estbID       = get_col_On_key('orders','est_id','id',$orderID);

    $estbUserName = get_col_On_key('est','username','id',$estbID);

    $firstName    = mysql_prep($_POST['first-name']);

    $middleName   = mysql_prep($_POST['middle-name']);

    $lastName     = mysql_prep($_POST['last-name']);

    $gender       = $_POST['gender'];

    $dob          = mysql_prep($_POST['date-birth']);

    $nationality  = $_POST['nationality'];

    $residence    = $_POST['residence'];

    $phone        = mysql_prep($_POST['phone']);

    $email        = mysql_prep($_POST['email']);

    $certSerialID = mysql_prep($_POST['cert-serial']);

    if ($certSerialID != '') {

        $certSerialID = remove_white_spaces($certSerialID);

        $certSerialNo = $estbUserName.$certSerialID;

        $searchCertSerialNo = search_tbl('student', 'cert_serial_no', $certSerialNo);

        if ($searchCertSerialNo > 0) {

            $_SESSION['msgType'] = "0";

            $_SESSION['msg'] = $certSerialID." certificate ID already used. Try another one!";

            if ($from=="admin") {

                header('Location: ../admin/order-cert-new.php?orderID='.$orderID);

                exit();

            }

            else{

                header('Location: ../establishment/add-cert.php?orderID='.$orderID);

                exit();

            }



        } // End: if ($searchCertSerialNo > 0) {

    } // End: if ($certSerialID != '') {

    else{

        $certSerialNo = "";

    } // End:  else{

    $certDate = get_col_On_key('orders', 'cert_date','id',$orderID);

    $globalID = mysql_prep($_POST['global-id']);

    $createDate         = date( "Y-d-m h:i:s A" );

    $modifyDate         = date( "Y-d-m h:i:s A" );

    $status             = 1;

    $allowedFiles = array("jpg", "jpeg", "png");



    // uplaod Pictures

    $target_dir        = "../admin/uploads/student/";



    // person photo

    $personPhotoTMP     = $_FILES['std-photo']['tmp_name'];

    $personPhotoName    = mysql_prep($_FILES["std-photo"]["name"]);



    // Personal Photo

    if ($personPhotoName != "") {

        $personalPhotoFileExt = get_file_ext($personPhotoName);

        if (!in_array($personalPhotoFileExt, $allowedFiles)) {

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']  = "Uploading failed. Only jpg, jpeg, and png files are allowed !";

            if ($from == "add-cert.php") {

                header('Location: ../establishment/add-cert.php?orderID='.$orderID);

                exit();

            }



        } // End: if (!in_array($ext, $allowedFiles)) {

        $personPhotoNewName = date("Y-d-m--h-i-s-A")."-".$personPhotoName;

        $targetPersonPhoto  = $target_dir."photo/".basename($personPhotoNewName);

        move_uploaded_file($personPhotoTMP, $targetPersonPhoto);

    } // End: if ($personPhotoName != "") {

    else {

        $personPhotoNewName = "";

    }



    // Personal ID card

    $personIDCardTMP =$_FILES['std-id-card']['tmp_name'];

    $personIDCardName = mysql_prep($_FILES["std-id-card"]["name"]);

    if ($personIDCardName) {

        $certFileExt = get_file_ext($personIDCardName);

        if (!in_array($certFileExt, $allowedFiles)) {

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']  = "Uploading failed. Only jpg, jpeg, and png files are allowed !";

            if ($from == "add-cert.php") {

                header('Location: ../establishment/add-cert.php?orderID='.$orderID);

                exit();

            }



        } // End: if (!in_array($ext, $allowedFiles)) {

        $personIDCardNewName = date("Y-d-m--h-i-s-A")."-".$personIDCardName;

        $targetIDCard = $target_dir ."id-card/". basename($personIDCardNewName);

        move_uploaded_file($personIDCardTMP, $targetIDCard);

    } // End: if ($personIDCardName) {

    else{

        $personIDCardNewName = "";

    } // End:

    // Now insert record in database

    $addPersonQry  ="INSERT INTO student( ";

    $addPersonQry .="first_name, ";

    $addPersonQry .="middle_name, ";

    $addPersonQry .="last_name, ";

    $addPersonQry .="globalid, ";

    $addPersonQry .="gender, ";

    $addPersonQry .="birth_date, ";

    $addPersonQry .="nationality, ";

    $addPersonQry .="residence, ";

    $addPersonQry .="phone, ";

    $addPersonQry .="email, ";

    $addPersonQry .="estb_id, ";

    if ($personPhotoName != '') {

        $addPersonQry .="photo_path, ";

    }

    if ($personIDCardName !="") {

        $addPersonQry .="id_path, ";

    }



    if ($certSerialID != '') {

        $addPersonQry .="cert_serial_no, ";

    }

    $addPersonQry .="order_id, ";

    $addPersonQry .="cert_date, ";

    $addPersonQry .="create_date, ";

    $addPersonQry .="modify_date, ";

    $addPersonQry .="editor, ";

    $addPersonQry .="editor_type, ";

    $addPersonQry .="status ";

    $addPersonQry .=") VALUES ( ";

    $addPersonQry .="'$firstName', ";

    $addPersonQry .="'$middleName', ";

    $addPersonQry .="'$lastName', ";

    $addPersonQry .="'$globalID', ";

    $addPersonQry .="'$gender', ";

    $addPersonQry .="'$dob', ";

    $addPersonQry .="'$nationality', ";

    $addPersonQry .="'$residence', ";

    $addPersonQry .="'$phone', ";

    $addPersonQry .="'$email', ";

    $addPersonQry .="$estbID, ";

    if ($personPhotoName != '') {

        $addPersonQry .="'$personPhotoNewName', ";

    }

    if ($personIDCardName != "") {

        $addPersonQry .="'$personIDCardNewName', ";

    }

    if ($certSerialID != '') {

        $addPersonQry .="'$certSerialNo', ";

    }

    $addPersonQry .="$orderID, ";

    $addPersonQry .="'$certDate', ";

    $addPersonQry .="'$modifyDate', ";

    $addPersonQry .="'$createDate', ";

    $addPersonQry .="$user, ";

    $addPersonQry .="'$userType', ";

    $addPersonQry .="$status ";

    $addPersonQry .=")";

    $addPersonRun = mysqli_query($connection, $addPersonQry);

    if ( $addPersonRun ) {

        // Record added succussfully.

        // Session already started so we will start it again.

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "Person added succussfully !";

        if ($from=="admin") {

            header('Location: ../admin/order-cert-new.php?orderID='.$orderID);

            exit();

        }

        else{

            header('Location: ../establishment/add-cert.php?orderID='.$orderID);

            exit();

        }

        //redirect_to('../establishment/certificates.php');

    } // End: if ( $addPersonRun ) {

    else {

        //echo $addPersonQry;

        //exit();

        $_SESSION['msgType'] = "0";

        $_SESSION['msg'] = "Failed !. Open Add Certificate Form when page is fully loaded or Contact with ISCN";

        if ($from=="admin") {

            header('Location: ../admin/order-cert-new.php?orderID='.$orderID);

            exit();

        }

        else{

            header('Location: ../establishment/add-cert.php?orderID='.$orderID);

            exit();

        }

    } // End: else {

} // End: function add_new_cert() {





function upload_cert_file($pageFrom) {

    session_start();

    global $connection;

    $personID = mysql_prep($_POST['person-id']);

    $personID = remove_white_spaces($personID);

    $orderID = get_col_On_key('student', 'order_id', 'id', $personID);

    // Certificate

    if ($pageFrom == "cert-list.php" || $pageFrom == "cert-grid.php") {

        $target_dir        = "../admin/uploads/student/";

    }

    else{

        $target_dir        = "uploads/student/";

    }



    $CertFileTMP =$_FILES['cert-img']['tmp_name'];

    $CertFileName = mysql_prep($_FILES["cert-img"]["name"]);



    $ext = get_file_ext($CertFileName);

    $allowedFiles = array("jpg", "jpeg", "png");

    if (!in_array($ext, $allowedFiles)) {

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Uploading failed. Only jpg, jpeg, and png files are allowed !";

        if ($pageFrom == "cert-list.php") {

            header('Location: ../establishment/cert-list.php?orderID='.$orderID);

            exit();

        }

        elseif ($pageFrom == "cert-grid.php") {

            header('Location: ../establishment/cert-grid.php?orderID='.$orderID);

            exit();

        }

        elseif($pageFrom=="admin/order-cert-grid.php"){

            header('Location: ../admin/order-cert-grid.php?order-id='.$orderID);

            exit();

        }

        elseif($pageFrom=="admin/order-cert-list.php"){

            header('Location: ../admin/order-cert-list.php?order-id='.$orderID);

            exit();

        }



    } // End: if (!in_array($ext, $allowedFiles)) {

    else {

        $CertFileNewName = date("Y-d-m--h-i-s-A")."-".$CertFileName;

        echo $targetScannedCert = $target_dir ."certificate/". basename($CertFileNewName);

        if (move_uploaded_file($CertFileTMP,$targetScannedCert)) {

            // Update The record inside Database

            $uploadCertQry  = "UPDATE student SET ";

            $uploadCertQry .= "cert_path = '$CertFileNewName' ";

            $uploadCertQry .= "WHERE id = $personID ";

            $uploadCertRun = mysqli_query($connection, $uploadCertQry);

            if ($uploadCertRun) {

                $_SESSION['msgType'] = "1";

                $_SESSION['msg']  = "Certificate File has been uploaded successfully !";

                if ($pageFrom == "cert-list.php") {

                    header('Location: ../establishment/cert-list.php?orderID='.$orderID.'&msg=uploaded');

                    exit();

                }

                elseif($pageFrom == "cert-grid.php"){

                    header('Location: ../establishment/cert-grid.php?orderID='.$orderID.'&msg=uploaded');

                    exit();

                }

                elseif($pageFrom=="admin/order-cert-grid.php"){

                    header('Location: ../admin/order-cert-grid.php?order-id='.$orderID.'&msg=uploaded');

                    exit();

                }

                elseif($pageFrom=="admin/order-cert-list.php"){

                    header('Location: ../admin/order-cert-list.php?order-id='.$orderID);

                    exit();

                }



            } // End: if ($uploadCertRun) {

            else {

                //echo $uploadCertQry;

                $_SESSION['msgType'] = "0";

                echo $_SESSION['msg']  = "Error in Query. Contact With ISCN !";

                if ($pageFrom == "cert-list.php") {

                    header('Location: ../establishment/cert-list.php?orderID='.$orderID.'&msg=failed');

                    exit();

                }

                elseif ($pageFrom == "cert-grid.php") {

                    header('Location: ../establishment/cert-gird.php?orderID='.$orderID.'&msg=failed');

                    exit();

                }

                elseif($pageFrom=="admin/order-cert-grid.php"){

                    header('Location: ../admin/order-cert-grid.php?order-id='.$orderID.'&msg=failed');

                    exit();

                }

                elseif($pageFrom=="admin/order-cert-list.php"){

                    header('Location: ../admin/order-cert-list.php?order-id='.$orderID);

                    exit();

                }



            } // End: else{

        } // End: if (move_uploaded_file(...)) {

        else {

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']  = "Certificate Not Uploaded. Your File size must be 2 MB or less than 2 MB";

            if ($pageFrom == "cert-list.php") {

                header('Location: ../establishment/cert-list.php?orderID='.$orderID.'&msg=failed');

                exit();

            }

            elseif ($pageFrom == "cert-grid.php") {

                header('Location: ../establishment/cert-gird.php?orderID='.$orderID.'&msg=failed');

                exit();

            }

            elseif($pageFrom=="admin/order-cert-grid.php"){

                header('Location: ../admin/order-cert-grid.php?order-id='.$orderID.'&msg=failed');

                exit();

            }

            elseif($pageFrom=="admin/order-cert-list.php"){

                header('Location: ../admin/order-cert-list.php?order-id='.$orderID);

                exit();

            }



        } // End: else {

    } // End: else {

} // End: function upload_cert_file($pageFrom) {













function edit_portal_cert($pageFrom) {

    global $connection;

    session_start();

    $userType = $_SESSION['userType'];

    $userID = $_SESSION['userID'];

    $stdID = $_POST['stdID'];

    $orderID = get_col_On_key('student', 'order_id', 'id', $stdID);

    $estbID = get_col_On_key('orders', 'est_id', 'id', $orderID);

    $estbUserName = get_col_On_key('est', 'username', 'id', $estbID);

    $firstName = mysql_prep($_POST['firstName']);

    $middleName = mysql_prep($_POST['middleName']);

    $lastName = mysql_prep($_POST['lastName']);

    $gender = $_POST['gender'];

    $dob = mysql_prep($_POST['dob']);

    $nationalityID = $_POST['nationalityID'];

    $residenceID = $_POST['residenceID'];

    $phone = mysql_prep($_POST['phone']);

    $email = mysql_prep($_POST['email']);

    $personalID = mysql_prep($_POST['personalID']);

    $certSerialID = mysql_prep($_POST['certSerialNo']);

    echo $certSerialID = remove_white_spaces($certSerialID);

    echo '<br/>';

    $now = NOW;

    if ($certSerialID != "") {

        echo "new=".$newCertSerialNo = $estbUserName.$certSerialID;

        echo '<br/>';

        echo "total".$searchCertSerialNo = search_tbl('student', 'cert_serial_no', $newCertSerialNo);

        if ($searchCertSerialNo > 0) {

            // Check its old Certificate Serial ID

            echo "<br/>Old=".$oldCertSerialNo = get_col_On_key('student', 'cert_serial_no', 'id', $stdID);

            // Compare old cert id with new one

            $newCertSerialNo = $estbUserName.$certSerialID;

            if ($oldCertSerialNo != $newCertSerialNo) {

                $_SESSION['msgType'] = "0";

                echo $_SESSION['msg'] = $certSerialID." certificate ID already used. Try another!";

                if ($pageFrom == "cert-details.php") {

                    header('Location: ../establishment/cert-cert.php?id='.$stdID);

                    exit();

                }

                elseif($pageFrom == "edit-cert.php"){

                    header('Location: ../establishment/edit-cert.php?id='.$stdID);

                    exit();

                }

                elseif($pageFrom == "admin"){

                    header('Location: ../admin/order-cert-edit.php?id='.$stdID);

                    exit();

                }



            } // End: if ($oldCertSerialNo != $newCertSerialNo) {

        } // End: if ($searchCertSerialNo > 0) {

    }

    //exit();

    // uplaod Pictures

    $target_dir        = "../admin/uploads/student/";

    // person photo

    $personPhotoTMP =$_FILES['photo']['tmp_name'];

    $personPhotoName = $_FILES["photo"]["name"];

    $personPhotoNewName = date("Y-d-m--h-i-s-A")."-".$personPhotoName;

    $targetPersonPhoto = $target_dir ."photo/". basename($personPhotoNewName);

    // If Personal Photo is Provided then upload, otherwise do nothing.

    if ($personPhotoName != "") {

        if (!move_uploaded_file($personPhotoTMP, $targetPersonPhoto)) {

            echo '<br/>personPhotoName Not uploaded';

        }

    } // End: if (move_uploaded_file(...) ) {



    // person id card

    $personIDCardTMP =$_FILES['id-card']['tmp_name'];

    $personIDCardName = $_FILES["id-card"]["name"];

    $personIDCardNewName = date("Y-d-m--h-i-s-A")."-".$personIDCardName;

    $targetIDCard = $target_dir ."id-card/". basename($personIDCardNewName);



    if ($personIDCardName != '') {

        if (!move_uploaded_file($personIDCardTMP, $targetIDCard)) {

            echo '<br/>personIDCardName Not uploaded';

        }

    } // End: if(move_uploaded_file(...)){

    $editStdQry  = "UPDATE student SET ";

    $editStdQry .= "first_name = '$firstName', ";

    $editStdQry .= "middle_name = '$middleName', ";

    $editStdQry .= "last_name = '$lastName', ";

    $editStdQry .= "globalid = '$personalID', ";

    if ($certSerialID != "") {

        $editStdQry .= "cert_serial_no = '$newCertSerialNo', ";

    }

    $editStdQry .= "gender = '$gender', ";

    $editStdQry .= "birth_date = '$dob', ";

    $editStdQry .= "nationality = $nationalityID, ";

    $editStdQry .= "residence = $residenceID, ";

    $editStdQry .= "phone = $phone, ";

    $editStdQry .= "email = '$email', ";

    if ($personPhotoName != "") {

        $editStdQry .= "photo_path = '$personPhotoNewName', ";

    }

    if ($personIDCardName != "") {

        $editStdQry .= "id_path = '$personIDCardNewName', ";

    }

    if ($userType != '') {

        $editStdQry .= "editor_type = '$userType', ";

    }

    $editStdQry .= "modify_date = '$now', ";

    $editStdQry .= "editor = $userID ";

    $editStdQry .= "WHERE ";

    $editStdQry .= "id  = $stdID";

    $editStdRun = mysqli_query($connection, $editStdQry);

    if ($editStdRun) {

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "Certificate record has been updated successfully !";

        if ($pageFrom == "cert-details.php") {

            header('Location: ../establishment/cert-details.php?cert-id='.$stdID);

            exit();

        }

        elseif($pageFrom == "edit-cert.php"){

            header('Location: ../establishment/edit-cert.php?id='.$stdID);

            exit();

        }

        elseif($pageFrom == "admin"){

            header('Location: ../admin/order-cert-edit.php?id='.$stdID);

            exit();

        }

    } // End: if ($editStdRun) {

    else{

        //echo $editStdQry;

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Certificate record not updated! Contact with ISCN !";

        if ($pageFrom == "cert-details.php") {

            header('Location: ../establishment/cert-details.php?cert-id='.$stdID);

            exit();

        }

        elseif($pageFrom == "edit-cert.php"){

            header('Location: ../establishment/edit-cert.php?id='.$stdID);

            exit();

        }

        elseif($pageFrom == "admin"){

            echo 'string';

            echo $stdID;

            echo '<br/>';

            echo $editStdQry;

            exit();

            header('Location: ../admin/order-cert-edit.php?id='.$stdID);

            exit();

        }

    }

} // End: function edit_portal_cert() {





function publish_order($pageFrom,$orderID) {

    session_start();

    $safeOrderID = mysql_prep($orderID);

    global $connection;

    $user = $_SESSION['userID'];

    $orderPaymentStatus = get_col_On_key('orders', 'status', 'id', $safeOrderID);

    if ($orderPaymentStatus == 2) {

        // Payment has been recieved for this order

        // Now check the number of certificates inside this order.

        // If there is no certificate then give error message to the user

        $count = search_tbl('student', 'order_id', $safeOrderID);

        if ($count < 1 ) {

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']  = "Cannot be change to public. There is no record in this order!";

            if ($pageFrom =="establishment/order-list.php") {

                redirect_to('../establishment/order-list.php');

                exit();

            }

            elseif ($pageFrom =="establishment/order-grid.php") {

                redirect_to('../establishment/order-grid.php');

                exit();

            }



        }

        else{

            // There is certificates inside this order

            // Now Check, Is there any certifcate record whose certificate

            // picture is not yet uploaded.

            //$empty = '';

            $countStdsNoCertQry = "SELECT id FROM student WHERE cert_path IS NUll AND order_id = $safeOrderID AND status <> 0";

            $countStdsNoCertRun = mysqli_query($connection, $countStdsNoCertQry);

            $countStdsNoCert = mysqli_num_rows($countStdsNoCertRun);

            if ($countStdsNoCert > 0) {

                // There is record whose certificate picture is not uploaded.

                $_SESSION['msgType'] = "0";

                $_SESSION['msg']  = "Cannot be publish because ".$countStdsNoCert." records inside this order whose certificate picture is not uploaded !";

                if ($pageFrom =="establishment/order-list.php") {

                    redirect_to('../establishment/order-list.php');

                    exit();

                }

                elseif ($pageFrom =="establishment/order-grid.php") {

                    redirect_to('../establishment/order-grid.php');

                    exit();

                }

            }

            // All records inside this order has certificate picture

            // So now we change the Visibilty Status of this order to 1 i.e Public

            $visibilityOrderQry  = "UPDATE orders SET ";

            $visibilityOrderQry .= "visibility = 1 ";

            $visibilityOrderQry .= "WHERE ";

            $visibilityOrderQry .= "id = $safeOrderID";

            $visibilityOrderRun = mysqli_query($connection, $visibilityOrderQry);

            if ( $visibilityOrderRun) {

                // Order visibility changed succussfully from Private to Published

                $stdStatusQry  = "UPDATE student SET ";

                $stdStatusQry .= "status = 5 ";

                $stdStatusQry .= "WHERE ";

                $stdStatusQry .= "order_id = $safeOrderID ";

                $stdStatusQry .= "AND ";

                $stdStatusQry .= "status = 4";

                $stdStatusRun = mysqli_query($connection, $stdStatusQry);

                if ($stdStatusRun) {

                    // Session already started, so we will not start it again.

                    $_SESSION['msgType'] = "1";

                    $_SESSION['msg']  = "All certificates inside this order is public now !";

                    if ($pageFrom =="establishment/order-list.php") {

                        redirect_to('../establishment/order-list.php');

                        exit();

                    }

                    elseif ($pageFrom =="establishment/order-grid.php") {

                        redirect_to('../establishment/order-grid.php');

                        exit();

                    }

                }

                else {

                    echo $stdStatusQry;

                    exit();

                    $_SESSION['msgType'] = "0";

                    $_SESSION['msg']  = "Query Failed ! Contact with ISCN.";

                    if ($pageFrom =="establishment/order-list.php") {

                        redirect_to('../establishment/order-list.php');

                        exit();

                    }

                    elseif ($pageFrom =="establishment/order-grid.php") {

                        redirect_to('../establishment/order-grid.php');

                        exit();

                    }

                } // End: else {

            } // End: if ( $visibilityOrderRun && $stdStatusRun ) {

            else {

                echo $visibilityOrderQry;

                exit();

                $_SESSION['msgType'] = "0";

                $_SESSION['msg']  = "Query Failed ! Contact with ISCN.";

                if ($pageFrom =="establishment/order-list.php") {

                    redirect_to('../establishment/order-list.php');

                    exit();

                }

                elseif ($pageFrom =="establishment/order-grid.php") {

                    redirect_to('../establishment/order-grid.php');

                    exit();

                }

            } // End: else {

        } // End: else{

    } // End: if ($orderPaymentStatus == 2) {

    else {

        // Payment not recieved for this order

        // To Back and give error message to the user

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "You cannot make it public because payment for this order not recieved !";

        redirect_to('../establishment/order-grid.php');

        exit();

    } // End: else {

}



/*=====  End of Portal Admin Functions  ======*/

/*------------------------------------------------------------------------------

                    Public Side Functions

------------------------------------------------------------------------------*/







/*------------------------------------------------------------------------------

                        individual Pay With Voucher

------------------------------------------------------------------------------*/

if ( isset($_POST['certPayVoucher'])) {

    //certPayVoucher clicked

    session_start();

    global $connection;

    $certID             = safe_int($_SESSION['certID']);

    if ($_SESSION['certID']=="") {

        redirect_to('../index.php');

        exit();

    }

    // Here the user is client so we take userID = 0

    $userID        = 0;

    $now           = date( "Y-d-m h:i:s A" );

    $voucherNmbr   = mysql_prep(safe_int(remove_white_spaces($_POST['voucherNmbr'])));

    $reqAmount     =mysql_prep(safe_string(remove_white_spaces($_POST['packageAmount'])));

    $voucherQry  = "SELECT COUNT(*) AS count, ";

    $voucherQry .= "price, ";

    $voucherQry .= "balance, ";

    $voucherQry .= "unq_num ";

    $voucherQry .= "FROM voucher WHERE ";

    $voucherQry .= "unq_num = '$voucherNmbr'";

    $voucherQry .= "AND ";

    $voucherQry .= "status = 1 ";

    $voucherRun = mysqli_query( $connection, $voucherQry );

    $voucherRecord         = mysqli_fetch_assoc( $voucherRun );

    if ( $voucherRecord[ 'count' ] == 1 ) {

        //Voucher Existed

        $balance = $voucherRecord[ 'balance' ];

        if ( $balance >= $reqAmount ) {

            // Balance in voucher is enough for for registration

            // reqAmount minus from current voucher balance

            $newBalance          = $balance - $reqAmount;

            // Now update Voucher table with new balance

            $updateVoucherQry  = "UPDATE voucher SET ";

            $updateVoucherQry .= "balance = $newBalance ";

            $updateVoucherQry .= " WHERE ";

            $updateVoucherQry .= "unq_num = $voucherNmbr";

            $updateVoucherRun = mysqli_query( $connection, $updateVoucherQry );

            if ( $updateVoucherRun ) {

                // Amount for the certificate ( 15 $) has

                // been deducted from the entered voucher

                $insertPaymentQry = "INSERT INTO payments ( ";

                $insertPaymentQry .= "voucher_nmbr, ";

                $insertPaymentQry .= "payment_gross, ";

                $insertPaymentQry .= "currency_code, ";

                $insertPaymentQry .= "payment_status, ";

                $insertPaymentQry .= "cert_id, ";

                $insertPaymentQry .= "type, ";

                $insertPaymentQry .= "pay_date ";

                $insertPaymentQry .= ") VALUES ( ";

                $insertPaymentQry .= "$voucherNmbr, ";

                $insertPaymentQry .= "$reqAmount, ";

                $insertPaymentQry .= "'USD', ";

                $insertPaymentQry .= "1, ";

                $insertPaymentQry .= "$certID, ";

                $insertPaymentQry .= "'Voucher', ";

                $insertPaymentQry .= "'$now' ";

                $insertPaymentQry .= ")";

                $insertPaymentRun = mysqli_query( $connection, $insertPaymentQry );

                if ( $insertPaymentRun ) {

                    //Payment record has been saved

                    do {

                        $appIDLength = 5;

                        $appID     = random_number($appIDLength);

                        $searchAppIDQry = "SELECT COUNT(*) AS appIDCount ";

                        $searchAppIDQry .= "FROM cert ";

                        $searchAppIDQry .= "WHERE ";

                        $searchAppIDQry .="app_id =$appID";

                        $searchAppIDRun=mysqli_query($connection,$searchUsedPassQry);

                        $appIDRecord = mysqli_fetch_assoc($searchAppIDRun);

                        $appIDCount = $appIDRecord['appIDCount'];



                    } while ( $appIDCount > 0);





                    // Now add note

                    $noteDesc = "Application ID = ".$appID;

                    $addNoteQry  = "INSERT INTO certificate_note_tbl( ";

                    $addNoteQry .= "cert_id, ";

                    $addNoteQry .= "note_desc, ";

                    $addNoteQry .= "create_date, ";

                    $addNoteQry .= "user, ";

                    $addNoteQry .= "status ";

                    $addNoteQry .= ") VALUES ( ";

                    $addNoteQry .= "$certID, ";

                    $addNoteQry .= "'$noteDesc', ";

                    $addNoteQry .= "'$now', ";

                    $addNoteQry .= "$userID, ";

                    $addNoteQry .= "1 ";

                    $addNoteQry .= ") ";

                    $addNoteRun = mysqli_query($connection, $addNoteQry);

                    if ($addNoteRun) {



                        // Now update certificate table with appIDForThisCert

                        $updateCertQuery  = "UPDATE cert SET ";

                        $updateCertQuery .= "app_id = '$appID' ";

                        $updateCertQuery .= " WHERE ";

                        $updateCertQuery .= "id = $certID";

                        $updateCertQueryRun = mysqli_query($connection,$updateCertQuery);

                        if ( $updateCertQueryRun ) {

                            // New Password has been alloted

                            // to the certificate of the individual

                            // Now We will get the full name and his/her

                            // Email to send him/her confirmation email

                            $SelectIndiQuery = "SELECT ";

                            $SelectIndiQuery .= "indv.email, ";

                            $SelectIndiQuery .= "indv.first_name, ";

                            $SelectIndiQuery .= "indv.middle_name, ";

                            $SelectIndiQuery .= "indv.last_name ";

                            $SelectIndiQuery .= "FROM ";

                            $SelectIndiQuery .= "indv ";

                            $SelectIndiQuery .= "INNER JOIN ";

                            $SelectIndiQuery .= "cert ON ";

                            $SelectIndiQuery .= "indv.id = cert.indv_id ";

                            $SelectIndiQuery .= "WHERE ";

                            $SelectIndiQuery .= "cert.id = $certID";

                            $SelectIndiQueryRun = mysqli_query( $connection, $SelectIndiQuery );

                            if ( $SelectIndiQueryRun ) {

                                //Got full name and email successfully

                                $indiRecord   = mysqli_fetch_assoc( $SelectIndiQueryRun );

                                $indiEmail    = $indiRecord[ 'email' ];

                                $indifullName = $indiRecord[ 'first_name' ] . " " . $indiRecord[ 'middle_name' ] . " " . $indiRecord[ 'last_name' ];

                                $to           = $indiEmail;

                                // For testing I put my Email

                                // $to = "jahangirkhattak13@gmail.com";

                                $subject      = "Confirmation of application recipt";

                                $message= "

                                    <html>

                                        <head>

                                            <title>Confirmation Of Application Recipt</title>

                                        </head>

                                        <body>

                                            <h3>Dear " . $indifullName . "</h3>

                                            <p>

                                                Thank you, we have received your registration and 

                                                your payment. We’re glad that you’ve shown interest 

                                                in joining <span style='font-weight:900'>ISCN</span>

                                                system.

                                            </p>

                                            <p>

                                                Your application has been sent to the admissions 

                                                committee so it may take us a little time to 

                                                respond, but we will as soon as possible.

                                            </p>

                                            <br />

                                            <p>For the online status checker: </p>

                                            <a href='http://www.iscnsystem.org/'>

                                                http://www.iscnsystem.org/

                                            </a>

                                            <br />

                                            <br />

                                            

                                            <strong>Application ID = <span>" . $appID . "</span></strong>

                                            

                                            <p>

                                                We will update your status and send you an email 

                                                once your application has been processed.

                                            </p>

                                            <br />

                                            <p style='font-weight:900'>

                                                Thank you in advance for your anticipated  

                                                cooperation.

                                            </p>

                                            <br />

                                            <p style='font-weight:900'>Best Regards,</p>

                                            <br />

                                            <pre style='color:blue;font-size:14px;'>

                                                International standard certificate number ( 

                                                <span style='font-weight:900'>ISCN</span>) team.

                                                International Standards Dataflow www.iscnsystem.org 

                                            </pre>

                                        </body>

                                    </html>

                                ";

                                // Always set content-type when sending HTML email

                                $headers      = "MIME-Version: 1.0" . "\r\n";

                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                                // More headers

                                $headers .= 'From: <admin@iscnsystem.org>' . "\r\n";

                                $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";

                                if ( mail( $to, $subject, $message, $headers ) ) {

                                    // Mail sent

                                    // Payment for individual's certificate paid successfully.

                                    // Session already started

                                    $_SESSION['msgType'] = "1";

                                    $_SESSION['certID']  = "";

                                    $_SESSION['certID']  = NULL;

                                    unset( $_SESSION['certID']);

                                    redirect_to('../success.php');

                                } // End: if

                                else {

                                    // Payment for individual's certificate paid successfully.

                                    // Email not send on Localserver

                                    // Session already started

                                    unset( $_SESSION[ 'certID' ] );

                                    redirect_to( '../success.php' );

                                } // End: else

                            } // End: if ($SelectIndiQueryRun)

                        } // End:  if ($updateCertQueryRun)

                    } // End: if ($addNoteRun) {

                } // End: if ($insertPaymentQueryRun)

                else {

                    //echo "<br/>";

                    //echo $insertPaymentQry;

                    $_SESSION['msgType'] = "0";

                    $_SESSION['msg']     = $insertPaymentQry;

                    //"Error in submitting form. Contact With ISCN";

                    redirect_to( '../payment.php' );

                    exit();

                }

            } // End: if ($updateVoucherQueryRun)

        } // End: if ($balance >= $reqAmount)

        else {

            //You have insufficient balance in your voucher

            $_SESSION[ 'msgType' ] = "0";

            $_SESSION[ 'msg' ]     = "You have insufficient balance in your voucher.";

            redirect_to( '../payment.php' );

        }

    } //$voucherRecord[ 'count' ] == 1

    else {

        session_start();

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ]     = "You have entered invalid voucher number.";

        redirect_to( '../payment.php' );

    }

} //isset( $_POST[ 'certPayVoucher' ] )



/*------------------------------------------------------------------------------

                        individual Pay With Voucher

------------------------------------------------------------------------------*/

if ( isset($_POST['RequestcertPayVoucher'])) {

    //certPayVoucher clicked

    session_start();

    global $connection;

    $certID             = $_POST['cert_id'];

    // Here the user is client so we take userID = 0

    $userID        = 0;

    $now           = date( "Y-d-m h:i:s A" );

    $voucherNmbr   = mysql_prep(safe_int(remove_white_spaces($_POST['voucherNmbr'])));

    $reqAmount     =mysql_prep(safe_string(remove_white_spaces($_POST['packageAmount'])));

    $voucherQry  = "SELECT COUNT(*) AS count, ";

    $voucherQry .= "price, ";

    $voucherQry .= "balance, ";

    $voucherQry .= "unq_num ";

    $voucherQry .= "FROM voucher WHERE ";

    $voucherQry .= "unq_num = '$voucherNmbr'";

    $voucherQry .= "AND ";

    $voucherQry .= "status = 1 ";

    $voucherRun = mysqli_query( $connection, $voucherQry );

    $voucherRecord         = mysqli_fetch_assoc( $voucherRun );

    if ( $voucherRecord[ 'count' ] == 1 ) {

        //Voucher Existed

        $balance = $voucherRecord[ 'balance' ];

        if ( $balance >= $reqAmount ) {

            // Balance in voucher is enough for for registration

            // reqAmount minus from current voucher balance

            $newBalance          = $balance - $reqAmount;

            // Now update Voucher table with new balance

            $updateVoucherQry  = "UPDATE voucher SET ";

            $updateVoucherQry .= "balance = $newBalance ";

            $updateVoucherQry .= " WHERE ";

            $updateVoucherQry .= "unq_num = $voucherNmbr";

            $updateVoucherRun = mysqli_query( $connection, $updateVoucherQry );

            if ( $updateVoucherRun ) {

                // Amount for the certificate ( 15 $) has

                // been deducted from the entered voucher

                $insertPaymentQry = "INSERT INTO payments ( ";

                $insertPaymentQry .= "voucher_nmbr, ";

                $insertPaymentQry .= "payment_gross, ";

                $insertPaymentQry .= "currency_code, ";

                $insertPaymentQry .= "payment_status, ";

                $insertPaymentQry .= "student_id, ";

                $insertPaymentQry .= "type, ";

                $insertPaymentQry .= "pay_date ";

                $insertPaymentQry .= ") VALUES ( ";

                $insertPaymentQry .= "$voucherNmbr, ";

                $insertPaymentQry .= "$reqAmount, ";

                $insertPaymentQry .= "'USD', ";

                $insertPaymentQry .= "1, ";

                $insertPaymentQry .= "$certID, ";

                $insertPaymentQry .= "'Voucher', ";

                $insertPaymentQry .= "'$now' ";

                $insertPaymentQry .= ")";

                $insertPaymentRun = mysqli_query( $connection, $insertPaymentQry );

                if ( $insertPaymentRun ) {

                    //Payment record has been saved

                    $email = get_col_On_key( 'student', 'email', 'id', $certID );

                    $first_name = get_col_On_key( 'student', 'first_name', 'id', $certID );

                    $middle_name = get_col_On_key( 'student', 'middle_name', 'id', $certID );

                    $last_name = get_col_On_key( 'student', 'last_name', 'id', $certID );

                    $indifullName = $first_name." ".$middle_name." ".$last_name;

                    $to           = $email;

                    // For testing I put my Email

                    // $to = "jahangirkhattak13@gmail.com";

                    $subject      = "Confirmation of application recipt";

                    $message= "

                        <html>

                            <head>

                                <title>Confirmation Of Application Recipt</title>

                            </head>

                            <body>

                                <h3>Dear " . $indifullName . "</h3>

                                <p>

                                    Thank you, we have received your registration and 

                                    your payment. We’re glad that you’ve shown interest 

                                    in joining <span style='font-weight:900'>ISCN</span>

                                    system.

                                </p>

                                <p>

                                    Your application has been sent to the admissions 

                                    committee so it may take us a little time to 

                                    respond, but we will as soon as possible.

                                </p>

                                <br />

                                <p>For the online status checker: </p>

                                <a href='http://www.iscnsystem.org/'>

                                    http://www.iscnsystem.org/

                                </a>

                                <br />

                                <p>

                                    We will update your status and send you an email 

                                    once your application has been processed.

                                </p>

                                <br />

                                <p style='font-weight:900'>

                                    Thank you in advance for your anticipated  

                                    cooperation.

                                </p>

                                <br />

                                <p style='font-weight:900'>Best Regards,</p>

                                <br />

                                <pre style='color:blue;font-size:14px;'>

                                    International standard certificate number ( 

                                    <span style='font-weight:900'>ISCN</span>) team.

                                    International Standards Dataflow www.iscnsystem.org 

                                </pre>

                            </body>

                        </html>

                    ";

                    // Always set content-type when sending HTML email

                    $headers      = "MIME-Version: 1.0" . "\r\n";

                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                    // More headers

                    $headers .= 'From: <admin@iscnsystem.org>' . "\r\n";

                    $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";

                    if ( mail( $to, $subject, $message, $headers ) ) {

                        // Mail sent

                        // Payment for individual's certificate paid successfully.

                        // Session already started

                        $_SESSION['msgType'] = "1";

                        $_SESSION['certID']  = "";

                        $_SESSION['certID']  = NULL;

                        unset( $_SESSION['certID']);

                        redirect_to('../success.php');

                    } // End: if

                    else {

                        // Payment for individual's certificate paid successfully.

                        // Email not send on Localserver

                        // Session already started

                        unset( $_SESSION[ 'certID' ] );

                        redirect_to( '../success.php' );

                    } // End: else

                } // End: if ($insertPaymentQueryRun)

                else {

                    //echo "<br/>";

                    //echo $insertPaymentQry;

                    $_SESSION['msgType'] = "0";

                    $_SESSION['msg']     = $insertPaymentQry;

                    //"Error in submitting form. Contact With ISCN";

                    redirect_to( '../verification-payment.php?id='.$certID );

                    exit();

                }

            } // End: if ($updateVoucherQueryRun)

        } // End: if ($balance >= $reqAmount)

        else {

            //You have insufficient balance in your voucher

            $_SESSION[ 'msgType' ] = "0";

            $_SESSION[ 'msg' ]     = "You have insufficient balance in your voucher.";

            redirect_to( '../verification-payment.php?id='.$certID );

        }

    } //$voucherRecord[ 'count' ] == 1

    else {

        session_start();

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ]     = "You have entered invalid voucher number.";

        redirect_to( '../verification-payment.php?id='.$certID );

    }

} //isset( $_POST[ 'certPayVoucher' ] )



/*------------------------------------------------------------------------------

    Establishment Payment Through Voucher

------------------------------------------------------------------------------*/

if (isset($_POST['estbVoucherPayment'])) {

    session_start();

    global $connection;

    $estbID        = safe_int($_SESSION['estbIDPay']);

    if ($_SESSION['estbIDPay']=="") {

        redirect_to('../index.php');

        exit();

    }

    $requiredAmount =mysql_prep(remove_white_spaces(safe_float($_POST['packageAmount'])));

    $plan           = mysql_prep(remove_white_spaces(safe_int($_POST['plan'])));

    $voucherNmbr    = mysql_prep(remove_white_spaces(safe_string($_POST['voucherNmbr'])));

    $now            = date( 'Y-d-m h:i:s A' );

    do {

        $appIDLength = 5;

        $appID     = random_number($appIDLength);

        $searchAppIDQry = "SELECT COUNT(*) AS appIDCount ";

        $searchAppIDQry .= "FROM est ";

        $searchAppIDQry .= "WHERE ";

        $searchAppIDQry .="app_id =$appID";

        $searchAppIDRun=mysqli_query($connection,$searchAppIDQry);

        $appIDRecord = mysqli_fetch_assoc($searchAppIDRun);

        $appIDCount = $appIDRecord['appIDCount'];

    } while ( $appIDCount > 0);



    $estbID = $_SESSION[ 'estbIDPay' ];

    $voucherQry  = "SELECT ";

    $voucherQry .= "balance, ";

    $voucherQry .= "status ";

    $voucherQry .= "FROM ";

    $voucherQry .= "voucher ";

    $voucherQry .= "WHERE ";

    $voucherQry .= "unq_num='$voucherNmbr'";

    $voucherRun = mysqli_query( $connection, $voucherQry );

    $voucher    = mysqli_fetch_assoc( $voucherRun );

    $count      = mysqli_num_rows($voucherRun);

    $status    =  $voucher['status'];

    if ( $count > 0 && $status == 1) {

        // Voucher existed

        $balance = $voucher['balance'];

        if ( $balance >= $requiredAmount ) {

            // Balance in voucher is enough for for registration

            // reqAmount minus from current voucher balance

            $newBalance            = $balance - $requiredAmount;

            // Now update Voucher table with new balance

            $updateVoucherQry   = "UPDATE voucher SET ";

            $updateVoucherQry  .= "balance= $newBalance ";

            $updateVoucherQry  .= "WHERE ";

            $updateVoucherQry  .= "unq_num = $voucherNmbr";

            $updateVoucherRun = mysqli_query( $connection, $updateVoucherQry );

            $nmbrOfUpdatedVoucher  = mysqli_affected_rows( $connection );

            if ( $updateVoucherRun ) {

                //updateVoucherQuery run successfully";

                $updateEstbQry = "UPDATE est SET ";

                $updateEstbQry .= "reg_type = $plan , ";

                $updateEstbQry .= "app_id = $appID , ";

                $updateEstbQry .= "last_payment = '$now' ";

                $updateEstbQry .= "WHERE id = $estbID";

                $updateEstbRun = mysqli_query( $connection, $updateEstbQry );

                $nmbrOfUpdatedEstb  = mysqli_affected_rows( $connection );

                if ( $updateEstbRun ) {

                    //updateEstbQry run successfully";

                    global $connection;

                    $addPaymQry = "INSERT INTO payments ( ";

                    $addPaymQry .= "voucher_nmbr , ";

                    $addPaymQry .= "payment_gross , ";

                    $addPaymQry .= "currency_code , ";

                    $addPaymQry .= "payment_status , ";

                    $addPaymQry .= "est_id , ";

                    $addPaymQry .= "type , ";

                    $addPaymQry .= "pay_date , ";

                    $addPaymQry .= "plan ";

                    $addPaymQry .= ") VALUES ( ";

                    $addPaymQry .= "$voucherNmbr, ";

                    $addPaymQry .= "$requiredAmount, ";

                    $addPaymQry .= "'usd' , ";

                    $addPaymQry .= "1 , ";

                    $addPaymQry .= "$estbID , ";

                    $addPaymQry .= "'Voucher', ";

                    $addPaymQry .= "'$now' , ";

                    $addPaymQry .= "$plan ";

                    $addPaymQry .= ")";

                    $addPaymRun = mysqli_query( $connection, $addPaymQry );

                    if ( $addPaymRun ) {

                        //addPaymRun run successfully"

                        // Get name, email from establishmet table for sending

                        // confirmation email to the user.

                        $estbQry    = "SELECT name,email FROM est WHERE id=$estbID";

                        $estbRun = mysqli_query( $connection, $estbQry );

                        $count              = mysqli_num_rows( $estbRun );

                        if ( $count > 0 ) {

                            //estbQry run successfully

                            $estbRecord = mysqli_fetch_assoc( $estbRun );

                            $estbName   = $estbRecord[ 'name' ];

                            $estbEmail  = $estbRecord[ 'email' ];

                            $to         = $estbEmail;

                            // For testing I put my email.

                            // $to = "jahangirkhattak13@gmail.com";

                            $subject = "Confirmation of Establishment Registeration";

                            $message = "

                                        <html>

                                            <head>

                                                <title>Confirmation of Establishment Registeration</title>

                                            </head>

                                            <body>

                                                <h3>To " . $estbName . "</h3>

                                                <p>

                                                    We have received your registration and your payment. 

                                                    You will be receiving a response from us shortly to 

                                                    approve registration for your esteemed establishment.

                                                </p>

                                                <p>You can check your establishment status with application ID on the following link.

                                                </p>

                            <p>Application ID " . $appID . "</p>

                            <p>http://www.iscnsystem.org/</p>

                                                <br />

                                                <p style='font-weight:900'>Thanks in advance,</p>

                                                <p style='font-weight:900' >Best Regards,</p>

                                                <p style='color:blue;font-size:14px;'>

                                                    International standard certificate number ( 

                                                    <span style='font-weight:900'>ISCN</span>) team. 

                                                        International Standards Dataflow 

                                                        www.iscnsystem.org

                                                </p>

                                            </body>

                                        </html>

                                       ";

                            // Always set content-type when sending HTML email

                            $headers    = "MIME-Version: 1.0" . "\r\n";

                            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                            // More headers

                            $headers .= 'From: <admin@iscnsystem.org>' . "\r\n";

                            $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";

                            if ( mail($to, $subject, $message, $headers )) {

                                session_start();

                                $_SESSION['msg']     = "Payment for your establishment has beed recieved. Check your this " . $estbEmail . " email";

                                $_SESSION[ 'msgType' ] = "3";

                                unset($_SESSION['estbIDPay']);

                                redirect_to('../success.php');

                            } //mail( $to, $subject, $message, $headers )

                            else {

                                // Payment for your establishment has beed recieved

                                // But mail didn't send on LocalServer

                                session_start();

                                $_SESSION['msg']     = "Payment for your establishment has beed recieved but mail didn't send on LocalServer.";

                                $_SESSION['msgType'] = "3";

                                unset($_SESSION['estbIDPay']);

                                redirect_to('../success.php');

                            } // End: else {

                        } // End: if ($count > 0 ) {

                    } // End: if ( $addPaymRun ) {

                } // End: if ( $updateEstbRun ) {

            } // End: if ( $updateVoucherRun ) {

        } // End: if ( $balance >= $requiredAmount ) {

        else {

            // present balance is less than the registration fee.

            session_start();

            $_SESSION['msg']="Insufficient balance in your voucher.";

            $_SESSION['msgType'] = "0";

            redirect_to('../payment.php');

        } // End: else {

    } // End: if ( $count > 0 ) {

    else {

        // Voucher doesn't exist

        session_start();

        $_SESSION['msg']   = "You have entered invalid voucher number.";

        $_SESSION['msgType'] = "0";

        redirect_to('../payment.php');

    } // End: else {

} // End: if (isset($_POST['estbVoucherPayment'])) {







/*------------------------------------------------------------------------------

                    Registeration of Establishment

------------------------------------------------------------------------------*/

if ( isset($_POST['reg-estb'])) {

    session_start();

    global $connection;

    $estbName       = mysql_prep(safe_string($_POST[ 'estb-name']));

    $estbActivity   = mysql_prep(safe_int($_POST['estb-acti']));

    $estbCountry    = mysql_prep(safe_int($_POST['estb-country']));

    $estbCity       = mysql_prep(safe_string($_POST['estb-city']));

    $estbAddress    = mysql_prep(safe_string($_POST['estb-addr']));

    $estbWebsite    = mysql_prep(safe_string(remove_white_spaces($_POST['estb-website'])));

    $estbLicenseNo  = mysql_prep(safe_string(remove_white_spaces($_POST['estb-license-no'])));

    $estbLicenseExp = mysql_prep(safe_string($_POST['estb-license-exp']));

    $estbEmail      = mysql_prep(remove_white_spaces(safe_email($_POST['estb-email'])));

    $estbPhone      = mysql_prep(remove_white_spaces(safe_int($_POST['estb-phone'])));

    $estbFax        = mysql_prep(remove_white_spaces(safe_int($_POST['estb-fax' ] )));

    $estbManager    = mysql_prep(safe_string($_POST['estb-manager']));

    $estbPOBox      = mysql_prep(safe_string(remove_white_spaces($_POST['estb-pobox'])));

    $estbCerts      = $_POST['estbCertifciates'];

    $agentID        = mysql_prep(safe_string($_POST['agent-id']));

    $estbCaptcha    = mysql_prep(safe_string(remove_white_spaces($_POST['estb-captcha'])));

    $condition      = mysql_prep(safe_string($_POST['condition']));

    $createDate     = date('Y-d-m h:i:s A');

    $modifyDate     = date('Y-d-m h:i:s A');

    $now            = date('Y-d-m h:i:s A');

    $status         = safe_int("1");

    // Where the file is going to be placed

    $target_dir    = "../admin/uploads/establishment/";

    // individual Scanned Certificate

    $estbBusinessLicenseTMP =$_FILES['estb-business-license']['tmp_name'];

    $estbBusinessLicenseName=$_FILES["estb-business-license"]["name"];

    $estbBusinessLicenseNewName = date("Y-d-m--h-i-s-A")."-".$estbBusinessLicenseName;

    $estbBusinessLicenseNewName = safe_string($estbBusinessLicenseNewName);

    $targetEstbBusinessLicense = $target_dir.basename($estbBusinessLicenseNewName);

    if (move_uploaded_file($estbBusinessLicenseTMP,$targetEstbBusinessLicense))

    {

        // Now its time to run query. all data is valid

        $addEstbQry    = "INSERT INTO est ( ";

        // Columns

        $addEstbQry    .= "name, ";

        $addEstbQry    .= "activity, ";

        $addEstbQry    .= "country, ";

        $addEstbQry    .= "city, ";

        $addEstbQry    .= "address, ";

        $addEstbQry    .= "website, ";

        $addEstbQry    .= "email, ";

        $addEstbQry    .= "phone, ";

        if ($estbFax != "") {

            $addEstbQry    .= "fax, ";

        }

        $addEstbQry    .= "manager, ";

        $addEstbQry    .= "prof_lic_path, ";

        $addEstbQry    .= "create_date, ";

        $addEstbQry    .= "last_modify, ";

        $addEstbQry    .= "status, ";

        $addEstbQry    .= "lic_num, ";

        if ($agentID != "") {

            $addEstbQry    .= "agent_id, ";

        }

        if ($estbPOBox != "") {

            $addEstbQry    .= "pobox, ";

        }

        $addEstbQry    .= "lic_exp ";

        $addEstbQry    .= ") VALUES ( ";

        // Values

        $addEstbQry    .= "'$estbName', ";

        $addEstbQry    .= "$estbActivity, ";

        $addEstbQry    .= "$estbCountry, ";

        $addEstbQry    .= "'$estbCity', ";

        $addEstbQry    .= "'$estbAddress', ";

        $addEstbQry    .= "'$estbWebsite', ";

        $addEstbQry    .= "'$estbEmail', ";

        $addEstbQry    .= "$estbPhone, ";

        if ($estbFax != "") {

            $addEstbQry    .= "$estbFax, ";

        }

        $addEstbQry    .= "'$estbManager', ";

        $addEstbQry    .= "'$estbBusinessLicenseNewName', ";

        $addEstbQry    .= "'$createDate', ";

        $addEstbQry    .= "'$modifyDate', ";

        $addEstbQry    .=  "$status, ";

        $addEstbQry    .= "'$estbLicenseNo', ";



        if ($agentID != "") {

            $addEstbQry    .= "'$agentID', ";

        }

        if ($estbPOBox != "") {

            $addEstbQry    .= "'$estbPOBox', ";

        }

        $addEstbQry    .= "'$estbLicenseExp' ";

        $addEstbQry    .= ")";

        $addEstbQryRun = mysqli_query($connection, $addEstbQry);

        if ( $addEstbQryRun ) {

            $lastInsertEstbID = mysqli_insert_id($connection);

            foreach ( $estbCerts as $estbCert ) {

                $SafeEstbCertNo = safe_int($estbCert);

                $addEstbCertQry    = "INSERT INTO `est_cert`( ";

                $addEstbCertQry    .= "`est_id`, ";

                $addEstbCertQry    .= "`cert_type`, ";

                $addEstbCertQry    .= "`status` ";

                $addEstbCertQry    .= ") VALUES ( ";

                $addEstbCertQry    .= "$lastInsertEstbID, ";

                $addEstbCertQry    .= "$SafeEstbCertNo, ";

                $addEstbCertQry    .= "$status ";

                $addEstbCertQry    .= ")";

                $addEstbCertQryRun =   mysqli_query ($connection ,$addEstbCertQry);

            } // End: foreach (...) {

            if ($addEstbCertQryRun) {

                $_SESSION[ 'msgType' ] = "1";

                $_SESSION[ 'estbIDPay' ] = $lastInsertEstbID;

                $_SESSION['msg'] = "Your establishment data has been

                    recieved. Now we are redirecting youn to payment page";

                redirect_to( '../payment.php' );

                exit();

            } // End: if ($addEstbCertQryRun) {

            else{

                $_SESSION[ 'msgType' ] = "0";

                $_SESSION['msg']="Certificate list not saved. Contact with ISCN !";

                redirect_to( '../new-est-intl-id.php' );

                exit();

            }

        } // End: if ($addEstbQueryRun) {

        else{

            // Error in addEstbQry query

            echo "<br/>".$addEstbQry."<br/>";

            $_SESSION[ 'msgType' ] = "0";

            $_SESSION['msg'] = "Query Failed. Try again or Contact with ISCN!";

            //redirect_to( '../new-est-intl-id.php' );

            exit();

        }

    } // if (move_uploaded_file ...)

    else{

        echo "<br/>Certificate Image not uplaoded<br/>";

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION['msg'] = "Image not uplaoded. Try again !";

        redirect_to( '../new-est-intl-id.php' );

        exit();

    }

} //isset( $_POST['registerEstbBtn'])



/*----------  Establishment Login  ----------*/

if (isset($_POST['btn-login-est'])) {

    echo "establishment login";

    session_start();

    unset( $_SESSION['estbID'] );

    unset( $_SESSION['userID'] );

    unset( $_SESSION['userType'] );

    session_destroy();

    global $connection;

    $estbUserName = mysql_prep($_POST['estb-username']);

    $estbUserName =remove_white_spaces($estbUserName);

    $estbUserQry    = "SELECT users.id AS estbUserID,est_version, users.username AS estbUserName, users.password AS estbPasswordDB, users.estb_id AS estbID FROM users WHERE users.username = '$estbUserName'";

    $estbUserRun = mysqli_query( $connection, $estbUserQry );

    // Mysql_num_row is counting table row

    $count             = mysqli_num_rows( $estbUserRun );

    // If the entered username exist in database then $count

    // must be equal to 1

    if ( $count == 1 ) {

        //user existed. Now we are going to check password.

        $userRecord         = mysqli_fetch_assoc( $estbUserRun );

        // Geting md5 password from database of this username

        $estbUserPasswordDB = $userRecord['estbPasswordDB'];

        $enterPassword      = mysql_prep($_POST['estb-password']);

        $enterPassword      = remove_white_spaces($enterPassword);

        // convert user entered password with md5 function and add Salt

        $salt               = "isdf2017";

        $saltedpass           = sha1( $enterPassword . $salt );

        $hashedPassword       = md5($saltedpass);

        if ($hashedPassword == $estbUserPasswordDB) {

            // password is correct

            session_start();

             if($userRecord["est_version"] == "v1") {

                 $_SESSION['EST_ID_VALUE'] = $userRecord['estbID'];

                 $_SESSION['EST_ISCN_SESSION_CODE'] = 0;


                 $_SESSION['estbID'] = $userRecord['estbID'];

                 $_SESSION['userID'] = $userRecord['estbUserID'];


                 $_SESSION['msgType'] = "1";

                 $_SESSION['msg'] = "Welcome " . $userRecord['estbUserName'];


                 redirect_to('../select-version.php');

                 exit();
             }
             else if($userRecord["est_version"] == "v2")
             {
                 $_SESSION['EST_ID_VALUE'] = $userRecord['estbID'];

                 $_SESSION['EST_ISCN_SESSION_CODE'] = 0;


                 $_SESSION['msgType'] = "1";

                 $_SESSION['msg'] = "Welcome " . $userRecord['estbUserName'];


                 redirect_to('../establishment_v2/index.php');

                 exit();
             }

           

        } // End: if ($passwordSalted == $estbUserPasswordDB) {

        else {

            // Wrong password.

            session_start();

            $_SESSION['msgType']     = "0";

            $_SESSION['msg']         = "Wrong password.";

            redirect_to( '../index.php');

            exit();

        } // End: else {

    } // End: if ( $count == 1 ) {

      else {

       //search in editors
        $estbUserQry    = "SELECT est_editor.id AS editorID, est_editor.password AS estbPasswordDB FROM est_editor WHERE est_editor.username = '$estbUserName' AND active='Yes'";

        $estbUserRun = mysqli_query( $connection, $estbUserQry );

        // Mysql_num_row is counting table row

        $count             = mysqli_num_rows( $estbUserRun );

        // If the entered username exist in database then $count

        // must be equal to 1

        if ( $count == 1 ) {

            //user existed. Now we are going to check password.

            $userRecord         = mysqli_fetch_assoc( $estbUserRun );

            // Geting md5 password from database of this username

            $estbUserPasswordDB = $userRecord['estbPasswordDB'];

            $enterPassword      = mysql_prep($_POST['estb-password']);

            $enterPassword      = remove_white_spaces($enterPassword);

            // convert user entered password with md5 function and add Salt

            $salt               = "isdf2017";

            $saltedpass           = sha1( $enterPassword . $salt );

            $hashedPassword       = md5($saltedpass);

            if ($hashedPassword == $estbUserPasswordDB) {

                // password is correct

                session_start();

                // $_SESSION['estbID'] = $userRecord[ 'estbID' ];

                // $_SESSION['userID'] = $userRecord[ 'estbUserID' ];

                $_SESSION['msgType']     = "1";

                $_SESSION['EDITOR_ID_VALUE'] = $userRecord[ 'editorID' ];

                $_SESSION['EDITOR_ISCN_SESSION_CODE'] = 0;


                redirect_to( '../editor/index.php' );

                exit();

            } // End: if ($passwordSalted == $estbUserPasswordDB) {

            else {

                // Wrong password.

                session_start();

                $_SESSION['msgType']     = "0";

                $_SESSION['msg']         = "Wrong password.";

                redirect_to( '../index.php');

                exit();

            } // End: else {

        } // End: if ( $count == 1 ) {

        else {

            // Wrong user name.

            session_start();

            $_SESSION['msgType']     = "0";

            $_SESSION['msg']         = "Wrong user name.";

            redirect_to( '../index.php');

            exit();

        }//end else not found in editors

    } // End: else not found in est


} // End: if (isset($_POST['btn-login-est'])) {

function find_phd( $whereColumn, $key )

{

    global $connection;

    $safeKey = mysqli_real_escape_string( $connection, $key );

    $indvQry  = "SELECT ";

    $indvQry .= "phd_users.first_name AS firstName, ";

    $indvQry .= "phd_users.middle_name AS middleName, ";

    $indvQry .= "phd_users.last_name AS lastName, ";

    $indvQry .= "phd_users.gender AS indvGender, ";

    $indvQry .= "phd_users.nationality AS indvNation, ";

    $indvQry .= "phd_users.phone AS indvContact, ";

    $indvQry .= "phd_users.residence AS indvResidence, ";

    $indvQry .= "phd_users.birth_date AS indvDOB, ";

    $indvQry .= "phd_users.email AS indvEmail, ";

    $indvQry .= "phd_users.photo_path AS indvPhoto, ";

    $indvQry .= "phd_users.status AS indvStatus, ";

    $indvQry .= "phd_cert.phd_id AS indivID, ";

    $indvQry .= "phd_cert.estb_name AS certEstbName, ";

    $indvQry .= "phd_cert.country AS certEstbCountryID, ";

    $indvQry .= "phd_cert.city AS certEstbCity, ";

    $indvQry .= "phd_cert.address AS certEstbAddr, ";

    $indvQry .= "phd_cert.website AS certEstbWebsite, ";

    $indvQry .= "phd_cert.email AS certEstbEmail, ";

    $indvQry .= "phd_cert.phone AS certEstbPhone, ";

    $indvQry .= "phd_cert.major AS major, ";

    $indvQry .= "phd_cert.fax AS certEstbFax, ";

    $indvQry .= "phd_cert.major AS certTypeID, ";  //new

    $indvQry .= "phd_cert.field AS certField, ";  //new

    $indvQry .= "phd_cert.cert_date AS certDate, ";

    $indvQry .= "phd_cert.app_id AS appID, ";

    $indvQry .= "phd_cert.qr_image AS certQRImg, ";

    $indvQry .= "phd_cert.director As certEstbDirec, ";

    $indvQry .= "phd_cert.cert_path AS certImgName, ";

    $indvQry .= "phd_cert.create_date AS createDate, ";

    $indvQry .= "phd_cert.modify_date AS approvalDate, ";

    $indvQry .= "phd_cert.status AS certStatus, ";

    $indvQry .= "phd_cert.unq_code AS certUnqCode, ";

    $indvQry .= "phd_cert.code AS certISCNCode ";



    $indvQry .= "FROM phd_users INNER JOIN ";

    $indvQry .= "phd_cert ON phd_users.id = phd_cert.phd_id ";

    $indvQry .= "WHERE ";

    $indvQry.="phd_cert.{$whereColumn} = '$safeKey'";



    $indvRun = mysqli_query( $connection, $indvQry );



    confirm_query( $indvRun );



    if ($indvRun) {

        return $indvRun;

    } // End: if ( $indvRun ) {

    else {



        echo "error";

        return NULL;

    } // End: else {

}
function find_eng( $whereColumn, $key )

{

    global $connection;

    $safeKey = mysqli_real_escape_string( $connection, $key );

    $indvQry  = "SELECT ";

    $indvQry .= "eng_users.first_name AS firstName, ";

    $indvQry .= "eng_users.middle_name AS middleName, ";

    $indvQry .= "eng_users.last_name AS lastName, ";

    $indvQry .= "eng_users.gender AS indvGender, ";

    $indvQry .= "eng_users.nationality AS indvNation, ";

    $indvQry .= "eng_users.phone AS indvContact, ";

    $indvQry .= "eng_users.residence AS indvResidence, ";

    $indvQry .= "eng_users.birth_date AS indvDOB, ";

    $indvQry .= "eng_users.email AS indvEmail, ";

    $indvQry .= "eng_users.photo_path AS indvPhoto, ";

    $indvQry .= "eng_users.status AS indvStatus, ";

    $indvQry .= "eng_cert.eng_id AS indivID, ";

    $indvQry .= "eng_cert.estb_name AS certEstbName, ";

    $indvQry .= "eng_cert.country AS certEstbCountryID, ";

    $indvQry .= "eng_cert.city AS certEstbCity, ";

    $indvQry .= "eng_cert.address AS certEstbAddr, ";

    $indvQry .= "eng_cert.website AS certEstbWebsite, ";

    $indvQry .= "eng_cert.email AS certEstbEmail, ";

    $indvQry .= "eng_cert.phone AS certEstbPhone, ";

    $indvQry .= "eng_cert.major AS major, ";

    $indvQry .= "eng_cert.fax AS certEstbFax, ";

    $indvQry .= "eng_cert.major AS certTypeID, ";  //new

    $indvQry .= "eng_cert.field AS certField, ";  //new

    $indvQry .= "eng_cert.cert_date AS certDate, ";

    $indvQry .= "eng_cert.app_id AS appID, ";

    $indvQry .= "eng_cert.qr_image AS certQRImg, ";

    $indvQry .= "eng_cert.director As certEstbDirec, ";

    $indvQry .= "eng_cert.cert_path AS certImgName, ";

    $indvQry .= "eng_cert.create_date AS createDate, ";

    $indvQry .= "eng_cert.modify_date AS approvalDate, ";

    $indvQry .= "eng_cert.status AS certStatus, ";

    $indvQry .= "eng_cert.unq_code AS certUnqCode, ";

    $indvQry .= "eng_cert.code AS certISCNCode ";



    $indvQry .= "FROM eng_users INNER JOIN ";

    $indvQry .= "eng_cert ON eng_users.id = eng_cert.eng_id ";

    $indvQry .= "WHERE ";

    $indvQry.="eng_cert.{$whereColumn} = '$safeKey'";



    $indvRun = mysqli_query( $connection, $indvQry );



    confirm_query( $indvRun );



    if ($indvRun) {

        return $indvRun;

    } // End: if ( $indvRun ) {

    else {



        echo "error";

        return NULL;

    } // End: else {

}

function find_vtrainer( $whereColumn, $key )

{

    global $connection;

    $safeKey = mysqli_real_escape_string( $connection, $key );

    $indvQry  = "SELECT ";

    $indvQry .= "vtrainer_users.first_name AS firstName, ";

    $indvQry .= "vtrainer_users.middle_name AS middleName, ";

    $indvQry .= "vtrainer_users.last_name AS lastName, ";

    $indvQry .= "vtrainer_users.gender AS indvGender, ";

    $indvQry .= "vtrainer_users.nationality AS indvNation, ";

    $indvQry .= "vtrainer_users.phone AS indvContact, ";

    $indvQry .= "vtrainer_users.residence AS indvResidence, ";

    $indvQry .= "vtrainer_users.birth_date AS indvDOB, ";

    $indvQry .= "vtrainer_users.email AS indvEmail, ";

    $indvQry .= "vtrainer_users.photo_path AS indvPhoto, ";

    $indvQry .= "vtrainer_users.status AS indvStatus, ";

    $indvQry .= "vtrainer_cert.vtrainer_id AS indivID, ";

    $indvQry .= "vtrainer_cert.estb_name AS certEstbName, ";

    $indvQry .= "vtrainer_cert.country AS certEstbCountryID, ";

    $indvQry .= "vtrainer_cert.city AS certEstbCity, ";

    $indvQry .= "vtrainer_cert.address AS certEstbAddr, ";

    $indvQry .= "vtrainer_cert.website AS certEstbWebsite, ";

    $indvQry .= "vtrainer_cert.email AS certEstbEmail, ";

    $indvQry .= "vtrainer_cert.phone AS certEstbPhone, ";

    $indvQry .= "vtrainer_cert.major AS major, ";

    $indvQry .= "vtrainer_cert.fax AS certEstbFax, ";

    $indvQry .= "vtrainer_cert.major AS certTypeID, ";  //new

    $indvQry .= "vtrainer_cert.field AS certField, ";  //new

    $indvQry .= "vtrainer_cert.cert_date AS certDate, ";

    $indvQry .= "vtrainer_cert.app_id AS appID, ";

    $indvQry .= "vtrainer_cert.qr_image AS certQRImg, ";

    $indvQry .= "vtrainer_cert.director As certEstbDirec, ";

    $indvQry .= "vtrainer_cert.cert_path AS certImgName, ";

    $indvQry .= "vtrainer_cert.create_date AS createDate, ";

    $indvQry .= "vtrainer_cert.modify_date AS approvalDate, ";

    $indvQry .= "vtrainer_cert.status AS certStatus, ";

    $indvQry .= "vtrainer_cert.unq_code AS certUnqCode, ";

    $indvQry .= "vtrainer_cert.code AS certISCNCode ";



    $indvQry .= "FROM vtrainer_users INNER JOIN ";

    $indvQry .= "vtrainer_cert ON vtrainer_users.id = vtrainer_cert.vtrainer_id ";

    $indvQry .= "WHERE ";

    $indvQry.="vtrainer_cert.{$whereColumn} = '$safeKey'";



    $indvRun = mysqli_query( $connection, $indvQry );



    confirm_query( $indvRun );



    if ($indvRun) {

        return $indvRun;

    } // End: if ( $indvRun ) {

    else {



        echo "error";

        return NULL;

    } // End: else {

}

function find_doc( $whereColumn, $key )

{

    global $connection;

    $safeKey = mysqli_real_escape_string( $connection, $key );

    $indvQry  = "SELECT ";

    $indvQry .= "doc_users.first_name AS firstName, ";

    $indvQry .= "doc_users.middle_name AS middleName, ";

    $indvQry .= "doc_users.last_name AS lastName, ";

    $indvQry .= "doc_users.gender AS indvGender, ";

    $indvQry .= "doc_users.nationality AS indvNation, ";

    $indvQry .= "doc_users.phone AS indvContact, ";

    $indvQry .= "doc_users.residence AS indvResidence, ";

    $indvQry .= "doc_users.birth_date AS indvDOB, ";

    $indvQry .= "doc_users.email AS indvEmail, ";

    $indvQry .= "doc_users.photo_path AS indvPhoto, ";

    $indvQry .= "doc_users.status AS indvStatus, ";

    $indvQry .= "doc_cert.doc_id AS indivID, ";

    $indvQry .= "doc_cert.estb_name AS certEstbName, ";

    $indvQry .= "doc_cert.country AS certEstbCountryID, ";

    $indvQry .= "doc_cert.city AS certEstbCity, ";

    $indvQry .= "doc_cert.address AS certEstbAddr, ";

    $indvQry .= "doc_cert.website AS certEstbWebsite, ";

    $indvQry .= "doc_cert.email AS certEstbEmail, ";

    $indvQry .= "doc_cert.phone AS certEstbPhone, ";

    $indvQry .= "doc_cert.major AS major, ";

    $indvQry .= "doc_cert.fax AS certEstbFax, ";

    $indvQry .= "doc_cert.major AS certTypeID, ";  //new

    $indvQry .= "doc_cert.field AS certField, ";  //new

    $indvQry .= "doc_cert.cert_date AS certDate, ";

    $indvQry .= "doc_cert.app_id AS appID, ";

    $indvQry .= "doc_cert.qr_image AS certQRImg, ";

    $indvQry .= "doc_cert.director As certEstbDirec, ";

    $indvQry .= "doc_cert.cert_path AS certImgName, ";

    $indvQry .= "doc_cert.create_date AS createDate, ";

    $indvQry .= "doc_cert.modify_date AS approvalDate, ";

    $indvQry .= "doc_cert.status AS certStatus, ";

    $indvQry .= "doc_cert.unq_code AS certUnqCode, ";

    $indvQry .= "doc_cert.code AS certISCNCode ";



    $indvQry .= "FROM doc_users INNER JOIN ";

    $indvQry .= "doc_cert ON doc_users.id = doc_cert.doc_id ";

    $indvQry .= "WHERE ";

    $indvQry.="doc_cert.{$whereColumn} = '$safeKey'";



    $indvRun = mysqli_query( $connection, $indvQry );



    confirm_query( $indvRun );



    if ($indvRun) {

        return $indvRun;

    } // End: if ( $indvRun ) {

    else {



        echo "error";

        return NULL;

    } // End: else {

}

// Find Individual By ISCN Code

function find_indv( $whereColumn, $key )

{

    global $connection;

    $safeKey = mysqli_real_escape_string( $connection, $key );

    $indvQry  = "SELECT ";

    $indvQry .= "indv.first_name AS firstName, ";

    $indvQry .= "indv.middle_name AS middleName, ";

    $indvQry .= "indv.last_name AS lastName, ";

    $indvQry .= "indv.gender AS indvGender, ";

    $indvQry .= "indv.nationality AS indvNation, ";

    $indvQry .= "indv.phone AS indvContact, ";

    $indvQry .= "indv.residence AS indvResidence, ";

    $indvQry .= "indv.birth_date AS indvDOB, ";

    $indvQry .= "indv.email AS indvEmail, ";

    $indvQry .= "indv.photo_path AS indvPhoto, ";

    $indvQry .= "indv.status AS indvStatus, ";



    $indvQry .= "cert.indv_id AS indivID, ";

    $indvQry .= "cert.estb_name AS certEstbName, ";

    $indvQry .= "cert.estb_field AS certEstbActi, ";

    $indvQry .= "cert.country AS certEstbCountryID, ";

    $indvQry .= "cert.city AS certEstbCity, ";

    $indvQry .= "cert.address AS certEstbAddr, ";

    $indvQry .= "cert.website AS certEstbWebsite, ";

    $indvQry .= "cert.email AS certEstbEmail, ";

    $indvQry .= "cert.phone AS certEstbPhone, ";

    $indvQry .= "cert.fax AS certEstbFax, ";

    $indvQry .= "cert.type AS certTypeID, ";

    $indvQry .= "cert.cert_field AS certField, ";

    $indvQry .= "cert.cert_date AS certDate, ";

    $indvQry .= "cert.app_id AS appID, ";

    $indvQry .= "cert.qr_image AS certQRImg, ";

    $indvQry .= "cert.director As certEstbDirec, ";

    $indvQry .= "cert.cert_path AS certImgName, ";

    $indvQry .= "cert.create_date AS createDate, ";

    $indvQry .= "cert.modify_date AS approvalDate, ";

    $indvQry .= "cert.status AS certStatus, ";

    $indvQry .= "cert.unq_code AS certUnqCode, ";

    $indvQry .= "cert.code AS certISCNCode ";



    $indvQry .= "FROM indv INNER JOIN ";

    $indvQry .= "cert ON indv.id = cert.indv_id ";

    $indvQry .= "WHERE ";

    $indvQry.="cert.{$whereColumn} = '$safeKey'";



    $indvRun = mysqli_query( $connection, $indvQry );

    confirm_query( $indvRun );

    if ($indvRun) {

        return $indvRun;

    } // End: if ( $indvRun ) {

    else {

        echo "string";

        return NULL;

    } // End: else {

}

function find_student( $whereColumn, $key )

{

    global $connection;

    $safeKey = mysqli_real_escape_string( $connection, $key );

    $stdQry  = "SELECT ";

    $stdQry .= "student.first_name AS firstName, ";

    $stdQry .= "student.middle_name AS middleName, ";

    $stdQry .= "student.last_name AS lastName, ";

    $stdQry .= "student.globalid AS globalID, ";

    $stdQry .= "student.cert_serial_no AS certSerialNo, ";

    $stdQry .= "student.code AS certISCNCode, ";

    $stdQry .= "student.create_date AS createDate, ";

    $stdQry .= "student.unq_code AS certUnqCode, ";

    $stdQry .= "student.qr_image_path AS certQRImg, ";

    $stdQry .= "student.gender AS stdGender, ";

    $stdQry .= "student.nationality AS stdNation, ";

    $stdQry .= "student.phone AS stdContact, ";

    $stdQry .= "student.residence AS stdResidence, ";

    $stdQry .= "student.birth_date AS stdDOB, ";

    $stdQry .= "student.email AS stdEmail, ";

    $stdQry .= "student.photo_path AS stdPhoto, ";

    $stdQry .= "student.cert_path AS certImgName, ";

    $stdQry .= "student.id_path AS stdIDCard, ";

    $stdQry .= "student.order_id AS stdOrderID, ";

    $stdQry .= "student.modify_date AS approvalDate, ";

    $stdQry .= "student.estb_id , ";

    $stdQry .= "student.document_type , ";

    $stdQry .= "student.field_name , ";

    $stdQry .= "student.status AS stdStatus ";

    $stdQry .= "FROM student WHERE ";

    $stdQry .= "student.{$whereColumn} = '$safeKey'  ";

    $indvRun = mysqli_query( $connection, $stdQry );

    confirm_query( $indvRun );

    if ( $indvRun ) {

        return $indvRun;

    } // End: if ( $indvRun ) {

    else {

        return NULL;

    } // End: else {



}



// View establishment Details in popup Modal

if ( isset($_GET['estbID']) && isset($_GET['action']) ) {



    $action = $_GET['action'];



    if ($action == "showEstb" ) {

        show_estb_in_popup();

        $action = NULL;

    }

    elseif ($action == "editEstb") {

        edit_estb_in_popup();



    }

    elseif ($action == "changeEstbStatus") {

        change_estb_status();

    }

    elseif ($action == "newNote") {

        new_note();

    }

}

function new_note() {

    $safeEstbId = mysql_prep($_GET['estbID']); ?>

    <form id="add_note_form" action="../include/functions.php" method="POST">

        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">×</button>

            <h4 class="modal-title">New Establishment Note</h4>

        </div>

        <div class="modal-body">

            <!-- mysql data will be load here -->

            <div id="form-wrapper">

                <input type="hidden" readonly="" class="form-control"

                       value="<?php echo $safeEstbId; ?>" name="estbID">

                <div class="form-group" style="height: auto;">

                    <label for="note">Note :</label>

                    <textarea class="form-control" name="noteField" rows="5" id="note" placeholder="Enter note here ..."></textarea>

                </div>



            </div>

            <div class="clearfix"></div>

        </div>

        <div class="modal-footer">

            <button type="submit" id="btnSubmitNote" name="btnSubmitNote" class="btn btn-success btn-sm">Save Note</button>

            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>



        </div>

    </form>



    <?php

}

// Save Establishment Note

if ( isset( $_POST[ 'btnSubmitNote' ] ) ) {

    //btnSubmitNote clicked

    session_start();

    $userID = $_SESSION[ 'userID' ];

    $estbID = mysql_prep($_POST[ 'estbID' ]);

    $noteField = mysql_prep($_POST[ 'noteField' ]);

    $createDate = date( 'Y-d-m h:i:s A' );

    $addNoteQry    = "INSERT INTO `ests_notes`( ";

    $addNoteQry    .= "`text`, ";

    $addNoteQry    .= "`admin`, ";

    $addNoteQry    .= "`create_date`, ";

    $addNoteQry    .= "`est_id` ";

    $addNoteQry    .= ") VALUES ( ";

    $addNoteQry    .= "'$noteField', ";

    $addNoteQry    .= "$userID, ";

    $addNoteQry    .= "'$createDate', ";

    $addNoteQry    .= "'$estbID' ";

    $addNoteQry    .= ")";

    $addNoteRun = mysqli_query( $connection, $addNoteQry );

    if ( $addNoteRun ) {

        session_start();

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "Your note has been saved successfully !";

        redirect_to( '../admin/estb-list.php' );

    } //$addNoteRun

    else {

        // addNoteQry Query failed.

        session_start();

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ] = "Error #27 in query. Contact the admin of the website.";

        redirect_to( '../admin/estb-list.php' );

    }

} //isset( $_POST[ 'btnSubmitNote' ] )







// Edit Establishment Record

function edit_estb() {

    global $connection;

    session_start();

    $user = $_SESSION['userID'];

    $now = NOW;

    $estbID = mysql_prep( $_POST[ 'estbID' ] );

    $name       = mysql_prep( $_POST[ 'name' ] );

    $activityID = mysql_prep( $_POST[ 'estb-acti' ] );

    $countryID  = mysql_prep( $_POST[ 'nationality' ] );

    $city       = mysql_prep( $_POST[ 'city' ] );

    $address    = mysql_prep( $_POST[ 'address' ] );

    $website    = mysql_prep( $_POST[ 'website' ] );

    $email      = mysql_prep( $_POST[ 'email' ] );

    $phone      = mysql_prep( $_POST[ 'phone' ] );

    $fax        = mysql_prep( $_POST[ 'fax' ] );

    $manager    = mysql_prep( $_POST[ 'manager' ] );

    $licenseNo  = mysql_prep( $_POST[ 'licenseNo' ] );

    $licExpDate = mysql_prep( $_POST[ 'licExpDate' ] );

    $credit     = mysql_prep( $_POST[ 'presentCredit' ] );

    $agentID    = mysql_prep( $_POST[ 'agentID' ] );

    $pobox      = mysql_prep( $_POST[ 'pobox' ] );

    $estbCerts      = $_POST['estbCertifciates'];

    $selectCerts = sizeof($estbCerts);

    if ($selectCerts < 1) {

        session_start();

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ]     = "Choose at least one certificate !";

        header('Location: ../admin/estb-edit.php?id='.$estbID);

        exit();

    }

    $LicenseImg = "";

    // Where the file is going to be placed

    $target_dir    = "../admin/uploads/establishment/";

    $licenseTMP =$_FILES['scanned-cert']['tmp_name'];

    $LicenseImg   =$_FILES["scanned-cert"]["name"];

    $LicenseImgNewName = date("Y-d-m--h-i-s-A")."-".$LicenseImg;

    $targetLicenseImg = $target_dir.basename($LicenseImgNewName);



    $editEstbQry  = "UPDATE `est` SET ";

    $editEstbQry  .= "name='$name', ";

    $editEstbQry  .= "activity=$activityID, ";

    $editEstbQry  .= "country='$countryID', ";

    $editEstbQry  .= "city='$city', ";

    $editEstbQry  .= "address='$address', ";

    $editEstbQry  .= "website='$website', ";

    $editEstbQry  .= "email='$email', ";

    $editEstbQry  .= "phone='$phone', ";

    $editEstbQry  .= "fax='$fax', ";

    $editEstbQry  .= "manager='$manager', ";

    if ($LicenseImg != "") {

        if (move_uploaded_file($licenseTMP, $targetLicenseImg) ) {

            $editEstbQry  .= "prof_lic_path='$LicenseImgNewName', ";

        }

    } // End: if ($estbNewBusinessLicense != "") {

    $editEstbQry  .= "agent_id='$agentID', ";

    $editEstbQry  .= "last_modify='$now', ";

    $editEstbQry  .= "lic_num='$licenseNo', ";

    $editEstbQry  .= "lic_exp='$licExpDate', ";

    $editEstbQry  .= "present_credit='$credit', ";

    $editEstbQry  .= "pobox='$pobox', ";

    $editEstbQry  .= "modify_by= $user ";

    $editEstbQry  .= "WHERE id = '$estbID' ";

    $editEstbRun   = mysqli_query($connection,$editEstbQry);

    if ($editEstbRun) {

        $deleteOldEstbCertsQry = "UPDATE est_cert SET ";

        $deleteOldEstbCertsQry .= "status = 0 ";

        $deleteOldEstbCertsQry .= "WHERE ";

        $deleteOldEstbCertsQry .= "est_id = $estbID";

        $deleteOldEstbCertsRun = mysqli_query($connection, $deleteOldEstbCertsQry);

        if ($deleteOldEstbCertsRun) {

            foreach ( $estbCerts as $estbCert ) {

                echo '<br/>';

                echo "estbCert = ".$estbCert;

                echo '<br/>';

                $addEstbCertQry    = "INSERT INTO est_cert( ";

                $addEstbCertQry    .= "est_id, ";

                $addEstbCertQry    .= "cert_type, ";

                $addEstbCertQry    .= "status ";

                $addEstbCertQry    .= ") VALUES ( ";

                $addEstbCertQry    .= "$estbID, ";

                $addEstbCertQry    .= "$estbCert, ";

                $addEstbCertQry    .= "1";

                $addEstbCertQry    .= ")";

                $addEstbCertQryRun =   mysqli_query ($connection ,$addEstbCertQry);

            } // End: foreach (...) {

            if ($addEstbCertQryRun) {

                session_start();

                $_SESSION[ 'msgType' ] = "1";

                $_SESSION[ 'msg' ]     = "Establishment has been updated successfully !";

                header('Location: ../admin/estb-edit.php?id='.$estbID);

                exit();

            }

            else{

                //echo $addEstbCertQry;

                $_SESSION['msgType'] = "0";

                $_SESSION['msg']   = "Try again or Contact with ISCN !";

                header('Location: ../admin/estb-edit.php?id='.$estbID);

                exit();

            }

        } // End: if ($deleteOldEstbCertsRun) {

        else{

            //echo $deleteOldEstbCertsQry;

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']   = "Try again or Contact with ISCN !";

            header('Location: ../admin/estb-edit.php?id='.$estbID);

            exit();

        }

    } //$editEstbRun

    else {

        //echo $editEstbQry;

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']   = "Try again or Contact with ISCN !";

        header('Location: ../admin/estb-edit.php?id='.$estbID);

        exit();

    }

} // End: function edit_estb() {



/*----------  Go to Establishment Account From Admin  ----------*/

if (isset($_POST['goToEstb'])) {

    $estbID = $_POST['estb-id'];

    session_start();

    echo $_SESSION['userType'] = "admin";

    echo "<br/>";

    echo $estbSession = $_SESSION['estbID'] = $estbID;



    redirect_to( '../establishment/order.php' );

    exit();

}









// View Notes

if (isset($_GET['estbIDNotes'])) {

    $estbID = $_GET['estbIDNotes'];

    global $connection;

    $estbName = get_col_On_key( 'est', 'name', 'id', $estbID );

    // Now run your query here on the basic of

    $estbNotesQry   = "SELECT * ";

    $estbNotesQry  .= "FROM `ests_notes` WHERE ";

    $estbNotesQry  .= "`est_id` = $estbID ";

    $estbNotesQry  .= "ORDER BY id ASC";

    $estbNotesRun = mysqli_query( $connection, $estbNotesQry );

    confirm_query( $estbNotesRun );

    $count = mysqli_num_rows($estbNotesRun);

    if ($count > 0 ) { ?>

        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">×</button>

            <h4 class="modal-title">Establishment Note List</h4>

        </div>

        <div class="modal-body">

            <div class="row">

                <div class="box-body table-responsive">

                    <table class="table table-hover">

                        <tbody><tr class="bg-navy">

                            <th>Note ID</th>

                            <th>Date</th>

                            <th>Note Description</th>

                            <th>Added By</th>

                        </tr>

                        <?php while ( $oneNote = mysqli_fetch_assoc( $estbNotesRun ) ) { ?>

                            <tr>

                                <td><?php echo $oneNote['id']; ?></td>

                                <td><?php echo $oneNote['create_date']; ?></td>

                                <td><?php echo $oneNote['text']; ?></td>

                                <td><?php $userID = $oneNote['admin'];

                                    echo $userName = get_col_On_key( 'admin', 'username', 'id', $userID );





                                    ?></td>

                            </tr>

                        <?php } ?>

                        </tbody></table>

                </div><!-- /.box-body -->

            </div>

        </div>

        <div class="modal-footer">

            <button type="button" onclick="new_estb_note(this.value)"  value="<?php echo $estbID; ?>"  class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-second">Add New Note</button>

            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>

        </div>

        </div><?php

    } // End: if ($count > 0 ) {

    else { ?>

        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">×</button>

            <h4 class="modal-title">Establishment Note List</h4>

        </div>

        <div class="modal-body">

            <div class="row">

                <div class="box-body table-responsive">

                    <h5 class="text-center">No Record Found !</h5>

                </div>

            </div>

        </div>

        <div class="modal-footer">

        <div class="form-group">

            <button type="button" onclick="new_estb_note(this.value)" value="<?php echo $estbID; ?>" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#modal-second">Add New Note</button>

            <div class="clearfix"></div>

        </div>

        </div><?php

    } // End: } else {

} // End: if (isset($_GET['estbIDNotes'])) {

















if ( isset( $_GET[ 'estbIDToApprove' ] ) ) {

    $estbIDToApprove = $_GET[ 'estbIDToApprove' ];

    ?>















    <input type="hidden" name="estb-id" value="<?php

    echo $estbIDToApprove;

    ?>">















    <select class="form-control" name="estbStatusChange">















        <option value="">Choose Status</option>















        <option value="1">Arrived</option>















        <option value="2">Missing Information</option>











        <option value="3">Under Process</option>















        <option value="4">Approved</option>















    </select>















    <?php

} //isset( $_GET[ 'estbIDToApprove' ] )

function randomLetters( $length )

{

    $randomLetters = "";

    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    $max          = strlen( $codeAlphabet ); // edited

    for ( $i = 0; $i < $length; $i++ ) {

        $randomLetters .= $codeAlphabet[ crypto_rand_secure( 0, $max - 1 ) ];

    } //$i = 0; $i < $length; $i++

    return $randomLetters;

}

function randomNumbers( $length )

{

    $randomNumbers = "";

    $codeNumbers  = "0123456789";

    $max = strlen( $codeNumbers ); // edited

    for ( $i = 0; $i < $length; $i++ ) {

        $randomNumbers .= $codeNumbers[ crypto_rand_secure( 0, $max - 1 ) ];

    } //$i = 0; $i < $length; $i++

    return $randomNumbers;

}

function crypto_rand_secure( $min, $max )

{

    $range = $max - $min;

    if ( $range < 1 )

        return $min; // not so random...

    $log    = ceil( log( $range, 2 ) );

    $bytes  = (int) ( $log / 8 ) + 1; // length in bytes

    $bits   = (int) $log + 1; // length in bits

    $filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1

    do {

        $rnd = hexdec( bin2hex( openssl_random_pseudo_bytes( $bytes ) ) );

        $rnd = $rnd & $filter; // discard irrelevant bits

    } while ( $rnd > $range );

    return $min + $rnd;

}









/*------------------------------------------------------------------------------

                                add new voucher

------------------------------------------------------------------------------*/

if ( isset( $_POST[ 'add-new-voucher' ] ) ) {

    global $connection;

    //echo "addNewVoucher";

    $price              = $_POST['amount'];

    $balance            = $price;

    $quantity           = $_POST['quantity'];

    $owner           = $_POST['voucher-owner'];

    $now                = date( 'Y-d-m h:i:s A' );

    $addVoucherOrderQry = "INSERT INTO voucherorders ( ";

    $addVoucherOrderQry.= "price, ";

    $addVoucherOrderQry.= "create_date, ";

    $addVoucherOrderQry.= "voucher_owner, ";

    $addVoucherOrderQry.= "status ";

    $addVoucherOrderQry.= ") VALUES ( ";

    $addVoucherOrderQry.= "$price, ";

    $addVoucherOrderQry.= "'$now', ";

    $addVoucherOrderQry.= "'$owner', ";

    $addVoucherOrderQry.= "1 ";

    $addVoucherOrderQry.= ")";

    $addVoucherOrderRun = mysqli_query( $connection, $addVoucherOrderQry );
   
    if ( $addVoucherOrderRun ) {

        //voucher order added";

        $orderID = mysqli_insert_id( $connection );

        for ( $i = 0; $i < $quantity; $i++ ) {

            $newVoucher      = randomNumbers( 14 );

            $addVoucherQuery = "INSERT INTO voucher ( ";

            $addVoucherQuery .= "price , ";

            $addVoucherQuery .= "balance , ";

            $addVoucherQuery .= "unq_num, ";

            $addVoucherQuery .= "status, ";

            $addVoucherQuery .= "order_id";

            $addVoucherQuery .= ") VALUES ( ";

            $addVoucherQuery .= "$price , ";

            $addVoucherQuery .= "$balance , ";

            $addVoucherQuery .= "$newVoucher , ";

            $addVoucherQuery .= "1 , ";

            $addVoucherQuery .= "$orderID ";

            $addVoucherQuery .= ")";

            $addVoucherQueryRun = mysqli_query( $connection, $addVoucherQuery );

        } //$i = 0; $i < $quantity; $i++

        if ( $addVoucherQueryRun ) {

            // echo '<br>';

            // echo "voucher added";

            // echo '<br>';

            session_start();

            $_SESSION[ 'msgType' ] = "1";

            $_SESSION[ 'msg' ]     = "New voucher has been added sucessfully !";

            redirect_to( '../admin/voucher.php' );

        } //$addVoucherQueryRun

    } //End: if($addVoucherQueryRun)

} // End: if(isset($_POST['addNewVoucher'])){



/*-----------------------------------------------------------------------------

                    Individual Actions

------------------------------------------------------------------------------*/

if ( isset($_GET['indiID']) && isset($_GET['action']) ) {

    $action = $_GET['action'];

    if ($action == "viewIndi") {

        show_indi_modal();

    }

    elseif ($action == "editIndi") {

        edit_indi_modal();

    }



}





/*

                    Edit Individual Certificate

------------------------------------------------------------------------------*/

function update_indv_cert($from){

    global $connection;

    echo $from = $from;

    $indiID = mysql_prep($_POST[ 'indiID' ]);

    $indivFirstName = mysql_prep($_POST[ 'first-name' ]);

    $indivMiddleName = mysql_prep($_POST[ 'middle-name' ]);

    $indivLastName = mysql_prep($_POST[ 'last-name' ]);

    $indivGender = mysql_prep($_POST[ 'gender' ]);

    $indivNationality = mysql_prep($_POST[ 'nationality' ]);

    $indivResidence = mysql_prep($_POST[ 'residence' ]);

    $indivDOB = mysql_prep($_POST[ 'dob' ]);

    $indiEmail = mysql_prep($_POST[ 'email' ]);

    $indiPhone = mysql_prep($_POST[ 'phone' ]);

    //$indivPhoto = $_POST['photo'];

    // Individual Establishment Details

    $indivGlobalID = mysql_prep($_POST['personal-id']);

    $indivEstbName = mysql_prep($_POST['estb-name']);

    $indivEstbActivity = mysql_prep($_POST['estb-activity-id']);

    $indivEstbCity = mysql_prep($_POST['estb-city']);

    $indivEstbCountry = mysql_prep($_POST['estb-country-id']);

    $indivEstbAddress = mysql_prep($_POST['estb-address']);

    $indivEstbWebsite = mysql_prep($_POST['estb-website']);

    $indivEstbEmail = mysql_prep($_POST['estb-email']);

    $indivEstbPhone = mysql_prep($_POST['estb-phone']);

    $indivEstbFax = mysql_prep($_POST['estb-fax']);

    $indivEstbDirector = mysql_prep($_POST['estb-director']);

    // Certificate Information indivCertType

    $indivCertType = mysql_prep($_POST['cert-type-id']);

    $indivCertField = mysql_prep($_POST['cert-field']);

    $indivCertDate = mysql_prep($_POST['cert-issue-date']);

    $modifyDate  = date( "Y-d-m h:i:s A" );

    $user = $_SESSION['userID'];

    // Pictures

    echo $indivPhoto = mysql_prep($_POST[ 'photo' ]);

    echo $indivIDCard = mysql_prep($_POST[ 'id-card' ]);

    echo $indivScannedCert = mysql_prep($_POST[ 'scanned-cert' ]);



    // uplaod Pictures

    $target_dir        = "../admin/uploads/individual/";

    // individual photo

    $indivPhotoTMP     = $_FILES['photo']['tmp_name'];

    $indivPhotoName    = $_FILES["photo"]["name"];

    if ($indivPhotoName != '') {

        $indivPhotoNewName = date("Y-d-m--h-i-s-A")."-".$indivPhotoName;

    }

    $targetPhoto       = $target_dir ."photo/". basename($indivPhotoNewName);

    if ($indivPhotoName != '' ) {

        if (move_uploaded_file($indivPhotoTMP, $targetPhoto)) {

            echo '<br/>Photo oploaded<br/>';

        }

        else{

            echo '<br/>Error in Photo uploading<br/>';

            exit();

        }

    } // End: if (move_uploaded_file($indivPhotoTMP, $targetPhoto) ) {



    // individual ID card

    $indivIDCardTMP    = $_FILES['id-card']['tmp_name'];

    $indivIDCardName   = $_FILES["id-card"]["name"];

    $indivIDCardNewName= date("Y-d-m--h-i-s-A")."-".$indivIDCardName;

    $targetIDCard      = $target_dir ."id-card/". basename($indivIDCardNewName);

    if ($indivIDCardName!='') {

        if (move_uploaded_file($indivIDCardTMP, $targetIDCard)) {

            echo '<br/>ID Card oploaded<br/>';

        }

        else{

            echo '<br/>Error in ID Card uploading<br/>';

            exit();

        }

    }



    // individual Scanned Certificate

    $indivScannedCertTMP     = $_FILES['scanned-cert']['tmp_name'];

    $indivScannedCertName    = $_FILES["scanned-cert"]["name"];

    $indivScannedCertNewName = date("Y-d-m--h-i-s-A")."-".$indivScannedCertName;

    $targetScannedCert  = $target_dir ."certificate/". basename($indivScannedCertNewName);

    if ($indivScannedCertName!='') {

        if (move_uploaded_file($indivScannedCertTMP, $targetScannedCert)) {

            echo '<br/>Certificate oploaded<br/>';

        }

        else{

            echo '<br/>Error in Certificate uploading<br/>';

            exit();

        }

    }



    // Now update the indv table

    $indvUpdateQry = "UPDATE indv, cert  SET ";

    $indvUpdateQry .= "indv.first_name = '$indivFirstName', ";

    $indvUpdateQry .= "indv.middle_name = '$indivMiddleName', ";

    $indvUpdateQry .= "indv.last_name = '$indivLastName', ";

    $indvUpdateQry .= "indv.gender = '$indivGender', ";

    $indvUpdateQry .= "indv.nationality = $indivNationality, ";

    $indvUpdateQry .= "indv.residence = $indivResidence, ";

    $indvUpdateQry .= "indv.birth_date = '$indivDOB', ";

    $indvUpdateQry .= "indv.phone = '$indiPhone', ";

    $indvUpdateQry .= "indv.email = '$indiEmail', ";

    if ($indivPhotoName != '' ) {

        $indvUpdateQry .= "indv.photo_path  = '$indivPhotoNewName', ";

    }

    if ($indivIDCardName != '' ) {

        $indvUpdateQry .= "indv.id_path  = '$indivIDCardNewName', ";

    }

    $indvUpdateQry .= "indv.modify_date = '$modifyDate', ";

    $indvUpdateQry .= "indv.user = '$user', ";

    // Now update the cert table

    $indvUpdateQry .= "cert.globalid = '$indivGlobalID', ";

    $indvUpdateQry .= "cert.estb_name = '$indivEstbName', ";

    $indvUpdateQry .= "cert.estb_field = '$indivEstbActivity', ";

    $indvUpdateQry .= "cert.country = '$indivEstbCountry', ";

    $indvUpdateQry .= "cert.city = '$indivEstbCity', ";

    $indvUpdateQry .= "cert.address = '$indivEstbAddress', ";

    $indvUpdateQry .= "cert.website = '$indivEstbWebsite', ";

    $indvUpdateQry .= "cert.email = '$indivEstbEmail', ";

    $indvUpdateQry .= "cert.phone = '$indivEstbPhone', ";

    $indvUpdateQry .= "cert.fax = '$indivEstbFax', ";

    $indvUpdateQry .= "cert.type = '$indivCertType', ";

    $indvUpdateQry .= "cert.cert_date = '$indivCertDate', ";

    $indvUpdateQry .= "cert.cert_field = '$indivCertField', ";

    $indvUpdateQry .= "cert.director = '$indivEstbDirector', ";

    if ($indivScannedCertName != '' ) {

        $indvUpdateQry .= "cert.cert_path  = '$indivScannedCertNewName', ";

    }

    $indvUpdateQry .= "cert.modify_date = '$modifyDate' ";

    $indvUpdateQry .= "WHERE ";

    $indvUpdateQry .= "indv.id= $indiID AND ";

    $indvUpdateQry .= "cert.indv_id= $indiID";

    $indvUpdateRun = mysqli_query($connection, $indvUpdateQry);

    if ($indvUpdateRun) {

        // indvUpdateQry query run successfully.

        // indv and cert table updated successfully.

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "Individual record updated successfully .";

        if ($from="cert-edit.php") {

            session_start();

            $_SESSION['msg'] = 'Certificate record updated successfully !';

            header('Location: cert-edit.php?id='.$indiID);

            //zzz

        }

    }

    else {

        session_start();

        $_SESSION['msgType'] = "0";

        $_SESSION['msg'] = 'Certificate record not updated! Contact with ISCN ';

        header('Location: cert-edit.php?id='.$indiID);

        //echo '<br/>';

        //echo $indvUpdateQry;

        //exit();

    }

}



/*-----------------------------------------------------------------------------

                    Video Actions

------------------------------------------------------------------------------*/

if (isset($_GET['videoID']) && isset($_GET['action'])) {

    $videoID = $_GET['videoID'];

    if ($_GET['action'] == "videoEdit") {

        edit_video();

    }

}

/*

                    Edit Video's Form in Modal

------------------------------------------------------------------------------*/

function edit_video() {

    $videoID = $_GET['videoID'];

    $videoSet = get_all_cols_on_key( 'video', 'id', $videoID );

    $video = mysqli_fetch_assoc($videoSet);

    ?>

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Update Video </h4>

    </div>

    <div class="modal-body">

        <input type="hidden" name="video-id" value="<?php echo $videoID; ?>">

        <div class="form-group">

            <label for="title">Tilte</label>

            <input tabindex="1" type="text" class="form-control" id="video-title" name="title" value="<?php echo $video['video_title'] ?>">

        </div>

        <div class="form-group">

            <label for="link">Link</label>

            <input tabindex="2" type="text" class="form-control" id="video-link" name="link" value="<?php echo $video['video_url'] ?>">

        </div>

        <div class="form-group">

            <label for="videoStatus">Status</label>

            <select name="status" id="" class="form-control">

                <option value="">--Choose Status--</option>

                <option value="Publish">Publish</option>

                <option value="Draft">Draft</option>

                <option value="Trash">Trash</option>

            </select>

        </div>

        <div class="form-group">

            <label for="videoStatus">Display Page</label>

            <select name="page" class="form-control">

                <option value="">--Choose a Page--</option>

                <option value="home">Home</option>

                <option value="gallery">Gallery</option>

            </select>

        </div>

    </div>

    <div class="modal-footer">

        <button class="btn btn-info" type="submit" name="updateVideo" id="updateVideo">Update Video</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

    </div>

<?php }



/*

                    Update Video's Record

------------------------------------------------------------------------------*/

if (isset($_POST['updateVideo'])) {

    global $connection;

    session_start();

    $userID = $_SESSION['userID'];

    $videoID = $_POST['video-id'];

    $videoTitle = $_POST['title'];

    $videoURL = $_POST['link'];

    $videoArray = explode('=', $videoURL);

    $videoShortURL = $videoArray['1'];

    $modifyDate = date("Y-d-m h:i:s A" );

    $videoPage = $_POST['page'];

    $videoStatus = $_POST['status'];

    $videoUpdateQry  = "UPDATE video SET ";

    $videoUpdateQry .= "video.video_title = '$videoTitle', ";

    $videoUpdateQry .= "video.video_url = '$videoURL', ";

    $videoUpdateQry .= "video.video_short_url = '$videoShortURL', ";

    $videoUpdateQry .= "video.modify_date = '$modifyDate', ";

    $videoUpdateQry .= "video.user = $userID, ";

    $videoUpdateQry .= "video.display_page = '$videoPage', ";

    $videoUpdateQry .= "video.status = '$videoStatus' ";

    $videoUpdateQry .= "WHERE ";

    $videoUpdateQry .= "id = $videoID ";

    $videoUpdateRun = mysqli_query($connection, $videoUpdateQry);

    if ($videoUpdateRun) {

        // videoUpdateRun query run successfully.

        // video table updated successfully.

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "Video record has been updated successfully .";

        redirect_to( '../admin/video.php' );

    }

    else{

        // indvUpdateQry query failed.

        // indv and cert table not updated.

        session_start();

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ] = "Error #17 in query. Contact the admin of the website.";

        redirect_to( '../admin/video.php' );

    }

}



/*

                    Add New Video in Modal

------------------------------------------------------------------------------*/

if (isset($_POST['addNewVideo'])) {

    global $connection;

    session_start();

    $userID = $_SESSION['userID'];

    $videoTitle = $_POST['title'];

    $link = $_POST['link'];

    $video_url = explode('=', $link);

    $videoShortURL = $video_url['1'];

    $createDate = date("Y-d-m h:i:s A" );

    $modifyDate = date("Y-d-m h:i:s A" );

    $videoStatus = $_POST['videoStatus'];

    $displayPage = $_POST['page'];

    $addVideQry  = "INSERT INTO `video`( ";

    $addVideQry .= "`video_title`, ";

    $addVideQry .= "`video_url`, ";

    $addVideQry .= "`video_short_url`, ";

    $addVideQry .= "`create_date`, ";

    $addVideQry .= "`modify_date`, ";

    $addVideQry .= "`display_page`, ";

    $addVideQry .= "`user`, ";

    $addVideQry .= "`status` ";

    $addVideQry .= ") VALUES ( ";

    $addVideQry .= "'$videoTitle', ";

    $addVideQry .= "'$link', ";

    $addVideQry .= "'$videoShortURL', ";

    $addVideQry .= "'$createDate', ";

    $addVideQry .= "'$modifyDate', ";

    $addVideQry .= "'$displayPage', ";

    $addVideQry .= "$userID, ";

    $addVideQry .= "'$videoStatus'";

    $addVideQry .= ")";

    $addVideRun = mysqli_query($connection, $addVideQry);

    if ($addVideRun) {

        // addVideQry query run successfully.

        // video added successfully.

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "Video added successfully .";

        redirect_to( '../admin/video.php' );

    }

}



/*-----------------------------------------------------------------------------

                    Users Actions

------------------------------------------------------------------------------*/

if (isset($_GET['userID']) && isset($_GET['action'])) {

    $userID = $_GET['userID'];

    if ($_GET['action'] == "editUser") {

        user_edit_form();

    }

}

/*

                    User Edit Form in Modal

------------------------------------------------------------------------------*/

function user_edit_form()

{

    global $connection;

    $userID = $_GET['userID'];

    $usersQry  = "SELECT ";

    $usersQry .= "admin.id AS userID, ";

    $usersQry .= "admin.full_name AS fullName, ";

    $usersQry .= "admin.username AS userName, ";

    $usersQry .= "admin.email AS userEmail, ";

    $usersQry .= "admin.designation AS userDesig, ";

    $usersQry .= "admin.plain_password AS userPass, ";

    $usersQry .= "admin.create_date AS createDate, ";

    $usersQry .= "admin.modify_date AS modifyDate, ";

    $usersQry .= "admin.modify_by AS modifiedBy, ";

    $usersQry .= "admin.status AS userStatus ";

    $usersQry .= "FROM admin ";

    $usersQry .= "WHERE ";

    $usersQry .= "admin.status = 1 ";

    $usersQry .= "AND ";

    $usersQry .= "admin.id = $userID";

    $usersRun = mysqli_query($connection, $usersQry);

    $userRecord = mysqli_fetch_assoc($usersRun);

    ?>

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">×</button>

        <h4 class="modal-title">Edit Account</h4>

    </div>

    <div class="modal-body">

        <div class="input-group">

            <input type="hidden" name="user-id" value="<?php echo $userID; ?>">

        </div>

        <label for="full-name">Full Name</label>

        <div class="input-group">

            <span class="input-group-addon"><i class="fa fa-user"></i></span>

            <input type="text" name="full-name" class="form-control" value="<?php echo $userRecord['fullName'] ?>">

        </div>

        <br>

        <label for="username">User Name</label>

        <div class="input-group">

            <span class="input-group-addon">@</span>

            <input type="text" name="username" class="form-control" value="<?php echo $userRecord['userName'] ?>">

        </div>

        <br>

        <label for="user-email">Email Name</label>

        <div class="input-group">

            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

            <input type="email" name="user-email" class="form-control" value="<?php echo $userRecord['userEmail'] ?>">

        </div>

        <br>

        <label for="user-desig">Designation</label>

        <div class="input-group">

            <span class="input-group-addon"><i class="fa fa-tasks"></i></span>

            <div class="form-group">

                <select name="user-desig" class="form-control" required="required">

                    <option value="">---User Type---</option>

                    <option value="Administrator">Administrator</option>

                    <option value="Editor">Editor</option>

                    <option value="Author">Author</option>

                    <option value="Contributor">Contributor</option>

                    <option value="Subscriber">Subscriber</option>

                </select>

            </div>

        </div>

        <br>

        <label for="user-pass">New Password</label>

        <div class="input-group">

            <span class="input-group-addon"><i class="fa fa-user-secret"></i></span>

            <input type="text" name="new-password" class="form-control" value="<?php echo $userRecord['userPass'] ?>">

        </div>

        <br>

        <label for="user-pass">Picture</label>

        <div class="input-group">

            <span class="input-group-addon"><i class="fa fa-photo"></i></span>

            <input type="file" name="userPhoto" class="form-control">

        </div>

        <br>

    </div> <!-- End: modal-body-->

    <div class="modal-footer">

        <button type="submit" name="btn-edit-user" class="btn btn-success">Update</button>

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

    </div>

<?php } // End: user_edit_form()



/*

                    User Edit

------------------------------------------------------------------------------*/

if (isset($_POST['btn-edit-user'])) {

    global $connection;

    $newPlainPass = NULL;

    $userPhotoName = NULL;

    $userID = $_POST['user-id'];

    $fullName = $_POST['full-name'];

    $userName = $_POST['username'];

    $userEmail = $_POST['user-email'];

    $userDesig = $_POST['user-desig'];

    $newPlainPass = $_POST['new-password'];

    $modifyDate = date("Y-d-m h:i:s A" );

    session_start();

    $editorID = $_SESSION['userID'];

    $target_dir        = "../admin/uploads/user-pictures/";

    // Get all IDs of those admins where designation = Administrator

    $checkAdminQry  = "SELECT id FROM admin ";

    $checkAdminQry .= "WHERE (CONVERT(`designation` USING utf8) ";

    $checkAdminQry .= "LIKE 'Administrator' ";

    $checkAdminQry .= ")";

    $checkAdminRun = mysqli_query($connection, $checkAdminQry);

    // Change designation = Administrator to designation = Subscriber of all records

    while ($checkAdminRecord = mysqli_fetch_assoc($checkAdminRun)) {

        // Get the ID of that record whose designation = Administrator

        $adminID = $checkAdminRecord['id'];

        // Change designation = Administrator to designation = Subscriber

        $updateAdminQry  ="UPDATE `admin` SET ";

        $updateAdminQry .="`designation` = 'Subscriber' ";

        $updateAdminQry .="WHERE ";

        $updateAdminQry .="`id` = $adminID";

        $updateAdminRun = mysqli_query($connection, $updateAdminQry);

    } // End: while (...)

    // Now there is no record whose designation` = 'Administrator

    $userPhotoTMP =$_FILES['userPhoto']['tmp_name'];

    $userPhotoName = $_FILES["userPhoto"]["name"];

    $userPhotoNewName =  date("Ymdhis")."-".$userPhotoName;

    $updateUserQry  = "UPDATE admin SET ";

    $updateUserQry .= "username = '$userName', ";

    $updateUserQry .= "email = '$userEmail', ";

    $updateUserQry .= "full_name = '$fullName', ";

    if ( $userPhotoName != NULL) {



        $targetPhoto = $target_dir . basename($userPhotoNewName);

        move_uploaded_file($userPhotoTMP , $targetPhoto);

        $updateUserQry .= "photo_path = '$userPhotoNewName', ";

    }

    if ($newPlainPass != NULL) {

        $salt = "isdf2017";

        $newPassMD5 = md5($newPlainPass.$salt);

        $updateUserQry .= "password = '$newPassMD5', ";

    }

    $updateUserQry .= "designation = '$userDesig', ";

    $updateUserQry .= "modify_date = '$modifyDate' ";

    $updateUserQry .= "WHERE id = $userID";

    $updateUserRun = mysqli_query($connection, $updateUserQry);

    if ($updateUserRun) {

        // updateUserQry query run successfully.

        // user record  has beedn updated successfully.

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "User record has been updated successfully .";

        redirect_to( '../admin/users.php' );

    } // End: if ($updateUserRun) {

    else{

        //echo "<br>".$updateUserQry;

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ] = "Operation failed. Try again or contact with ISCN.";

        redirect_to( '../admin/users.php' );

    }



}





if (isset($_POST['btn-add-user'])) {



    global $connection;

    $userName = $_POST['user-name'];

    $userEmail = $_POST['user-email'];

    $fullName = $_POST['full-name'];

    $userPlainPass = $_POST['password'];

    $userConfirmPass = $_POST['confirm-pass'];

    $salt = "isdf2017";

    $userPassMD5 = md5($userPlainPass.$salt);

    $userDesig = $_POST['user-desig'];

    $now = date("Y-d-m h:i:s A" );

    session_start();

    $creatorID = $_SESSION['userID'];

    if ($userPlainPass == $userConfirmPass) {

        $target_dir        = "../admin/uploads/user-pictures/";

        // User photo

        $userPhotoTMP =$_FILES['userPhoto']['tmp_name'];

        $userPhotoName = $_FILES["userPhoto"]["name"];

        $userPhotoNewName =  date("Ymdhis")."-".$userPhotoName;

        $targetPhoto = $target_dir . basename($userPhotoNewName);

        if ($userDesig == "Administrator") {

            // Get all IDs of those admins where designation = Administrator

            $checkAdminQry  = "SELECT id FROM admin ";

            $checkAdminQry .= "WHERE (CONVERT(`designation` USING utf8) ";

            $checkAdminQry .= "LIKE 'Administrator' ";

            $checkAdminQry .= ")";

            $checkAdminRun = mysqli_query($connection, $checkAdminQry);

            // Change designation = Administrator to designation = Subscriber of all records

            while ($checkAdminRecord = mysqli_fetch_assoc($checkAdminRun)) {

                // Get the ID of that record whose designation = Administrator

                $adminID = $checkAdminRecord['id'];

                // Change designation = Administrator to designation = Subscriber

                $updateAdminQry  ="UPDATE `admin` SET ";

                $updateAdminQry .="`designation` = 'Subscriber' ";

                $updateAdminQry .="WHERE ";

                $updateAdminQry .="`id` = $adminID";

                $updateAdminRun = mysqli_query($connection, $updateAdminQry);

            }

        }

        // Now there is no record whose designation` = 'Administrator

        // Upload admin picture

        if (move_uploaded_file($userPhotoTMP , $targetPhoto) ) {

            // Picture uploaded

            // insert record

            $addUserQry  = "INSERT INTO admin ( ";

            $addUserQry .= "username, ";

            $addUserQry .= "email, ";

            $addUserQry .= "full_name, ";

            $addUserQry .= "photo_path, ";

            $addUserQry .= "password, ";

            $addUserQry .= "plain_password, ";

            $addUserQry .= "designation, ";

            $addUserQry .= "create_date, ";

            $addUserQry .= "modify_date, ";

            $addUserQry .= "modify_by, ";

            $addUserQry .= "`status`";

            $addUserQry .= ") VALUES ( ";

            $addUserQry .= "'$userName', ";

            $addUserQry .= "'$userEmail', ";

            $addUserQry .= "'$fullName', ";

            $addUserQry .= "'$userPhotoNewName', ";

            $addUserQry .= "'$userPassMD5', ";

            $addUserQry .= "'$userPlainPass', ";

            $addUserQry .= "'$userDesig', ";

            $addUserQry .= "'$now', ";

            $addUserQry .= "'$now', ";

            $addUserQry .= "$creatorID, ";

            $addUserQry .= "1";

            $addUserQry .= ")";

            $addUserRun = mysqli_query($connection, $addUserQry);

            if ($addUserRun) {

                // password and confirm password are not same.

                // user record  has beedn updated successfully.

                // We already started session that's why we will not start it here.

                $_SESSION[ 'msgType' ] = "1";

                $_SESSION[ 'msg' ] = "User added successfully !";

                redirect_to( '../admin/users.php' );

            }

        } // End: if (move_uploaded_file($userPhotoTMP, $userTargetPhoto) ) {





    } // End: if ($userPass === $userConfirmPass) {

    else {

        // password and confirm password are not same.

        // user record  has beedn updated successfully.

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ] = "User not added because password and confirm password were not same !";

        redirect_to( '../admin/users.php' );

    } // End: else {

} // End: if (isset($_POST['btn-add-user'])) {



function get_extension($file) {

    $extension = end(explode(".", $file));

    return $extension ? $extension : false;

}



/*-----------------------------------------------------------------------------

                    File Actions

------------------------------------------------------------------------------*/

if (isset($_GET['fileID']) && isset($_GET['action'])) {

    $fileID = $_GET['fileID'];

    if ($_GET['action'] == "editFile") {

        file_edit_form();

    }

}

/*

                    File Edit Form in Modal

------------------------------------------------------------------------------*/

function file_edit_form()

{

    global $connection;

    $fileID = $_GET['fileID'];

    $fileQry  = "SELECT ";

    $fileQry .= "files_upload.id AS fileID, ";

    $fileQry .= "files_upload.file_title AS fileTitle, ";

    $fileQry .= "files_upload.file_path AS filePath, ";

    $fileQry .= "files_upload.modify_date AS modifyDate, ";

    $fileQry .= "files_upload.user AS modifyUser, ";

    $fileQry .= "files_upload.status AS fileStatus ";

    $fileQry .= "FROM files_upload ";

    $fileQry .= "WHERE ";

    $fileQry .= "id = $fileID";

    $fileRun = mysqli_query($connection, $fileQry);

    $fileRecord = mysqli_fetch_assoc($fileRun);

    ?>

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">×</button>

        <h4 class="modal-title">Edit File</h4>

    </div>

    <div class="modal-body">

        <input type="hidden" readonly="readonly" class="form-control" name="file-id" value="<?php echo $fileID; ?>">

        <input type="hidden" readonly="readonly" class="form-control" name="file-old-name" value="<?php echo $fileRecord['filePath'] ?>">

        <label for="full-name">Title</label>

        <div class="form-group">

            <input type="text" name="title" class="form-control" value="<?php echo $fileRecord['fileTitle'] ?>">

        </div>

        <br>

        <label for="user-desig">Status</label>

        <div class="form-group">

            <select name="file-status" required="required" class="form-control">

                <option value="">--Choose Status--</option>

                <option value="Publish">Publish</option>

                <option value="Draft">Draft</option>

                <option value="Trash">Trash</option>

            </select>

        </div>

        <br>

    </div> <!-- End: modal-body-->

    <div class="modal-footer">

        <button class="btn btn-info" type="submit" name="edit-file" id="update-file">Update</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

    </div>

<?php } // End: user_edit_form()





if (isset($_POST['edit-file'])) {

    global $connection;

    $fileName = NULL;

    echo "<br/>";

    echo "fileID =".$fileID = $_POST['file-id'];

    echo "<br/>";

    echo "fileOldNameWithPath =".$fileOldNameWithPath = '../admin/uploads/files/'.$_POST['file-old-name'];

    echo "<br/>";

    $fileTitle = $_POST['title'];

    echo "<br/>";

    echo "fileNewName =".$fileNewName = remove_white_spaces(date("Ymdhis")."-".$fileTitle).".pdf";

    echo "<br/>";

    echo "fileNewNameWithPath =".$fileNewNameWithPath = '../admin/uploads/files/'.$fileNewName;

    echo "<br/>";

    if (rename($fileOldNameWithPath,$fileNewNameWithPath)) {

        echo "renamed.";

    }

    else{

        echo "Not renamed.";

    }

    echo "<br/>";

    echo "fileStatus =".$fileStatus = $_POST['file-status'];

    echo "<br/>";

    $modifyDate = date("Y-d-m h:i:s A" );

    session_start();

    echo "<br/>";

    echo "editorID =".$editorID = $_SESSION['userID'];

    echo "<br/>";

    // Get all IDs of those files where status = Publish

    $checkFileQry  = "SELECT id FROM files_upload ";

    $checkFileQry .= "WHERE (CONVERT(`status` USING utf8) ";

    $checkFileQry .= "LIKE 'Publish' ";

    $checkFileQry .= ")";

    $checkFileRun = mysqli_query($connection, $checkFileQry);

    // Change status = Publish to status = Draft of all records

    while ($checkFileRecord = mysqli_fetch_assoc($checkFileRun)) {



        // Get the ID of that record whose designation = Administrator

        $id = $checkFileRecord['id'];

        // Change designation = Administrator to designation = Subscriber

        $updateFileQry  ="UPDATE `files_upload` SET ";

        $updateFileQry .="`status` = 'Draft' ";

        $updateFileQry .="WHERE ";

        $updateFileQry .="`id` = $id";

        $updateFileRun = mysqli_query($connection, $updateFileQry);

    } // End: while (...)

    // Now there is no record whose designation` = 'Administrator

    $updateFileQry  = "UPDATE files_upload SET ";

    $updateFileQry .= "files_upload.file_title = '$fileTitle', ";

    $updateFileQry .= "files_upload.status = '$fileStatus', ";

    $updateFileQry .= "files_upload.file_path = '$fileNewName', ";

    $updateFileQry .= "files_upload.user = $editorID ";

    $updateFileQry .= "WHERE files_upload.id = $fileID";

    $updateFileRun = mysqli_query($connection, $updateFileQry);

    if ($updateFileRun) {

        // updateUserQry query run successfully.

        // user record  has beedn updated successfully.

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "File record has been updated successfully .";

        redirect_to( '../admin/file.php' );

    } // End: if ($updateUserRun) {

} // End: if (isset($_POST['update-file'])) {







if (isset($_POST['save-file'])) {

    global $connection;

    $fileTitle = $_POST['title'];

    $fileStatus = $_POST['fileStatus'];

    $now = date("Y-d-m h:i:s A" );

    session_start();

    $creatorID = $_SESSION['userID'];

    if ($fileStatus == "Publish") {

        // Get all IDs of those admins where designation = Administrator

        $checkFileQry  = "SELECT id FROM files_upload ";

        $checkFileQry .= "WHERE (CONVERT(`status` USING utf8) ";

        $checkFileQry .= "LIKE 'Publish' ";

        $checkFileQry .= ")";

        $checkFileRun = mysqli_query($connection, $checkFileQry);

        // Change designation = Administrator to designation = Subscriber of all records

        while ($checkFileRecord = mysqli_fetch_assoc($checkFileRun)) {

            // Get the ID of that record whose designation = Administrator

            $fileID = $checkFileRecord['id'];

            // Change designation = Administrator to designation = Subscriber

            $updateAdminQry  ="UPDATE `files_upload` SET ";

            $updateAdminQry .="`status` = 'Draft' ";

            $updateAdminQry .="WHERE ";

            $updateAdminQry .="`id` = $fileID";

            $updateAdminRun = mysqli_query($connection, $updateAdminQry);

        }

    }

    // Now there is no record whose status` = 'Publish

    // File

    $target_dir        = "../admin/uploads/files/";

    $fileTMP =$_FILES['file']['tmp_name'];

    $fileName = $_FILES["file"]["name"];

    $fileExplode = explode('.', $fileName);

    $fileExt = $fileExplode['1'];

    $fileNewName =  date("Ymdhis")."-".$fileTitle.".".$fileExt;

    $targetFile = $target_dir . basename($fileNewName);

    // Upload file

    if(move_uploaded_file($fileTMP , $targetFile))

    {

        // File has been uploaded successfully.

        $uploadFileQry  ="INSERT INTO files_upload ( ";

        $uploadFileQry .="file_title, ";

        $uploadFileQry .="file_path, ";

        $uploadFileQry .="create_date, ";

        $uploadFileQry .="user, ";

        $uploadFileQry .="status ";

        $uploadFileQry .=") VALUES ( ";

        $uploadFileQry .="'$fileTitle', ";

        $uploadFileQry .="'$fileNewName', ";

        $uploadFileQry .="'$now', ";

        $uploadFileQry .="$creatorID, ";

        $uploadFileQry .="'$fileStatus' ";

        $uploadFileQry .=")";

        $uploadFileRun = mysqli_query($connection, $uploadFileQry);

        if ($uploadFileRun) {

            // password and confirm password are not same.

            // user record  has beedn updated successfully.

            // We already started session that's why we will not start it here.

            $_SESSION[ 'msgType' ] = "1";

            $_SESSION[ 'msg' ] = "File has been uploaded successfully !";

            redirect_to( '../admin/file.php' );

        }

    }

}

/*-----------------------------------------------------------------------------

                    Country Actions

------------------------------------------------------------------------------*/

if (isset($_POST['edit-country-btn'])) {

    global $connection;

    echo "<br/>";

    echo "countryID=".$countryID = $_POST['country-id'];

    echo "<br/>";

    echo "countryName=".$countryName = $_POST['country-name'];

    echo "<br/>";

    echo "countryCode=".$countryCode = $_POST['country-code'];

    echo "<br/>";

    echo "countryAbbr=".$countryAbbr = $_POST['country-abbr'];

    echo "<br/>";

    $updateCountryQry  = "UPDATE countries_tbl SET ";

    $updateCountryQry .= "countries_tbl.country_name = '$countryName', ";

    $updateCountryQry .= "countries_tbl.country_code = '$countryCode', ";

    $updateCountryQry .= "countries_tbl.country_abbr = '$countryAbbr' ";

    $updateCountryQry .= "WHERE  countries_tbl.id = $countryID";

    $updateCountryRun = mysqli_query($connection, $updateCountryQry);

    if ($updateCountryRun) {

        // Country has been updated successfully !

        session_start();

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "Country has been uploaded successfully !";

        redirect_to( '../admin/country.php' );

    }



}



/*-----------------------------------------------------------------------------

                    Establishment Dashboard

------------------------------------------------------------------------------*/

function add_new_order($pageFrom) {

    echo 'add_new_order';

    global $connection;

    session_start();



    echo "creatorID=".$creatorID   = $_SESSION['userID'];

    if ($pageFrom=="admin/order-new.php") {

        $creatorType = "admin";

    }

    else{

        $creatorType = "user";

    }

    $estbID      = mysql_prep($_POST['estb-id']);

    $orderTitle  = mysql_prep($_POST['order-title']);

    $estbOrderCertType = mysql_prep($_POST['estb-cert-type']);

    $estbOrderCertField = mysql_prep($_POST['estb-cert-field']);

    $certDate = mysql_prep($_POST['cert-date']);

    $createDate = $modifyDate = NOW;

    $status = 1;

    $addOrderQry  = "INSERT INTO orders( ";

    $addOrderQry .= "title, ";

    $addOrderQry .= "est_id, ";

    $addOrderQry .= "cert_type, ";

    $addOrderQry .= "field, ";

    $addOrderQry .= "cert_date, ";

    $addOrderQry .= "editor, ";

    $addOrderQry .= "editor_type, ";

    $addOrderQry .= "create_date, ";

    $addOrderQry .= "modify_date, ";

    $addOrderQry .= "visibility, ";

    $addOrderQry .= "status ";

    $addOrderQry .= ") VALUES ( ";

    $addOrderQry .= "'$orderTitle', ";

    $addOrderQry .= "$estbID, ";

    $addOrderQry .= "$estbOrderCertType, ";

    $addOrderQry .= "'$estbOrderCertField', ";

    $addOrderQry .= "'$certDate', ";

    $addOrderQry .= "$creatorID, ";

    $addOrderQry .= "'$creatorType', ";

    $addOrderQry .= "'$createDate', ";

    $addOrderQry .= "'$modifyDate', ";

    $addOrderQry .= "'0', ";

    $addOrderQry .= "$status ";

    $addOrderQry .= ")";

    $addOrderRun = mysqli_query($connection, $addOrderQry);

    if ($addOrderRun) {

        // Establishment Order has been added successfully !

        // We already started session that's why we will not start it here.

        $_SESSION[ 'msgType' ] = "1";

        $_SESSION[ 'msg' ] = "Order has been added successfully !";

        if ($pageFrom=="add-order.php") {

            redirect_to( '../establishment/add-order.php' );

            exit();

        }

        elseif ($pageFrom=="add-order.php") {

            redirect_to( '../establishment/add-order.php' );

            exit();

        }

        elseif ($pageFrom=="admin/order-new.php") {

            header('Location: ../admin/order-list.php?estb-id='.$estbID);

            //redirect_to( '../admin/order-list.php' );

            exit();

        }



    }

    else{

        // Query Failed.

        //echo '<br/>';

        //echo $addOrderQry;

        //echo '<br/>';

        $_SESSION[ 'msgType' ] = "0";

        $_SESSION[ 'msg' ] = "Order not added. Try again or contact with ISCN.";

        if ($pageFrom=="establishment/order-list.php") {

            redirect_to( '../establishment/order-list.php' );

            exit();

        }

        elseif ($pageFrom=="establishment/order-grid.php") {

            redirect_to( '../establishment/order-grid.php' );

            exit();

        }

        elseif ("establishment/order-edit.php") {

            redirect_to( '../establishment/order-list.php' );

            exit();

        }

        elseif ("establishment/cert-list.php") {

            redirect_to( '../establishment/order-list.php' );

            exit();

        }

        elseif ("admin/order-new.php") {

            redirect_to( '../admin/order-list.php' );

            exit();

        }



    }

}





/*

                    Establishment Order

------------------------------------------------------------------------------*/

if (isset($_GET['orderID']) && isset($_GET['action'])) {

    $orderID = $_GET['orderID'];

    if ($_GET['action'] == "viewPersonsInOrder") {

        get_all_students_of_estb_order();

    }

    elseif ( $_GET['action'] == "payNow") {

        pay_for_estb_order();

    }

}

function pay_for_estb_order() {

    global $connection;

    $orderID = $_GET['orderID'];

    $estbID = get_col_On_key('orders','est_id','id',$orderID);

    $estbName = get_col_On_key('est','name','id',$estbID);

    session_start();

    $user = $_SESSION['userID'];

    //$totalStds = total_rows($tbl, $where, $key);

    $totalStds = total_rows("student", "order_id", $orderID);

    $pricePerDoc = 10;

    $totalPayment = ( $pricePerDoc * $totalStds );



    ?>

    <div class="modal-content">

        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">&times;</button>

            <h4 class="modal-title">Pay for

                <?php echo $estbName; ?> Order #

                <?php echo $orderID; ?>

            </h4>

        </div>

        <div class="modal-body">

            <div class="hide  alert alert-warning alert-dismissible">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <p><i class="icon fa fa-warning"></i>

                    After payment all persons of this order will be approved and they will recieve ISCN number along with QR code in email.

                </p>

                <p><strong>After payment this order will locked and you wouldn't change any thing of this order.</strong></p>



            </div>

            <fieldset>

                <legend>

                    <span>Pay with PayPal</span>

                </legend>

                <form id="paypalform" class="form-inline" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">

                    <input type="hidden" name="cmd" value="_xclick">

                    <input type="hidden" name="business" value="info@intlstandards.com">

                    <input type="hidden" name="item_name" value="Validate Certificate">

                    <div class="row">

                        <div class="col-xs-5">

                            <label class="pull-right">Order ID :</label>

                        </div>

                        <div class="col-xs-1 noPad">

                            <input type="text" style="background: transparent;border: 0;width: 50px;" name="order-id" id="order-id" value="<?php echo $orderID; ?>" readonly />

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div class="row">

                        <div class="col-xs-5">

                            <label  class="pull-right">Price Per Document :</label>

                        </div>

                        <div class="col-xs-1 noPad">

                            <input style="background: transparent;border: 0;width: 50px;" type="text" id="amount" name="amount" value="<?php echo $pricePerDoc; ?>" readonly>

                        </div>

                        <div class="col-xs-6 noPad">

                            <p style="margin-top:-4px;margin-left:-20px;margin-bottom: 0;">$</p>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <input type="hidden" id="custom" name="custom" value="" >

                    <input type="hidden" name="notify_url" value="http://www.iscnsystem.org/paypal/doneorder.php">

                    <input type="hidden" name="currency_code" value="USD">

                    <div class="row">

                        <div class="col-xs-5">

                            <label  class="pull-right">Number of Documents :</label>

                        </div>

                        <div class="col-xs-1 noPad">

                            <input style="background: transparent;border: 0;width: 50px;" type="text" id="quantity" name="quantity" value="<?php echo $totalStds; ?>" readonly >

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div class="row">

                        <div class="col-xs-5">

                            <label class="pull-right">Total :</label>

                        </div>

                        <div class="col-xs-7 noPad">

                            <?php echo $pricePerDoc." * ".$totalStds." = "; ?>

                            <input type="text" id="totalStds" style="background: transparent;border: 0;width: 50px; font-weight: bold;" name="totalStds" value="<?php echo $totalPayment; ?>" readonly >

                            <span>US $</span>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-7 col-xs-offset-5 noPad">

                            <br>

                            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

                        </div>

                        <div class="clearfix"></div>

                    </div>

                </form>

            </fieldset>

            <fieldset>

                <legend>

                    <span>Pay with ISCN Credit</span>

                </legend>

                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST">

                    <div class="row">

                        <div class="col-xs-5">

                            <label class="pull-right">Order ID :</label>

                        </div>

                        <div class="col-xs-1 noPad">

                            <input type="text" style="background: transparent;border: 0;width: 50px;" name="order-id" id="order-id" value="<?php echo $orderID; ?>" readonly />

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div class="row">

                        <div class="col-xs-5">

                            <label class="pull-right">Price Per Document :</label>

                        </div>

                        <div class="col-xs-1 noPad">

                            <input type="text" style="background: transparent;border: 0;width: 50px;" id="price-doc" name="price-doc" value="<?php echo $pricePerDoc; ?>" readonly />

                        </div>

                        <div class="col-xs-6 noPad">

                            <p style="margin-top:-4px;margin-left:-20px;margin-bottom: 0;">$</p>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <div class="row">

                        <div class="col-xs-5">

                            <label class="pull-right">Number of Documents :</label>

                        </div>

                        <div class="col-xs-1 noPad">

                            <input type="text" id="totalStds" style="background: transparent;border: 0;width: 50px;" name="total-stds" value="<?php echo $totalStds; ?>" readonly >

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-5">

                            <label class="pull-right">Total :</label>

                        </div>

                        <div class="col-xs-7 noPad">

                            <?php echo $pricePerDoc." * ".$totalStds." = "; ?>

                            <input type="text" id="totalStds" style="background: transparent;border: 0;width: 50px; font-weight: bold" name="total-payment" value="<?php echo $totalPayment; ?>" readonly >

                            <span>US $</span>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-7 col-xs-offset-5 noPad">

                            <br>

                            <button type="submit" name="pay-order-voucher" class="btn btn-success">Pay With Credit</button>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                </form>

            </fieldset>

        </div>

        <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        </div>

    </div>

    <?php

} // End: function pay_for_estb_order() {





function get_all_students_of_estb_order()

{

    global $connection;

    $orderID = $_GET['orderID'];

    $getAllStudentQry = "SELECT ";

    $getAllStudentQry .= "id AS personID, ";

    $getAllStudentQry .= "first_name, ";

    $getAllStudentQry .= "middle_name, ";

    $getAllStudentQry .= "last_name, ";

    $getAllStudentQry .= "code, ";

    $getAllStudentQry .= "unq_code, ";

    $getAllStudentQry .= "globalid, ";

    $getAllStudentQry .= "qr_image_path, ";

    $getAllStudentQry .= "gender, ";

    $getAllStudentQry .= "birth_date, ";

    $getAllStudentQry .= "nationality, ";

    $getAllStudentQry .= "residence, ";

    $getAllStudentQry .= "phone, ";

    $getAllStudentQry .= "email, ";

    $getAllStudentQry .= "photo_path, ";

    $getAllStudentQry .= "id_path, ";

    $getAllStudentQry .= "cert_path, ";

    $getAllStudentQry .= "editor, ";

    $getAllStudentQry .= "editor_type, ";

    $getAllStudentQry .= "missing_note, ";

    $getAllStudentQry .= "order_id, ";

    $getAllStudentQry .= "create_date, ";

    $getAllStudentQry .= "status ";

    $getAllStudentQry .= "FROM student WHERE ";

    $getAllStudentQry .= "order_id = $orderID ";

    $getAllStudentQry .= "AND ";

    $getAllStudentQry .= "status <> 0";

    $getAllStudentRun = mysqli_query ($connection, $getAllStudentQry);



    ?>

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">×</button>

        <h4 class="modal-title">Persons List of Order # <?php echo $orderID; ?></h4>

    </div>

    <div class="modal-body">

    <input type="hidden" value="<?php echo $orderID; ?>" id="order-id">

    <div class="tablecontainer">

        <table class="table-bordered table-striped table-condensed">

            <thead>

            <tr>

                <th><div class="col-extra-small">ID</div></th>

                <th><div class="col-medium">Name</div></th>

                <th><div class="col-medium">QR Code</div></th>

                <th><div class="col-extra-small">Gender</div></th>

                <th><div class="col-medium">Date of Birth</div></th>

                <th><div class="col-large">Status</div></th>

                <th><div class="col-large">Nationality</div></th>

                <th><div class="col-large">Residenc</div></th>

                <th><div class="col-large">Registration Date</div></th>

                <th><div class="col-small">Phone</div></th>

                <th><div class="col-small">Email</div></th>

                <th><div class="col-small">Global ID</div></th>

                <th><div class="col-medium">Personal Photo</div></th>

                <th><div class="col-medium">Scanned ID Card</div></th>

                <th>

                    <div class="col-medium">Scanned Certificate</div>

                </th>

                <th>

                    <div class="col-medium">

                        User

                    </div>

                </th>

                <th><div class="col-extra-small">Report</div></th>

                <th><div class="col-extra-small">Edit</div></th>

                <th><div class="col-extra-small">Delete</div></th>

                <th><div class="col-large">ISCN Code</div></th>

            </tr>

            </thead>

            <tbody>

            <?php while ($student = mysqli_fetch_assoc($getAllStudentRun)) { ?>

                <tr>

                    <td><?php echo $student['personID']; ?></td>

                    <td><?php echo $fullName = $student['first_name']." ".$student['middle_name']." ".$student['last_name']; ?></td>

                    <td>

                        <div class="small-photo-wrapper">

                            <?php if ($student['qr_image_path'] == "") { ?>

                                <img src="../include/qrimages/no-qr-code.png" alt="No QR Code Image">



                            <?php } else { ?>

                                <img src="../include/qrimages/student/<?php echo $student['qr_image_path'];?>" alt="QR Code Image">

                            <?php } ?>

                        </div>

                    </td>

                    <td><?php echo $student['gender']; ?></td>

                    <td><?php echo $student['birth_date']; ?></td>

                    <td>

                        <?php if ($student['status'] == '1') {

                            echo "Arrived";

                        }

                        elseif ($student['status'] == "2") {

                            echo "Missing Information";

                        }

                        elseif ($student['status'] == "3") {

                            echo "Under Process";

                        }

                        else if($student['status'] == "4"){

                            echo "Approved";

                        }

                        if ( $student['status'] != 4 || $student['status'] != "4") { ?>

                        <button type="button"

                                person-id="<?php echo $student['personID'];  ?>"  class="btn btn-link btn-xs

                                    btn-change-status" data-toggle="modal"

                                data-target="#modal-four"> Change Status

                            </button><?php

                        }

                        else { ?>

                            <button type="button" class="btn btn-danger">Person is Locked<i class="fa fa-fw fa-lock"></i></button>

                        <?php } ?>



                    </td>

                    <td><?php echo $nationality = get_col_On_key('countries_tbl','country_name','id',$student['nationality']);

                        ?>

                    </td>

                    <td>

                        <?php echo $residence = get_col_On_key('countries_tbl','country_name','id',$student['residence']);

                        ?>

                    </td>

                    <td>

                        <?php echo $student['create_date']; ?>

                    </td>

                    <td>

                        <?php echo $student['phone']; ?></td>

                    <td><?php echo $student['email']; ?></td>

                    <td><?php echo $student['globalid']; ?></td>

                    <td>

                        <div class="small-photo-wrapper">

                            <?php $photo = $student['photo_path']; if ($photo !='') {

                                $photo = '../admin/uploads/student/photo/'.$photo;

                            } else{

                                $photo = '../admin/uploads/no-image.jpg';

                            } ?>

                            <img src="<?php echo $photo; ?>" alt="Photo">

                        </div>

                    </td>

                    <td>

                        <div class="small-photo-wrapper">

                            <?php

                            $IDCard = $student['id_path'];

                            $ext = pathinfo($IDCard, PATHINFO_EXTENSION);

                            $ext = strtolower($ext);

                            if ($ext == "pdf") { ?>

                                <button onclick="view_person_cert(this.value)" value="<?php echo $student['personID']; ?>" type="button" data-toggle="modal" data-target="#ModalViewCertPerson">

                                    <img src="../assets/img/certificate-preview.png" alt="">

                                </button>

                            <?php } // End: if ($ext == "pdf") {

                            elseif ($ext == "jpeg" || $ext == "jpg" ||$ext == "png" ) { ?>

                            <button onclick="view_person_cert(this.value)" style="background-color: #fff;border: 1px solid #ddd;" type="button" value="<?php echo $student['personID']; ?>" data-toggle="modal" data-target="#ModalViewCertPerson">

                                <img src="../admin/uploads/student/id-card/<?php echo $IDCard; ?>" alt="ID Card">

                                </button><?php

                            } // End: elseif ($ext == "JPEG" || $ext == "JPG" ||$ext == "png" )

                            ?>



                        </div>

                    </td>

                    <td>

                        <div class="small-photo-wrapper">

                            <?php

                            $scannedCert = $student['cert_path'];

                            if ($scannedCert !='') {

                                $scannedCert = '../admin/uploads/student/certificate/'.$scannedCert;

                            } else {

                                $scannedCert = '../admin/uploads/no-image.jpg';

                            }

                            ?>

                            <img src="<?php echo $scannedCert; ?>" alt="Certificate">

                        </div>

                    </td>

                    <td>

                        <?php  $userType = $student['editor_type'];

                        if ($userType == 'admin') {

                            echo $user = get_col_On_key('admin','full_name','id',$student['editor']);

                            echo " - Admin";

                        }

                        else{

                            echo $user = get_col_On_key('users','full_name','id',$student['editor']);

                        }



                        ?>

                    </td>

                    <td>

                        <?php if ($student['status'] == 4 || $student['status']== "4") { ?>

                            <button type="button" onclick="view_std_report(this.value)" value="<?php echo $student['personID'];  ?>"  class="btn btn-success btn-xs view-report" data-toggle="modal" data-target="#report-modal">

                                <i class="fa fa-certificate" aria-hidden="true"></i>

                            </button>

                        <?php } ?>

                    </td>

                    <td>

                        <?php

                        if ( $student['status'] != 4 || $student['status'] != "4") { ?>

                        <button type="button"

                                onclick="edit_person(this.value)"

                                value="<?php echo $student['personID'];  ?>"  class="btn btn-success btn-xs

                                    edit-person" data-toggle="modal"

                                data-target="#modal-seven">

                                <i class="fa fa-edit" aria-hidden="true"></i>

                            </button><?php

                        }

                        else { ?>

                            <button type="button" class="btn btn-danger">Locked<i class="fa fa-fw fa-lock"></i></button>

                        <?php } ?>

                    </td>

                    <td>

                        <?php

                        if ( $student['status'] != 4 || $student['status'] != "4") { ?>

                        <button type="button"

                                id="<?php echo $student['personID']; ?>"  class="btn btn-success btn-xs delete-person" data-toggle="modal"

                                data-target="#ModalDeletePerson">

                                <i class="fa fa-remove" aria-hidden="true"></i>

                            </button><?php

                        }

                        else { ?>

                            <button type="button" class="btn btn-xs btn-danger">Locked<i class="fa fa-fw fa-lock"></i></button>

                        <?php } ?>

                    </td>

                    <td>

                        <?php echo $student['code']; ?>

                    </td>



                </tr>

            <?php } // End while(...) ?>

            </tbody>

        </table>

    </div><!-- End: tablecontainer -->

    <div class="modal-footer">

        <?php $orderStatus=get_col_On_key('orders','status','id',$orderID);

        if ($orderStatus == 1 || $orderStatus == "1") { ?>

            <button type="button" data-toggle="modal" data-target="#modal-third" name="add-new-student" id="add-new-student" class="btn btn-success">Add New Person</button>

        <?php } else { ?>

            <button type="button" class="btn btn-danger">

                Order is Locked

                <i class="fa fa-fw fa-lock"></i>

            </button>

        <?php   }   ?>



        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

    </div>

<?php }



/*

                    Change Person/Studen Status

------------------------------------------------------------------------------*/

if (isset($_POST['btn-person-status'])) {

    global $connection;

    session_start();

    $user = $_SESSION['userID'];

    //$estbID = $_SESSION['estbID'];

    $personID = $_POST['person-id'];

    $status = $_POST['person-status'];

    $now = date( 'Y-d-m h:i:s A' );

    $presentStatus = get_col_On_key('student','status','id',$personID);

    if ($status == 4 || $status == "4") {

        // For changing status to 4 i.e. Approved, first check its present

        // status and if present status = 4 (Approved) then give message to

        // the user "This person already Approved !"

        if ($presentStatus == 4 || $presentStatus == "4") {

            // We already started session that's why we will not start it here.

            $_SESSION[ 'msgType' ] = "1";

            $_SESSION[ 'msg' ] = "This person already Approved !";

            redirect_to( '../establishment/order.php' );

        } // End: if ($presentStatus == 4 || $presentStatus == "4") {

        else

        {

            // This person is not Approve.

            // Now we are going to Approve

            // First we will create an ISCN number

            $newISCNCode = NULL;

            // Get Establishment Country from Establishment table

            $countryID =get_col_On_key('student','nationality','id',$personID);

            $countryAbbr =get_col_On_key('countries_tbl','country_abbr','id',$countryID);

            $estbActiID =get_col_On_key('est','activity','id',$estbID);

            $estbActiAbbr =get_col_On_key('estab__activities_tbl','abbreviation','id',$estbActiID);

            //$orderID =get_col_On_key('student','order_id','id',$personID);



            $orderCertTypeID =get_col_On_key('orders','cert_type','est_id',$estbID);

            $certTypeAbbr =get_col_On_key('certificate_tbl','abbreviation','id',$orderCertTypeID);





            $orderID =get_col_On_key('student','order_id','id',$personID);



            $certIssueDate =get_col_On_key('student','cert_date','id',$personID);



            $certIssuedYear = substr($certIssueDate,2,2);

            $estbUserName =get_col_On_key('est','username','id',$estbID);

            do {

                $fourLetters = randomLetters( 4 );

                $fourNumbers = randomNumbers( 4 );

                $newUniqCode = $fourLetters . $fourNumbers;

                $searchUniqCodeInStdTblQry = "SELECT COUNT(*) AS stdCount ";

                $searchUniqCodeInStdTblQry .= "FROM student ";

                $searchUniqCodeInStdTblQry .= "WHERE ";

                $searchUniqCodeInStdTblQry .= "unq_code = '$newUniqCode'";

                $searchUniqCodeInStdTblRun = mysqli_query($connection, $searchUniqCodeInStdTblQry);

                $searchedStdRecord = mysqli_fetch_assoc($searchUniqCodeInStdTblRun);

                $countStd = $searchedStdRecord['stdCount'];



                $searchUniqCodeInCertQry = "SELECT COUNT(*) AS certCount ";

                $searchUniqCodeInCertQry .= "FROM cert ";

                $searchUniqCodeInCertQry .= "WHERE ";

                $searchUniqCodeInCertQry .= "unq_code = '$newUniqCode'";

                $searchUniqCodeInCertRun = mysqli_query($connection, $searchUniqCodeInCertQry);

                $searchedCertRecord = mysqli_fetch_assoc($searchUniqCodeInCertRun);

                $certCount = $searchedCertRecord['certCount'];



            } // End: do {

            while ( $countStd > 0 && $certCount > 0);

            $newISCNCode  = "57".$countryAbbr."-";

            $newISCNCode .= $estbActiAbbr."-";

            $newISCNCode .= $certTypeAbbr."-";

            $newISCNCode .= $certIssuedYear."-";

            $newISCNCode .= $estbUserName."-";

            $newISCNCode .= $newUniqCode;

            // Now generate QR code for this Person

            $domainName   = 'http://www.iscnsystem.org';;

            $fullPath   = $domainName;

            $fullPath  .= "/individual.php?iscn_code=" . $newUniqCode;

            $tempDir              = "qrimages/student/";

            $codeContents         = $fullPath;

            $fileName = "qrcode-" . $newUniqCode . ".png";

            $pngAbsoluteFilePath  = $tempDir . $fileName;

            $qrPath = $domainName . "/include/qrimages/student/" . $fileName;

            $indivLink             = $domainName . "individual.php?iscn_code=" . $newUniqCode;

            QRcode::png($codeContents,"$pngAbsoluteFilePath");

            // Update student.status = 4

            $approved = 4;

            $updateStudentQry = "UPDATE student SET ";

            $updateStudentQry .= "student.unq_code = '$newUniqCode', ";

            $updateStudentQry .= "student.code = '$newISCNCode', ";

            $updateStudentQry .= "student.qr_image_path = '$fileName', ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.status = $approved, ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.editor = $user ";

            $updateStudentQry .= "WHERE ";

            $updateStudentQry .= "student.id = $personID ";

            $updateStudentQry .= "AND ";

            $updateStudentQry .= "student.order_id = $orderID ";

            $updateStudentRun = mysqli_query($connection, $updateStudentQry);

            if ($updateStudentRun) {

                // Status has been approved successfully.

                QRcode::png($codeContents,"$pngAbsoluteFilePath");

                // QR Code is also generated successfully.

                $selectStdQry = "SELECT COUNT(*) AS count, ";

                $selectStdQry .= "first_name, ";

                $selectStdQry .= "middle_name, ";

                $selectStdQry .= "last_name,";

                $selectStdQry .= "code, ";

                $selectStdQry .= "unq_code,";

                $selectStdQry .= "globalid, ";

                $selectStdQry .= "email ";

                $selectStdQry .= "FROM student WHERE ";

                $selectStdQry .= "id = $personID";

                $selectStdRun = mysqli_query($connection,$selectStdQry);

                if ($selectStdRun) {

                    // selectStdQry run successfully

                    $stdRecord = mysqli_fetch_assoc($selectStdRun);

                    $count     = $stdRecord['count'];

                    if ($count > 0) {

                        // Person existed

                        // Now get full name, email, code, global ID, and ISCN

                        // code and we will send them to the person

                        $fullName  = $stdRecord['first_name']." ".$stdRecord['middle_name']." ".$stdRecord['last_name'];

                        $email       = $stdRecord['email'];

                        $ISCNCode    = $stdRecord['code'];

                        $newUniqCode = $stdRecord['unq_code'];

                        $to = $email;

                        $subject = "Congrats Your application have been approved";

                        $message = "

                                    <html>

                                    <head>

                                    <title>Application Approved</title>

                                    </head>

                                    <body>

                                    <p>Dear <span style='color:orange'>".$fullName."</span>:</p>

                                    <h2>Welcome to <span style='font-weight:900'>ISCN</span>!!!</h2>

                                    <p>Your application to <span style='font-weight:900'>ISCN</span> has been approved. We are proud to have you as one of our members.</p>

                                    <p>Find your details below: </p>



                                    <table border='1'>

                                    <tr>

                                    <th>

                                    ISCN CODE

                                    </th>

                                    <th>".$ISCNCode."

                                    </th>

                                    </tr>

                                    </table>



                                    <p><span style='font-weight:900'>Note:</th> You need just the last part to check the certificate.</p>

                                    <table border='1'>

                                    <tr>

                                    <th>CODE:</th><th>".$newUniqCode."</th>

                                    </tr>

                                    <tr>

                                    <th>QR CODE:</th><th><img src='".$qrPath."' /></th>

                                    </tr>

                                    <tr>

                                        <th>Link:</th><a href='".$indivLink."'>Click</a>



                                    </tr>

                                    </table>

                                    <br />

                                    <p>The entire ISCN team looks forward to a very professional working relationship with you, and we are ready to support you in any way possible to serve our members better. </p>

                                    <br />

                                    <p style='font-weight:900'>Best Regards,</p>

                                    <br />



                                    <pre style='color:blue;font-size:14px;'>

                                    International standard certificate number (<span style='font-weight:900'>ISCN</span>) team.

                                    International Standards Dataflow 

                                    www.iscnsystem.org 

                                    </pre>





                                    </body>

                                    </html>

                                    ";

                        // Always set content-type when sending HTML email

                        $headers = "MIME-Version: 1.0" . "\r\n";

                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                        // More headers

                        $headers .= 'From: <admin@iscnsystem.org>' . "\r\n";

                        $headers .= 'Cc: it.manager@iscnsystem.org' . "\r\n";

                        if(mail($to,$subject,$message,$headers))

                        {

                            // Everything have done successfully.

                            session_start();

                            $_SESSION['msgType'] = "1";

                            $_SESSION['msg']  = "Person status has been approved successfully.";

                            redirect_to('../establishment/order.php');

                            exit();

                        } // End: if(mail($to,$subject,$message,$headers))

                        else {

                            session_start();

                            $_SESSION['msgType'] = "1";

                            $_SESSION['msg']  = "Person status has been approved successfully. Email not sent";

                            redirect_to('../establishment/order.php');

                            exit();

                        } // End: else {

                    } // End if($count > 0)

                } // End: if ($selectStdRun) {

            } // End: if ($selectStdRun) {

        } // End: else {

    } // End: if ($status == 4 || $status == "4") {

    elseif ( $status == 3 || $status == "3" ) {

        // If Present Status of this person is 4(Approved), then we will not

        // change its status again.We will give message "This person is approved

        // and its record is locked, you cannot change its status.

        if ($presentStatus == 4 || $presentStatus == "4") {

            // We already started session that's why we will not start it here.

            $_SESSION[ 'msgType' ] = "1";

            $_SESSION[ 'msg' ] = "This person is Approved. You can't change it's status !";

            redirect_to( '../establishment/order.php' );

            exit();

        } // End: if ($presentStatus == 4 || $presentStatus == "4") {

        else {



            $underProcess = 3;

            $updateStudentQry = "UPDATE student SET ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.status = $underProcess, ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.editor = $user ";

            $updateStudentQry .= "WHERE ";

            $updateStudentQry .= "student.id = $personID ";

            $updateStudentRun = mysqli_query($connection, $updateStudentQry);

            if ( $updateStudentRun ) {

                // updateStudentQry run successfully.

                session_start();

                $_SESSION['msgType'] = "1";

                $_SESSION['msg']  = "Person status has been changed to under process successfully !";

                redirect_to('../establishment/order.php');

                exit();

            } // End: if ( $updateStudentRun ) {

        } // End: else {



    } // End: elseif ( $status == 3 || $status == "3" ) {

    elseif ( $status == 2 || $status == "2" ) {

        // If Present Status of this person is 4(Approved), then we will not

        // change its status again.We will give message "This person is approved

        // and its record is locked, you cannot change its status.

        if ($presentStatus == 4 || $presentStatus == "4") {

            // We already started session that's why we will not start it here.

            $_SESSION[ 'msgType' ] = "1";

            $_SESSION[ 'msg' ] = "This person is Approved. You can't change it's status !";

            redirect_to( '../establishment/order.php' );

            exit();

        } // End: if ($presentStatus == 4 || $presentStatus == "4") {

        else {

            $missInfo = 2;

            $updateStudentQry = "UPDATE student SET ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.status = $missInfo, ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.editor = $user ";

            $updateStudentQry .= "WHERE ";

            $updateStudentQry .= "student.id = $personID ";

            $updateStudentRun = mysqli_query($connection, $updateStudentQry);

            if ( $updateStudentRun ) {

                // updateStudentQry run successfully.

                session_start();

                $_SESSION['msgType'] = "1";

                $_SESSION['msg']  = "Person status has been changed to missing information successfully !";

                redirect_to('../establishment/order.php');

                exit();

            } // End: if ( $updateStudentRun ) {

        } // else {

    } // End: elseif ( $status == 2 || $status == "2" ) {

    elseif ( $status == 1 || $status == "1" ) {

        // If Present Status of this person is 4(Approved), then we will not

        // change its status again.We will give message "This person is approved

        // and its record is locked, you cannot change its status.

        if ($presentStatus == 4 || $presentStatus == "4") {

            // We already started session that's why we will not start it here.

            $_SESSION[ 'msgType' ] = "1";

            $_SESSION[ 'msg' ] = "This person is Approved. You can't change it's status !";

            redirect_to( '../establishment/order.php' );

            exit();

        } // End: if ($presentStatus == 4 || $presentStatus == "4") {

        else {

            $arrived = 1;

            $updateStudentQry = "UPDATE student SET ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.status = $arrived, ";

            $updateStudentQry .= "student.modify_date = '$now', ";

            $updateStudentQry .= "student.editor = $user ";

            $updateStudentQry .= "WHERE ";

            $updateStudentQry .= "student.id = $personID ";

            $updateStudentRun = mysqli_query($connection, $updateStudentQry);

            if ( $updateStudentRun ) {

                // updateStudentQry run successfully.

                session_start();

                $_SESSION['msgType'] = "1";

                $_SESSION['msg']  = "Person status has been changed to arrived successfully !";

                redirect_to('../establishment/order.php');

                exit();

            } // End: if ( $updateStudentRun ) {

        } // else {

    } // End: elseif ( $status == 1 || $status == "1" ) {

} // End: if (isset($_POST['btn-person-status'])) {





/*

                    Edit Person Record

------------------------------------------------------------------------------*/

if ( isset($_GET['personID'])) {

    if ($_GET['action'] == "editPerson") {

        $personID = $_GET['personID'];

        $stdRun = find_student( 'id', $personID );

        $stdRecord = mysqli_fetch_assoc( $stdRun );

        $firstName = $stdRecord['firstName'];

        $middleName = $stdRecord['middleName'];

        $lastName = $stdRecord['lastName'];

        $dob = $stdRecord['stdDOB'];

        $email = $stdRecord['stdEmail'];

        $stdNationID = $stdRecord['stdNation'];

        $phone = $stdRecord['stdContact'];

        $stdResidenceID = $stdRecord['stdResidence'];

        $photo = $stdRecord['stdPhoto'];

        if ($photo == "" || $photo == NULL) {

            $photo = "../admin/uploads/no-image.jpg";

        }

        else{

            $photo = "../admin/uploads/student/photo/".$photo;

        }

        $certImgName = $stdRecord['certImgName'];

        $certImgName = "../admin/uploads/student/certificate/".$certImgName;

        $gender = $stdRecord['stdGender'];

        $globalID = $stdRecord['globalID'];

        $certSerialNo = $stdRecord['certSerialNo'];

        $certSerialNo = substr($certSerialNo, 4);

        $stdIDCard = $stdRecord['stdIDCard'];

        $stdIDCard = "../admin/uploads/student/id-card/".$stdIDCard;

        ?>

        <fieldset class="fieldset">

            <legend><span>Personal Details</span></legend>

            <div class="row noMar">

                <div class="col-xs-12">

                    <div class="col-xs-4">

                        <div class="form-group">

                            <input type="hidden" name="person-id" class="form-control" value="<?php echo $personID ?>">

                            <label for="first-name">First Name </label>

                            <input type="text" name="first-name" class="form-control" value="<?php echo $firstName ?>">

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="first-name">Middle Name </label>

                            <input type="text" name="middle-name" class="form-control" value="<?php echo $middleName ?>">

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="first-name">Last Name </label>

                            <input type="text" name="last-name" class="form-control" value="<?php echo $lastName ?>">

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="last-name">Gender </label>

                            <select name="gender" required="required" class="form-control">

                                <option value="">--Choose Gender--</option>

                                <option value="Male" <?php if ($gender =="Male") {

                                    echo "selected";

                                } ?>>Male</option>

                                <option value="Female" <?php if ($gender =="Female") {

                                    echo "selected";

                                } ?>>Female</option>

                            </select>

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="date-birth">Date of Birth </label>

                            <input type="text" name="date-birth" class="form-control" placeholder="YYYY-MM-DD" value="<?php echo $dob; ?>">

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="nationality">Nationality </label>

                            <select required="required" tabindex="6" class="form-control" name="nationality">

                                <option value="">Choose Nationality</option><?php

                                // Get List of the Country List from database

                                $nationalitySet = get_all_from_tbl('countries_tbl');

                                while($nationality=mysqli_fetch_assoc($nationalitySet)) { ?>

                                <option value="<?php echo $nationality['id']; ?>" <?php if ($stdNationID == $nationality['id']) {

                                    echo 'selected';

                                } ?>>

                                    <?php echo $nationality['country_name']; ?>

                                    </option><?php } ?>

                            </select>

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="residence">Residence </label>

                            <select required="required" tabindex="6" class="form-control" name="residence">

                                <option value="">Choose Residence</option><?php

                                // Get List of the Country List from database

                                $residnceSet = get_all_from_tbl('countries_tbl');

                                while($residenceRow=mysqli_fetch_assoc($residnceSet)) { ?>

                                <option value="<?php echo $residenceRow['id']; ?>" <?php if ($stdResidenceID == $residenceRow['id']) {

                                    echo 'selected';

                                } ?>>

                                    <?php echo $residenceRow['country_name']; ?>

                                    </option><?php } ?>

                            </select>

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="residence">Phone </label>

                            <input type="text" name="phone" class="form-control" value="<?php echo $phone; ?>" ">

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="email">Email </label>

                            <input type="email" required="required" name="email" class="form-control" value="<?php echo $email; ?>">

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="global-id">Global ID</label>

                            <input type="text" name="global-id" class="form-control" value="<?php echo $globalID; ?>">

                        </div>

                    </div>

                    <div class="col-xs-4">

                        <div class="form-group">

                            <label for="global-id">Certificate Serial No</label>

                            <input type="text" name="cert-serial" class="form-control" value="<?php echo $certSerialNo; ?>">

                        </div>

                    </div>

                </div> <!-- End: col-xs-12 -->

                <div class="clearfix"></div>

            </div><!-- End: row -->

        </fieldset>

        <fieldset class="fieldset">

            <legend><span>Documents</span></legend>

            <div class="row">

                <div class="col-xs-12">

                    <div class="col-xs-4">

                        <div class="doc-box">

                          <span class="imgBox">

                            <img src="<?php echo $photo; ?>" class="uploadPreview" id="editIndivPhotoPreview" alt="Photo "/>

                          </span>

                            <div class="clearfix"></div>

                            <input style="width: 100%; border: 0; margin-top: 20px;" value="Upload Photo" name="person-photo" type="file"/>

                            <div class="clearfix"></div>

                        </div>

                    </div> <!-- End: col-xs-4 -->

                    <div class="col-xs-4">

                        <div class="doc-box">

                          <span class="imgBox">

                            <img src="<?php echo $stdIDCard; ?>"  class="uploadPreview"  id="indivIDCardPreview" alt="ID Card"/>

                          </span>

                            <div class="clearfix"></div>

                            <input style="width: 100%; border: 0; margin-top: 20px;" name="person-IDCard" type="file"/>

                            <div class="clearfix"></div>

                        </div>

                    </div> <!-- End: col-xs-4 -->



                    <div class="clearfix"></div>

                </div> <!-- End: col-xs-12 -->

                <div class="clearfix"></div>

            </div> <!-- End: row -->

        </fieldset>

        <div class="row">

            <div class="col-xs-12">

                <div class="modal-footer">

                    <button type="submit" name="edit-person" class="btn btn-success">Save</button>

                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>

                </div><!-- End: modal-footer-->

            </div><!-- End: col-xs-12-->

            <div class="clearfix"></div>

        </div>

        <?php

    }

} // End: if (isset($_GET['personID']) && isset($_GET['action'])) {





/*----------  Upload Certificate of Person after approval  ----------*/



if (isset($_POST['btn-upload-cert'])) {

    session_start();

    global $connection;

    $count = 0;

    $personID = mysql_prep($_POST['person-id']);

    $personID = remove_white_spaces($personID);

    $orderID = get_col_On_key('student', 'order_id', 'id', $personID);

    $count = search_tbl('student', 'id', $personID);

    if ($count < 1 || $count == NULL || $count == "") {

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Certificate ID not found !";

        header('Location: ../establishment/cert-list.php?orderID='.$orderID);

        exit();

    }

    // Certificate

    $target_dir        = "../admin/uploads/student/";

    $personScannedCertTMP =$_FILES['cert-img']['tmp_name'];



    $personScannedCertName = mysql_prep($_FILES["cert-img"]["name"]);



    $personScannedCertNewName = date("Y-d-m--h-i-s-A")."-".$personScannedCertName;

    $targetScannedCert = $target_dir ."certificate/". basename($personScannedCertNewName);

    if (move_uploaded_file($personScannedCertTMP,$targetScannedCert)) {

        // Update The record inside Database

        $uploadCertQry  = "UPDATE student SET ";

        $uploadCertQry .= "cert_path = '$personScannedCertNewName' ";

        $uploadCertQry .= "WHERE id = $personID ";

        $uploadCertRun = mysqli_query($connection, $uploadCertQry);

        if ($uploadCertRun) {

            $_SESSION['msgType'] = "1";

            $_SESSION['msg']  = "Certificate File has been uploaded successfully !";

            header('Location: ../establishment/cert-list.php?orderID='.$orderID);

            exit();

        } // End: if ($uploadCertRun) {

        else {

            echo $uploadCertQry;

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']  = "Error in Query. Contact With ISCN !";

            header('Location: ../establishment/cert-list.php?orderID='.$orderID);

            exit();

        } // End: else{

    } // End: if (move_uploaded_file(...)) {

    else{

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Certificate Not Uploaded. Your File size must be 2 MB or less than 2 MB";

        header('Location: ../establishment/cert-list.php?orderID='.$orderID);

        exit();

    }

}





















if (isset($_POST['edit-person'])) {

    // Initialize all variables with empty values

    $userType = $userID = $personID = $firstName = $middleName = $lastName = $gender = $dob = $nationality = $residence = $phone = $email = $globalID = $personPhotoName = $personIDCardName = $personScannedCertName = '';

    global $connection;

    session_start();

    echo "userType = ".$userType = $_SESSION['userType'];

    echo '<br/>';

    echo "userID = ".$userID = $_SESSION['userID'];

    echo '<br/>';

    echo "personID = ".$personID = $_POST['person-id'];

    echo '<br/>';

    $orderID = get_col_On_key('student', 'order_id', 'id', $personID);

    $estbID = get_col_On_key('orders', 'est_id', 'id', $orderID);

    $estbUserName = get_col_On_key('est', 'username', 'id', $estbID);



    $now = date( 'Y-d-m h:i:s A' );

    $firstName = mysql_prep($_POST['first-name']);

    $middleName = mysql_prep($_POST['middle-name']);

    $lastName = mysql_prep($_POST['last-name']);

    $gender = $_POST['gender'];

    $dob = mysql_prep($_POST['date-birth']);

    $nationality = $_POST['nationality'];

    $residence = $_POST['residence'];

    $phone = mysql_prep($_POST['phone']);

    $email = mysql_prep($_POST['email']);

    echo "globalID = ".$globalID = mysql_prep($_POST['global-id']);

    echo '<br/>';

    $certSerialID = mysql_prep($_POST['cert-serial']);

    echo "certSerialID = ".$certSerialID = remove_white_spaces($certSerialID);

    echo '<br/>';

    if ($certSerialID != "") {

        $certSerialNo = $estbUserName.$certSerialID;

        $searchCertSerialNo = search_tbl('student', 'cert_serial_no', $certSerialNo);

        if ($searchCertSerialNo > 0) {

            // Check its old Certificate Serial ID

            $oldCertSerialNo = get_col_On_key('student', 'cert_serial_no', 'id', $personID);

            // Compare old cert id with new one

            $newCertSerialNo = $estbUserName.$certSerialID;

            if ($oldCertSerialNo != $newCertSerialNo) {

                $_SESSION['msgType'] = "0";

                $_SESSION['msg'] = $certSerialID." certificate ID already used. Try another one!";

                header('Location: ../establishment/cert-list.php?orderID='.$orderID);

                exit();

            } // End: if ($oldCertSerialNo != $newCertSerialNo) {

        } // End: if ($searchCertSerialNo > 0) {

    }

    else {

        $certSerialNo = "";

    }

    // uplaod Pictures

    $target_dir        = "../admin/uploads/student/";

    // person photo

    $personPhotoTMP =$_FILES['person-photo']['tmp_name'];

    $personPhotoName = $_FILES["person-photo"]["name"];

    $personPhotoNewName = date("Y-d-m--h-i-s-A")."-".$personPhotoName;

    $targetPersonPhoto = $target_dir ."photo/". basename($personPhotoNewName);



    // person id card

    $personIDCardTMP =$_FILES['person-IDCard']['tmp_name'];

    $personIDCardName = $_FILES["person-IDCard"]["name"];

    $personIDCardNewName = date("Y-d-m--h-i-s-A")."-".$personIDCardName;

    $targetIDCard = $target_dir ."id-card/". basename($personIDCardNewName);



    // If Personal Photo is Provided then upload, otherwise do nothing.

    if ($personPhotoName != '') {

        if (move_uploaded_file($personPhotoTMP, $targetPersonPhoto)) {

            // Image is uploaded.

            echo "<br/> personPhotoName uploaded.<br/>";

        }

    } // End: if (move_uploaded_file(...) ) {

    if ($personIDCardName != '') {

        if (move_uploaded_file($personIDCardTMP, $targetIDCard)) {

            // Image is uploaded.

            echo "<br/> personIDCardName uploaded.<br/>";

        }

    } // End: if(move_uploaded_file(...)){

    echo '<br/>';

    $editStdQry  = "UPDATE student SET ";

    $editStdQry .= "first_name = '$firstName', ";

    $editStdQry .= "middle_name = '$middleName', ";

    $editStdQry .= "last_name = '$lastName', ";

    $editStdQry .= "globalid = '$globalID', ";

    if ($certSerialID != "") {

        $editStdQry .= "cert_serial_no = '$certSerialNo', ";

    }

    else{

        $editStdQry .= "cert_serial_no = '$certSerialNo', ";

    }

    $editStdQry .= "gender = '$gender', ";

    $editStdQry .= "birth_date = '$dob', ";

    $editStdQry .= "nationality = $nationality, ";

    $editStdQry .= "residence = $residence, ";

    $editStdQry .= "phone = $phone, ";

    $editStdQry .= "email = '$email', ";

    if ($personPhotoName != '') {

        $editStdQry .= "photo_path = '$personPhotoNewName', ";

    }

    if ($personIDCardName != '') {

        $editStdQry .= "id_path = '$personIDCardNewName', ";

    }

    if ($personScannedCertName != '') {

        $editStdQry .= "cert_path = '$personScannedCertNewName', ";

    }

    $editStdQry .= "modify_date = '$now', ";

    if ($userType != '') {

        $editStdQry .= "editor_type = '$userType' ";

    }

    $editStdQry .= "editor = $userID ";



    $editStdQry .= "WHERE ";

    $editStdQry .= "id  = $personID";



    $editStdRun = mysqli_query($connection, $editStdQry);



    if ($editStdRun) {

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "Certificate record has been updated successfully !";

        header('Location: ../establishment/cert-list.php?orderID='.$orderID);

        //redirect_to('../establishment/order.php');

        exit();

    } // End: if ($editStdRun) {

    else{

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Certificate record not updated! Contact with ISCN !";

        header('Location: ../establishment/cert-list.php?orderID='.$orderID);

        //redirect_to('../establishment/order.php');

        exit();

    }

} // End: if (isset($_POST['edit-person'])) {



/*

                    Establishment Order Payment Through Voucher

------------------------------------------------------------------------------*/

if ( isset( $_POST[ 'pay-order-voucher' ] ) ) {

    //pay-order-voucher clicked

    global $connection;

    session_start();

    $user = $_SESSION['userID'];

    $user = $_SESSION['estbID'];

    $pricePerDoc = $_POST['price-doc'];

    $totalStds = $_POST['total-stds'];

    $orderID   = $_POST['order-id'];

    $now                = date( "Y-d-m h:i:s A" );

    $reqAmount        = $_POST[ 'total-payment' ];

    if ( $reqAmount < 1 ) {

        // Amount that you want to pay is less than 1, it means

        // no person added to in this order yet, so we will give error

        // message to the user.

        session_start();

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "No certificate added in this order yet.";

        redirect_to('../establishment/order-grid.php');

        exit();

    } // End: if ( $reqAmount < 1 ) {



    $estbID = get_col_On_key('orders', 'est_id', 'id', $orderID);

    $presentCredit = get_col_On_key('est', 'present_credit', 'id', $estbID);

    if ($presentCredit < $reqAmount) {

        // Not enough credit in your ISCN acount

        session_start();

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Not enough credit in your ISCN acount !";

        redirect_to('../establishment/order-grid.php');

        exit();

    }

    else {

        // Balance in ISCN Account is enough for for registration

        // Substract reqAmount from presentCredit of your ISCN account

        $newCredit = ( $presentCredit - $reqAmount );

        // Now update present_credit column in est table with newCredit

        $updateCreditQry = "UPDATE est SET present_credit = $newCredit WHERE est.id = $estbID";

        $updateCreditRun = mysqli_query($connection, $updateCreditQry);

        if ($updateCreditRun) {

            // present_credit.est is updated

            $insertPaymentQry = "INSERT INTO payments ( ";

            $insertPaymentQry .= "payment_gross, ";

            $insertPaymentQry .= "currency_code, ";

            $insertPaymentQry .= "payment_status, ";

            $insertPaymentQry .= "estb_order_id, ";

            $insertPaymentQry .= "type, ";

            $insertPaymentQry .= "pay_date ";



            $insertPaymentQry .= ") VALUES ( ";

            $insertPaymentQry .= "$reqAmount, ";

            $insertPaymentQry .= "'USD', ";

            $insertPaymentQry .= "1, ";

            $insertPaymentQry .= "$orderID, ";

            $insertPaymentQry .= "'ISCN Credit', ";

            $insertPaymentQry .= "'$now' ";

            $insertPaymentQry .= ")";

            $insertPaymentRun = mysqli_query( $connection, $insertPaymentQry );

            if ( $insertPaymentRun ) {

                // Payment record has been saved

                // Payment has been recieve for all the persons that are

                // belong to this order. Now update order status

                $paid = 2;

                $updOrderQry  = "UPDATE orders SET ";

                $updOrderQry .= "status = $paid ";

                $updOrderQry .= "WHERE id  = $orderID";

                $updOrderRun  = mysqli_query($connection, $updOrderQry);

                if ($updOrderRun) {

                    // Order status has been changed from Not Paid to Paid.

                    // Now Change all students status = 4 i.e Approved that are in this order

                    // Student Status has been changed to Approved

                    $stdQry  = "SELECT ";

                    $stdQry .= "student.id AS stdID, ";

                    $stdQry .= "student.first_name AS stdFirstName, ";

                    $stdQry .= "student.middle_name AS stdMiddleName, ";

                    $stdQry .= "student.last_name AS stdLastName, ";

                    $stdQry .= "student.phone AS stdPhone, ";

                    $stdQry .= "student.email AS stdEmail, ";

                    $stdQry .= "orders.cert_type AS orderCertTypeID, ";

                    $stdQry .= "orders.cert_date AS certIssueDate, ";

                    $stdQry .= "est.username AS estbUserName, ";

                    $stdQry .= "est.activity AS estbActiID, ";

                    $stdQry .= "est.country AS estbCountryID ";

                    $stdQry .= "FROM student ";

                    $stdQry .= "INNER JOIN ";

                    $stdQry .= "orders ON ";

                    $stdQry .= "student.order_id = orders.id ";

                    $stdQry .= "INNER JOIN est ON ";

                    $stdQry .= "orders.est_id = est.id ";

                    $stdQry .= "WHERE ";

                    $stdQry .= "student.order_id =$orderID";

                    $stdRun = mysqli_query($connection,$stdQry);

                    if ($stdRun) {

                        // Got student record

                        $stdsInOrder = mysqli_num_rows($stdRun);

                        $i = 1;

                        while ($stdRecord = mysqli_fetch_assoc($stdRun)) {



                            echo "Serial # = ".$i; $i++;

                            $stdID   = $stdRecord['stdID'];

                            $stdFirstName = $stdRecord['stdFirstName'];

                            $stdMiddleName = $stdRecord['stdMiddleName'];

                            $stdLastName = $stdRecord['stdLastName'];

                            $stdEmail = $stdRecord['stdEmail'];

                            $stdFullName = $stdFirstName." ".$stdMiddleName." ".$stdLastName;

                            $certID =  $stdRecord['orderCertTypeID'];

                            $estbCertAbbr = get_col_On_key('certificate_tbl','abbreviation','id', $certID);

                            $actiID =  $stdRecord['estbActiID'];

                            $estbActiAbbr = get_col_On_key('estab__activities_tbl','abbreviation','id', $actiID);

                            $estbCountryID =  $stdRecord['estbCountryID'];

                            $estbCountryAbbr = get_col_On_key('countries_tbl','country_abbr','id', $estbCountryID);

                            $certIssueDate =  $stdRecord['certIssueDate'];

                            $stdCertYear = substr($certIssueDate,2,2);

                            $estbUserName =  $stdRecord['estbUserName'];

                            do {

                                $fourLetters = randomLetters( 4 );

                                $fourNumbers = randomNumbers( 4 );



                                $newUniqCode = $fourLetters . $fourNumbers;

                                $searchUniqCodeInStdTblQry = "SELECT COUNT(*) AS stdCount ";

                                $searchUniqCodeInStdTblQry .= "FROM student ";

                                $searchUniqCodeInStdTblQry .= "WHERE ";

                                $searchUniqCodeInStdTblQry .= "unq_code = '$newUniqCode'";

                                $searchUniqCodeInStdTblRun = mysqli_query($connection, $searchUniqCodeInStdTblQry);

                                $searchedStdRecord = mysqli_fetch_assoc($searchUniqCodeInStdTblRun);

                                $countStd = $searchedStdRecord['stdCount'];





                                $searchUniqCodeInCertQry = "SELECT COUNT(*) AS certCount ";

                                $searchUniqCodeInCertQry .= "FROM cert ";

                                $searchUniqCodeInCertQry .= "WHERE ";

                                $searchUniqCodeInCertQry .= "unq_code = '$newUniqCode'";

                                $searchUniqCodeInCertRun = mysqli_query($connection, $searchUniqCodeInCertQry);

                                $searchedCertRecord = mysqli_fetch_assoc($searchUniqCodeInCertRun);

                                $certCount = $searchedCertRecord['certCount'];





                                $searchUniqCodeInPhdQry = "SELECT COUNT(*) AS phdCount ";
                                $searchUniqCodeInPhdQry .= "FROM phd_cert ";
                                $searchUniqCodeInPhdQry .= "WHERE ";
                                $searchUniqCodeInPhdQry .= "unq_code = '$newUniqCode'";
                                $searchUniqCodeInPhdRun = mysqli_query($connection, $searchUniqCodeInPhdQry);
                                $searchedPhdRecord = mysqli_fetch_assoc($searchUniqCodeInPhdRun);
                                $phdCount = $searchedPhdRecord['phdCount'];


                                $searchUniqCodeInvtrainerQry = "SELECT COUNT(*) AS vtrainerCount ";
                                $searchUniqCodeInvtrainerQry .= "FROM vtrainer_users ";
                                $searchUniqCodeInvtrainerQry .= "WHERE ";
                                $searchUniqCodeInvtrainerQry .= "unq_code = '$newUniqCode'";
                                $searchUniqCodeInvtrainerRun = mysqli_query($connection, $searchUniqCodeInvtrainerQry);
                                $searchedvtrainerRecord = mysqli_fetch_assoc($searchUniqCodeInvtrainerRun);
                                $vtrainerCount = $searchedvtrainerRecord['vtrainerCount'];

                                $searchUniqCodeEngQry = "SELECT COUNT(*) AS engCount ";
                                $searchUniqCodeEngQry .= "FROM vtrainer_users ";
                                $searchUniqCodeEngQry .= "WHERE ";
                                $searchUniqCodeEngQry .= "unq_code = '$newUniqCode'";
                                $searchUniqCodeEngRun = mysqli_query($connection, $searchUniqCodeEngQry);
                                $searchEngRecord = mysqli_fetch_assoc($searchUniqCodeEngRun);
                               $EngCount = $searchEngRecord['engCount'];


                                $searchUniqCodeIndocQry = "SELECT COUNT(*) AS docCount ";
                                $searchUniqCodeIndocQry .= "FROM doc_users ";
                                $searchUniqCodeIndocQry .= "WHERE ";
                                $searchUniqCodeIndocQry .= "unq_code = '$newUniqCode'";
                                $searchUniqCodeIndocRun = mysqli_query($connection, $searchUniqCodeIndocQry);
                                $searchdocRecord = mysqli_fetch_assoc($searchUniqCodeIndocRun);
                                $docCount = $searchdocRecord['docCount'];

                            } while ($countStd > 0 || $certCount > 0 || $phdCount > 0 || $vtrainerCount  > 0 || $docCount  > 0 || $engCount  > 0);

                            $iscnCode = "57".$estbCountryAbbr."-".$estbActiAbbr."-".$estbCertAbbr."-".$stdCertYear."-".$estbUserName."-".$newUniqCode;

                            // Now generate QR code for this Person

                            $domainName = 'http://www.iscnsystem.org';

                            $fullPath = $domainName."/individual.php?iscn_code=".$newUniqCode;

                            $tempDir     ="qrimages/student/";

                            $codeContents = $fullPath;

                            $fileName = "qrcode-".$newUniqCode.".png";

                            $pngAbsoluteFilePath = $tempDir.$fileName;

                            $qrPath    =$domainName."/include/qrimages/student/".$fileName;

                            $indivLink=$domainName."/individual.php?iscn_code=".$newUniqCode;

                            QRcode::png($codeContents,"$pngAbsoluteFilePath");

                            // Save new ISCN Code in student table

                            // Save 8 Characters Unique Code in Student table

                            // Save the QR Image name in student table

                            // Change student status to 4 = Approved

                            $approved = 4;

                            $updStdQry  ="UPDATE student SET ";

                            $updStdQry .="code = '$iscnCode', ";

                            $updStdQry .="unq_code='$newUniqCode', ";

                            $updStdQry .="qr_image_path='$fileName', ";

                            $updStdQry .="status = $approved ";

                            $updStdQry .= "WHERE ";

                            $updStdQry .= "id =  $stdID";

                            $updStdRun  = mysqli_query($connection, $updStdQry);

                            if ($updStdRun) {

                                // Now Send Email to Student

                                $to = $stdEmail;

                                $subject = "Congrats Your application have been approved";

                                $message = "

                                    <html>

                                    <head>

                                    <title>Application Approved</title>

                                    </head>

                                    <body>

                                    <p>Dear <span style='color:orange'>".$stdFullName."</span>:</p>

                                    <h2>Welcome to <span style='font-weight:900'>ISCN</span>!!!</h2>

                                    <p>Your application to <span style='font-weight:900'>ISCN</span> has been approved. We are proud to have you as one of our members.</p>

                                    <p>Find your details below: </p>



                                    <table border='1'>

                                    <tr>

                                    <th>

                                    ISCN CODE

                                    </th>

                                    <th>".$iscnCode."

                                    </th>

                                    </tr>

                                    </table>

                                    <p><span style='font-weight:900'>Note:</th> You need just the last part to check the certificate.</p>

                                    <table border='1'>

                                    <tr>

                                    <th>CODE:</th><th>".$newUniqCode."</th>

                                    </tr>

                                    <tr>

                                    <th>QR CODE:</th><th><img src='".$qrPath."' /></th>

                                    </tr>

                                    <tr>

                                        <th>Link:</th><a href='".$indivLink."'>Click</a>

                                    </tr>

                                    </table>

                                    <br />

                                    <p>The entire ISCN team looks forward to a very professional working relationship with you, and we are ready to support you in any way possible to serve our members better. </p>

                                    <br />

                                    <p style='font-weight:900'>Best Regards,</p>

                                    <br />

                                    <pre style='color:blue;font-size:14px;'>

                                    International standard certificate number (<span style='font-weight:900'>ISCN</span>) team.

                                    International Standards Dataflow 

                                    www.iscnsystem.org 

                                    </pre>

                                    </body>

                                    </html>

                                ";

                                // Always set content-type when sending HTML email

                                $headers = "MIME-Version: 1.0" . "\r\n";

                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                                // More headers

                                $headers .= 'From: <admin@iscnsystem.org>'."\r\n";

                                $headers .= 'Cc: it.manager@iscnsystem.org'."\r\n";

                                if(mail($to,$subject,$message,$headers)) {

                                    // Everything have done successfully.

                                    // But we will redirect page when all

                                    // person associated with this order have

                                    // been approved successfully.

                                    session_start();

                                    $_SESSION['msgType'] = "1";

                                    $_SESSION['msg']  = "All Persons in this order have been approved successfully. Email also sent to them.";

                                } // End: if(mail($to,$subject,$message,$headers))

                                else {

                                    $_SESSION['msgType'] = "1";

                                    $_SESSION['msg']  = "All Persons in this order have been approved successfully. Email Not sent.";

                                } // End: else {

                            } // End: updStdRun



                        } // End: while ($stdRecord = mysqli_fetch_assoc($stdRun)) {

                        echo $i;

                        echo $stdsInOrder;

                        if ($i > $stdsInOrder) {

                            redirect_to('../establishment/order-grid.php');

                        }

                    } // End: if ($stdRun) {

                    else {

                        //echo $stdQry;

                        $_SESSION['msgType'] = "0";

                        echo $_SESSION['msg'] = "Try again or Contact with ISCN System";

                        //redirect_to('../establishment/order.php');

                    }

                } // End: if ($updOrderRun) {

                else {

                    //echo $updOrderQry;

                    $_SESSION['msgType'] = "0";

                    echo $_SESSION['msg'] = "Try again or Contact with ISCN System";

                    //redirect_to('../establishment/order.php');

                }

            } // End: if ( $insertPaymentRun ) {

            else {

                echo $insertPaymentQry;

                $_SESSION['msgType'] = "0";

                echo $_SESSION['msg'] = "Try again or Contact with ISCN System";

                //redirect_to('../establishment/order.php');

            }

        } // End: if ($updateCreditRun) {

        else {

            //echo $updateCreditQry;

            $_SESSION['msgType'] = "0";

            $_SESSION['msg'] = "Try again or Contact with ISCN System";

            redirect_to('../establishment/order-grid.php');

        } // End: else {

    } // End: else {

} // if ( isset( $_POST[ 'pay-order-voucher' ] ) ) {











/*----------  Delete Order  ----------*/

function delete_order($page) {

    session_start();

    global $connection;

    $user = $_SESSION['userID'];

    $safeOrderID = mysql_prep($_POST['order-id']);

    $estbID = get_col_On_key('orders','est_id','id',$safeOrderID);

    $count = search_tbl('orders', 'id', $safeOrderID);

    if ($count < 1 ) {

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Order ID not found !";

        if ($page == "establishment/order-list.php") {

            header('Location: ../establishment/order-list.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }

        elseif ($page == "establishment/order-grid.php") {

            header('Location: ../establishment/order-grid.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }

        elseif ($page == "admin/order-list.php"){

            header('Location: ../admin/order-list.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }

        elseif ($page == "admin/order-grid.php"){

            header('Location: ../admin/order-grid.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }



    }

    // Instead of running DELETE query we will change order status to 0

    // so that we can restore it in future if we want.

    $delete = 0;

    $deleteOrderQry  = "UPDATE orders SET ";

    $deleteOrderQry .= "status =$delete ";

    $deleteOrderQry .= "WHERE ";

    $deleteOrderQry .= "id = $safeOrderID";

    $deleteOrderRun = mysqli_query($connection, $deleteOrderQry);

    if ( $deleteOrderRun ) {

        // Order deleted succussfully

        // Session already started, so we will not start it again.

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "Order deleted succussfully !";

        if ($page == "establishment/order-list.php") {

            header('Location: ../establishment/order-list.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=1');

        }

        elseif ($page == "establishment/order-grid.php") {

            header('Location: ../establishment/order-grid.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=1');

        }

        elseif ($page == "admin/order-list.php"){

            header('Location: ../admin/order-list.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=1');

        }

        elseif ($page == "admin/order-grid.php"){

            header('Location: ../admin/order-grid.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=1');

        }

    }

    else{

        // Order not deleted

        // Session already started, so we will not start it again.

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "Order didn't deleted! Contact with ISCN.";

        if ($page == "establishment/order-list.php") {

            header('Location: ../establishment/order-list.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }

        elseif ($page == "establishment/order-grid.php") {

            header('Location: ../establishment/order-grid.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }

        elseif ($page == "admin/order-list.php"){

            header('Location: ../admin/order-list.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }

        elseif ($page == "admin/order-grid.php"){

            header('Location: ../admin/order-grid.php?estb-id='.$estbID.'&msg='.$_SESSION['msg'].'&msgType=0');

        }

    }

}

// End: function delete_order($page) {









/*----------  Edit Order  ----------*/

if ( isset($_GET['orderID'])) {

    $orderID = $_GET['orderID'];

    $action = $_GET['action'];



}



if (isset($_POST['edit-order'])) {

    global $connection;

    session_start();

    $editor = $_SESSION['userID'];

    $orderID = $_POST['order-id'];

    $estbID = $_POST['estb-id'];

    $certType = $_POST['estb-cert-type'];

    $certField = $_POST['estb-cert-field'];

    $certDate = $_POST['cert-date'];

    $modifyDate         = NOW;

    $updtOrderQry  = "UPDATE orders SET ";

    $updtOrderQry .= "cert_type= $certType, ";

    $updtOrderQry .= "field= '$certField', ";

    $updtOrderQry .= "cert_date='$certDate', ";

    $updtOrderQry .= "modify_date = '$modifyDate',";

    $updtOrderQry .= "editor = $editor ";

    $updtOrderQry .= "WHERE ";

    $updtOrderQry .= "id  = $orderID";

    $updtOrderRun = mysqli_query($connection, $updtOrderQry);

    if ( $updtOrderRun ) {

        // Order record has been updated succussfully !

        // Session already started, so we will not start it again.

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "Order record has been updated succussfully !";

        redirect_to('../establishment/order.php');

        exit();

    }

}



function edit_order($from)

{

    global $connection;

    session_start();

    $editor     = $_SESSION['userID'];

    $orderTitle = $_POST['order-title'];

    $orderID    = $_POST['order-id'];

    $estbID     = $_POST['estb-id'];

    $certType   = $_POST['estb-cert-type'];

    $certField  = $_POST['estb-cert-field'];

    $certDate   = $_POST['cert-date'];

    $modifyDate = NOW;



    $updtOrderQry  = "UPDATE orders SET ";

    $updtOrderQry .= "title= '$orderTitle', ";

    $updtOrderQry .= "cert_type= $certType, ";

    $updtOrderQry .= "field= '$certField', ";

    $updtOrderQry .= "cert_date='$certDate', ";

    $updtOrderQry .= "modify_date = '$modifyDate',";

    $updtOrderQry .= "editor = $editor ";

    $updtOrderQry .= "WHERE ";

    $updtOrderQry .= "id  = $orderID";



    $updtOrderRun = mysqli_query($connection, $updtOrderQry);

    if ($updtOrderRun) {

        // Order record has been updated succussfully !

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "Order record has been updated succussfully !";

        if ($from == "admin") {

            header('Location: ../admin/order-edit.php?id='.$orderID.'&estb-id='.$estbID);

        }

        else{



            header('Location: ../establishment/order-edit.php?id='.$orderID);

        }

    }

    else {

        //echo $updtOrderQry;

        //echo "<br/>";

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Try again or Contact with ISCN !";

        if ($from == "admin") {

            header('Location: ../admin/order-edit.php?id='.$orderID.'&estb-id='.$estbID);

        }

        else{

            header('Location: ../establishment/order-edit.php?id=$orderID');

        }

    }

}





if (isset($_POST['change-estb-pass'])) {

    global $connection;

    session_start();

    $userID = $_SESSION['userID'];

    $oldPassword        = mysql_prep($_POST['old-password']);

    $salt               = "isdf2017";

    $saltedOldPass      = sha1($oldPassword . $salt);

    $oldPassword        = md5($saltedOldPass);

    $oldPassDB          = get_col_On_key('users','password', 'id', $userID);

    $newPassword        = mysql_prep($_POST['new-password']);

    $saltedNewPass      = sha1( $newPassword . $salt );

    $newHashedPassword  = md5($saltedNewPass);

    // Always set content-type when sending HTML email

    $headers = "MIME-Version: 1.0" . "\r\n";

    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers

    $Cc = "it.manager@iscnsystem.org";

    $from = 'admin@iscnsystem.org';

    $headers .= 'From: ' .$from. "\r\n";

    $headers .= 'Cc: '.$Cc."\r\n";

    $subject = "Password Changed";

    $estbID = get_col_On_key('users','estb_id','id',$userID);

    $userName = get_col_On_key('users','username','id',$userID);

    $to = get_col_On_key('est','email','id',$estbID);

    $estbName = get_col_On_key('est','name','id',$estbID);

    $message = "<html><head><title>Password Changed</title>

                     </head><body><p>To <span style='color:orange'>"

        .$estbName."</span>:</p><h3>ISCN system</h3><p>

                     Your password has been changed successfully.</p>

                     <p>Kindly find username and password below: </p>

                     <table border='1'><tr><th>UserName: </th><th>"

        .$userName."</th></tr><tr><th>Password: </th><th>"

        .$newPassword."</th></tr></table><p>For further 

                     information or if you have any questions please do 

                     not hesitate to contact us: <span style='color:blue'>

                     Info@iscnsystem.org</span></p>

                     <p style='font-weight:900'>Thanks</p>

                     <p style='font-weight:900' >Best Regards</p><p>

                     International standard certificate number 

                     (<span style='font-weight:900'>ISCN</span>) team.</p>

                     <p>International Standards Dataflow <a 

                     href='www.iscnsystem.org' >www.iscnsystem.org</a></p>

                     </body></html>

        ";

    if ($oldPassDB == $oldPassword) {

        $changeEstbPassQry  = "UPDATE users SET ";

        $changeEstbPassQry .= "password = '$newHashedPassword', ";

        $changeEstbPassQry .= "plain_password = '$newPassword' ";

        $changeEstbPassQry .= "WHERE ";

        $changeEstbPassQry .= "id = $userID";

        $changeEstbPassRun = mysqli_query($connection, $changeEstbPassQry);

        if ($changeEstbPassRun) {

            // password changed

            if (mail($to,$subject,$message,$headers)) {

                $_SESSION['msgType'] = "1";

                $_SESSION['msg']="Password changed. Check Your Email !";

                redirect_to('../establishment/password.php');

                exit();

            } // End: if (mail($to,$subject,$message,$headers)) {

            else {

                $_SESSION['msgType'] = "1";

                $_SESSION['msg']  = "Password changed ! Email not sent";

                redirect_to('../establishment/password.php');

                exit();

            } // End: else {

        } // End: if ($changeEstbPassRun) {

    } // End: if ($oldPassDB == $oldPassword) {

    else{

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Old password was wrong";

        redirect_to('../establishment/password.php');

        exit();

    }

} // End: if (isset($_POST['change-estb-pass'])) {



if ( isset($_GET['voucherID']) && isset($_GET['action']) ) {

    $id = $_GET['voucherID'];

    $action = $_GET['action'];

    if ($action == "showVoucherNote") {

        show_voucher_notes($id);

    }

    # code...

}



function show_voucher_notes($voucherID) {

    global $connection;

    $voucherNotesQry  ="SELECT voucher_note.id, ";

    $voucherNotesQry .="voucher_note.note_desc, ";

    $voucherNotesQry .="voucher_note.create_date, ";

    $voucherNotesQry .="voucher_note.modify_date, ";

    $voucherNotesQry .="voucher_note.user ";

    $voucherNotesQry .="FROM voucher_note ";

    $voucherNotesQry .="WHERE ";

    $voucherNotesQry .="voucher_id = $voucherID ";

    $voucherNotesQry .="AND ";

    $voucherNotesQry .="status = 1";

    $voucherNotesRun = mysqli_query($connection, $voucherNotesQry);

    ?>

    <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Voucher # <?php echo $voucherID; ?> Note</h4>

    </div>

    <div class="modal-body">

        <div class="row">

            <div class="col-xs-12 table-responsive">

                <table class="table table-hover">

                    <tbody>

                    <tr class="bg-navy">

                        <th>Note ID</th>

                        <th>Create Date</th>

                        <th>Note Description</th>

                        <th>Added By</th>

                    </tr>

                    <?php while ($voucherNote = mysqli_fetch_assoc($voucherNotesRun)) { ?>

                        <tr>

                            <td><?php echo $voucherNote['id']; ?></td>

                            <td><?php echo $voucherNote['create_date']; ?></td>

                            <td><?php echo $voucherNote['note_desc']; ?></td>

                            <td><?php $userID = $voucherNote['user'];

                                echo $userFullName =get_col_On_key('admin','full_name', 'id', $userID);

                                ?></td>

                        </tr>

                    <?php } // End: while ($voucherNote = mysqli_fetch_assoc($voucherNotesRun)) { ?>

                    </tbody>

                </table>

            </div>

            <div class="clearfix"></div>

        </div>

    </div><!-- End: modal-body -->

    <div class="modal-footer">

        <button type="button" class="btn btn-success add-new-note" data-vid="<?php echo $voucherID; ?>" data-toggle="modal" data-target="#modal-third">Add New Note</button>

        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

    </div><!-- End: modal-footer -->

    <?php

} // End: function show_voucher_notes($voucherID) {





if (isset($_POST['save-voucher-note'])) {

    global $connection;

    session_start();

    $user = $_SESSION['userID'];

    $voucherID = $_POST['voucher-id'];

    $noteDesc = $_POST['voucher-desc'];

    $createDate         = date( "Y-m-d h:i:s" );

    $modifyDate         = date( "Y-m-d h:i:s" );

    $addVoucherNoteQry  ="INSERT INTO `voucher_note`( ";

    $addVoucherNoteQry .="note_desc, ";

    $addVoucherNoteQry .="create_date, ";

    $addVoucherNoteQry .="modify_date, ";

    $addVoucherNoteQry .="user, ";

    $addVoucherNoteQry .="voucher_id, ";

    $addVoucherNoteQry .="status ";

    $addVoucherNoteQry .=") VALUES ( ";

    $addVoucherNoteQry .="'$noteDesc', ";

    $addVoucherNoteQry .="'$createDate', ";

    $addVoucherNoteQry .="'$modifyDate', ";

    $addVoucherNoteQry .="$user, ";

    $addVoucherNoteQry .="$voucherID, ";

    $addVoucherNoteQry .="1 ";

    $addVoucherNoteQry .=")";

    $addVoucherNoteRun = mysqli_query($connection, $addVoucherNoteQry);

    if ($addVoucherNoteRun) {

        // Order record has been updated succussfully !

        // Session already started, so we will not start it again.

        $_SESSION['msgType'] = "1";

        $_SESSION['msg']  = "New note has been saved succussfully !";

        redirect_to('../admin/voucher.php');

        exit();

    }

    else{

        // Order record didnot updated  !

        // Session already started, so we will not start it again.

        $_SESSION['msgType'] = "0";

        $_SESSION['msg']  = "Failed ! Try again when page is fully loaded.";

        redirect_to('../admin/voucher.php');

        exit();

    }

} // End: if (isset($_POST['save-voucher-note'])) {



if (isset($_POST['btn-update-contact'])) {

    global $connection;

    $estbID = mysql_prep($_POST['estb-id']);

    $estbCity = mysql_prep($_POST['estb-city']);

    $estbAboutTxt = mysql_prep($_POST['estb-aboutus']);

    $estbAddr = mysql_prep($_POST['estb-addr']);

    $estbWebsite = mysql_prep($_POST['estb-website']);

    $estbEmail = mysql_prep($_POST['estb-email']);

    $estbPhone = mysql_prep($_POST['estb-phone']);

    $estbFax = mysql_prep($_POST['estb-fax']);

    $estbManager = mysql_prep($_POST['estb-manager']);

    // Uploads

    $mainDir        = "../admin/uploads/establishment/";

    // Photo

    $pictureName = mysql_prep($_FILES["another-picture"]["name"]);

    if ($pictureName != "") {

        //echo 'Photo is going to upload.';

        $photoDir     = $mainDir."another-picture";

        $photoTMP     = $_FILES['another-picture']['tmp_name'];

        $photoNewName = date("Y-d-m--h-i-s-A")."-".$pictureName;

        $targetPhoto  = $photoDir."/".basename($photoNewName);

        if (!move_uploaded_file($photoTMP,$targetPhoto)) {

            //Photo failed !

            $_SESSION['msgType'] = "0";

            $_SESSION['msg']     = "Picture size is greater than 2 MB.";

            header('Location: ../establishment/contact.php');

            exit();

        }

    }



    $updateEstbQry  = "UPDATE est SET ";

    $updateEstbQry .= "est.city = '$estbCity', ";

    $updateEstbQry .= "est.address = '$estbAddr', ";

    $updateEstbQry .= "est.about_us = '$estbAboutTxt', ";

    if ($pictureName != "") {

        $updateEstbQry .= "est.another_picture = '$photoNewName', ";



    }

    $updateEstbQry .= "est.website = '$estbWebsite', ";

    $updateEstbQry .= "est.email = '$estbEmail', ";

    $updateEstbQry .= "est.phone = '$estbPhone', ";

    $updateEstbQry .= "est.fax = '$estbFax', ";

    $updateEstbQry .= "est.manager = '$estbManager' ";

    $updateEstbQry .= "WHERE ";

    $updateEstbQry .= "est.id = $estbID";

    $updateEstbRun = mysqli_query($connection, $updateEstbQry);

    if ($updateEstbRun) {

        session_start();

        $_SESSION['msgType']     = "1";

        $_SESSION['msg']         = "Contact information updated successfully !";

        redirect_to( '../establishment/contact.php' );

        exit();

    }

    else {

        session_start();

        $_SESSION['msgType']     = "0";

        $_SESSION['msg']         = "Try again !";

        redirect_to( '../establishment/contact.php' );

        exit();

    }

}



if (isset($_GET['action']) && isset($_GET['stdID'])) {

    $action = $_GET['action'];

    if ($action=="viewStdReport") {

        $stdID = $_GET['stdID'];

        $orderID =get_col_On_key('student','order_id','id',$stdID);

        $estbID =get_col_On_key('orders','est_id','id',$orderID);

        $estbManager =get_col_On_key('est','manager','id',$estbID);

        $paymentDate =get_col_On_key('payments','pay_date','estb_order_id',$orderID);



        $approvalDate = substr($paymentDate, 0, 10);



        $stdRun =  get_all_cols_on_key( 'student', 'id', $stdID );

        $stdRecord = mysqli_fetch_assoc($stdRun);

        $fullName  = $stdRecord['first_name']." ";

        $fullName .= $stdRecord['middle_name']." ";

        $fullName .= $stdRecord['last_name'];

        $stdNationID .= $stdRecord['nationality'];

        $stdNationality = get_col_On_key('countries_tbl','country_name','id',$stdNationID);

        $stdResidenceID .= $stdRecord['residence'];

        $stdResidence =get_col_On_key('countries_tbl','country_name','id',$stdResidenceID);

        $certImgName = $stdRecord['cert_path'];

        $stdCertFullPath = "../admin/uploads/student/certificate/".$certImgName;

        $stdISCNCode .= $stdRecord['code'];

        $stdUniqueCode .= $stdRecord['unq_code'];

        $certQRImg = $stdRecord['qr_image_path'];

        $certQRImg = "../include/qrimages/student/".$certQRImg;

    } // End: if ($action=="viewStdReport") {

    ?>

    <div class="row noMar border-bottom">

        <div class="col-xs-2 left-cont">

            <div class="stamp-wrap">

                <img src="../assets/img/stamp1.png" alt="ISO">

            </div>

            <div class="stamp-wrap">

                <img src="../assets/img/stamp2.png" alt="ISO">

            </div>

            <div class="stamp-wrap">

                <img src="../assets/img/stamp3.png" alt="ISO">

            </div>

        </div>

        <!-- End: left-cont -->

        <div class="col-xs-10 right-cont noPad">

            <div class="row  noMar">

                <div class="logo-wrap">

                    <img class="full-width" src="../assets/img/top-logo.png" alt="ISCN">

                </div>

            </div>

            <!-- End: row -->

            <div class="row noMar">

                <div class="col-xs-6 noPadL small-box-wrap">

                    <div class="small-box">

                        <header>

                            <span>Establishment Information</span>

                        </header>

                        <div class="box-body">

                            <h3>

                                <label>

                                    Name :

                                </label>

                                <span>

                        <?php echo $fullName; ?>

                      </span>

                            </h3>

                            <h3>

                                <label>

                                    Nationality :

                                </label>

                                <span>

                        <?php

                        echo $stdNationality;

                        ?>

                      </span>

                            </h3>

                            <h3>

                                <label>

                                    Residence :

                                </label>

                                <span>

                        <?php

                        echo $stdResidence;

                        ?>

                      </span>

                            </h3>

                            <h3>

                                <label>

                                    Manager :

                                </label>

                                <span>

                        <?php

                        echo $estbManager;

                        ?>

                      </span>

                            </h3>

                        </div>

                    </div>

                </div>

                <div class="col-xs-4 small-box-wrap">

                    <div class="small-box">

                        <header class="text-center">

                            <span>Certificate</span>

                        </header>

                        <div class="box-body sides-border">

                            <h3>

                                <div class="cert-wrap ">

                                    <?php

                                    $ext = pathinfo($certImgName, PATHINFO_EXTENSION);

                                    $ext = strtolower($ext);

                                    if ($ext == "pdf") { ?>

                                        <iframe src="http://docs.google.com/gview?url=http://www.iscnsystem.org/<?php echo $stdCertFullPath; ?>&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>

                                    <?php } // End: if ($ext == "pdf") {

                                    elseif ($ext == "jpeg" || $ext == "jpg" ||$ext == "png" ) { ?>

                                        <img class="full-width" src="<?php echo $stdCertFullPath; ?>">

                                        <?php

                                    }

                                    // End: elseif($ext == "JPEG"...) {

                                    ?>

                                </div>

                            </h3>

                        </div>

                    </div>

                </div>

                <div class="col-xs-2 noPad small-box-wrap">

                    <div class="small-box">

                        <header  class="text-center">

                            <span>QR Code</span>

                        </header>

                        <div class="box-body text-center">

                            <h3>

                                <div class="qrcode-wrap">

                                    <img src="<?php echo $certQRImg ?>" alt="QR Code">

                                </div>

                            </h3>

                        </div>

                    </div>

                </div>

                <div class="clearfix"></div>

            </div><!-- End: row -->

            <div class="row noMar">

                <h1>

                    <span class="blue-text">

                        ISCN Code :

                    </span>

                    <span class="iscn-code">

                        <?php echo $stdISCNCode; ?>

                    </span>

                </h1>

            </div><!-- End: row -->

        </div><!-- End: right-cont -->

    </div>

    <div class="clearfix"></div>

    <div class="row noMar text-row">

        <div class="col-xs-10" style="text-align: justify;">

            <p>

                After verifying all the data and confirming the personal data, donor (Issuer) data, and document data, through the specialized committees and the responsible parties and agents, this code has been granted by ISCN team which declares that <strong><?php  echo $fullName; ?></strong> is designated as the person eligible to the details in this certificate. To verify this document please check the last part of the ISCN code (eight panels) on the official website: <a class="blue-text" href="www.iscnsystem.org"><span>www.iscnsystem.org</span></a>.

            </p>

            <p class="red-text" style="color: red !important;">

                We are only responsible for what appears on the ISCNsystem official website.

            </p>

            <p>

                If you have any observations, please visit the site <a href="www.iscnsystem.org"><span>www.iscnsystem.org</span></a> and write your notes in as a feedback.

            </p>

        </div>

        <div class="col-xs-2 noPadR">

            <div class="stamp-wrap">

                <img src="../assets/img/stamp4.png" alt="">

            </div>

        </div>

        <div class="clearfix"></div>

    </div><!-- End: row -->

    <div class="row report-footer">

        <div class="col-xs-3 noPadR">

            <p class="date"><span>Issued Date :</span>

                <?php echo $approvalDate; ?></p>

        </div>

        <div class="col-xs-3 ise-logo-wrap">

            <img src="../assets/img/intllogo2.png" alt="ISE Logo">

        </div>

        <div class="col-xs-6 noPadR">

            <p class="power-text">Powered by : <span>International Standards Establishment</span></p>

        </div>

        <div class="clearfix"></div>

    </div>

    </div><!-- End: row -->

    <?php

} // End: if (isset($_GET['action']) && isset($_GET['stdID'])) { ?>

<?php



if (isset($_POST['btn-delete-person']) && isset($_POST['person-id'])) {

    global $connection;

    session_start();

    $userID = $_SESSION['userID'];

    $personID = mysql_prep($_POST['person-id']);

    $orderID = get_col_On_key('student', 'order_id','id',$personID);

    $delete = 0;

    $deletPersonQry = "UPDATE student SET editor = $userID, status = $delete WHERE id = $personID";

    $deletPersonRun = mysqli_query($connection, $deletPersonQry);

    if ($deletPersonRun) {

        $_SESSION['msgType']     = "1";

        $_SESSION['msg']         = "Certificate deleted successfully !";

        header('Location: ../establishment/cert-grid.php?orderID='.$orderID);

        exit();

    }

    else{



        $_SESSION['msgType']     = "0";

        $_SESSION['msg']         = "Try again or contact with ISCN !";

        header('Location: ../establishment/cert-grid.php?orderID='.$orderID);

        exit();

    }

}

if (isset($_GET['personID']) && isset($_GET['action'])) {

    $personID = $_GET['personID'];

    $action = $_GET['action'];

    if ($action == 'viewPersonCert') {

        $personCert = get_col_On_key('student', 'id_path', 'id', $personID);

        $ext = pathinfo($personCert, PATHINFO_EXTENSION);

        $ext = strtolower($ext);

        $personCertFullPath = 'admin/uploads/student/id-card/'.$personCert;

        if ($ext == "pdf") { ?>

            <div><a target="_blank" href="<?php echo '../admin/uploads/student/id-card/'.$personCert; ?>">DownLoad</a></div>

            <div><iframe style="width: 100%; height: 400px;" src="http://docs.google.com/gview?url=http://www.iscnsystem.org/<?php echo $personCertFullPath; ?>&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe></div>

        <?php } // End: if ($ext == "pdf") {

        else if ($ext == "jpeg" || $ext == "jpg" ||$ext == "png" ) { ?>

            <img style="width: 100%;height: 400px;" class="img-responsive" src="../admin/uploads/student/id-card/<?php echo $personCert; ?>">

            <?php

        } // End: elseif ($ext == "JPEG" || $ext == "JPG" ||$ext == "png" )

    }

}







/*----------  Request For new voucher or recharge voucher  ----------*/

if (isset($_POST['btn-request-voucher'])) {

    global $connection;

    $userType=$userID=$to=$from=$requestFor=$amount=$message=NULL;

    session_start();

    $userType = $_SESSION['userType'];

    if ($userType == '') {

        $userType = "User";

    } // End: if ($userType == '') {

    $userType;echo "<br/>";

    $userID = $_SESSION['userID'];

    $to = mysql_prep($_POST['reciever']);

    $userEstbID = get_col_On_key('users','estb_id','id', $userID);

    $from = get_col_On_key('est','email','id', $userEstbID);

    $requestFor = mysql_prep($_POST['request-for']);

    $quantity = mysql_prep($_POST['voucher-quantity']);

    $amount = mysql_prep($_POST['amount']);

    $message = $_POST['request-msg'];

    $message = str_replace(array("\n", "\r"), '', $message);

    $message = str_replace("<p></p>", '', $message);

    $message = str_replace("<p><br></p>", '', $message);

    $message     = mysql_prep($message);

    $now = date( 'Y-d-m h:i:s A' );

    $estbName = get_col_On_key('est','name','id', $userEstbID);

    // Always set content-type when sending HTML email

    $headers = "MIME-Version: 1.0" . "\r\n";

    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

    // More headers

    $Cc = "it.manager@iscnsystem.org";

    $headers .= 'From: ' .$from. "\r\n";

    $headers .= 'Cc: '.$Cc."\r\n";

    if ($requestFor == 'New Voucher') {

        $subject  = "Request for ".$quantity." New Voucher From ".$estbName;

    } // End: if ($requestFor == 'New Voucher') {

    elseif ($requestFor == 'Recharge Voucher') {

        $subject  = "Request for Recharge Voucher from ".$estbName;

    } // End: elseif ($requestFor == 'Recharge Voucher') {

    if ($quantity == '') {

        $quantity = 1;

    } // End: if ($quantity == '') {

    $insertRequestQry  = "INSERT INTO recharge_request_tbl ( ";

    $insertRequestQry .= "requester_id, ";

    $insertRequestQry .= "requester_type, ";

    $insertRequestQry .= "request_to, ";

    $insertRequestQry .= "request_for, ";

    $insertRequestQry .= "request_msg, ";

    $insertRequestQry .= "voucher_quantity, ";

    $insertRequestQry .= "request_amount, ";

    $insertRequestQry .= "request_date, ";

    $insertRequestQry .= "status";

    $insertRequestQry .= ") VALUES ( ";

    $insertRequestQry .= "$userID, ";

    $insertRequestQry .= "'$userType', ";

    $insertRequestQry .= "'$to', ";

    $insertRequestQry .= "'$requestFor', ";

    $insertRequestQry .= "'$message', ";

    $insertRequestQry .= "$quantity, ";

    $insertRequestQry .= "$amount, ";

    $insertRequestQry .= "'$now', ";

    $insertRequestQry .= "1";

    $insertRequestQry .= ")";

    // exit();

    $insertRequestRun = mysqli_query($connection, $insertRequestQry);

    if ($insertRequestRun) {

        if (mail($to,$subject,$message,$headers)) {

            session_start();

            $_SESSION['msgType'] = "1";

            $_SESSION['msg']  = "Request sent successfully !";

            redirect_to( '../establishment/index.php' );

            exit();

        } // End: if (mail(...)) {

        else {

            session_start();

            $_SESSION['msgType'] = "1";

            $_SESSION['msg']  = "Request sent successfully ! Email not send on local !";

            redirect_to( '../establishment/index.php' );

            exit();

        } // End: else {

    } // End : if ($insertRequestRun) {

} // End : if (isset($_POST['btn-request-voucher'])) {







if ($_POST['action']=="savefeedback") {

    global $connection;

    $errors = array(); //To store errors

    $form_data = array(); //Pass back the data to `form.php`

    /* Validate the form on server side */

    if (empty($_POST['name'])) { //Name cannot be empty

        $errors['name'] = 'Name cannot be blank';

    }

    if (empty($_POST['email'])) { //Name cannot be empty

        $errors['email'] = 'Email cannot be blank';

    }

    if (empty($_POST['rating'])) { //Name cannot be empty

        $errors['rating'] = 'Choose rating !';

    }

    if (empty($_POST['feedback'])) { //Name cannot be empty

        $errors['feedback'] = 'Feedback cannot be blank';

    }

    if (empty($errors)) {

        $senderName = $_POST['name'];

        $senderEmail = $_POST['email'];

        $rate = $_POST['rating'];

        $comment = $_POST['feedback'];

        $now = date( 'Y-d-m h:i:s A' );

        $saveFeedbackQry = "INSERT INTO feedback_tbl( ";

        $saveFeedbackQry .= "sender_name, ";

        $saveFeedbackQry .= "sender_email, ";

        $saveFeedbackQry .= "rate, ";

        $saveFeedbackQry .= "comment, ";

        $saveFeedbackQry .= "create_date ";

        $saveFeedbackQry .= ") VALUES (";

        $saveFeedbackQry .= "'$senderName', ";

        $saveFeedbackQry .= "'$senderEmail', ";

        $saveFeedbackQry .= "$rate, ";

        $saveFeedbackQry .= "'$comment', ";

        $saveFeedbackQry .= "'$now' ";

        $saveFeedbackQry .= ")";

        $saveFeedbackRun = mysqli_query($connection,$saveFeedbackQry);

        if (!$saveFeedbackRun) {

            $errors['feedbackQry'] = "Failed. Try Again.";

        }



        $to = "admin@iscnsystem.org";

        //$to = "jahangirkhattak13@gmail.com";

        $from = $senderEmail;

        $subject  = "New Feedback From ISCN Visitor : ".$senderEmail;

        // Always set content-type when sending HTML email

        $headers = "MIME-Version: 1.0" . "\r\n";

        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers

        $Cc = "it.manager@iscnsystem.org";

        //$Cc = "jahangirkhattak13@gmail.com";

        $headers .= 'From: ' .$from. "\r\n";

        $headers .= 'Cc: '.$Cc."\r\n";

        $message = '<p><strong>User comments</strong></p>'.$comment.'<p><strong>Ratings :</strong></p><p>'.$rate. ' Star</p>';

        if (!mail($to,$subject,$message,$headers)) {

            $errors['feedbackEmail'] = "Feedback recieved to ISCN..";

        } // End: if (mail(...)) {



    }

    if (!empty($errors)) { //If errors in validation

        $form_data['success'] = false;

        $form_data['errors']  = $errors;

    }

    else {

        //If not, process the form, and return true on success

        $form_data['success'] = true;

        $form_data['posted'] = 'Thank you for your feedback !';

        //$form_data['posted'] = "Data Was Posted Successfully "."<br>Name = ".$senderName."<br>Email = ".$senderEmail."<br>Rate = ".$rate."<br>Feedback = ".$comment."<br>Date ".$now;

    }

    //Return the data back to form.php

    echo json_encode($form_data);

}

