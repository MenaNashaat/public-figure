<?php $page_title = "Verified Event | Home";
require_once("include/header.php");
require_once("include/navigation.php");



?>
<div class="wrapper wrapper-content animated">

    <div class="container">

        <div class="row">
            <?php

            function countryCodeToCountry($code) {
                $code = strtoupper($code);
                if ($code == 'AF') return 'Afghanistan';
                if ($code == 'AX') return 'Aland Islands';
                if ($code == 'AL') return 'Albania';
                if ($code == 'DZ') return 'Algeria';
                if ($code == 'AS') return 'American Samoa';
                if ($code == 'AD') return 'Andorra';
                if ($code == 'AO') return 'Angola';
                if ($code == 'AI') return 'Anguilla';
                if ($code == 'AQ') return 'Antarctica';
                if ($code == 'AG') return 'Antigua and Barbuda';
                if ($code == 'AR') return 'Argentina';
                if ($code == 'AM') return 'Armenia';
                if ($code == 'AW') return 'Aruba';
                if ($code == 'AU') return 'Australia';
                if ($code == 'AT') return 'Austria';
                if ($code == 'AZ') return 'Azerbaijan';
                if ($code == 'BS') return 'Bahamas the';
                if ($code == 'BH') return 'Bahrain';
                if ($code == 'BD') return 'Bangladesh';
                if ($code == 'BB') return 'Barbados';
                if ($code == 'BY') return 'Belarus';
                if ($code == 'BE') return 'Belgium';
                if ($code == 'BZ') return 'Belize';
                if ($code == 'BJ') return 'Benin';
                if ($code == 'BM') return 'Bermuda';
                if ($code == 'BT') return 'Bhutan';
                if ($code == 'BO') return 'Bolivia';
                if ($code == 'BA') return 'Bosnia and Herzegovina';
                if ($code == 'BW') return 'Botswana';
                if ($code == 'BV') return 'Bouvet Island (Bouvetoya)';
                if ($code == 'BR') return 'Brazil';
                if ($code == 'IO') return 'British Indian Ocean Territory (Chagos Archipelago)';
                if ($code == 'VG') return 'British Virgin Islands';
                if ($code == 'BN') return 'Brunei Darussalam';
                if ($code == 'BG') return 'Bulgaria';
                if ($code == 'BF') return 'Burkina Faso';
                if ($code == 'BI') return 'Burundi';
                if ($code == 'KH') return 'Cambodia';
                if ($code == 'CM') return 'Cameroon';
                if ($code == 'CA') return 'Canada';
                if ($code == 'CV') return 'Cape Verde';
                if ($code == 'KY') return 'Cayman Islands';
                if ($code == 'CF') return 'Central African Republic';
                if ($code == 'TD') return 'Chad';
                if ($code == 'CL') return 'Chile';
                if ($code == 'CN') return 'China';
                if ($code == 'CX') return 'Christmas Island';
                if ($code == 'CC') return 'Cocos (Keeling) Islands';
                if ($code == 'CO') return 'Colombia';
                if ($code == 'KM') return 'Comoros the';
                if ($code == 'CD') return 'Congo';
                if ($code == 'CG') return 'Congo the';
                if ($code == 'CK') return 'Cook Islands';
                if ($code == 'CR') return 'Costa Rica';
                if ($code == 'CI') return 'Cote d\'Ivoire';
                if ($code == 'HR') return 'Croatia';
                if ($code == 'CU') return 'Cuba';
                if ($code == 'CY') return 'Cyprus';
                if ($code == 'CZ') return 'Czech Republic';
                if ($code == 'DK') return 'Denmark';
                if ($code == 'DJ') return 'Djibouti';
                if ($code == 'DM') return 'Dominica';
                if ($code == 'DO') return 'Dominican Republic';
                if ($code == 'EC') return 'Ecuador';
                if ($code == 'EG') return 'Egypt';
                if ($code == 'SV') return 'El Salvador';
                if ($code == 'GQ') return 'Equatorial Guinea';
                if ($code == 'ER') return 'Eritrea';
                if ($code == 'EE') return 'Estonia';
                if ($code == 'ET') return 'Ethiopia';
                if ($code == 'FO') return 'Faroe Islands';
                if ($code == 'FK') return 'Falkland Islands (Malvinas)';
                if ($code == 'FJ') return 'Fiji the Fiji Islands';
                if ($code == 'FI') return 'Finland';
                if ($code == 'FR') return 'France, French Republic';
                if ($code == 'GF') return 'French Guiana';
                if ($code == 'PF') return 'French Polynesia';
                if ($code == 'TF') return 'French Southern Territories';
                if ($code == 'GA') return 'Gabon';
                if ($code == 'GM') return 'Gambia the';
                if ($code == 'GE') return 'Georgia';
                if ($code == 'DE') return 'Germany';
                if ($code == 'GH') return 'Ghana';
                if ($code == 'GI') return 'Gibraltar';
                if ($code == 'GR') return 'Greece';
                if ($code == 'GL') return 'Greenland';
                if ($code == 'GD') return 'Grenada';
                if ($code == 'GP') return 'Guadeloupe';
                if ($code == 'GU') return 'Guam';
                if ($code == 'GT') return 'Guatemala';
                if ($code == 'GG') return 'Guernsey';
                if ($code == 'GN') return 'Guinea';
                if ($code == 'GW') return 'Guinea-Bissau';
                if ($code == 'GY') return 'Guyana';
                if ($code == 'HT') return 'Haiti';
                if ($code == 'HM') return 'Heard Island and McDonald Islands';
                if ($code == 'VA') return 'Holy See (Vatican City State)';
                if ($code == 'HN') return 'Honduras';
                if ($code == 'HK') return 'Hong Kong';
                if ($code == 'HU') return 'Hungary';
                if ($code == 'IS') return 'Iceland';
                if ($code == 'IN') return 'India';
                if ($code == 'ID') return 'Indonesia';
                if ($code == 'IR') return 'Iran';
                if ($code == 'IQ') return 'Iraq';
                if ($code == 'IE') return 'Ireland';
                if ($code == 'IM') return 'Isle of Man';
                if ($code == 'IL') return 'Israel';
                if ($code == 'IT') return 'Italy';
                if ($code == 'JM') return 'Jamaica';
                if ($code == 'JP') return 'Japan';
                if ($code == 'JE') return 'Jersey';
                if ($code == 'JO') return 'Jordan';
                if ($code == 'KZ') return 'Kazakhstan';
                if ($code == 'KE') return 'Kenya';
                if ($code == 'KI') return 'Kiribati';
                if ($code == 'KP') return 'Korea';
                if ($code == 'KR') return 'Korea';
                if ($code == 'KW') return 'Kuwait';
                if ($code == 'KG') return 'Kyrgyz Republic';
                if ($code == 'LA') return 'Lao';
                if ($code == 'LV') return 'Latvia';
                if ($code == 'LB') return 'Lebanon';
                if ($code == 'LS') return 'Lesotho';
                if ($code == 'LR') return 'Liberia';
                if ($code == 'LY') return 'Libyan Arab Jamahiriya';
                if ($code == 'LI') return 'Liechtenstein';
                if ($code == 'LT') return 'Lithuania';
                if ($code == 'LU') return 'Luxembourg';
                if ($code == 'MO') return 'Macao';
                if ($code == 'MK') return 'Macedonia';
                if ($code == 'MG') return 'Madagascar';
                if ($code == 'MW') return 'Malawi';
                if ($code == 'MY') return 'Malaysia';
                if ($code == 'MV') return 'Maldives';
                if ($code == 'ML') return 'Mali';
                if ($code == 'MT') return 'Malta';
                if ($code == 'MH') return 'Marshall Islands';
                if ($code == 'MQ') return 'Martinique';
                if ($code == 'MR') return 'Mauritania';
                if ($code == 'MU') return 'Mauritius';
                if ($code == 'YT') return 'Mayotte';
                if ($code == 'MX') return 'Mexico';
                if ($code == 'FM') return 'Micronesia';
                if ($code == 'MD') return 'Moldova';
                if ($code == 'MC') return 'Monaco';
                if ($code == 'MN') return 'Mongolia';
                if ($code == 'ME') return 'Montenegro';
                if ($code == 'MS') return 'Montserrat';
                if ($code == 'MA') return 'Morocco';
                if ($code == 'MZ') return 'Mozambique';
                if ($code == 'MM') return 'Myanmar';
                if ($code == 'NA') return 'Namibia';
                if ($code == 'NR') return 'Nauru';
                if ($code == 'NP') return 'Nepal';
                if ($code == 'AN') return 'Netherlands Antilles';
                if ($code == 'NL') return 'Netherlands the';
                if ($code == 'NC') return 'New Caledonia';
                if ($code == 'NZ') return 'New Zealand';
                if ($code == 'NI') return 'Nicaragua';
                if ($code == 'NE') return 'Niger';
                if ($code == 'NG') return 'Nigeria';
                if ($code == 'NU') return 'Niue';
                if ($code == 'NF') return 'Norfolk Island';
                if ($code == 'MP') return 'Northern Mariana Islands';
                if ($code == 'NO') return 'Norway';
                if ($code == 'OM') return 'Oman';
                if ($code == 'PK') return 'Pakistan';
                if ($code == 'PW') return 'Palau';
                if ($code == 'PS') return 'Palestinian Territory';
                if ($code == 'PA') return 'Panama';
                if ($code == 'PG') return 'Papua New Guinea';
                if ($code == 'PY') return 'Paraguay';
                if ($code == 'PE') return 'Peru';
                if ($code == 'PH') return 'Philippines';
                if ($code == 'PN') return 'Pitcairn Islands';
                if ($code == 'PL') return 'Poland';
                if ($code == 'PT') return 'Portugal, Portuguese Republic';
                if ($code == 'PR') return 'Puerto Rico';
                if ($code == 'QA') return 'Qatar';
                if ($code == 'RE') return 'Reunion';
                if ($code == 'RO') return 'Romania';
                if ($code == 'RU') return 'Russian Federation';
                if ($code == 'RW') return 'Rwanda';
                if ($code == 'BL') return 'Saint Barthelemy';
                if ($code == 'SH') return 'Saint Helena';
                if ($code == 'KN') return 'Saint Kitts and Nevis';
                if ($code == 'LC') return 'Saint Lucia';
                if ($code == 'MF') return 'Saint Martin';
                if ($code == 'PM') return 'Saint Pierre and Miquelon';
                if ($code == 'VC') return 'Saint Vincent and the Grenadines';
                if ($code == 'WS') return 'Samoa';
                if ($code == 'SM') return 'San Marino';
                if ($code == 'ST') return 'Sao Tome and Principe';
                if ($code == 'SA') return 'Saudi Arabia';
                if ($code == 'SN') return 'Senegal';
                if ($code == 'RS') return 'Serbia';
                if ($code == 'SC') return 'Seychelles';
                if ($code == 'SL') return 'Sierra Leone';
                if ($code == 'SG') return 'Singapore';
                if ($code == 'SK') return 'Slovakia (Slovak Republic)';
                if ($code == 'SI') return 'Slovenia';
                if ($code == 'SB') return 'Solomon Islands';
                if ($code == 'SO') return 'Somalia, Somali Republic';
                if ($code == 'ZA') return 'South Africa';
                if ($code == 'GS') return 'South Georgia and the South Sandwich Islands';
                if ($code == 'ES') return 'Spain';
                if ($code == 'LK') return 'Sri Lanka';
                if ($code == 'SD') return 'Sudan';
                if ($code == 'SR') return 'Suriname';
                if ($code == 'SJ') return 'Svalbard & Jan Mayen Islands';
                if ($code == 'SZ') return 'Swaziland';
                if ($code == 'SE') return 'Sweden';
                if ($code == 'CH') return 'Switzerland, Swiss Confederation';
                if ($code == 'SY') return 'Syrian Arab Republic';
                if ($code == 'TW') return 'Taiwan';
                if ($code == 'TJ') return 'Tajikistan';
                if ($code == 'TZ') return 'Tanzania';
                if ($code == 'TH') return 'Thailand';
                if ($code == 'TL') return 'Timor-Leste';
                if ($code == 'TG') return 'Togo';
                if ($code == 'TK') return 'Tokelau';
                if ($code == 'TO') return 'Tonga';
                if ($code == 'TT') return 'Trinidad and Tobago';
                if ($code == 'TN') return 'Tunisia';
                if ($code == 'TR') return 'Turkey';
                if ($code == 'TM') return 'Turkmenistan';
                if ($code == 'TC') return 'Turks and Caicos Islands';
                if ($code == 'TV') return 'Tuvalu';
                if ($code == 'UG') return 'Uganda';
                if ($code == 'UA') return 'Ukraine';
                if ($code == 'AE') return 'United Arab Emirates';
                if ($code == 'GB') return 'United Kingdom';
                if ($code == 'US') return 'United States of America';
                if ($code == 'UM') return 'United States Minor Outlying Islands';
                if ($code == 'VI') return 'United States Virgin Islands';
                if ($code == 'UY') return 'Uruguay, Eastern Republic of';
                if ($code == 'UZ') return 'Uzbekistan';
                if ($code == 'VU') return 'Vanuatu';
                if ($code == 'VE') return 'Venezuela';
                if ($code == 'VN') return 'Vietnam';
                if ($code == 'WF') return 'Wallis and Futuna';
                if ($code == 'EH') return 'Western Sahara';
                if ($code == 'YE') return 'Yemen';
                if ($code == 'XK') return 'Kosovo';
                if ($code == 'ZM') return 'Zambia';
                if ($code == 'ZW') return 'Zimbabwe';
                return '';
            }

            $Qry_sql = "SELECT country FROM event_visitors GROUP BY country";
            $Qry = mysqli_query($connection, $Qry_sql);
            $countries=array();
            $nums=array();
            $graph=array();
            while($row=mysqli_fetch_assoc($Qry))
            {
                $country=$row["country"];

                $Qry_sql2 = "SELECT DISTINCT ip_address  FROM event_visitors WHERE country='$country'";
                $Qry2 = mysqli_query($connection, $Qry_sql2);
                $total=mysqli_num_rows($Qry2);

                $countries[]=$country;
                $nums[]=$total;
                $graph[]=$country.":".$total;
            }//end while


            $Qry_sql3 = "SELECT  *  FROM event_visitors WHERE DATE(STR_TO_DATE(time ,'%Y-%d-%m %h:%i:%s')) = CURDATE()";
            $Qry3 = mysqli_query($connection, $Qry_sql3);
            $total_today=mysqli_num_rows($Qry3);


            $Qry_sql3 = "SELECT  *  FROM event_visitors ";
            $Qry3 = mysqli_query($connection, $Qry_sql3);
            $total_all=mysqli_num_rows($Qry3);
            ?>

            <div class="col-lg-12">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">
                                <h2> Verified Event Website visitors &nbsp; &nbsp;&nbsp; Today : <?php echo $total_today; ?> &nbsp; &nbsp; &nbsp; All Visitors :  <?php echo $total_all; ?></h2>


                            </div>
                            <div class="ibox-content">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <table class="table table-hover margin bottom">
                                            <thead>
                                            <tr>
                                                <th style="width: 1%" class="text-center">No.</th>
                                                <th class="text-center">Countries
                                                </th>

                                                <th class="text-center">Number of visitors</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $i=-1;
                                            foreach($countries as $item)
                                            {

                                                $i++;
                                                ?>
                                                <tr>
                                                    <td class="text-center"> <?php echo $i+1; ?></td>
                                                    <td class="text-center">
                                                        <?php echo countryCodeToCountry($item); ?>
                                                    </td>

                                                    <td class="text-center"><span class="label label-primary">     <?php echo $nums[$i]; ?></span></td>

                                                </tr>
                                                <?php
                                            }
                                            ?>


                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-6">
                                        <div id="world-map" style="height: 300px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>

        <div class="row">
            <?php



            $Qry_sql = "SELECT * FROM event_visitors ORDER BY STR_TO_DATE(time ,'%Y-%d-%m %h:%i:%s') DESC LIMIT 200";
            $Qry = mysqli_query($connection, $Qry_sql);


            ?>

            <div class="col-lg-12">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Last 200 visits</h5>

                            </div>
                            <div class="ibox-content">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-hover margin bottom">
                                            <thead>
                                            <tr>
                                                <th style="width: 1%" class="text-center">No.</th>

                                                <th class="text-center">Country</th>
                                                <th class="text-center">
                                                    ip address
                                                </th>
                                                <th class="text-center">
                                                    time
                                                </th>
                                                <th class="text-center">
                                                    visited pages
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $i=0;
                                            while($row=mysqli_fetch_assoc($Qry))
                                            {
                                                $i++;

                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $i; ?></td>
                                                    <td class="text-center">
                                                        <?php echo countryCodeToCountry($row["country"]); ?>
                                                    </td>

                                                    <td class="text-center"><span class="label label-primary">     <?php echo $row["ip_address"]; ?></span></td>
                                                    <td class="text-center">
                                                        <?php echo $row["time"]; ?>
                                                    </td>

                                                    <td class="text-center">
                                                        <?php echo $row["freq"]; ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }//end while
                                            ?>


                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>

    </div>

</div><?php require_once("include/footer.php"); ?><!-- Mainly scripts -->

<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>



<!-- Jvectormap -->
<script src="js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>


<script>
    $(".visitors").addClass("active");
    $(document).ready(function() {


        var countries=<?php echo json_encode($countries) ?>;
        var nums=<?php echo json_encode($nums) ?>;
        var i=-1;
        var obj = {};
        countries.forEach(function(element) {
            i++;
            obj[element]=nums[i];
        });


        var mapData =obj;
        var gdpData = {


        };

        $('#world-map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: "transparent",
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 0.9,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 0
                }
            },

            series: {
                regions: [{
                    values: mapData,
                    scale: ["#1ab394", "#22d6b1"],
                    normalizeFunction: 'polynomial'
                }]
            },
            onRegionTipShow: function(e, el, code){


                el.html(el.html()+" " + obj[code]);
            }
        });
    });
</script>

</body></html>