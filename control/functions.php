<?php  ini_set('MAX_EXCUTION_TIME',0);
ignore_user_abort(true);


session_start();
require "../db-connect.php";
global $connection;

function remove_white_spaces( $data )

{

    $data = trim( $data );

    $data = stripslashes( $data );

    $data = htmlspecialchars( $data );

    $data = preg_replace('/\s+/', '', $data);

    return $data;

}
function mysql_prep( $string )
{
    global $connection;
    $escapeString = mysqli_real_escape_string( $connection, $string );
    return $escapeString;
}
function redirect_to( $newLocation )
{
    header( "Location: " . $newLocation );
    exit( );
}


if(isset($_POST["control_login"]))
{

    if( isset($_POST["username"]) && isset($_POST["password"]) )
    {
        $username=mysql_prep($_POST["username"]);
        $password=mysql_prep($_POST["password"]);
        $password= remove_white_spaces($password);

        $Qry    = "SELECT id,name,password FROM est_control_admin WHERE username = '$username'";

        $Run = mysqli_query( $connection, $Qry );

        // Mysql_num_row is counting table row

        $count      = mysqli_num_rows( $Run );

        // If the entered username exist in database then $count

        // must be equal to 1

        if ( $count == 1 ) {

            //user existed. Now we are going to check password.

            $Record         = mysqli_fetch_assoc( $Run );

            // Geting md5 password from database of this username

            $PasswordDB = $Record['password'];





            // convert user entered password with md5 function and add Salt

            $salt               = "isdf2017";

            $saltedpass           = sha1( $password . $salt );

            $hashedPassword       = md5($saltedpass);

            if ($hashedPassword == $PasswordDB) {

                // password is correct

                $_SESSION['ADMIN_CONTROL_ID'] = $Record[ 'id' ];

                $_SESSION['EST_CONTROL_SESSION_CODE']     = "0";

                redirect_to( '../index.php' );

                exit();

            } // End: if ($passwordSalted == $estbUserPasswordDB) {

            else {

                // Wrong password.



                $_SESSION['EST_CONTROL_SESSION_CODE']     = "2";

                redirect_to( '../login.php');

                exit();

            } // End: else {

        } // End: if ( $count == 1 ) {

        else {


            $_SESSION['EST_CONTROL_SESSION_CODE']  = "1";

            redirect_to( '../login.php');

            exit();

        } // End: else {




    }//end if


}




if(isset($_POST["get_phd"]))
{

    $result=array();

    $usersQry  = "SELECT ";
    $usersQry .= "phd_users.id , ";
    $usersQry .= "phd_users.phd_code , ";
    $usersQry .= "phd_users.first_name, ";
    $usersQry .= "phd_users.middle_name , ";
    $usersQry .= "phd_users.photo_path AS indvPhoto, ";
    $usersQry .= "phd_users.last_name , ";
    $usersQry .= "phd_users.email , ";
    $usersQry .= "phd_users.status,  ";
    $usersQry .= "phd_cert.app_id ";
    $usersQry .= "FROM phd_users  INNER JOIN phd_cert ON phd_cert.phd_id=phd_users.id  WHERE phd_users.plan_name ='' OR  phd_users.plan_name IS NULL ";
    $usersQry .= "ORDER BY  phd_users.id DESC ";
    $usersRun = mysqli_query($connection, $usersQry);

    while ($userRecord = mysqli_fetch_assoc($usersRun)) {


        $userRecord["indvPhoto"] = "https://verifiedphd.org/register/uploads/phd/".$userRecord["indvPhoto"];

        if($userRecord['status']=="new") {

            $userRecord["label"]="label-warning";
            $userRecord["laber_text"]="Arrived";
        }
        elseif($userRecord['status']=="under_process") {

            $userRecord["label"]="label-primary";
            $userRecord["laber_text"]="Under process";
        }
        elseif($userRecord['status']=="missing_fields") {

            $userRecord["label"]="label-danger";
            $userRecord["laber_text"]="Missing Fields";
        }
        elseif($userRecord['status']=="approved") {

            $userRecord["label"]="label-success";
            $userRecord["laber_text"]="Approved";
        }


        $result[]=$userRecord;
    }



    echo json_encode($result);




}


if(isset($_POST["send_emails"]))
{
    $result=array();
    $mails=$_POST["mails"];
    $mails = json_decode($mails);

    //$emails = array();
    //$counter = 0;
    foreach ($mails as $obj )
    {
        //$counter++;
        $use_id = $obj->estbID;

        $to = $obj->estbEmail;
        $name = $obj->estbName;
        $app_id = $obj->app_id;

        if($to == "") continue;  //empty mail string

        $subject="Verified Event Support";
        $text_action="";
        $text_action='<div style="text-align:left">
    <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><br><span style="font-size: 12pt;"><strong>DEAR Organizer of '.$name.'</strong></span><br><br><br>
        With all our greetings we are welcomed you to the world of reliability<br> and we are honored to join the
        <strong><span style="color: #008000;">International Standard Verified Event</span></strong><br><strong><span style="color: #008000;"> ID.</span></strong><br><br> We have reviewed your request and <br> we
        would like to inform you that You must complete the payment process.<br> to get the <strong><span style="color: #0000ff;">Event ID</span></strong> and The <span style="color: #0000ff;"><strong>QR</strong></span> code and enjoy the rest of other benefits and services.<br><br> Follow the link below and choose your
        suitable plan &amp; complete the payment process.<br><span style="color: #0000ff;"><strong><a style="color: #0000ff;" href="https://verifiedevent.org/payment.php?app_id='.$app_id.'" target="_blank" rel="noopener noreferrer">https://verifiedevent.org/payment.php?app_id='.$app_id.'</a></strong></span></div>
    <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">&nbsp;</div>
    <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">OR VISIT Website and check your app number
        .
    </div>
    <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><br><span style="color: #0000ff;"><strong><a style="color: #0000ff;" href="https://www.verifiedphd.org" target="_blank" rel="noopener noreferrer">https://verifiedevent.org</a></strong></span>&nbsp;
    </div>
    <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><br> Your App Number :&nbsp;<strong><span style="color: #0000ff;"><a class="" style="color: #0000ff;" href="https://verifiedphd.org/phd_status.php?appID='.$app_id.'" target="_blank" rel="noopener noreferrer">'.$app_id.'</a></span></strong><br><br></div>
    <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">
        <br>
        --
        <br>
        <span style="color:#008000">   The most important benefits</span>
        <br>
        <ul style="LINE-HEIGHT: 1.2;color:black;     font-size: 12px;   direction: ltr;">

            <li>Verify all your certificates, research and scientific works.
            </li>

            <li> Keep an electronic verified copy of your academic papers safe and available forever.
            </li>
            <li>
                Get your ID that gives a free access to anyone to see all your verified qualifications .
            </li>
            <li>
                Create your own documented profile, share your publications and publish your data.
            </li>
            <li>
                Receive job offers from the employers who are interested in your profile.
            </li>
            <li>
                Connect and collaborate with colleagues, peers, co-authors, and specialists.
            </li>
            <li>
                Get statistics and find out who\'s been reading and citing your work.
            </li>


            <li>
                Be part of a safe and verified environment where Frauds are not allowed in .
            </li>

            <li>
                Share updates about your current project, and keep up with the latest research.
            </li>



        </ul>
        <br>


        <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><br> This is a demo profile example&nbsp;<strong><span style="color: #0000ff;"><a class="" style="color: #0000ff;" href="https://verifiedevent.org/event_profile.php?event_id=KIZ724" target="_blank" rel="noopener noreferrer">Here</a></span></strong>
        </div>


        <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><br> This is an ISCN profile example&nbsp;<strong><span style="color: #0000ff;"><a class="" style="color: #0000ff;" href="https://iscnsystem.org/phd_certificate.php?iscn_code=WFWQ3438" target="_blank" rel="noopener noreferrer">Here</a></span></strong><br><br>
        </div>

        <br> For any information please do not
        hesitate to contact us.<br><br>





        <strong>Best regards </strong><br><br><strong>verification
            Team </strong><br><br>
        <div>
            <p><span>--&nbsp;</span></p>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><span style="color: #0000ff; font-size: 12pt;"><em><strong>International Standards EST</strong></em></span><br><br><span><em><strong>General coordinator&nbsp;&nbsp;<br><br> USA-MICHIGAN </strong></em></span><span style="font-size: 10pt;">&nbsp;</span></div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">&nbsp;</div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">&nbsp;</div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><img src="https://estid.org/uploads/establishment/qr_profile/qrcode-IS57.png"></div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">
                <div class="cover">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-xs-12 m-t-em-4">
                                <div class="col-md-12 col-xs-12">
                                    <div class="text-left">
                                        <h4 class="main-color">International Verified Establishment ID:&nbsp;<span style="color: #0000ff;"><span style="font-size: 16px;">IS57</span></span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <span style="text-decoration: underline; color: #0000ff;"></span></div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><strong><span style="text-decoration: underline; color: #0000ff;">https://<a style="color: #0000ff; text-decoration: underline;" href="http://www.iscnsystem.org/" target="_blank" rel="noopener noreferrer">www.</a>verifiedphd.org</span></strong></div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;"><strong><span style="text-decoration: underline; color: #0000ff;"><a style="color: #0000ff; text-decoration: underline;" href="https://estid.org/" target="_blank" rel="noopener noreferrer">https://</a><a style="color: #0000ff; text-decoration: underline;" href="http://www.iscnsystem.org/" target="_blank" rel="noopener noreferrer">www.iscnsystem.org</a></span></strong>
            </div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">&nbsp;</div>
            <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">
                <img src="https://verifiedphd.org/img/ecc46d4f.png" width="61" height="61">
                <img src="https://verifiedphd.org/img/download.png" width="57" height="55">
                <img src="https://verifiedphd.org/img/7161422d.png" width="57" height="55">
                <img src="https://verifiedevent.org/img/Asset%207.svg" width="138" height="50" >

                <div class="pre" style="margin: 0; padding: 0; font-family: monospace;">
                    <table width="577">
                        <tbody>
                        <tr>
                            <td width="576">
                                <p><span><strong>Confidentiality Note</strong>:</span><span>&nbsp;</span><span>The content of this e-mail and any attachments thereto are privileged information for the named addressee only.&nbsp;</span>
                                </p>
                            </td>
                            <td width="1">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                    <p><span>Think of the Environment and your responsibilities before printing this email.</span></p>
                </div>
            </div>
        </div>
    </div>
</div>';
        $text_action2=mysqli_real_escape_string($connection, $text_action);
        $from = "pr@verifiedevent.org";
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: Verified Event <" .$from. "> \r\n";
        $Cc = "support@verifiedevent.org";
        $headers .= 'Cc: '.$Cc."\r\n";

        //if($to != "r.engmahmoud@yahoo.com") continue;  //for testing
        if (mail($to,$subject,$text_action,$headers)) {
            $now = date('Y-d-m h:i:s A');
            $indvQry = "INSERT INTO event_followup_emails (event_id,subject,content,time) VALUES ('$use_id','$subject','$text_action2','$now') ";
            $certRun = mysqli_query($connection, $indvQry);

        }

        //$emails[] = $to;

    }

    //$result["emails"] = $emails;
    //$result["hello"] = $mails;
    $result["status"]="ok";
    echo json_encode($result);



}















?>