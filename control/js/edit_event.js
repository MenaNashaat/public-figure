$(".edit_event_btn").click(function () {


    $(this).attr('disabled','disabled');
    $(this).button('loading');



    var event_name = $("#event_name");
    var event_name_val = $("#event_name").val();

    var event_type = $("#event_type");
    var event_type_val = $("#event_type").val();

    var event_subject = $("#event_subject");
    var event_subject_val = $("#event_subject").val();

    var event_field = $("#event_field");
    var event_field_val = $("#event_field").val();

    var email = $("#email");
    var email_val = $("#email").val();

    var country = $("#country");
    var country_val = $("#country").val();

    var city = $("#city");
    var city_val = $("#city").val();



    var address = $("#address");
    var address_val = $("#address").val();


//optional
    var website = $("#website");
    var website_val = $("#website").val();

    var organizer_name = $("#organizer_name");
    var organizer_name_val = $("#organizer_name").val();

    var mobile = $("#mobile");
    var mobile_val = $("#mobile").val();




    var data = new FormData();
    data.append('edit_event', 'edit_event');
    data.append('event_id', event_id);
    data.append('event_name', event_name_val);
    data.append('event_type', event_type_val);
    data.append('event_subject', event_subject_val);
    data.append('event_field', event_field_val);
    data.append('email', email_val);
    data.append('country', country_val);
    data.append('city', city_val);
    data.append('address', address_val);
    data.append('website', website_val);
    data.append('organizer_name', organizer_name_val);
    data.append('mobile', mobile_val);



    $.ajax({
        url: 'api/edit_event.php',
        data: data,
        dataType: "text",
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){



            if(data == "error") {
                swal({
                    title: "  Error ",
                    text: "",
                    icon: "warning",
                    closeOnClickOutside: false,
                    dangerMode: true,
                });
            }
            else {

                location.reload();

            }//end else

        }


    });

}); //end click
