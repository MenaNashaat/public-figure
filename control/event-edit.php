<?php ob_start();
$page_title="Events ";

require_once("include/header.php");

require_once("include/navigation.php");

 global $connection;


if ($_GET['id']) {

    global $connection;

    $event_ID = mysql_prep(safe_int($_GET['id']));


}
else
{
    redirect_to('events.php');
}
$estbQry="SELECT " ;
$estbQry .="name, ";
$estbQry .="event_type, ";
$estbQry .="event_subject, ";
$estbQry .="event_field, ";
$estbQry .="country, ";
$estbQry .="city, ";
$estbQry .="address, ";
$estbQry .="website, ";
$estbQry .="email, ";
$estbQry .="mobile, ";
$estbQry .="organizer_name ";
$estbQry .="FROM event WHERE id='$event_ID'";
$estbRun=mysqli_query($connection, $estbQry);

$row=mysqli_fetch_assoc($estbRun);






?>
<style>
    @media screen and (min-width: 1024px) {
        .modal-dialog {
            width: 1024px;

        }
    }
    ul{
        list-style: none;
    }
    li{
        display: inline-block;
    }
</style>
<div class="row wrapper border-bottom white-bg ">
    <br/>
    <div class="col-lg-12">

        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <span>Events</span>
            </li>
            <li class="active">
                <strong>Events edit </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"><br/></div><br/>
</div>
<div class="clearfix"><br/></div>


<div class="wrapper animated">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">

                                    <fieldset>
                                        <h2 class="fs-title">Events Information</h2>

                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label class="pull-left">Event Name</label>
                                                <input type="text" id="event_name" value="<?php echo $row["name"]; ?>" class="form-control require_text" placeholder="Event Name">
                                            </div>
                                        </div>

                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label for="sel1" class="pull-left">Select Event Type </label>
                                                <select class="form-control require_select" id="event_type">
                                                    <option value="">Select Event type</option>
                                                    <option value="conference">Conference</option>
                                                    <option value="forum">Forum</option>
                                                    <option value="workshop">Workshop</option>
                                                    <option value="seminar">Seminar</option>
                                                    <option value="research">Research Discussion</option>
                                                    <option value="festival">Festival</option>
                                                    <option value="exhibition">Exhibition</option>
                                                    <option value="training">Training course</option>
                                                    <option value="meeting">Meeting</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label class="pull-left">Event Subject</label>
                                                <input id="event_subject" type="text" value="<?php echo $row["event_subject"]; ?>" class="form-control require_text" placeholder="Event Subject">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label for="sel1" class="pull-left">Event Field </label>
                                                <select class="form-control require_select" id="event_field">
                                                    <!-- categories of event field -->
                                                    <option value="">Select Specialization</option>
                                                    <option value="Accounting">Accounting</option>
                                                    <option value="Agriculture and Natural Racecourse">Agriculture and Natural Racecourse</option>
                                                    <option value="Architecture">Architecture</option>
                                                    <option value="Art and Design">Art and Design</option>
                                                    <option value="Biological / Life sciences">Biological / Life sciences</option>
                                                    <option value="Built environment">Built environment</option>
                                                    <option value="Business and Management">Business and Management</option>
                                                    <option value="Chemistry">Chemistry</option>
                                                    <option value="Communication and Media">Communication and Media</option>
                                                    <option value="Computing and Information Technology">Computing and Information Technology</option>
                                                    <option value="Creative Arts">Creative Arts</option>
                                                    <option value="Cultural studies">Cultural studies</option>
                                                    <option value="Dental studies">Dental studies</option>
                                                    <option value="Economics">Economics</option>
                                                    <option value="Education and Training">Education and Training</option>
                                                    <option value="Employment skills">Employment skills</option>
                                                    <option value="English languages / Literature">English languages / Literature</option>
                                                    <option value="Engineering and Technology">Engineering and Technology</option>
                                                    <option value="Environmental studies">Environmental studies</option>
                                                    <option value="Fashion and Design">Fashion and Design</option>
                                                    <option value="Food and Hospitality">Food and Hospitality</option>
                                                    <option value="Foreign languages / Literature">Foreign languages / Literature</option>
                                                    <option value="Geography">Geography</option>
                                                    <option value="Health science / Studies">Health science / Studies</option>
                                                    <option value="Humanities and Social sciences">Humanities and Social sciences</option>
                                                    <option value="Journalism">Journalism</option>
                                                    <option value="Languages and literature">Languages and literature</option>
                                                    <option value="Law">Law</option>
                                                    <option value="Legal studies">Legal studies</option>
                                                    <option value="Liberal arts / General sciences">Liberal arts / General sciences</option>
                                                    <option value="Logistics studies">Logistics studies</option>
                                                    <option value="marketing">marketing</option>
                                                    <option value="Mathematics">Mathematics</option>
                                                    <option value="Medicine">Medicine</option>
                                                    <option value="Military technologies">Military technologies</option>
                                                    <option value="Multi / Interdisciplinary">Multi / Interdisciplinary</option>
                                                    <option value="Music">Music</option>
                                                    <option value="Nursing">Nursing</option>
                                                    <option value="Personal services">Personal services</option>
                                                    <option value="Pharmacy">Pharmacy</option>
                                                    <option value="Philosophy and Religion">Philosophy and Religion</option>
                                                    <option value="Physical sciences">Physical sciences</option>
                                                    <option value="Physics">Physics</option>
                                                    <option value="Political science">Political science</option>
                                                    <option value="Psychology">Psychology</option>
                                                    <option value="Public administration">Public administration</option>
                                                    <option value="Rehabilitation">Rehabilitation</option>
                                                    <option value="Sciences">Sciences</option>
                                                    <option value="Social sciences / History">Social sciences / History</option>
                                                    <option value="Social work">Social work</option>
                                                    <option value="Sport , Leisure and Recreation">Sport , Leisure and Recreation</option>
                                                    <option value="Surveying">Surveying</option>
                                                    <option value="Technology">Technology</option>
                                                    <option value="Theological studies">Theological studies</option>
                                                    <option value="Travel and Tourism / Hotel">Travel and Tourism / Hotel</option>
                                                    <option value="Veterinary studies and Animal care">Veterinary studies and Animal care</option>
                                                    <option value="Visual and Performing art care">Visual and Performing art care</option>
                                                    <option value="Welfare and Community services">Welfare and Community services</option>
                                                    <option value="other">Other</option>

                                                    <!-- categoreies of event field end -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label class="pull-left">Email


                                                </label>
                                                <input id="email" value="<?php echo $row["email"]; ?>" type="email" class="form-control require_email" placeholder="Email">
                                            </div>
                                        </div>

                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label class="pull-left">Event website (optional) </label>
                                                <input id="website" value="<?php echo $row["website"]; ?>" type="text" class="form-control " placeholder="Event website">
                                            </div>
                                        </div>

                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label for="sel1" class="pull-left">Country (Event location) </label>
                                                <select id="country"   class="form-control require_select">
                                                    <option value="">---Choose Country---</option>
                                                    <?php
                                                    $residenceSet = get_all_from_tbl('countries_tbl');
                                                    while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                                                    <option value="<?php echo $residence['id']; ?>">
                                                        <?php echo $residence['country_name']; ?></option><?php
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label class="pull-left">City ( Event location ) </label>
                                                <input id="city" type="text" value="<?php echo $row["city"]; ?>" class="form-control require_text" placeholder="City">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 ">
                                            <div class="form-group">
                                                <label class="pull-left">Address ( Event location )</label>
                                                <input id="address" type="text" value="<?php echo $row["address"]; ?>" class="form-control require_text" placeholder="Address">
                                            </div>
                                        </div>



                                               </fieldset>
                                    <fieldset>
                                        <h2 class="fs-title">Organizer Information</h2>
                                        <div class="col-sm-6 ">
                                            <div class="form-group">
                                                <label class="pull-left">Organizer Name</label>
                                                <input id="organizer_name" value="<?php echo $row["organizer_name"]; ?>" type="text" class="form-control require_text" placeholder="organizer Name">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 ">
                                            <div class="form-group">
                                                <label class="pull-left">Mobile</label>
                                                <input id="mobile" type="text" value="<?php echo $row["mobile"]; ?>" class="form-control require_text" placeholder="Mobile">
                                            </div>
                                        </div>



                                                 <button data-loading-text="<i class='fa fa-spinner fa-spin '></i> Loading" type="button" data-id="page_2" class="btn btn-primary action-button pull-right edit_event_btn"> Submit</button>

                                    </fieldset>



                                </div>
                            </div>
                        </div>



                    </div>



            </div>
        </div>
    </div>

</div>






<?php require_once("include/footer.php");?>

<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">

<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href=js/bootstrap3-wysihtml5.min.css">
<!--  -->
<script src="js/bootstrap3-wysihtml5.all.min.js"></script>

<script>

    $("#event_type").val("<?php echo $row["event_type"]; ?>");
    $("#event_field").val("<?php echo $row["event_field"]; ?>");
    $("#country").val("<?php echo $row["country"]; ?>");

    $(function(){
        $(".chosen").chosen();
    });
    $('#emailBody').wysihtml5();
    $('#EmailBody').wysihtml5();
    $('#EmailBodycert').wysihtml5();
    $('#message').wysihtml5();
    $('#RequestMsg').wysihtml5();
    $('#paragraph1').wysihtml5();
    $('#paragraph2').wysihtml5();
    $('#paragraph3').wysihtml5();

    $('#EstbStatusChange').on('change', function() {
        var status = $('#EstbStatusChange :selected').val();
        if (status == 2 ) {
            $("[name = 'subject']").val('Establishment Details InComplete : From ISCN');
            $("[name = 'note']").val('Establishment Details InComplete : Check Your Email for Details.');
            $("#emailBodyWrapper").removeClass('hide');

            missingInfoEmailMsg = "<p>Hi</p><p>Hope this email find you well.</p><p>Kindly we would like to inform you that your establishment is incomplete.</p><p>Your application will not be reviewed by the admissions committee until it is complete.</p><p>Please send required information to the following email: missing@iscnsystem.org.</p><p>............ Required information..............................</p><p><i>Do not forget to add the following application number to the email:</i></p><p>Application number ... </p><p>Thanks in advance.</p><p>Best Regards,</p><p>International Standard Certificate Number (ISCN) team.</p><p>www.iscnsystem.org</p>";
            $('iframe').contents().find('.wysihtml5-editor').html(missingInfoEmailMsg);

        }
        else if (status == 3 ) {
            $("[name = 'subject']").val('Establishment Under Process : From ISCN');
            $("[name = 'note']").val('Under Process : ISCN is Working on Your Establishment Data.');
            $("#emailBodyWrapper").removeClass('hide');

            underProcessEmailMsg = "<p>Hi</p><p>Hope this email find you well.</p><p>Kindly we would like to inform you that your establishment is now under process.</p><p>You will get an approval email along with your ISCN number as your establishment approved.</p><p>Application number ... </p><p>Thanks in advance.</p><p>Best Regards,</p><p>International Standard Certificate Number (ISCN) team.</p><p>www.iscnsystem.org</p>";
            $('iframe').contents().find('.wysihtml5-editor').html(underProcessEmailMsg);
        }
        else if (status == 4) {
            confirm("Check its payment status and all details carefully ! If everything is fine then press OK and then Yes.")
            $("[name = 'subject']").val('Congratulation Your Establishment Approved : From ISCN');
            $("[name = 'note']").val('Approved : Check Your Email for ISCN number : From ISCN');
            $("#emailBodyWrapper").addClass('hide');
        }
    });


</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="js/plugins/dataTables/datatables.min.js"></script>
<script>
    var event_id="<?php echo $event_ID; ?>";
</script>
<script src="js/edit_event.js"></script>
<script>
    $(".documents").addClass("active");
    $(".published").addClass("active");
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

        });

    });
    $(".events").addClass("active");
</script>
</body>

</html>
