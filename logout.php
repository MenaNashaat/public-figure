<?php 

	ob_start();
	session_start();


	// $loggedIn = false;
	// if(isset($_SESSION["user"]))
    // 	$loggedIn = true;

    require_once("../includes/initialize.php");
    
    if(isset($_SESSION["user"])){
        $_SESSION["user"] = null;
        unset($_SESSION["user"]);

        $_SESSION["user_id"] = null;
        unset($_SESSION["user_id"]);
    }

    redirect_to("index.php");

?>