<?php 

    ob_start();
    session_start();

    // $loggedIn = false;
	// if(isset($_SESSION["user"]))
	// 	$loggedIn = true;

	$pageTitle = "search";
	require_once("../includes/initialize.php");


	//get database connection
    $dbConnection = getDatabaseConnection();

    // print_r($_SESSION);
    

    $search = isset($_GET["search_string"]) ? urldecode($_GET["search_string"]) : ""; 
    $gander = isset($_GET["gander"]) ? urldecode($_GET["gander"]) : "";
    $nationality = isset($_GET["nationality"]) ? urldecode($_GET["nationality"]) : "";
    $categoryPathString = isset($_GET["category_path"]) ? urldecode($_GET["category_path"]) : "";
    $pageNumber = isset($_GET["page_number"]) ? urldecode($_GET["page_number"]) : "1";

    $searchQuery = "search_string=" . urlencode($search) . "&gander=" . urlencode($gander) . 
                    "&nationality=" . urlencode($nationality) . "&category_path=" . urlencode($categoryPathString); 

    $categoryPath = explode("-", $categoryPathString);
    array_pop($categoryPath);

    $searchFields = array();
        
    $searchFields[] = $gander;
    $searchFields[] = $nationality;

    $searchFields[] = count($categoryPath) > 0 ? $categoryPath[count($categoryPath) - 1] : -1;
    $searchFields[] = count($categoryPath);
    $searchFields[] = $categoryPathString;

    $usersTotalCount = Search::searchUsersNumber($search, $searchFields);
    $perPage = 2;

    if($pageNumber > ceil($usersTotalCount / $perPage)) $pageNumber = 1;

    $pagination = new Pagination($pageNumber, $perPage, $usersTotalCount);
    $matchedUsers = Search::searchWithParams($search, $searchFields, $pagination);


    if(isset($_SESSION["user"])){
        $userId = $_SESSION["user_id"];
        //get user favorites array
        $userFavorites = array();
        $favQuery = "SELECT favorite_id FROM users_favorites WHERE user_id='{$userId}'";
        $favResult = $dbConnection->performQuery($favQuery);
    
        if($favResult){
    
            while($favorite = mysqli_fetch_assoc($favResult))
                $userFavorites[] = $favorite["favorite_id"];
    
        }  
    }
    

    // print_r($userFavorites);

    // print_r($search);

    // $categoryPathString = "";
    // if(isset($_GET["search_params"])){

    //     $searchParams = json_decode(urldecode($_GET["search_params"]));

    //     $search = $searchParams->search_string;
    //     $gander = $searchParams->gander;
    //     $nationality = $searchParams->nationality;
    //     $categoryPathString = $searchParams->category_path;

        

    //     // print_r($categoryPath);

        
    // }/*else redirect_to("index.php");*/

    include "header.php";
    

?>


    <div id="tg-homebanner" class="tg-homebanner tg-haslayout">
        <figure class="item">
            <div class="cover">
                <figcaption>
                    <div class="container">
                        <div class="row text-center white-color">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="tg-bannercontent">
                                    <h1>We will help you to find all
                                    </h1>
                                    <h2>Search from 12,45,754 Awesome Verified PHD, Doctor ....!</h2>

                                </div>
                            </div>
                        </div>
                    </div>
                </figcaption>
            </div>
        </figure>
    </div>

    <main id="tg-main" class="tg-main tg-haslayout">

    <!--************************************
						Categories Search Start
				*************************************-->
		<section class="tg-haslayout" style="margin-bottom: 3em">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
						<div class="tg-categoriessearch">
							<div class="tg-title">
								<h2><span>Boost your search with</span> Trending Categories</h2>
							</div>
							<div class="row">
                                <form class="domain-checker">
                                            
                                    <div class="col-md-4">
    
                                        <select class="cat form-control" id="category1" data-level="1" style="height: 50px; display: inline-block">
                                            <option value="">---Category---</option><?php

                                                $level = 1;
                                                $catId = count($categoryPath) > 0 ? $categoryPath[0] : -1;
                                                $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id IS NULL";
                                                $categories = $dbConnection->performQuery($sqlQuery);
                                                while ($category = mysqli_fetch_assoc($categories)) { 
                                                // if($category["level"] == 1):
                                                ?>

                                                <?php 
                                                if($category['id'] == $catId){ ?>
                                                    <option value="<?php echo $category['id']; ?>" selected> <?php echo $category['name']; ?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $category['id']; ?>"> <?php echo $category['name']; ?></option>
                                                <?php } ?>

                                            <?php } ?>
                                        </select>
                                        
                                            <!-- <button id="search" class="btn btn-primary" type="submit" style="height: 50px;">
                                                Search
                                            </button> -->
                                    </div>

                                    <div class="col-md-8">
                                        
                                        
                                            <!-- <select class="select2_demo_3 form-control">
                                                <option value="1"> </*?php echo $search;*/ ?> </option>
                                                    
                                            </select> -->

                                            <div class="group" style="  position: relative;display: table;border-collapse: separate;">
												<select class="select2_demo_3 form-control" style="">
                                                    <option value="1"> <?php echo $search; ?> </option>
														
													</select>

											<div class="input-group-btn">
												<button id="search" class="btn btn-primary" type="submit" style="height: 50px;">
													Search
												</button>
											</div>
										</div>


                                    </div>
                             

                                
                                </form>
                            </div>


                            <div class="row">
                                <form id="search_form" action="<?php echo htmlspecialchars(basename($_SERVER["PHP_SELF"])); ?>" method="GET">
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="sel1">Gender:</label>

                                            <select class="form-control" id="sel2" name="gander">

                                                <option value="" selected>Gender</option>
                                                <?php if($gander == "Male"){ ?>
                                                    <option value="Male" selected>Male</option>
                                                    <option value="Female">Female</option>
                                                <?php }else if($gander == "Female"){ ?>
                                                    <option value="Male">Male</option>
                                                    <option value="Female" selected>Female</option>
                                                <?php }else{ ?>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                <?php } ?>


                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="sel3">nationality:</label>

                                            <select class="form-control" id="sel3" name="nationality">
                                                <option value="" selected> nationality </option>
                                                        <?php
                                                            $countryId = $nationality;
                                                            $sqlQuery="SELECT * FROM countries";
                                                            $countries = $dbConnection->performQuery($sqlQuery);
                                                            while ($country = mysqli_fetch_assoc($countries)) { ?>
                                                            <?php
                                                                // print_r($country);
                                                                if($country['id'] == $countryId){ ?>
                                                                    <option value="<?php echo $country['id']; ?>" selected> <?php echo $country['country_name']; ?></option>
                                                                <?php }else{ ?>
                                                                    <option value="<?php echo $country['id']; ?>"> <?php echo $country['country_name']; ?></option>
                                                                <?php } ?>
                                                       <?php } ?>

                                            </select>

                                        </div>
                                    </div>
                                    
                                    <!-- <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="sel1"></label>

                                            <button id="search" type="submit" class="btn btn-primary btn-block btn-sm m-t-em-4">Search</button>

                                        </div>
                                    </div> -->

                                    <div class="loader pull-right" style="margin-top: 4em; margin-right: 3em;"></div>

                                        <input type="hidden" id="search_string" name="search_string" value="" />
                                        <input type="hidden" id="category_path" name="category_path" value="" />
                                        <div class="clearfix"></div>

                                    </form>
                                    <div class="row" id="categories" style="padding: 0 14px">

                                        <?php for($i = 1; $i < count($categoryPath); $i++){
                                            $parentCategory = $categoryPath[$i - 1]; 
                                            ?>
                                                <div class="col-md-3" style="margin-bottom: 1em">
                                                <select id="<?php echo "category" . ($i + 1);  ?>" class="form-control category" data-level="<?php echo ($i + 1);  ?>">
                                                <option value=""> ---Sub Category--- </option>
                                            <?php
                                                $level = $i + 1;
                                                $catId = $categoryPath[$i];
                                                $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id='{$parentCategory}'";
                                                $categories = $dbConnection->performQuery($sqlQuery);
                                                while ($category = mysqli_fetch_assoc($categories)) {   
                                                    
                                            ?>
                                            <?php 
                                                
                                                    if($category['id'] == $catId){ ?>
                                                        <option value="<?php echo $category['id']; ?>" selected> <?php echo $category['name']; ?></option>
                                                    <?php }else{ ?>
                                                        <option value="<?php echo $category['id']; ?>"> <?php echo $category['name']; ?></option>
                                                    <?php } ?>
                                                

                                                <?php } ?>
                                                </select>
                                                </div>
                                                    
                                        <?php } ?>

                                        <?php 
                                        if(count($categoryPath) > 0){
                                            //if leave has childs
                                            $level = count($categoryPath) + 1;
                                            $parentCategory = $categoryPath[$level - 2];
                                            $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id='{$parentCategory}'";
                                            $categories = $dbConnection->performQuery($sqlQuery);
                                            if($dbConnection->numRows($categories) > 0){ ?>

                                                <div class="col-md-3" style="margin-bottom: 1em">
                                                <select id="<?php echo "category" . $level;  ?>" class="form-control category" data-level="<?php echo $level;  ?>">
                                                <option value=""> ---Sub Category--- </option>
                                            <?php
                                                while ($category = mysqli_fetch_assoc($categories)) {
                                                    ?>

                                                        <option value="<?php echo $category['id']; ?>"> <?php echo $category['name']; ?></option>

                                                    <?php

                                                } ?>
                                                </select>
                                            </div>
                                            <?php
                                            
                                            }

                                        }
                                            
                                        ?>
                                    
                                    </div>

                                

                            </div>

						</div>
					</div>
                </div>
                
                
			</div>
		</section>

        <!--************************************
					Categories Search Start
			*************************************-->
        <!-- <section class="tg-haslayout" style="margin-top:10pc">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-push-1 col-lg-10">
                        <div class="tg-categoriessearch">
                            <div class="tg-title">
                                <h2>filters</h2>
                            </div>
                            
      

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="loader pull-right" style="margin-right: 10%"></div>
                </div>
            </div>
        </section> -->

    </main>

    <section class="results">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 search-results">

                <?php foreach($matchedUsers as $user):?>
                    <div class="result-item">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="item-media">
                                    <div class="image-cover"><img src="<?php echo "uploads/" . $user["photo_path"] ?>" class="img-responsive center-block"
                                            alt=""></div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="item-body">
                                    <div class="item-title">
                                        <h2>
                                            <a href="<?php echo 'profile.php?username=' . $user["username"]; ?>"> <?php echo $user["first_name"] . " " . $user["last_name"]; ?> </a>
                                            <?php if(isset($_SESSION["user"])): ?>

                                                <?php if(in_array($user["id"], $userFavorites)): ?>
                                                    <i class="fas fa-heart  favorite pull-right" data-fav="1" data-user="<?php echo $_SESSION["user_id"]; ?>" data-favorite="<?php echo $user["id"]; ?>" style="color: #F00"></i>
                                                <?php else: ?>
                                                    <i class="fas fa-heart  favorite pull-right" data-fav="0" data-user="<?php echo $_SESSION["user_id"]; ?>" data-favorite="<?php echo $user["id"]; ?>"></i>
                                                <?php endif; ?>

                                            <?php endif; ?>
                                        </h2>
                                    </div>
                                    
                                    <div class="item-address"><i class="fas fa-user"></i> <?php echo $user["username"]; ?></div>
                                    <div class="item-address"><i class="fas fa-map-marker-alt"></i>
                                        <?php 
                                            $countryId = $user["residence"];
                                            $sqlQuery="SELECT * FROM countries WHERE id='{$countryId}'";
                                            $country = $dbConnection->performQuery($sqlQuery);

                                            echo mysqli_fetch_assoc($country)["country_name"];
                                        ?>
                                    </div>
                                    <div class="item-address"><i class="fas fa-calendar-alt"></i>
                                        <!-- November 15, 1991 (age 27) -->
                                        <?php 
                                            $userDate = strtotime($user["birth_date"]);
                                            $currenDate = strtotime(date('Y-m-d'));
                                            $formatteDate = strftime("%B %e, %Y", $userDate);
                                    

                                            echo $formatteDate . " (age " . floor((($currenDate - $userDate) / (60 * 60 * 24 * 30 * 12))) . ")"; 
                                        ?>
                                    </div>
                                    <div class="item-address"><i class="fas fa-copy"></i>
                                        <?php echo htmlentities($user["description"]); ?>
                                    </div>
                                    

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="item-more text-center ">
                                    <p>Occupation
                                    </p>
                                    <strong class="amount">
                                        <?php 
                                            $categoryPath = explode("-", $user["category_path"]);
                                            array_pop($categoryPath);
                                            
                                            $parentId = $categoryPath[0];
                                            $childId = $categoryPath[count($categoryPath) - 1];

                                            $output = "";
                                            
                                            $sqlQuery="SELECT name FROM categories_chain WHERE id='{$parentId}'";
                                            $category = $dbConnection->performQuery($sqlQuery);
                                            $output .= mysqli_fetch_assoc($category)["name"];

                                            if(count($categoryPath) > 1){
                                                $sqlQuery="SELECT name FROM categories_chain WHERE id='{$childId}'";
                                                $category = $dbConnection->performQuery($sqlQuery);
                                                $output .= " - " . mysqli_fetch_assoc($category)["name"];
                                            }

                                            echo $output;
                                            
                                        ?>
                                    </strong>

                                    <a href="<?php echo 'profile.php?username=' . $user["username"]; ?>" class="btn btn-danger m-t-15 btn-block">Details</a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
        
    </section>


    <section class="padding-main">
        <div class="container">
            <div class="row">
                <!-- <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-responsive img-circle center-block" src="https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_01.png"
                                                alt="card image"></p>
                                        <h4 class="card-title m-t-em-2">Martina Nashaat Roshdy</h4>
                                        <p class="card-text">Egypt</p>
                                        <a href="#" class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>
                                        <a href="#" class="btn btn-danger btn-sm"><i class="fas fa-heart"></i></a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div> -->

                <form id="pagination" method="GET" action="<?php echo htmlspecialchars(basename($_SERVER["PHP_SELF"])); ?>">
                    <!-- <input type="hidden" name="search_string" value="<?php echo $search; ?>" />
                    <input type="hidden" name="gander" value="<?php echo $gander; ?>" />
                    <input type="hidden" name="nationality" value="<?php echo $nationality; ?>" />
                    <input type="hidden" name="category_path" value="<?php echo $categoryPathString; ?>" /> -->
                    <!-- <input type="hidden" name="" value="" /> -->
                
                <div class="col-md-12">
                        <nav class="tg-pagination">
                                <ul>
                                    <?php if($pageNumber != 1): ?>
                                        <li class="tg-prevpage"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . $pagination->previous(); ?>"><i class="fa fa-angle-left"></i></a></li>
                                    <?php endif; ?>
                                    <li class="<?php if($pageNumber == 1) echo "tg-active";  ?>"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . "1"; ?>"><?php echo "1"; ?></a></li>

                                    <?php if($pagination->pagesNumber() >= 4){ ?>

                                        <?php if($pageNumber > 2): ?>
                                            <span> ... </span>
                                        <?php endif; ?>

                                        
                                        <?php if($pageNumber <= 2): ?>
                                            <li class="<?php if($pageNumber == 2) echo "tg-active";  ?>"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . "2"; ?>"><?php echo "2"; ?></a></li>
                                            <li class=""><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . "3"; ?>"><?php echo "3"; ?></a></li>
                                        <?php elseif($pageNumber > $pagination->pagesNumber() - 2):  ?>
                                            <li class=""><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . ($pagination->pagesNumber() - 2); ?>"><?php echo ($pagination->pagesNumber() - 2); ?></a></li>
                                            <li class="<?php if($pageNumber == $pagination->pagesNumber() - 1) echo "tg-active";  ?>"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . ($pagination->pagesNumber() - 1); ?>"><?php echo ($pagination->pagesNumber() - 1); ?></a></li>
                                        <?php else: ?>
                                            <li class=""><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . ($pageNumber - 1); ?>"><?php echo ($pageNumber - 1); ?></a></li>
                                            <li class="tg-active"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . $pageNumber; ?>"><?php echo $pageNumber; ?></a></li>
                                            <li class=""><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . ($pageNumber + 1); ?>"><?php echo ($pageNumber + 1); ?></a></li>
                                        <?php endif; ?>

                                        <?php if($pageNumber <= $pagination->pagesNumber() - 2): ?>
                                            <span> ... </span>
                                        <?php endif; ?>

                                    <?php }else if($pagination->pagesNumber() == 3){ ?>
                                        <li class="<?php if($pageNumber == 2) echo "tg-active";  ?>"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . "2"; ?>"><?php echo "2"; ?></a></li>
                                    <?php } ?>

                                    <?php if($pagination->pagesNumber() >= 2): ?>
                                        <li class="<?php if($pageNumber == $pagination->pagesNumber()) echo "tg-active";  ?>"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . $pagination->pagesNumber(); ?>"><?php echo $pagination->pagesNumber(); ?></a></li>
                                    <?php endif; ?>
                                    <?php if($pageNumber != $pagination->pagesNumber()): ?>
                                        <li class="tg-nextpage"><a href="<?php echo "results.php?" . $searchQuery . "&page_number=" . $pagination->next(); ?>"><i class="fa fa-angle-right"></i></a></li>
                                    <?php endif; ?>
                                </ul>
                            </nav>

                </div>

            </form>


            
                


            </div>

        </div>
    </section>


    <?php include "footer.php"; ?>

	<!-- Main js -->
	<script src="js/main.js"></script>
	<script>
		$(".select2_demo_1").select2();
		$(".select2_demo_2").select2();
		// $(".select2_demo_3").select2({
		// 	placeholder: "Select a state",
		// 	allowClear: true,
			
		// });

		var i = 1;
		$(".select2_demo_3").select2({
			placeholder: "Search Username/Id",
			allowClear: true,
			width: 'resolve',
			closeOnSelect: false,
			height: 20,
			ajax: {
				url: 'api/search_api.php',
					data: function (params) {
						var query = {};
						if(params.term != "")
							query = {
								search: params.term
							};

						// Query parameters will be ?search=[term]&type=public
						return query;
				},

				processResults: function (data) {
					// Tranforms the top-level key of the response object from 'items' to 'results'
					// console.log(data);
					var data = JSON.parse(data);  //array of result set
					var searchString = data["search_string"];
					var searchResult = [];
					
					if(data["error"] == 0 && data["search"] != null && data["search"].length > 0){
						
						for(i = 1; i < data["search"].length; i++){
							var item = {};
							item.id = i;
							item.text = data["search"][i]["username"];

							searchResult.push(item);
						}

						// console.log("hello!");
						
						
						if(searchResult.length > 0){
							var item = {};
							item.id = -1;
							item.text = "search: " + searchString;

							searchResult.push(item);
							return {
								results: searchResult
							};
						}else {
							var item = {};
							item.id = -1;
							item.text = "search: " + searchString;

							searchResult.push(item);
							return {results: searchResult};

						}
					}else{
						
						if(searchString != ""){
							var item = {};
							item.id = -1;
							item.text = "search: " + searchString;

							searchResult.push(item);
							return {results: searchResult};
						}else return {results: []};
					} 
				}
				
			}
		});



        var categoryPath = [];

		$(document).ready(function(){
            categoryPath = [];
            categoryPath = ('<?php echo $categoryPathString; ?>').split("-");
            categoryPath.pop();


            // console.log(categoryPath);

			// $("#search_results").append("<option value=" + "hello" + ">" + "hello" + "</option>");

			
			// $(document).on("keyup", ".select2-search__field", function(){
			// 	// console.log($(".select2-search__field").val());
			// 	var searchString =  $(".select2-search__field").val();
				
			// 	$("#search_results").empty();
			// 	if(searchString == "") return;



				
			// 	// $.ajax({
			// 	// 	url: 'api/search_api.php',
			// 	// 	data: data,
			// 	// 	dataType: "text",
			// 	// 	cache: false,
			// 	// 	contentType: false,
			// 	// 	processData: false,
			// 	// 	type: 'POST',
			// 	// 	success: function(data){

			// 	// 		// console.log(data);
			// 	// 		var data = JSON.parse(data);  //array of result set
			// 	// 		resultSearch = data["searchResult"];
						
			// 	// 		if(data["error"] == 0 && data["searchResult"] != null){

			// 	// 			var searchResult = [];
			// 	// 			for(var i = 0; i < data["searchResult"].length; i++){
			// 	// 				var item = {};
			// 	// 				item.id = i;
			// 	// 				item.text = data["searchResult"][i]["username"];

			// 	// 				searchResult.push(item);
			// 	// 			}
							
			// 	// 			// for(var i = 0; i < data["searchResult"].length; i++)
			// 	// 			// 	// console.log("<option value=\"" + data["searchResult"][i]["username"] + "\">" + data["searchResult"][i]["username"] + "</option>");
			// 	// 			//     $("#search_results").append("<option>" + data["searchResult"][i]["username"] + "</option>");

			// 	// 			$(".select2_demo_3").select2({
			// 	// 				data: searchResult
			// 	// 			});
								
							
			// 	// 		}

			// 	// 	}




			// 	// });
			// });

			// $(document).on("keyup", ".select2-search__field", function(){
			// 	// console.log("keypress");
			// });

			// console.log($(".select2_demo_3").find(':selected'));

			// $(".select2_demo_3").change(function(){
			// 	var selectedOption = $(this).children("option:selected").data("data");

			// 	console.log(selectedOption);
				
			// 	// if(selectedOption.id != i)
			// 	// 	location.href = "profile.php?username=" + selectedOption.text;
				
            // });
            // console.log($(".select2_demo_3").children("option:selected").data("data").text);
            $("#search_form").on("submit", function(){

                //prepare submit values search_string, category_path
                //search string
                var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
                var searchString = "";
                if(selectedOption != null)
                    searchString = selectedOption.id != -1 ? selectedOption.text.trim() : selectedOption.text.trim().substring(8);
                
                var categoryPathString = "";
                for(var i = 0; i < categoryPath.length; i++)
                    categoryPathString += categoryPath[i] + "-";

                $("#search_string").val(encodeURI(searchString));
                $("#category_path").val(encodeURI(categoryPathString));

                

                return true;
            });

            $("#pagination").on("submit", function(e){
                e.preventDefault();

                alert("hello");
            });

            
			$("#search").on("click", function(e){
                e.preventDefault();

                $("#search_form").submit();
                return;  //instead of function code comment
                
                // console.log(categoryPath);

				//collect searching params

                //search string
                var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
                var searchString = selectedOption.text.substring(8);
                
				
				//fixed filters
                // var subCategory = "";
                var gander = "";
                var nationality = "";

                // subCategory = $("#sel1").children("option:selected").val();
                gander = $("#sel2").children("option:selected").val();
                nationality = $("#sel3").children("option:selected").val();

                // console.log(nationality);
                
                

                //category and dynamic filters
                // var categoryId = $(".cat").children("option:selected").val();
                // var dynamicFields = [];
                // console.log(fields);
                // for (var n = 0; n < fields.length; n++){
                    
                //     var dynamicField = {id: "", value: ""};
                    
                //     dynamicField["id"] = fields[n]["id"];
                //     dynamicField["value"] = $("#search" + fields[n]["id"]).val();

                //     dynamicFields.push(dynamicField);

                    
                // }


                var categoryPathString = "";
                for(var i = 0; i < categoryPath.length; i++)
                    categoryPathString += categoryPath[i] + "-";

                // console.log(categoryPathString);

                var data = new FormData();
                data.append('search', 'search');
                data.append('search_string', searchString);

                // data.append('sub_category', subCategory);
                data.append('gander', gander);
                data.append('nationality', nationality);

                // data.append('category_id', categoryId);
                // data.append('dynamic_fields', JSON.stringify(dynamicFields));

                data.append('category_level', categoryPath.length);
                data.append('category_id', categoryPath[categoryPath.length - 1]);
                data.append('category_path', categoryPathString);

                // console.log(categoryPath);


                $.ajax({
                    url: 'api/search_api.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){

                        // console.log(returnData);
                        // return;

                        var returnData = JSON.parse(returnData);
                        var users = returnData["matched_users"];
                        // console.log(users);
                        $(".search-results").empty();
                        if(returnData["error"] == 0){
                            for(var n = 0; n < users.length; n++){
                                // console.log(users[n]);
                                
                                // $(".search-results").append(
                                //     "<div class=\"col-xs-12 col-sm-6 col-md-4\">" + 
                                //         "<div class=\"image-flip\" ontouchstart=\"this.classList.toggle('hover');\">" + 
                                //             "<div class=\"mainflip\">" + 
                                //                 "<div class=\"frontside\">" + 
                                //                     "<div class=\"card\">" + 
                                //                         "<div class=\"card-body text-center\">" +
                                //                             "<p><img class=\" img-responsive img-circle center-block\" src=\"https://sunlimetech.com/portfolio/boot4menu/assets/imgs/team/img_01.png\"" + 
                                //                                     "alt=\"card image\"></p>" + 
                                //                             "<h4 class=\"card-title m-t-em-2\">" + users[n]["username"] + "</h4>" +
                                //                             "<p class=\"card-text\">Egypt</p>" +  
                                //                             "<a href=\"profile.php?username=" + users[n]["username"] + "\" class=\"btn btn-success btn-sm\"><i class=\"fas fa-eye\"></i></a>" + 
                                //                             "<a href=\"profile.php?username=" + users[n]["username"] + "\" class=\"btn btn-danger btn-sm\"><i class=\"fas fa-heart\"></i></a>" + 
                                //                         "</div>" + 
                                //                     "</div>" + 
                                //                 "</div>" + 
                                //             "</div>" + 
                                //         "</div>" + 
                                //     "</div>"
                                // );

                                $(".search-results").append(
                                    "<div class=\"result-item\">" + 
                                        "<div class=\"row\">" + 
                                            "<div class=\"col-md-4\">" + 
                                                "<div class=\"item-media\">" + 
                                                    "<div class=\"image-cover\"><img src=\"img/download.jpg\" class=\"img-responsive center-block\" alt=\"\"></div>" + 
                                                "</div>" + 
                                            "</div>" + 
                                            "<div class=\"col-md-5\">" + 
                                                "<div class=\"item-body\">" + 
                                                    "<div class=\"item-title\">" + 
                                                        "<h2><a href=\"profile.php?username=" + users[n]["username"] + "\">" + users[n]["username"] + "</a></h2>" + 
                                                    "</div>" + 
                                                    "<div class=\"item-address\"><i class=\"fas fa-map-marker-alt\"></i>" + 
                                                        "San Bernardino, California, U.S." + 
                                                    "</div>" + 
                                                    "<div class=\"item-address\"><i class=\"fas fa-calendar-alt\"></i>" + 
                                                        "November 15, 1991 (age 27)" + 
                                                    "</div>" + 
                                                    "<div class=\"item-address\"><i class=\"fas fa-copy\"></i>" + 
                                                        "is an American actress and activist. Brought up in Simi Valley," + 
                                                        "California, Woodley began modeling at the age of 4 and began acting" + 
                                                        "professionally in minor television" + 
                                                    "</div>" +


                                                "</div>" + 
                                            "</div>" + 
                                            "<div class=\"col-md-3\">" + 
                                                "<div class=\"item-more text-center\">" + 
                                                    "<p>Occupation</p>" + 
                                                    "<strong class=\"amount\">" + "category" + "</strong>" +

                                                    "<a href=\"profile.php?username=" + users[n]["username"] + "\" class=\"btn btn-danger m-t-15 btn-block\">Details</a>" + 
                                                "</div>" + 
                                            "</div>" + 
                                        "</div>" + 
                                    "</div>"
                                );
                            }
                        }

                    }




                });



				
			});

            /////////////////////////////////////////////////////////////////////////////////////////
            // if('<?php /*echo $search;*/ ?>' != ""){
            //     var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
            //     selectedOption["text"] = "search: " + '<?php /*echo $search;*/ ?>';
            //     $(".select2_demo_3").children("option:selected").data("data", selectedOption);
            //     // $(".select2-selection__rendered").html("<span class=\"select2-selection__clear\">×</span>");
            //     // $(".select2-selection__rendered").html(selectedOption["text"]);
            //     $("#search").click();

            //     var selectedOption = $(".select2_demo_3").children("option:selected").data("data");
            //     selectedOption["text"] = "";
            //     $(".select2_demo_3").children("option:selected").data("data", selectedOption);

            // }
            //////////////////////////////////////////////////////////////////////////////////////////

            

            $(".cat").change(function(){
                $(".loader").css("display", "inline-block");

                // console.log(categoryDone);
                var selection = $(this);
                var categoryId = parseInt(selection.children("option:selected").val());
                // console.log(selection.data("level"));
                
                if(categoryId != -1){

                    //get next level categories
                    // oldLevel = categoryLevel;
                    var categoryLevel = selection.data("level");
                    var nextLevel = categoryLevel + 1;

                    while(categoryPath.length >= categoryLevel)
                        categoryPath.pop();

                    categoryPath.push(categoryId);

                    // if(categoryLevel > oldLevel) /*categoryPath += categoryId + "-"*/ categoryPath.push(categoryId);
                    // else {
                    //     while(categoryPath.length >= categoryLevel)
                    //     categoryPath.pop();

                    //     categoryPath.push(categoryId);
                    // }
                    // else if(categoryLevel == oldLevel) categoryPath = categoryPath.slice(0, categoryPath.lastIndexOf("-", categoryPath.length - 2) + 1) + categoryId + "-"


                    $.get( "api/next_level_categories.php?category_id=" + categoryId + "&category_level=" + categoryLevel, function( data ) {
                        data = JSON.parse(data);
                        if(data["error"] == 0){
                            var categories = data["categories"];

                            // console.log(categories);

                            // selection.nextAll().remove();
                            $("#categories").empty();

                            if(categories.length > 0){
                                var container = $('<div class="col-md-3" style="margin-bottom: 1em"></div>');
                                var nextSelection = $('<select id="category' + nextLevel + '"  class="form-control category" name="category' + nextLevel + '" data-level="' + nextLevel + '"></select>');
                            
                                nextSelection.append($('<option value="' + -1 + '"> ' + '---Sub Category---' + ' </option>'));
                                //add options
                                for(var i = 0; i < categories.length; i++)
                                    nextSelection.append($('<option value="' + categories[i]["id"] + '"> ' + categories[i]["name"] + ' </option>'));

                                container.append(nextSelection);
                                $("#categories").append(container);

                            }
                        }

                        $(".loader").css("display", "none");

                    });
                

                }else {
                    
                    // selection.nextAll().remove();
                    // selection.css("border", "1px solid red");
                    // categoryDone = false;
                    // oldLevel = categoryLevel;
                    // categoryLevel = selection.data("level");
                    // while(categoryPath.length >= categoryLevel)
                    //     categoryPath.pop();

                    categoryPath = [];
                    $("#categories").empty();
                }

                // console.log(categoryPath);
                
			// 	var categoryId = $(this).children("option:selected").val();
            //     if(categoryId == "reset"){
            //         $(".search-fields").empty();
            //         return;
            //     }

			// 	//get category searchable fileds
            //     $.get( "api/category_searchable_fields.php?category_id=" + categoryId, function( data ) {
            //         // console.log(data);
            //         data = JSON.parse(data);

            //         //add search filds
            //         fields = data["fields"];
            //         // console.log(fields);
            //         for(var i = 0; i < fields.length; i++){
            //             $(".search-fields").empty();
            //             $(".search-fields").append("<div class=\"col-sm-4\">" + 
            //                 "<div class=\"form-group\">" +
            //                     "<label class=\"pull-left\">" + fields[i]["name"] + " </label>" +
            //                     "<input id=search" + fields[i]["id"] + " type=" + fields[i]["type"] + " class=\"form-control\" placeholder=" + fields[i]["name"] + ">"+
            //                 "</div>" + 
            //             "</div>");
            //         }
            //     });

            // console.log(categoryPath);
				
            });
            
            $("#categories").on("change", ".category", function(e){
            
                $(".loader").css("display", "inline-block");

                // console.log(categoryDone);
                var selection = $(e.target);
                var categoryId = parseInt(selection.children("option:selected").val());
                // console.log(selection.data("level"));
                
                if(categoryId != -1){

                    //get next level categories
                    // oldLevel = categoryLevel;
                    var categoryLevel = selection.data("level");
                    var nextLevel = categoryLevel + 1;

                    while(categoryPath.length >= categoryLevel)
                        categoryPath.pop();

                    categoryPath.push(categoryId);

                    // if(categoryLevel > oldLevel) /*categoryPath += categoryId + "-"*/ categoryPath.push(categoryId);
                    // else {
                    //     while(categoryPath.length >= categoryLevel)
                    //         categoryPath.pop();

                    //     categoryPath.push(categoryId);
                    // }
                    // else if(categoryLevel == oldLevel) categoryPath = categoryPath.slice(0, categoryPath.lastIndexOf("-", categoryPath.length - 2) + 1) + categoryId + "-"


                    $.get( "api/next_level_categories.php?category_id=" + categoryId + "&category_level=" + categoryLevel, function( data ) {
                        data = JSON.parse(data);
                        if(data["error"] == 0){
                            var categories = data["categories"];

                            // console.log(categories);

                            selection.parent().nextAll().remove();

                            if(categories.length > 0){
                                var container = $('<div class="col-md-3" style="margin-bottom: 1em"></div>');
                                var nextSelection = $('<select id="category' + nextLevel + '"  class="form-control category" name="category' + nextLevel + '" data-level="' + nextLevel + '"></select>');
                            
                                nextSelection.append($('<option value="' + -1 + '"> ' + '---Sub Category---' + ' </option>'));
                                //add options
                                for(var i = 0; i < categories.length; i++)
                                    nextSelection.append($('<option value="' + categories[i]["id"] + '"> ' + categories[i]["name"] + ' </option>'));

                                container.append(nextSelection);
                                selection.parent().parent().append(container);

                                
                            }
                        }

                        $(".loader").css("display", "none");

                    });
                    

                }else {
                    selection.parent().nextAll().remove();
                    var categoryLevel = selection.data("level");
                    while(categoryPath.length >= categoryLevel)
                        categoryPath.pop();
                }

            // console.log(oldLevel + "==" + categoryLevel);
            // console.log(categoryPath);
          });

          $(document).on('change','.upload',function (){

            var img_id=$(this).data('id');

            $("#img"+img_id).css({top: 0, left: 0});
            preview(this,img_id);

          });


          $("#modal_submit").on("click", function(e){
				e.preventDefault();

				//ajax check login
				var username = $("#login_username");
				var password = $("#login_password");

				if(username.val() == ""){
					username.css("border", "1px solid red");
					return;
				}else username.css("border", "1px solid #ccc");

				if(password.val() == ""){
					password.css("border", "1px solid red");
					return;
				}else password.css("border", "1px solid #ccc");


				var data = new FormData();
                data.append('login', 'login');
                data.append('username', username.val());
                data.append('password', password.val());


                $.ajax({
                    url: 'api/login.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
						// console.log(returnData);
						returnData = JSON.parse(returnData);

						if(returnData["error"] == 0){
							location.href="http://localhost:8080/public_figure/mena_mahmoud/profile.php";
						}else{
							$("#login_hint").text("username/password not correct!");
						}
					}

                    

				});


			});

        // });

        // $(".nav").children("li").removeClass("active");
        // $('*[data-name="search"]').addClass("active");
        
        $(".favorite").on("click", function(){

            if($(this).data("fav") == "0"){
                $(this).data("fav", "1");
                $(this).css("color", "#F00");
            }else if($(this).data("fav") == "1"){
                $(this).data("fav", "0");
                $(this).css("color", "#444444");
            }

            //sync database
            var data = new FormData();
            data.append('favorite_sync', 'favorite_sync');
            data.append('user_id', $(this).data("user"));
            data.append('favorite_id', $(this).data("favorite"));
            data.append('favorite', $(this).data("fav"));


            $.ajax({
                url: 'api/favorite_sync.php',
                data: data,
                dataType: "text",
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(returnData){
                    // console.log(returnData);
                    returnData = JSON.parse(returnData);

                    if(returnData["error"] == 0){
                        
                    }else{
                        
                    }
                }

                

            });

            // console.log($(this).data("fav"));
        });

			
    });

	</script>



</body>

</html>