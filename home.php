<?php 

    ob_start();
    session_start();
    
    // $loggedIn = false;
	// if(isset($_SESSION["user"]))
    // 	$loggedIn = true;
    
    // print_r($_SESSION);

	$pageTitle = "Home";
	require_once("../includes/initialize.php");


	//get database connection
    $dbConnection = getDatabaseConnection();

    $username = "";
    
    if(!isset($_SESSION["user"]) && !isset($_GET["username"])){
        redirect_to("index.php");
    }

    $username = isset($_GET["username"]) ? $dbConnection->prepareQueryValue($_GET["username"]) : $dbConnection->prepareQueryValue($_SESSION["user"]);
    $sqlQuery="SELECT * FROM public_figure_user WHERE username='{$username}' LIMIT 1";
    $user = $dbConnection->performQuery($sqlQuery);
    if($dbConnection->numRows($user) < 1){
        redirect_to("index.php");
    }

    $user = mysqli_fetch_assoc($user);

    // print_r($user);

	include "header.php";

	

?>


    <div class="page-header-padding page-header-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h1 class="page-title white-color"> <?php echo $user["first_name"] . " " . $user["last_name"]; ?>

                    </h1>
                </div>
            </div>
        </div>
    </div>

    <section id="details" class="m-t-em-3 m-b-em-3">
        <div class="container">


            <div class="row">

                <!--Grid column-->
                <div class="col-lg-4 col-md-12">

                    <!--Section: Basic Info-->
                    <section class="card card-cascade card-avatar mb-4">

                        <img alt="" src="<?php echo "uploads/" . $user["photo_path"] ?>" class="avatar avatar-wordpress-social-login avatar-160 photo"
                            height="160" width="160">
                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <h4 class="card-title m-t-em-2">
                                <strong><?php echo $user["first_name"] . " " . $user["middle_name"] . " " . $user["last_name"]; ?></strong>
                            </h4>

                            <p>
                                <!-- ------------------------------->
                                <?php 
                                    $categoryPath = explode("-", $user["category_path"]);
                                    array_pop($categoryPath);
                                    
                                    $parentId = $categoryPath[0];
                                    $childId = $categoryPath[count($categoryPath) - 1];

                                    $output = "";
                                    
                                    $sqlQuery="SELECT name FROM categories_chain WHERE id='{$parentId}'";
                                    $category = $dbConnection->performQuery($sqlQuery);
                                    $output .= mysqli_fetch_assoc($category)["name"];

                                    if(count($categoryPath) > 1){
                                        $sqlQuery="SELECT name FROM categories_chain WHERE id='{$childId}'";
                                        $category = $dbConnection->performQuery($sqlQuery);
                                        $output .= " - " . mysqli_fetch_assoc($category)["name"];
                                    }

                                    echo $output;
                                    
                                ?>
                                <!-- ------------------------------->
                            </p>
                            <p>
                                <?php 
                                    $userDate = strtotime($user["birth_date"]);
                                    $currenDate = strtotime(date('Y-m-d'));
                                    $formatteDate = strftime("%B %e, %Y", $userDate);
                            

                                    echo $formatteDate . " (age " . floor((($currenDate - $userDate) / (60 * 60 * 24 * 30 * 12))) . ")"; 
                                ?>
                            </p>
                            <p>
                                <?php 
                                    $countryId = $user["residence"];
                                    $sqlQuery="SELECT * FROM countries WHERE id='{$countryId}'";
                                    $country = $dbConnection->performQuery($sqlQuery);

                                    echo mysqli_fetch_assoc($country)["country_name"];
                                ?>
                            </p>

                            <?php if(isset($user["facebook"]) && $user["facebook"] != ""): ?>
                                <a href="<?php echo $user["facebook"]; ?>" target="_blank" type="button" class="btn-floating btn-small waves-effect waves-light"><i class="fab fa-facebook-f grey-text"></i></a>
                            <?php endif; ?>
                            <?php if(isset($user["twitter"]) && $user["twitter"] != ""): ?>
                            <a href="<?php echo $user["twitter"]; ?>" type="button" target="_blank" class="btn-floating btn-small waves-effect waves-light"><i class="fab fa-twitter grey-text"></i></a>
                            <?php endif; ?>
                            <?php if(isset($user["linkedin"]) && $user["linkedin"] != ""): ?>
                            <a href="<?php echo $user["linkedin"]; ?>" type="button" target="_blank" class="btn-floating btn-small waves-effect waves-light"><i class="fab fa-linkedin-in grey-text"></i></a>
                            <?php endif; ?>
                        </div>

                    </section>
                    <!--Section: Basic Info-->


                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-8 col-md-12  description">

                    <?php if( isset($_SESSION["user"]) && $user["username"] == $_SESSION["user"]/*(isset($_SESSION["user"]) && !isset($_GET["username"])) || 
                                (isset($_SESSION["user"]) && isset($_GET["username"]) && $_SESSION["user"] == $_GET["username"])*/): ?>
                        <!-- Heading -->
                        <div class="card">

                            <!-- <h2><?php /*echo $user["first_name"] . " " . $user["last_name"];*/ ?></h2>
                            <hr> -->

                            <?php 
                                
                                /*$userId = $user["id"];
                                $catId = $categoryPath[0];
                                $fieldId = 7;
                                $sqlQuery="SELECT * FROM public_figure_filed_values WHERE category_id='{$catId}' AND field_id='{$fieldId}' AND user_id='{$userId}'";
                                $value = $dbConnection->performQuery($sqlQuery);
                                
                                if($value = mysqli_fetch_assoc($value)) echo htmlentities($value["value"]);*/

                            ?>

                            <div class="row">

                                <div class="col-md-1">
                                    <img alt="" src="<?php echo "uploads/" . $user["photo_path"] ?>" class="avatar avatar-wordpress-social-login avatar-30 photo"
                                        height="40" width="40" style="border-radius: 50%">
                                </div>

                                <div class="col-md-11">
                                    <div class="form-group shadow-textarea">
                                        <!-- <label for="exampleFormControlTextarea6">Shadow and placeholder</label> -->
                                        <textarea class="form-control z-depth-1" id="text" rows="3" placeholder="Write something here..." style="resize: none; height: 127px"></textarea>
                                    </div>
                                </div>

                                <div class="spinner"  style="margin-left: 22px">
                                        <div class="double-bounce1"></div>
                                        <div class="double-bounce2"></div>
                                </div>
                                <button id="post" type="button" class="btn btn-primary pull-right" style="margin-right: 14px">Post</button>


                            </div>

                        </div>
                        <!-- Heading -->

                        <?php endif; ?>

                        <input type="hidden" id="user_id" data-session="<?php echo isset($_SESSION["user"]) ? "1" : "0"; ?>" value="<?php echo $user["id"]; ?>" >

                    <div class="card posts">

                    <?php 
                    
                        //get all user posts
                        $userId = $user["id"]; 
                        $sqlQuery="SELECT * FROM public_figure_posts WHERE user_id='{$userId}' ORDER BY id DESC LIMIT 7 OFFSET 0";
                        $posts = $dbConnection->performQuery($sqlQuery);
                        $i = 0;

                        while($post = mysqli_fetch_assoc($posts)){
                    ?>

                        <div class="row">

                            <div class="col-md-12">
                                <img alt="" src="<?php echo "uploads/" . $user["photo_path"] ?>" class="avatar avatar-wordpress-social-login avatar-30 photo"
                                    height="30" width="30" style="border-radius: 50%">

                                <h4 style="display: inline-block; margin-left: 3px; color: #0083ff"> <?php echo $user["username"]; ?> </h4>

                                <small class="pull-right">
                                
                                <?php 
                                    $postDate = strtotime($post["created_at"]);
                                    // $currenDate = strtotime(date("Y-m-d H:i:s"));
                                    $formatteDate = strftime("%B %e, %Y", $postDate);
                            

                                    // echo $formatteDate . " (age " . floor((($currenDate - $postDate) / (60 * 60 * 24 * 30 * 12))) . ")"; 
                                    echo $formatteDate;
                                ?>
                                
                                </small>
                            </div>

                            <div class="col-md-12" style="padding: 1em 2em;">

                            <p>
                                
                                <?php 

                                    echo htmlentities($post["text"]);

                                ?>

                            </p>

                            </div>

                        </div>

                        <hr />

                        <?php } ?>

                    </div>


                    <div class="spinner pull-right"  style="">
                        <div class="double-bounce1"></div>
                        <div class="double-bounce2"></div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?php include "footer.php"; ?>


    <script>
    
        $(document).ready(function(){

            var offset = 0;
            var limit = 7;

            var status = 0;

            $("#post").on("click", function(){

                $(".spinner").css("display", "inline-block");

                var post = $("#text");
                var userId = $("#user_id");

                if(userId.val() == "-1" || post.val() == "") return;

                var data = new FormData();
                data.append('post', 'post');
                data.append('user_id', $("#user_id").val());
                data.append('text', post.val());


                $.ajax({
                    url: 'api/post.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
                        // console.log(returnData);
                        returnData = JSON.parse(returnData);

                        if(returnData["error"] == 0){

                            // $(".posts").prepend(    
                            //     "<div class=\"row\">" + 
                            //         "<div class=\"col-md-12\">" + 
                            //             "<img alt=\"\" src=\"uploads/" + returnData["photo_path"] + "\" class=\"avatar avatar-wordpress-social-login avatar-30 photo\" " + 
                            //                 "height=\"30\" width=\"30\" style=\"border-radius: 50%\">" + 

                            //             "<h4 style=\"display: inline-block; margin-left: 3px; color: #0083ff\">" + returnData["username"] + "</h4>" + 

                            //             "<small class=\"pull-right\">" + returnData["created_at"] + "</small>" + 
                            //         "</div>" + 

                            //         "<div class=\"col-md-12\" style=\"padding: 1em 2em;\">" + 
                            //          "<p>" + returnData["text"] + "</p>" + 
                            //         "</div>" + 
                            //     "</div>" + 

                            //     "<hr />"
                            // );
                            
                            $(".posts").empty();

                            status = 1;
                            offset = 0;

                            loadPosts(limit, offset);

                            // setTimeout(() => {
                            //     loadPosts(limit, offset);
                            // }, 1000);

                            post.val("");
                            
                        }else{
                            
                        }

                        $(".spinner").css("display", "none");
                    }

                });

            });

            function loadPosts(limit, offset){

                var data = new FormData();
                data.append('load_posts', 'load_posts');
                data.append('user_id', $("#user_id").val());
                data.append('limit', limit);
                data.append('offset', offset);


                $.ajax({
                    url: 'api/post.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
                        console.log(returnData);
                        // return;
                        returnData = JSON.parse(returnData);

                        if(returnData["error"] == 0){

                            if(returnData["posts"].length > 0){

                                for($i = 0; $i < returnData["posts"].length; $i++){
                                    $(".posts").append(    
                                        "<div class=\"row\">" + 
                                            "<div class=\"col-md-12\">" + 
                                                "<img alt=\"\" src=\"uploads/" + returnData["photo_path"] + "\" class=\"avatar avatar-wordpress-social-login avatar-30 photo\" " + 
                                                    "height=\"30\" width=\"30\" style=\"border-radius: 50%\">" + 

                                                "<h4 style=\"display: inline-block; margin-left: 3px; color: #0083ff\">" + returnData["username"] + "</h4>" + 

                                                "<small class=\"pull-right\">" + returnData["posts"][$i]["created_at"] + "</small>" + 
                                            "</div>" + 

                                            "<div class=\"col-md-12\" style=\"padding: 1em 2em;\">" + 
                                            "<p>" + returnData["posts"][$i]["text"] + "</p>" + 
                                            "</div>" + 
                                        "</div>" + 

                                        "<hr />"
                                    );

                                    status = 0;

                                }

                            }else{
                                $(".posts").append("<button type=\"button\" class=\"btn btn-info\" style=\"margin-left: 40%\"> No Data Found.. </button>");
                                status = 1;
                            }

                        }else {

                            
                        }

                        $(".spinner").hide();

                    }
                });

            }

            $(window).scroll(function(){
                // console.log($("div.page-header-padding.page-header-bg").outerHeight());
                var height = 0;
                if($("#user_id").data("session") == "1")
                    height = $(".description").height() + $(".page-header-bg").outerHeight();
                else height = $(".posts").height() + $(".page-header-bg").outerHeight();

                // height = $(".posts").height() + $(".page-header-bg").outerHeight();
                // console.log($(window).scrollTop() + $(window).height() + " - " + height + " - " + status);
                
                if($(window).scrollTop() + $(window).height() >= height && status == 0){
                    $(".spinner").show();

                    status = 1;
                    offset += limit;

                    setTimeout(() => {
                        loadPosts(limit, offset);
                    }, 1000);

                    
                }
            });
        });
    
    </script>