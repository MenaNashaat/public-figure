<?php 

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["categories"] = array();

    if(isset($_GET["category_id"]) && isset($_GET["category_level"])){

        $categoryId = $_GET["category_id"];
        $category_level = $_GET["category_level"];
        $nextLevel = $category_level + 1;

        //get searchable fields
        $sqlQuery="SELECT * FROM categories_chain WHERE prev_id='{$categoryId}' AND category_level='{$nextLevel}'";
        $nextLevelCategories = $dbConnection->performQuery($sqlQuery);
                                                               
        while ($category = mysqli_fetch_assoc($nextLevelCategories)){
            $result["categories"][] = $category;
        }


    }else $result["error"] = 1;


    echo json_encode($result);

?>