<?php 

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;

    if(isset($_POST["favorite_sync"])){

        $userId = $dbConnection->prepareQueryValue($_POST["user_id"]);
        $favoriteId = $dbConnection->prepareQueryValue($_POST["favorite_id"]);
        $favorite = $dbConnection->prepareQueryValue($_POST["favorite"]);

        //get searchable fields
        $sqlQuery="SELECT COUNT(*) AS count FROM users_favorites WHERE user_id='{$userId}' AND favorite_id='{$favoriteId}'";
        $favoriteEntry = $dbConnection->performQuery($sqlQuery);

        if($favoriteEntry){

            $count = mysqli_fetch_assoc($favoriteEntry)["count"];
            if($favorite == "0" && $count > 0){
                //remove, sync
                $sqlQuery = "DELETE FROM users_favorites WHERE user_id='{$userId}' AND favorite_id='{$favoriteId}'";
                $favoriteSync = $dbConnection->performQuery($sqlQuery);

                if(!$favoriteSync) $result["error"] = 2;

            }else if($favorite == "1" && $count <= 0){
                //add, sync
                $sqlQuery = "INSERT INTO users_favorites (user_id, favorite_id) VALUES ('{$userId}', '{$favoriteId}')";
                $favoriteSync = $dbConnection->performQuery($sqlQuery);

                if(!$favoriteSync) $result["error"] = 2;
            }

            // if($count > 0){
            //     //edit favorite entry
            //     $sqlQuery = "UPDATE users_favorites SET favorite_id='{$favoriteId}' WHERE userId='{$userId}'";
            //     $favoriteSync = $dbConnection->performQuery($sqlQuery);

            //     if(!$favoriteSync) $result["error"] = 2;
            // }else{
            //     //add favorite entry
            //     $sqlQuery = "INSERT INTO users_favorites (user_id, favorite_id) VALUES ('{$userId}', '{$favoriteId}')";
            //     $favoriteSync = $dbConnection->performQuery($sqlQuery);

            //     if(!$favoriteSync) $result["error"] = 2;

            // }

        }else{
            $result["error"] = 2;
        }


    }else $result["error"] = 1;


    echo json_encode($result);

?>