<?php 

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["username"] = 1;

    if(isset($_POST["check_username"])){

        $username = $dbConnection->prepareQueryValue($_POST["username"]);
        

        //get searchable fields
        $sqlQuery="SELECT COUNT(*) AS count FROM public_figure_user WHERE username='{$username}'";
        $usernameCount = $dbConnection->performQuery($sqlQuery);
                                                               
        if($usernameCount){
            if(mysqli_fetch_assoc($usernameCount)["count"] > 0)  $result["username"] = -1;

        }else $result["error"] = 1;


    }else $result["error"] = 1;


    echo json_encode($result);

?>