<?php 

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["search"] = array();
    $result["matched_users"] = array();
    $result["search_string"] = "";

    if(isset($_GET["search"])){

        $searchString = $_GET["search"];

        //empty string case

        //get matched like records
        $searchResult = Search::normalSearch($searchString);

        //limit records to 7  //fron search class TODO------
        $result["search"] = $searchResult;

        //append search string to result array
        $result["search_string"] = $searchString;


    }else if(isset($_POST["search"])){
        //filters search
        $searchString = $_POST["search_string"];

        $searchFields = array();
        
        $searchFields[] = $_POST["gander"];
        $searchFields[] = $_POST["nationality"];

        $searchFields[] = $_POST["category_id"];
        $searchFields[] = $_POST["category_level"];
        $searchFields[] = $_POST["category_path"];


        // $categoryId = $_POST["category_id"];
        // $staticFields[] = $_POST["category_id"];
        // $dynamicFields = json_decode($_POST["dynamic_fields"]);

        //search
        $matchedUsers = Search::searchWithParams($searchString, $searchFields);

        $result["matched_users"] = $matchedUsers;



    }else $result["error"] = 1;


    echo json_encode($result);

?>