<?php 

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["fields"] = array();

    if(isset($_GET["category_id"])){

        $categoryId = $_GET["category_id"];

        //get searchable fields
        $sqlQuery="SELECT * FROM public_figure_category_fields WHERE category_id='$categoryId' AND searchable='yes'";
        $searchableFileds = $dbConnection->performQuery($sqlQuery);
                                                               
        while ($field = mysqli_fetch_assoc($searchableFileds)){
            $result["fields"][] = $field;
        }


    }else $result["error"] = 1;


    echo json_encode($result);

?>