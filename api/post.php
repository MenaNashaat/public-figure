<?php 

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["posts"] = array();
    

    if(isset($_POST["post"])){

        $userId = $dbConnection->prepareQueryValue($_POST["user_id"]);
        $text = $dbConnection->prepareQueryValue($_POST["text"]);

        //creation date
        $createdAt = date("Y-m-d H:i:s");

        //get searchable fields
        $sqlQuery="INSERT INTO public_figure_posts (user_id, text, created_at) VALUES ('{$userId}', '{$text}', '{$createdAt}')";
        $post = $dbConnection->performQuery($sqlQuery);

        if(!$post){
            $result["error"] = 2;
        }else {

            $sqlQuery="SELECT username, photo_path FROM public_figure_user WHERE id='{$userId}'";
            $user = $dbConnection->performQuery($sqlQuery);

            if($user){

                $user = mysqli_fetch_assoc($user);
                $result["username"] = $user["username"];
                $result["photo_path"] = $user["photo_path"];

                $result["text"] = $text;
                $result["created_at"] = strftime("%B %e, %Y", strtotime($createdAt));


            }

        }


    }else if(isset($_POST["load_posts"])){

        $userId = $dbConnection->prepareQueryValue($_POST["user_id"]);
        $limit = $dbConnection->prepareQueryValue($_POST["limit"]);
        $offset = $dbConnection->prepareQueryValue($_POST["offset"]);

        $sqlQuery="SELECT username, photo_path FROM public_figure_user WHERE id='{$userId}'";
        $user = $dbConnection->performQuery($sqlQuery);

        if($user){

            $user = mysqli_fetch_assoc($user);
            $result["username"] = $user["username"];
            $result["photo_path"] = $user["photo_path"];
        }

        $sqlQuery = "SELECT * FROM public_figure_posts WHERE user_id='{$userId}' ORDER BY id DESC LIMIT " . $limit . " OFFSET " . $offset;
        $sqlResult = $dbConnection->performQuery($sqlQuery);
        
        if($sqlResult){
            while($post = mysqli_fetch_assoc($sqlResult)){
                $post["created_at"] = strftime("%B %e, %Y", strtotime($post["created_at"]));
                $result["posts"][] = $post;
            }


        }else $result["error"] = 1;

    }else if(isset($_POST["load_feeds"])){

        $limit = $dbConnection->prepareQueryValue($_POST["limit"]);
        $offset = $dbConnection->prepareQueryValue($_POST["offset"]);

        // $sqlQuery="SELECT username, photo_path FROM public_figure_user WHERE id='{$userId}'";
        // $user = $dbConnection->performQuery($sqlQuery);

        // if($user){

        //     $user = mysqli_fetch_assoc($user);
        //     $result["username"] = $user["username"];
        //     $result["photo_path"] = $user["photo_path"];
        // }

        $sqlQuery = "SELECT * FROM public_figure_posts ORDER BY id DESC LIMIT " . $limit . " OFFSET " . $offset;
        $sqlResult = $dbConnection->performQuery($sqlQuery);
        
        if($sqlResult){
            while($post = mysqli_fetch_assoc($sqlResult)){
                $post["created_at"] = strftime("%B %e, %Y", strtotime($post["created_at"]));
                $result["posts"][] = $post;

                $userId = $post["user_id"];

                $userQuery="SELECT username, photo_path FROM public_figure_user WHERE id='{$userId}'";
                $user = $dbConnection->performQuery($userQuery);

                $user = mysqli_fetch_assoc($user);
                $result["username"] = $user["username"];
                $result["photo_path"] = $user["photo_path"];
            }


        }else $result["error"] = 1;

    }else $result["error"] = 1;


    echo json_encode($result);

?>