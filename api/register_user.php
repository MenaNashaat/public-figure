<?php 

    ob_start();
    session_start();
    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["error_message"] = "";
    

    if(isset($_POST["register"])){

        $firstName = $dbConnection->prepareQueryValue($_POST["first_name"]);
        $middleName = $dbConnection->prepareQueryValue($_POST["middle_name"]);
        $lastName = $dbConnection->prepareQueryValue($_POST["last_name"]);

        $gander = $dbConnection->prepareQueryValue($_POST["gander"]);
        $birthDate = $dbConnection->prepareQueryValue($_POST["birth_date"]);
        $nationality = $dbConnection->prepareQueryValue($_POST["nationality"]);

        $residence = $dbConnection->prepareQueryValue($_POST["residence"]);
        $email = $dbConnection->prepareQueryValue($_POST["email"]);
        $phone = $dbConnection->prepareQueryValue($_POST["phone"]);

        $userName = $dbConnection->prepareQueryValue($_POST["user_name"]);
        $password = $dbConnection->prepareQueryValue($_POST["password"]);
        $confirmPassword = $dbConnection->prepareQueryValue($_POST["confirm_password"]);

        $categoryId = $dbConnection->prepareQueryValue($_POST["category_id"]);
        $categoryLevel = $dbConnection->prepareQueryValue($_POST["category_level"]);
        $categoryPath = $dbConnection->prepareQueryValue($_POST["category_path"]);

        $description = $dbConnection->prepareQueryValue($_POST["description"]);

        $target_dir = "../uploads/";

        $nationalId = $personal = "";
        if (isset($_FILES["national_id"]["name"])) {
            $photo_path = $dbConnection->prepareQueryValue(trim($_FILES["national_id"]["name"])) /*mysql_prep(safe_string(trim($_FILES["national_id"]["name"])))*/;
            $photo_pathTMP = $_FILES['national_id']['tmp_name'];
            // $photo_pathName = $_FILES["national_id"]["name"];
            if ($photo_path != '') {
                $photo_path = date("Y-d-m--h-i-s-A") . "-" . $photo_path;
                // $photo_path = safe_string($photo_pathName);
                $targetPhoto = $target_dir . basename($photo_path);
                move_uploaded_file($photo_pathTMP, $targetPhoto);
            }

            $nationalId = $photo_path;
        }
        if (isset($_FILES["personal"]["name"])) {
            $photo_path = $dbConnection->prepareQueryValue(trim($_FILES["personal"]["name"])) /*mysql_prep(safe_string(trim($_FILES["national_id"]["name"])))*/;
            $photo_pathTMP = $_FILES['personal']['tmp_name'];
            // $photo_pathName = $_FILES["national_id"]["name"];
            if ($photo_path != '') {
                $photo_path = date("Y-d-m--h-i-s-A") . "-" . $photo_path;
                // $photo_path = safe_string($photo_pathName);
                $targetPhoto = $target_dir . basename($photo_path);
                move_uploaded_file($photo_pathTMP, $targetPhoto);
                
            }

            $personal = $photo_path;
        }

        //check username
        // $sqlQuery = "SELECT COUNT(*) as username_count FROM public_figure_user WHERE username='{$userName}'";
        // $userNameCount = $dbConnection->fetchRecordArray($dbConnection->performQuery($sqlQuery))["username_count"];

        // if($userNameCount > 0){
        //     $result["error"] = -2;
        //     $result["error_message"] = "username already exists!";
        // }else {
        // }

        if($password !== $confirmPassword){
            $result["error"] = -3;
            $result["error_message"] = "password missmatch!";
        }else{

            // $hashedPassword = PasswordEncryption::passwordEncrypt($password);
            $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
            // $app_id = rand(1, 1000000000);

            $sqlQuery = "INSERT INTO public_figure_user ( ";
            $sqlQuery .= "category_id, category_level, category_path, first_name, middle_name, last_name, gander, birth_date, nationality, residence, photo_path, passport_path, email, phone, username, password, plain_password, description";
            $sqlQuery .= " ) VALUES ( ";
            $sqlQuery .= "'{$categoryId}', '{$categoryLevel}', '{$categoryPath}', '{$firstName}', '{$middleName}', '{$lastName}', '{$gander}', '{$birthDate}', '{$nationality}', '{$residence}', '{$personal}', '{$nationalId}', '{$email}', '{$phone}', '{$userName}', '{$hashedPassword}', '{$password}', '{$description}'";
            $sqlQuery .= " )";

            // echo $sqlQuery;

            $queryResult = $dbConnection->performQuery($sqlQuery);

            if(!$queryResult){
                $result["error"] = -4;
                $result["error_message"] = "database error!";
            }else {
                $_SESSION["user"] = $userName;
            }
        }

        


    }else $result["error"] = -1;


    echo json_encode($result);

?>