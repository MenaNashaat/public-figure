<?php 

    ob_start();
    session_start();

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["error_message"] = "";
    

    if(isset($_POST["login"])){

        $username = $dbConnection->prepareQueryValue($_POST["username"]);
        $password = $dbConnection->prepareQueryValue($_POST["password"]);

        $sqlQuery = "SELECT * FROM public_figure_user WHERE username='{$username}' LIMIT 1";
        $queryResult = $dbConnection->performQuery($sqlQuery);

        if($dbConnection->numRows($queryResult) > 0){
            //check password
            $user = mysqli_fetch_assoc($queryResult);
            if(password_verify($password, $user["password"])){

                //set user session
                $_SESSION["user"] = $username;
                $_SESSION["user_id"] = $user["id"];


            }else {
                $result["error"] = -3;
                $result["error_message"] = "username/password not correct";

            }

        }else {
            $result["error"] = -2;
            $result["error_message"] = "username/password not correct";
        }
        


    }else $result["error"] = -1;


    echo json_encode($result);

?>