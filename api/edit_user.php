<?php 

    require_once("../../includes/initialize.php");

    //get database object
    $dbConnection = getDatabaseConnection();

    $result = array();
    $result["error"] = 0;
    $result["error_message"] = "";
    

    if(isset($_POST["edit"])){

        $firstName = $dbConnection->prepareQueryValue($_POST["first_name"]);
        $middleName = $dbConnection->prepareQueryValue($_POST["middle_name"]);
        $lastName = $dbConnection->prepareQueryValue($_POST["last_name"]);

        $birthDate = $dbConnection->prepareQueryValue($_POST["birth_date"]);
        $nationality = $dbConnection->prepareQueryValue($_POST["nationality"]);

        $residence = $dbConnection->prepareQueryValue($_POST["residence"]);
        $email = $dbConnection->prepareQueryValue($_POST["email"]);
        $phone = $dbConnection->prepareQueryValue($_POST["phone"]);

        $facebook = $dbConnection->prepareQueryValue($_POST["facebook"]);
        $twitter = $dbConnection->prepareQueryValue($_POST["twitter"]);
        $linkedin = $dbConnection->prepareQueryValue($_POST["linkedin"]);

        $categoryId = $dbConnection->prepareQueryValue($_POST["category_id"]);
        $categoryLevel = $dbConnection->prepareQueryValue($_POST["category_level"]);
        $categoryPath = $dbConnection->prepareQueryValue($_POST["category_path"]);

        $description = trim($dbConnection->prepareQueryValue($_POST["description"]));
        $userId = $dbConnection->prepareQueryValue($_POST["user_id"]);

        $samePhoto = $_POST["same_photo"];

        if($samePhoto == "false"){
            $target_dir = "../uploads/";

            $personal = "";
            if (isset($_FILES["personal_photo"]["name"])) {
                $photo_path = $dbConnection->prepareQueryValue(trim($_FILES["personal_photo"]["name"])) /*mysql_prep(safe_string(trim($_FILES["national_id"]["name"])))*/;
                $photo_pathTMP = $_FILES['personal_photo']['tmp_name'];
                // $photo_pathName = $_FILES["national_id"]["name"];
                if ($photo_path != '') {
                    $photo_path = date("Y-d-m--h-i-s-A") . "-" . $photo_path;
                    // $photo_path = safe_string($photo_pathName);
                    $targetPhoto = $target_dir . basename($photo_path);
                    move_uploaded_file($photo_pathTMP, $targetPhoto);
                }

                $personal = $photo_path;


                //delete old image
                $sqlQuery = "SELECT photo_path FROM public_figure_user WHERE id='{$userId}' LIMIT 1";
                $queryResult = $dbConnection->performQuery($sqlQuery);

                if($sqlQuery) unlink("../uploads/" . mysqli_fetch_assoc($queryResult)["photo_path"]);
            }


        }


        //check username
        // $sqlQuery = "SELECT COUNT(*) as username_count FROM public_figure_user WHERE username='{$userName}'";
        // $userNameCount = $dbConnection->fetchRecordArray($dbConnection->performQuery($sqlQuery))["username_count"];

        // if($userNameCount > 0){
        //     $result["error"] = -2;
        //     $result["error_message"] = "username already exists!";
        // }else {
        // }

        $sqlQuery = "UPDATE public_figure_user SET ";
        $sqlQuery .= "category_id='{$categoryId}', category_level='{$categoryLevel}', category_path='{$categoryPath}', ";
        $sqlQuery .= "first_name='{$firstName}', middle_name='{$middleName}', last_name='{$lastName}', ";
        if($samePhoto == "false") $sqlQuery .= "photo_path='{$personal}', ";
        $sqlQuery .= "birth_date='{$birthDate}', nationality='{$nationality}', residence='{$residence}', email='{$email}', phone='{$phone}', ";
        $sqlQuery .= "facebook='{$facebook}', twitter='{$twitter}', linkedin='{$linkedin}' WHERE id='{$userId}' LIMIT 1";

        // echo $sqlQuery;

        $queryResult = $dbConnection->performQuery($sqlQuery);

        if(!$queryResult){
            $result["error"] = -4;
            $result["error_message"] = "database error!";
        }

        $fieldId = 7;
        $baseCategoryId = explode("-", $categoryPath)[0];
        $sqlQuery = "SELECT COUNT(*) AS count FROM public_figure_filed_values WHERE category_id='{$baseCategoryId}' AND field_id='{$fieldId}' AND user_id='{$userId}'";
        $queryResult = $dbConnection->performQuery($sqlQuery);

        if($queryResult){
            
            if(mysqli_fetch_assoc($queryResult)["count"] > 0){
                //update
                $sqlQuery = "UPDATE public_figure_filed_values SET value='{$description}' WHERE category_id='{$baseCategoryId}' AND field_id='{$fieldId}' AND user_id='{$userId}'";
            }else{
                //insert
                $sqlQuery = "INSERT INTO public_figure_filed_values ( category_id, field_id, user_id, value ) VALUES ( '{$baseCategoryId}', '{$fieldId}', '{$userId}', '{$description}' )";
            }

            // echo $sqlQuery;

            $queryResult = $dbConnection->performQuery($sqlQuery);

            if(!$queryResult){
                $result["error"] = -4;
                $result["error_message"] = "database error!";
            }

        }else{
            $result["error"] = -4;
            $result["error_message"] = "database error!";
        }


    }else $result["error"] = -1;


    echo json_encode($result);

?>