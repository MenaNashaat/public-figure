<?php 

    ob_start();
    session_start();


    // $loggedIn = false;
    // if(isset($_SESSION["user"]))
    //     $loggedIn = true;

	$pageTitle = "register";
	require_once("../includes/initialize.php");



    if(isset($_SESSION["user"])) redirect_to("profile.php");

	//get database connection
	$dbConnection = getDatabaseConnection();


    include "header.php";

	// if(isset($_POST["search"])){
	// 	if($_POST["search_string"] != "")
	// 		$_SESSION["search_string"] = $_POST["search_string"];

	// 	redirect_to("results.php");
	// }

?>





    <div class="page-header-padding page-header-bg">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  
                  <h1 class="page-title white-color">Registration
    
                </h1>
                </div>
              </div>
            </div>
          </div>
   
     <!-- MultiStep Form -->
     <section class="m-t-em-2 m-b-em-3">
<div class="container">
    <div class="row">
            <div class="col-md-12">
                <form id="msform">
                    <!-- progressbar -->
                    <ul id="progressbar" style="padding-left: 22%;">
                        <li class="active">Personal Information</li>
                        <li>Category</li>
                        <li>Uploads</li>
                        <!-- <li>Upload </li> -->
                    </ul>
                    <!-- fieldsets -->
                    
                    <fieldset class="row">
                        <h2 class="fs-title">Personal Information</h2>
                        
                        <!-- ---------------------------------------------------------------------------------------->
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="pull-left">First Name</label>
                    <input id="first_name" name="first_name" type="text" class="form-control require_text" placeholder="First Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Middle Name</label>
                    <input  id="middle_name" name="middle_name" type="text" class="form-control" placeholder="Middle Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Last Name </label>
                    <input type="text" id="last_name" name="last_name" class="form-control require_text" placeholder="Last Name">
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left">Gender </label>
                    <select class="form-control require_select" id="gander" name="gander">
                        <option value="">- - Choose Gender - -</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>

                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group" id="">
                    <label class=" control-label pull-left"> Birth Date</label>

                    <div class=" input-group " style="display: block;">
                        <span class="input-group-addon" style=" background-color: transparent !important; border: none !important;"></span>
                        <input style="padding: 6px;" name="birth_date" id="birth_date" placeholder="M/D/Y" type="date" class="form-control require_select" value="">
                    </div>

                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left">Nationality </label>
                    <select id="nationality"   class="form-control require_select" name="nationality">
                        <option value="-1">---Nationality---</option>
                        <?php
                        // $residenceSet = get_all_from_tbl('countries_tbl');
                        $sqlQuery="SELECT * FROM countries";
                        $residenceSet = $dbConnection->performQuery($sqlQuery);
                        while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                        <option value="<?php echo $residence['id']; ?>">
                            <?php echo $residence['country_name']; ?></option><?php
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label for="sel1" class="pull-left" >Residence ( Optional ) </label>
                    <select id="residence"  class="form-control" name="residence">
                        <option value="-1">---Residence---</option><?php
                        // $residenceSet = get_all_from_tbl('countries_tbl');
                        $sqlQuery="SELECT * FROM countries";
                        $residenceSet = $dbConnection->performQuery($sqlQuery);
                        while ($residence = mysqli_fetch_assoc($residenceSet)) { ?>
                        <option value="<?php echo $residence['id']; ?>">
                            <?php echo $residence['country_name']; ?></option><?php
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 ">
                <div class="form-group">
                    <label class="pull-left">Email</label>
                    <input type="email" id="email_register" name="email" class="form-control require_email" placeholder="Email">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="pull-left"> Phone </label>
                    <input type="text"  id="phone" name="phone" class="form-control require_text" placeholder="Phone">
                </div>
                    
            </div>

            <div class="col-sm-4">
                    <div class="form-group">
                        <label class="pull-left">username</label>
                        <input id="user_name" type="text" class="form-control require_text" placeholder="username">
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="pull-left">passwrod</label>
                        <input  id="password" type="password" class="form-control require_password" placeholder="passwrod">
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="form-group">
                        <label class="pull-left">confirm password</label>
                        <input type="password" id="confirm_password"  class="form-control require_password" placeholder="confirm password">
                    </div>
                </div>

            
                
                        <!-- ---------------------------------------------------------------------------------------->

                        
                        <div class="col-sm-4 ">
                            <div class="form-group">
                                <small id="hint" class="pull-left" style="color: red"></small>
                            </div>
                        </div>
                        <input type="button" name="next" data-id="1" class="next action-button pull-right" value="Next"/>
                    </fieldset>
                    
                    
                        <fieldset>
                            <h2 class="fs-title">Category</h2>

                            <div class="row" id="categories">
                                <div class="col-sm-4 ">
                                    
                                    
                                    <select id="category1"  class="form-control category" name="category1" data-level="1">
                                        <option value="-1">---Category---</option><?php

                                        $level = 1;
                                        $sqlQuery="SELECT * FROM categories_chain WHERE category_level='{$level}' AND prev_id IS NULL";
                                        $categories = $dbConnection->performQuery($sqlQuery);
                                        while ($category = mysqli_fetch_assoc($categories)) { 
                                        // if($category["level"] == 1):
                                        ?>
                                        <option value="<?php echo $category['id']; ?>">
                                            <?php echo $category['name']; ?></option><?php

                                        // endif;
                                        } ?>
                                        
                                    </select>
                                
                                </div>
                            </div>

                            
                            

                            <div class="clearfix"></div>
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                            <input type="button" name="next" data-id="2" class="next action-button" value="Next"/>
                            <div class="loader pull-right" style="border: 3px solid #3498db; border-top: 3px solid #ffffff;"></div>
                        </fieldset>
                   
                    
                    
                        <fieldset class="row">
                                <h2 class="fs-title">Upload</h2>
                                <div class="col-md-4 col-md-offset-2">

                                    <div class="x_content">
                                        <div class="wrap">
                                            <h4> ID or Passport</h4>
                                            <div class="upload-thumb thumbnail">
                                                <img style="max-height: 200px;min-height: 200px;" id="img1" src="" alt="">
                                            </div>
                                            
                                            <label for="file1" id="text_1" class="btn btn-default">
                                                Upload Image
                                                <input data-id="1" id="file1" type="file" class="upload">
                                            </label>
                                            
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="x_content">
                                        <div class="wrap">
                                            <h4> Personal Photo</h4>
                                            <div class="upload-thumb thumbnail">
                                                <img style="max-height: 200px;min-height: 200px;" id="img2" src="" alt="">
                                            </div>
                                            
                                            <label for="file2" id="text_2" class="btn btn-default">
                                                Upload Image
                                                <input data-id="2" id="file2" type="file" class="upload">
                                            </label>
                                            
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix"></div>


                                <div class="col-md-10 col-md-offset-1">
                                    <textarea id="desc" style="width: 100%; height: 10em; margin: .7em 0;" placeholder="tell us a brief description about yourself.."></textarea>
                                </div>

                                <div class="clearfix"></div>
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                                <input type="submit" name="submit" data-id="3" class="next action-button" value="Submit"/>
                                <div class="spinner pull-right"  style="">
                                    <div class="double-bounce1"></div>
                                    <div class="double-bounce2"></div>
                                </div>
                            </fieldset>
                    
                </form>
               
            </div>

        </div>
    </section>
    <!-- /.MultiStep Form -->


<?php include "footer.php"; ?>
    
     <script>
         function preview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) { $('#img' + $(input).data("id")).attr('src', e.target.result); }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function(){
            categoryPath = [];

            //var oldLevel = -1;
            $("#categories").on("change", ".category", function(e){
                
                $(".loader").show();
                // console.log(categoryDone);
                // console.log(e.target);
                var selection = $(e.target);
                var categoryId = parseInt(selection.children("option:selected").val());
                // console.log(selection.data("level"));

                selection.css("border", "1px solid rgba(34,36,38,.15)");
                
                if(categoryId != -1){

                //get next level categories
                oldLevel = categoryLevel;
                categoryLevel = selection.data("level");
                var nextLevel = categoryLevel + 1;

                if(categoryLevel > oldLevel) /*categoryPath += categoryId + "-"*/ categoryPath.push(categoryId);
                else {
                    while(categoryPath.length >= categoryLevel)
                    categoryPath.pop();

                    categoryPath.push(categoryId);
                }
                // else if(categoryLevel == oldLevel) categoryPath = categoryPath.slice(0, categoryPath.lastIndexOf("-", categoryPath.length - 2) + 1) + categoryId + "-"


                $.get( "api/next_level_categories.php?category_id=" + categoryId + "&category_level=" + categoryLevel, function( data ) {
                    data = JSON.parse(data);
                    if(data["error"] == 0){
                    var categories = data["categories"];

                    // console.log(categories);

                    selection.parent().nextAll().remove();

                    if(categories.length > 0){
                        var container = $('<div class="col-md-4" style="margin-bottom: 1em"></div>');
                        var nextSelection = $('<select id="category' + nextLevel + '"  class="form-control category" name="category' + nextLevel + '" data-level="' + nextLevel + '"></select>');
                    
                        nextSelection.append($('<option value="' + -1 + '"> ' + '---Sub Category---' + ' </option>'));
                        //add options
                        for(var i = 0; i < categories.length; i++)
                            nextSelection.append($('<option value="' + categories[i]["id"] + '"> ' + categories[i]["name"] + ' </option>'));

                        container.append(nextSelection);
                        selection.parent().parent().append(container);

                        categoryDone = false;
                    }else categoryDone = true;
                    }

                    $(".loader").hide();

                });
                

                }else {
                selection.parent().nextAll().remove();
                selection.css("border", "1px solid red");
                categoryDone = false;
                oldLevel = categoryLevel;
                categoryLevel = selection.data("level");
                while(categoryPath.length >= categoryLevel)
                    categoryPath.pop();
                }

                // console.log(oldLevel + "==" + categoryLevel);
                // console.log(categoryPath);
            });

            // $('#data_1 .input-group.date').datepicker({
            //     todayBtn: "linked",
            //     keyboardNavigation: false,
            //     forceParse: false,
            //     calendarWeeks: true,
            //     autoclose: true
            // });

            

            $(".upload").change(function () {
                $("#img").css({ top: 0, left: 0 });
                preview(this);
                // $("#img" + $(this).data("id")).draggable({ containment: 'parent', scroll: false });
            });



            $("#modal_submit").on("click", function(e){
				e.preventDefault();

				//ajax check login
				var username = $("#login_username");
				var password = $("#login_password");

				if(username.val() == ""){
					username.css("border", "1px solid red");
					return;
				}else username.css("border", "1px solid #ccc");

				if(password.val() == ""){
					password.css("border", "1px solid red");
					return;
				}else password.css("border", "1px solid #ccc");


				var data = new FormData();
                data.append('login', 'login');
                data.append('username', username.val());
                data.append('password', password.val());


                $.ajax({
                    url: 'api/login.php',
                    data: data,
                    dataType: "text",
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(returnData){
						// console.log(returnData);
						returnData = JSON.parse(returnData);

						if(returnData["error"] == 0){
							location.href="https://goovd.com/public_figure/public_figure/profile.php";
						}else{
							$("#login_hint").text("username/password not correct!");
						}
					}

				});


			});

        });

        

</script>